FROM node:20

# Server runs on port 5000
EXPOSE 5000

# Create app directory
WORKDIR /usr/src/app

#COPY ./package.json /

COPY ./server/dist/ ./dist
COPY ./server/node_modules/ ./node_modules
COPY ./server/package*.json ./
COPY ./server/.env* ./

COPY ./client/build/ ./client/build

CMD npm run start:prod
