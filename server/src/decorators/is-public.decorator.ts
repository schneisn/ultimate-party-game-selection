import { SetMetadata } from '@nestjs/common';

export const PublicMetadataKey = 'isPublic';

/**
 * Public is used to add exceptions to websocket endpoints, that do not need any
 * kind of authorization, i.e. provide a player id.
 */
export const Public = () => SetMetadata(PublicMetadataKey, true);
