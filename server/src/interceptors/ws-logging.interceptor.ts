import {
  CallHandler,
  ExecutionContext,
  Global,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { WsResponse } from '@nestjs/websockets';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Socket } from 'socket.io';
import { LoggerService } from '../modules/logger/logger.service';

/**
 * A logger to log the called method and the elapsed time of websocket requests
 */
@Global()
@Injectable()
export class WsLoggingInterceptor implements NestInterceptor<any, WsResponse> {
  constructor(private readonly logger: LoggerService) {}

  intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): Observable<WsResponse> {
    const className = context.getClass().name;
    const handlerName = context.getHandler().name;
    const calleeName = className + ' ' + handlerName;
    const clientId = context.switchToWs().getClient<Socket>().id;
    this.logger.log(`[Request] ${clientId}`, calleeName);
    return next.handle().pipe(
      tap((data: WsResponse) => {
        this.logger.log(`[Response] ${clientId}`, calleeName);
      }),
    );
  }
}
