import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { WsResponse } from '@nestjs/websockets';
import { Observable } from 'rxjs';
import { Socket } from 'socket.io';
import { LoggerService } from '../modules/logger/logger.service';
import { GameRequest } from '../modules/common/gateway/event/request/game.request';
import { Game } from '../modules/common/game/domain/game.entity';
import { Player } from '../modules/common/player/domain/player.entity';
import { ISessionService } from '../modules/session/service/session.service.interface';

/**
 * The GetPlayerInterceptor checks if the client id belongs to a player id.
 * If there is a player id, the player id is added to the request body.
 */
@Injectable()
export class GetPlayerInterceptor implements NestInterceptor<any, WsResponse> {
  constructor(
    private readonly logger: LoggerService,
    private readonly sessionService: ISessionService,
  ) {
    this.logger.setContext(GetPlayerInterceptor.name);
  }

  intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): Observable<WsResponse> {
    const clientId = context.switchToWs().getClient<Socket>().id;
    const playerId = this.sessionService.getPlayerId(clientId);
    if (!playerId) {
      this.logger.warn(
        `Client id ${clientId} does not belong to any player. Continuing handling request`,
      );
      return next.handle();
    }

    const data = context.switchToWs().getData<GameRequest<Game<Player>>>();
    data.player = {
      ...data.player,
      id: playerId,
    };
    return next.handle();
  }
}
