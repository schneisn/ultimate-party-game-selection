import {
  CallHandler,
  ExecutionContext,
  Global,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { LoggerService } from '../modules/logger/logger.service';

/**
 * A logger to log the called method and the elapsed time of http requests
 */
@Global()
@Injectable()
export class HttpLoggingInterceptor implements NestInterceptor<any, Response> {
  constructor(private readonly logger: LoggerService) {}

  intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): Observable<Response> {
    const url = context.switchToHttp().getRequest<Request>().url;
    const className = context.getClass().name;
    const handlerName = context.getHandler().name;
    const calleeName = className + ' ' + handlerName;
    this.logger.log(`[Request] ${url}`, calleeName);
    return next.handle().pipe(
      tap((data: Response) => {
        this.logger.log(`[Response] ${url}`, calleeName);
      }),
    );
  }
}
