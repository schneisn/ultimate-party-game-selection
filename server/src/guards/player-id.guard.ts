import {
  CanActivate,
  ExecutionContext,
  Global,
  Injectable,
} from '@nestjs/common';
import { LoggerService } from '../modules/logger/logger.service';
import { Reflector } from '@nestjs/core';
import { PublicMetadataKey } from '../decorators/is-public.decorator';
import SocketIO from 'socket.io';
import { UnauthorizedError } from './error/unauthorized.error';
import { ISessionService } from '../modules/session/service/session.service.interface';

/**
 * The playerIdGuard checks whether the client id of the socket belongs
 * to any player id. It does not check whether
 * the id actually belongs to the game in the request.
 * If there is no player id, an error is returned.
 */
@Global()
@Injectable()
export class PlayerIdGuard implements CanActivate {
  constructor(
    private readonly reflector: Reflector,
    private readonly sessionService: ISessionService,
    readonly logger: LoggerService,
  ) {
    this.logger.setContext(PlayerIdGuard.name);
  }

  canActivate(context: ExecutionContext): boolean {
    // See https://github.com/nestjs/nest/issues/964
    const isPublic = this.reflector.get<boolean>(
      PublicMetadataKey,
      context.getHandler(),
    );
    if (isPublic) {
      return true;
    }

    const client = context.switchToWs().getClient<SocketIO.Socket>();
    const clientId = client?.id;
    const playerId = this.sessionService.getPlayerId(clientId);
    if (!playerId) {
      this.logger.warn(`Client ${clientId} does not belong to any player id`);
      throw new UnauthorizedError(client.id);
    }

    return true;
  }
}
