import { UpgsError } from '../../common/error/upgs.error';

export class UnauthorizedError extends UpgsError {
  constructor(clientId: string) {
    super(
      'UNAUTHORIZED_ERROR',
      'The client did not provide a player id',
      'You are not authorized',
      clientId,
    );
  }
}
