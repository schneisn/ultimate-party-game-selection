import { ValidateNested } from 'class-validator';
import { IdRequestModel } from '../../modules/common/gateway/event/request/model/id.request-model';

/**
 * The PlayerRequest contains a player with an id for identification of the player
 */
export abstract class PlayerRequest {
  /**
   * The player that the request sent
   */
  @ValidateNested()
  player: IdRequestModel;
}
