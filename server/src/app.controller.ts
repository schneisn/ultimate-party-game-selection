import { Controller, Get, UseInterceptors } from '@nestjs/common';
import { LoggerService } from './modules/logger/logger.service';
import { HttpLoggingInterceptor } from './interceptors/http-logging.interceptor';

@UseInterceptors(HttpLoggingInterceptor)
@Controller('api')
export class AppController {
  constructor(private readonly logger: LoggerService) {
    this.logger.setContext(AppController.name);
  }

  @Get()
  api(): void {
    return;
  }

  @Get('keep-alive')
  keepAlive(): void {
    // Used to keep server alive.
    // When no http is sent for some time, some hosting providers idle the app,
    // even though ws requests are sent.
    return;
  }
}
