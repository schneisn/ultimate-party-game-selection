export class DateUtils {
  /**
   * Creates a date that is in the provided amount of
   * seconds.
   * @param inSeconds Can also be negative
   */
  static createDateInSeconds(inSeconds: number): Date {
    const now = new Date();
    now.setSeconds(now.getSeconds() + inSeconds);
    return now;
  }
}
