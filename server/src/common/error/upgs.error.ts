import { WsException } from '@nestjs/websockets';
import { ClientError } from '../../modules/common/gateway/error/client.error';
import { IUPGSError } from './upgs.error.interface';

/**
 * A basic error object for websocket-errors
 */
export abstract class UpgsError extends WsException implements IUPGSError {
  /**
   * A unique identifier
   */
  readonly id: string;
  /**
   * A message for the user. Should be helpful for the user
   */
  userMessage: string;
  /**
   * Overwrites the default name of the error that is logged by nest.js.
   * This message should be useful for the developer
   */
  message: string;
  /**
   * The cause of the error
   */
  cause: any;

  protected constructor(
    id: string,
    logMessage: string,
    userMessage: string,
    cause: any,
  ) {
    /**
     * The error passed to super is sent to the client
     */
    super(new ClientError('error', id, userMessage, cause));
    this.name = id;
    this.message = logMessage;
    this.userMessage = userMessage;
    this.cause = cause;
  }
}
