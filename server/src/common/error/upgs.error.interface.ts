export interface IUPGSError {
  /**
   * The id of the error
   */
  readonly id: string;
  /**
   * Overwrites the default name of the error that is logged by nest.js.
   * This message should be useful for the developer
   */
  message: string;
  /**
   * The message for the end-user
   */
  userMessage: string;
  /**
   * The cause (object or id) of the error
   */
  cause: any;
}
