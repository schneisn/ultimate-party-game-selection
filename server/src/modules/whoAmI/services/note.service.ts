import { Injectable } from '@nestjs/common';
import { PlayerNotDefinedError } from '../../common/player/service/error/player-not-defined.error';
import { WAIGame } from '../domain/game.entity';
import { IWAIPlayerService } from './player.service.interface';
import { WAINote } from '../domain/note.entity';
import { IWAINoteService } from './note.service.interface';
import { IGameRepository } from '../../common/game/repository/game.repository.interface';

@Injectable()
export class WAINoteService implements IWAINoteService {
  constructor(
    private readonly playerService: IWAIPlayerService,
    private readonly gameRepository: IGameRepository,
  ) {}

  addNote(
    game: WAIGame,
    playerId: string,
    note: string,
    isCorrect: boolean,
  ): void {
    const player = this.playerService.getPlayer(game, playerId);
    if (!player) {
      throw new PlayerNotDefinedError(playerId);
    }
    note = note.trim();
    if (player.notes.some((existingNote) => existingNote.note === note)) {
      return;
    }
    player.notes.push(new WAINote(note, isCorrect));
    this.gameRepository.save(game);
  }

  deleteNote(game: WAIGame, playerId: string, note: string): void {
    const player = this.playerService.getPlayer(game, playerId);
    if (!player) {
      throw new PlayerNotDefinedError(playerId);
    }
    const deleteNoteIndex = player.notes.findIndex(
      (existingNote) => existingNote.note === note,
    );
    player.notes.splice(deleteNoteIndex, 1);
    this.gameRepository.save(game);
  }
}
