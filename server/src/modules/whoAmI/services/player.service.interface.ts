import { ICommonPlayerService } from '../../common/player/service/player.service.interface';
import { WAIGame } from '../domain/game.entity';
import { WAIPlayer } from '../domain/player.entity';

export abstract class IWAIPlayerService extends ICommonPlayerService<
  WAIPlayer
> {
  /**
   * Selects a player to be able to define their identity
   * @param game
   * @param playerId
   * @param selectedPlayerId
   */
  abstract selectPlayer(
    game: WAIGame,
    playerId: string,
    selectedPlayerId: string,
  ): void;

  /**
   * Deselects a player and removes its identity
   * @param game
   * @param playerId
   * @param deselectedPlayerId
   */
  abstract deselectPlayer(
    game: WAIGame,
    playerId: string,
    deselectedPlayerId: string,
  ): void;

  /**
   * Sets the identity of the selected player
   * @param game
   * @param playerId
   * @param selectedPlayerId
   * @param identity
   */
  abstract setIdentity(
    game: WAIGame,
    playerId: string,
    selectedPlayerId: string,
    identity: string,
  ): void;

  abstract revealIdentity(game: WAIGame, playerId: string): void;

  abstract resetIdentity(game: WAIGame, resetPlayerId: string): void;
}
