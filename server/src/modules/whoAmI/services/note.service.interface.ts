import { WAIGame } from '../domain/game.entity';

export abstract class IWAINoteService {
  abstract addNote(
    game: WAIGame,
    playerId: string,
    note: string,
    isCorrect: boolean,
  ): void;

  abstract deleteNote(game: WAIGame, playerId: string, note: string): void;
}
