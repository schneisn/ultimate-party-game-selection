import { IGameRepository } from '../../common/game/repository/game.repository.interface';
import { NewGameDto } from '../../common/game/service/dto/new-game.dto';
import { CommonGameService } from '../../common/game/service/game.service';
import { WAIPlayer } from '../domain/player.entity';
import { IWAIPlayerService } from './player.service.interface';
import { IWAIGameService } from './game.service.interface';
import { WAIGame } from '../domain/game.entity';
import { Injectable } from '@nestjs/common';

@Injectable()
export class WAIGameService extends CommonGameService<WAIGame, WAIPlayer>
  implements IWAIGameService {
  constructor(
    private readonly playerService: IWAIPlayerService,
    readonly gameRepository: IGameRepository<WAIGame>,
  ) {
    super(gameRepository);
  }

  newGame(playerName: string): NewGameDto {
    const game = new WAIGame();
    const player = this.playerService.addPlayer(game, null, playerName);
    this.gameRepository.save(game);
    return new NewGameDto(game, player);
  }

  setPlayerReadyState(game: WAIGame, playerId: string, isReady: boolean): void {
    this.playerService.getPlayer(game, playerId).isReady = isReady;
    if (this.allPlayersReady(game)) {
      this.startGame(game);
    }
    this.gameRepository.save(game);
  }

  startGame(game: WAIGame): void {
    game.isInLobby = false;
  }

  allPlayersReady(game: WAIGame): boolean {
    return game.players.every((player) => player.isReady);
  }
}
