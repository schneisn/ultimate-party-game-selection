import { ICommonGameService } from '../../common/game/service/game.service.interface';
import { WAIPlayer } from '../domain/player.entity';
import { WAIGame } from '../domain/game.entity';

export abstract class IWAIGameService extends ICommonGameService<
  WAIGame,
  WAIPlayer
> {
  abstract setPlayerReadyState(
    game: WAIGame,
    playerId: string,
    isReady: boolean,
  ): void;

  abstract startGame(game: WAIGame): void;

  abstract allPlayersReady(game: WAIGame): boolean;
}
