import { PlayerNotDefinedError } from '../../common/player/service/error/player-not-defined.error';
import { CommonPlayerService } from '../../common/player/service/player.service';
import { WAIGame } from '../domain/game.entity';
import { WAIPlayer } from '../domain/player.entity';
import { IWAIPlayerService } from './player.service.interface';
import { IWAIGameService } from './game.service.interface';
import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { IGameRepository } from '../../common/game/repository/game.repository.interface';

@Injectable()
export class WAIPlayerService extends CommonPlayerService<WAIGame, WAIPlayer>
  implements IWAIPlayerService {
  constructor(
    @Inject(forwardRef(() => IWAIGameService))
    private readonly gameService: IWAIGameService,
    readonly gameRepository: IGameRepository,
  ) {
    super(gameRepository);
  }

  addPlayer(game: WAIGame, playerId: string, playerName: string): WAIPlayer {
    playerName = this.addNrToPlayerNameIfExists(game, playerId, playerName);
    const existingPlayer = this.replaceExistingPlayer(
      game,
      playerId,
      playerName,
    );
    if (existingPlayer) {
      this.gameRepository.save(game);
      return existingPlayer;
    }
    const newPlayer = new WAIPlayer(null, playerName);
    game.players.push(newPlayer);
    this.gameRepository.save(game);
    return newPlayer;
  }

  selectPlayer(
    game: WAIGame,
    playerId: string,
    selectedPlayerId: string,
  ): void {
    if (playerId === selectedPlayerId) {
      return;
    }
    const selectedPlayer = this.getPlayer(game, selectedPlayerId);
    if (!selectedPlayer) {
      throw new PlayerNotDefinedError(selectedPlayerId);
    }
    if (!selectedPlayer.identityAuthor) {
      selectedPlayer.identityAuthor = playerId;
    } else if (
      selectedPlayer.identityAuthor === playerId &&
      !selectedPlayer.identity
    ) {
      selectedPlayer.identityAuthor = null;
    }
    this.gameRepository.save(game);
  }

  setIdentity(
    game: WAIGame,
    playerId: string,
    selectedPlayerId: string,
    identity: string,
  ): void {
    const selectedPlayer = this.getPlayer(game, selectedPlayerId);
    if (!selectedPlayer) {
      throw new PlayerNotDefinedError(selectedPlayerId);
    }
    selectedPlayer.identityAuthor = playerId;
    selectedPlayer.identity = identity.trim();
    selectedPlayer.identityConfirmed = true;
    this.gameRepository.save(game);
  }

  deselectPlayer(
    game: WAIGame,
    playerId: string,
    deselectedPlayerId: string,
  ): void {
    const deselectedPlayer = this.getPlayer(game, deselectedPlayerId);
    if (!deselectedPlayer) {
      throw new PlayerNotDefinedError(deselectedPlayerId);
    }
    if (playerId !== deselectedPlayer.identityAuthor) {
      return;
    }
    deselectedPlayer.identity = null;
    deselectedPlayer.identityAuthor = null;
    deselectedPlayer.identityConfirmed = false;
    this.gameRepository.save(game);
  }

  revealIdentity(game: WAIGame, playerId: string): void {
    const player = this.getPlayer(game, playerId);
    if (!player) {
      throw new PlayerNotDefinedError(playerId);
    }
    player.identityRevealed = !player.identityRevealed;
    this.gameRepository.save(game);
  }

  resetIdentity(game: WAIGame, resetPlayerId: string): void {
    const player = this.getPlayer(game, resetPlayerId);
    if (!player) {
      throw new PlayerNotDefinedError(resetPlayerId);
    }
    player.identity = null;
    player.identityAuthor = null;
    player.notes = [];
    player.identityConfirmed = false;
    player.identityRevealed = false;
    this.gameRepository.save(game);
  }

  disconnectPlayer(game: WAIGame, playerId: string) {
    const disconnectedPlayer = this.getPlayer(game, playerId);
    if (!disconnectedPlayer) {
      throw new PlayerNotDefinedError(playerId);
    }
    disconnectedPlayer.isConnected = false;
    if (game.isInLobby) {
      super.disconnectPlayer(game, playerId);
      if (
        this.gameService.allPlayersReady(game) &&
        !this.atMostOnePlayerConnected(game)
      ) {
        this.gameService.startGame(game);
      }
    }
    if (this.noPlayersAreConnected(game)) {
      this.gameService.scheduleGameForRemoval(game.id);
      return;
    }
    game.players.forEach((teamMate) => {
      if (
        disconnectedPlayer.id === teamMate.identityAuthor &&
        !teamMate.identityConfirmed
      ) {
        teamMate.identityAuthor = null;
      }
    });
    this.gameRepository.save(game);
  }
}
