import { Module } from '@nestjs/common';
import { WAIGateway } from './gateway/who-am-i.gateway';
import { IWAIGameService } from './services/game.service.interface';
import { WAIGameService } from './services/game.service';
import { IGameRepository } from '../common/game/repository/game.repository.interface';
import { GameRepository } from '../common/game/repository/game.repository';
import { IWAINoteService } from './services/note.service.interface';
import { WAINoteService } from './services/note.service';
import { IWAIPlayerService } from './services/player.service.interface';
import { WAIPlayerService } from './services/player.service';

@Module({
  providers: [
    WAIGateway,
    {
      provide: IWAIGameService,
      useClass: WAIGameService,
    },
    {
      provide: IGameRepository,
      useClass: GameRepository,
    },
    {
      provide: IWAINoteService,
      useClass: WAINoteService,
    },
    {
      provide: IWAIPlayerService,
      useClass: WAIPlayerService,
    },
  ],
})
export class WhoAmIModule {}
