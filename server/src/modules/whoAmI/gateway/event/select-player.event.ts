import { UpgsEvent } from '../../../common/gateway/event/upgs.event';

export class SelectPlayerEvent extends UpgsEvent {
  static readonly id = 'SelectPlayer';
}
