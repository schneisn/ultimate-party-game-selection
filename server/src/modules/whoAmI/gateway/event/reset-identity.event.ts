import { UpgsEvent } from '../../../common/gateway/event/upgs.event';

export class ResetIdentityEvent extends UpgsEvent {
  static readonly id = 'ResetIdentity';
}
