import { GameRequest } from '../../../../common/gateway/event/request/game.request';
import { WAIGame } from '../../../domain/game.entity';

export class RevealIdentityRequest extends GameRequest<WAIGame> {
  revealPlayerId: string;
}
