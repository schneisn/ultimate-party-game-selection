import { GameRequest } from '../../../../common/gateway/event/request/game.request';
import { WAIGame } from '../../../domain/game.entity';

export class SelectPlayerRequest extends GameRequest<WAIGame> {
  selectedPlayerId: string;
}
