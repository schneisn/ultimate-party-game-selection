import { GameRequest } from '../../../../common/gateway/event/request/game.request';
import { WAIGame } from '../../../domain/game.entity';

export class ConfirmIdentityRequest extends GameRequest<WAIGame> {
  selectedPlayerId: string;
  identity: string;
}
