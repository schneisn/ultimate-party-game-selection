import { GameRequest } from '../../../../common/gateway/event/request/game.request';
import { WAIGame } from '../../../domain/game.entity';

export class AddNoteRequest extends GameRequest<WAIGame> {
  note: string;
  isCorrect: boolean;
}
