import { GameRequest } from '../../../../common/gateway/event/request/game.request';
import { WAIGame } from '../../../domain/game.entity';

export class ResetIdentityRequest extends GameRequest<WAIGame> {
  resetPlayerId: string;
}
