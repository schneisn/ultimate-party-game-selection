import { UpgsEvent } from '../../../common/gateway/event/upgs.event';

export class ConfirmIdentityEvent extends UpgsEvent {
  static readonly id = 'ConfirmIdentity';
}
