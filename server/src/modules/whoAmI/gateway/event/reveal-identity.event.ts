import { UpgsEvent } from '../../../common/gateway/event/upgs.event';

export class RevealIdentityEvent extends UpgsEvent {
  static readonly id = 'RevealIdentity';
}
