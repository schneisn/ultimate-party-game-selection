import { UpgsEvent } from '../../../common/gateway/event/upgs.event';

export class DeleteNoteEvent extends UpgsEvent {
  static readonly id = 'DeleteNote';
}
