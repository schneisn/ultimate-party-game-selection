import { UpgsEvent } from '../../../common/gateway/event/upgs.event';

export class AddNoteEvent extends UpgsEvent {
  static readonly id = 'AddNote';
}
