import { UpgsEvent } from '../../../common/gateway/event/upgs.event';

export class DeselectPlayerEvent extends UpgsEvent {
  static readonly id = 'DeselectPlayer';
}
