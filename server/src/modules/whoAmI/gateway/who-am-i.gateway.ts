import {
  ConnectedSocket,
  MessageBody,
  SubscribeMessage,
  WebSocketGateway,
} from '@nestjs/websockets';
import { Socket } from 'socket.io';
import { ISessionService } from '../../session/service/session.service.interface';
import { CommonGateway } from '../../common/gateway/common.gateway';
import { SetPlayerReadyStateRequest } from '../../common/gateway/event/request/set-player-ready-state.request';
import { SetPlayerReadyStateEvent } from '../../common/gateway/event/set-player-ready-state.event';
import { LoggerService } from '../../logger/logger.service';
import { IWAIGameService } from '../services/game.service.interface';
import { IWAINoteService } from '../services/note.service.interface';
import { IWAIPlayerService } from '../services/player.service.interface';
import { AddNoteEvent } from './event/add-note.event';
import { ConfirmIdentityEvent } from './event/confirm-identity.event';
import { DeleteNoteEvent } from './event/delete-note.event';
import { DeselectPlayerEvent } from './event/deselect-player.event';
import { AddNoteRequest } from './event/request/add-note.request';
import { ConfirmIdentityRequest } from './event/request/confirm-identity.request';
import { DeleteNoteRequest } from './event/request/delete-note.request';
import { DeselectPlayerRequest } from './event/request/deselect-player.request';
import { ResetIdentityRequest } from './event/request/reset-identity.request';
import { SelectPlayerRequest } from './event/request/select-player.request';
import { ResetIdentityEvent } from './event/reset-identity.event';
import { SelectPlayerEvent } from './event/select-player.event';
import { Game } from '../../common/game/domain/game.entity';
import { WAIGame } from '../domain/game.entity';
import { Player } from '../../common/player/domain/player.entity';
import { UseInterceptors, UsePipes } from '@nestjs/common';
import { GetGamePipe } from '../../../pipes/get-game.pipe';
import { RevealIdentityEvent } from './event/reveal-identity.event';
import { RevealIdentityRequest } from './event/request/reveal-identity.request';
import { GetPlayerInterceptor } from '../../../interceptors/get-player.interceptor';

@WebSocketGateway({ namespace: 'whoami', cors: true })
export class WAIGateway extends CommonGateway {
  constructor(
    readonly logger: LoggerService,
    readonly gameService: IWAIGameService,
    readonly playerService: IWAIPlayerService,
    readonly noteService: IWAINoteService,
    readonly sessionService: ISessionService,
  ) {
    super('whomai', logger, gameService, playerService, sessionService);
  }

  @UsePipes(GetGamePipe)
  @UseInterceptors(GetPlayerInterceptor)
  @SubscribeMessage(SetPlayerReadyStateEvent.id)
  onSetPlayerReadyState(
    @MessageBody() data: SetPlayerReadyStateRequest<WAIGame>,
  ): void {
    this.gameService.setPlayerReadyState(
      data.game,
      data.player.id,
      data.isReady,
    );
    this.emitGame(data.game.id);
    this.emitOpenGames();
  }

  @UsePipes(GetGamePipe)
  @UseInterceptors(GetPlayerInterceptor)
  @SubscribeMessage(SelectPlayerEvent.id)
  onSelectPlayer(@MessageBody() data: SelectPlayerRequest): void {
    this.playerService.selectPlayer(
      data.game,
      data.player.id,
      data.selectedPlayerId,
    );
    this.emitGame(data.game.id);
  }

  @UsePipes(GetGamePipe)
  @UseInterceptors(GetPlayerInterceptor)
  @SubscribeMessage(DeselectPlayerEvent.id)
  onDeselectPlayer(@MessageBody() data: DeselectPlayerRequest): void {
    this.playerService.deselectPlayer(
      data.game,
      data.player.id,
      data.deselectedPlayerId,
    );
    this.emitGame(data.game.id);
  }

  @UsePipes(GetGamePipe)
  @UseInterceptors(GetPlayerInterceptor)
  @SubscribeMessage(ConfirmIdentityEvent.id)
  onConfirmIdentity(@MessageBody() data: ConfirmIdentityRequest): void {
    this.playerService.setIdentity(
      data.game,
      data.player.id,
      data.selectedPlayerId,
      data.identity,
    );
    this.emitGame(data.game.id);
  }

  @UseInterceptors(GetPlayerInterceptor)
  @SubscribeMessage(AddNoteEvent.id)
  onAddNote(
    @ConnectedSocket() client: Socket,
    @MessageBody(GetGamePipe) data: AddNoteRequest,
  ): void {
    this.noteService.addNote(
      data.game,
      data.player.id,
      data.note,
      data.isCorrect,
    );
    this.emitGameToRoom(data.game.id, client.id);
  }

  @UseInterceptors(GetPlayerInterceptor)
  @SubscribeMessage(DeleteNoteEvent.id)
  onDeleteNote(
    @ConnectedSocket() client: Socket,
    @MessageBody(GetGamePipe) data: DeleteNoteRequest,
  ): void {
    this.noteService.deleteNote(data.game, data.player.id, data.note);
    this.emitGameToRoom(data.game.id, client.id);
  }

  @UsePipes(GetGamePipe)
  @SubscribeMessage(RevealIdentityEvent.id)
  onRevealIdentity(@MessageBody() data: RevealIdentityRequest): void {
    this.playerService.revealIdentity(data.game, data.revealPlayerId);
    this.emitGame(data.game.id);
  }

  @UsePipes(GetGamePipe)
  @SubscribeMessage(ResetIdentityEvent.id)
  onResetIdentity(@MessageBody() data: ResetIdentityRequest): void {
    this.playerService.resetIdentity(data.game, data.resetPlayerId);
    this.emitGame(data.game.id);
  }

  protected transformGame(game: WAIGame, playerId: string): Game<Player> {
    const player = game.players.find((p) => p.id === playerId);
    if (player && !player.identityRevealed) {
      player.identity = null;
    }
    return game;
  }
}
