import { Player } from '../../common/player/domain/player.entity';
import { WAINote } from './note.entity';

export class WAIPlayer extends Player {
  /**
   * Whether this player has guessed his identity correctly
   */
  guessedCorrect: boolean;
  isReady: boolean;
  /**
   * The identity (person/role/...) of the player
   */
  identity: string;
  /**
   * The id of the player that is the author
   * of the identity
   */
  identityAuthor: string;
  /**
   * Whether the player has an identity
   */
  identityConfirmed: boolean;
  /**
   * The notes that the player took
   */
  notes: WAINote[];
  /**
   * Whether the identity is visible to the player themself
   */
  identityRevealed: boolean;

  constructor(id: string, name: string) {
    super(id, name);
    this.notes = [];
  }
}
