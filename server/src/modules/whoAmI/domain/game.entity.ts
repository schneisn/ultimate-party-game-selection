import { Game } from '../../common/game/domain/game.entity';
import { WAIPlayer } from './player.entity';

export class WAIGame extends Game<WAIPlayer> {
  /**
   * Whether the players are in the lobby
   */
  isInLobby: boolean;

  constructor() {
    super();
    this.isInLobby = true;
  }
}
