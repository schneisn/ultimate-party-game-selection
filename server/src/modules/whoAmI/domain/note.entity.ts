export class WAINote {
  note: string;
  /**
   * Whether the other players said, this question/statement/...
   * is correct
   */
  isCorrect: boolean;

  constructor(note: string, isCorrect: boolean) {
    this.note = note;
    this.isCorrect = isCorrect;
  }
}
