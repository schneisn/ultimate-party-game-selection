import { Module } from '@nestjs/common';
import { SFGateway } from './gateway/spyfall.gateway';
import { ISFGameService } from './services/game.service.interface';
import { SFGameService } from './services/game.service';
import { IGameRepository } from '../common/game/repository/game.repository.interface';
import { GameRepository } from '../common/game/repository/game.repository';
import { ISFLocationService } from './services/location.service.interface';
import { SFLocationService } from './services/location.service';
import { ISFPlayerService } from './services/player.service.interface';
import { SFPlayerService } from './services/player.service';

@Module({
  providers: [
    SFGateway,
    {
      provide: ISFGameService,
      useClass: SFGameService,
    },
    {
      provide: IGameRepository,
      useClass: GameRepository,
    },
    {
      provide: ISFLocationService,
      useClass: SFLocationService,
    },
    {
      provide: ISFPlayerService,
      useClass: SFPlayerService,
    },
  ],
})
export class SpyfallModule {}
