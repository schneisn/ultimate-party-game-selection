import { ICommonPlayerService } from '../../common/player/service/player.service.interface';
import { SFPlayer } from '../domain/player.entity';

export abstract class ISFPlayerService extends ICommonPlayerService<SFPlayer> {}
