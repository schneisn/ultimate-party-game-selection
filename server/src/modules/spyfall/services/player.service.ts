import { GameHasStartedError } from '../../common/player/service/error/game-has-started.error';
import { CommonPlayerService } from '../../common/player/service/player.service';
import { SFGame } from '../domain/game.entity';
import { SFPlayer } from '../domain/player.entity';
import { ISFPlayerService } from './player.service.interface';
import { ISFGameService } from './game.service.interface';
import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { PlayerNotDefinedError } from '../../common/player/service/error/player-not-defined.error';
import { IGameRepository } from '../../common/game/repository/game.repository.interface';

@Injectable()
export class SFPlayerService extends CommonPlayerService<SFGame, SFPlayer>
  implements ISFPlayerService {
  constructor(
    @Inject(forwardRef(() => ISFGameService))
    private readonly gameService: ISFGameService,
    readonly gameRepository: IGameRepository,
  ) {
    super(gameRepository);
  }

  addPlayer(game: SFGame, playerId: string, playerName: string): SFPlayer {
    playerName = this.addNrToPlayerNameIfExists(game, playerId, playerName);
    const existingPlayer = this.replaceExistingPlayer(
      game,
      playerId,
      playerName,
    );
    if (existingPlayer) {
      this.gameRepository.save(game);
      return existingPlayer;
    }
    if (game.statePlaying) {
      throw new GameHasStartedError(game);
    }
    const newPlayer = new SFPlayer(null, playerName);
    game.players.push(newPlayer);
    this.gameRepository.save(game);
    return newPlayer;
  }

  disconnectPlayer(game: SFGame, playerId: string) {
    const disconnectedPlayer = this.getPlayer(game, playerId);
    if (!disconnectedPlayer) {
      throw new PlayerNotDefinedError(playerId);
    }
    disconnectedPlayer.isConnected = false;
    if (!game.statePlaying) {
      super.disconnectPlayer(game, playerId);
    }
    if (this.noPlayersAreConnected(game)) {
      this.gameService.scheduleGameForRemoval(game.id);
      return;
    }
    this.gameRepository.save(game);
  }
}
