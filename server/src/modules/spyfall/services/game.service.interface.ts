import { ICommonGameService } from '../../common/game/service/game.service.interface';
import { SFPlayer } from '../domain/player.entity';
import { SettingsDto } from './dto/settings.dto';
import { SFGame } from '../domain/game.entity';

export abstract class ISFGameService extends ICommonGameService<
  SFGame,
  SFPlayer
> {
  abstract setPlayerReadyState(
    game: SFGame,
    playerId: string,
    isReady: boolean,
  ): void;

  /**
   * Start a vote. The player with the given id is suspected to be the agent.
   * The voter automatically votes for yes (= the suspected player is the agent)
   * @param game
   * @param voterId The id of the player that initiated the vote
   * @param playerId The id of the suspected player
   */
  abstract startAgentVote(
    game: SFGame,
    voterId: string,
    playerId: string,
  ): void;

  /**
   * Vote yes or no that the currently suspected player is the agent.
   * If a player votes no, the voting round is cancelled.
   * If all players (except the agent) vote yes, the agent is revealed
   * @param game
   * @param voterId The id of the voting player
   * @param isAgent Whether the player is an agent
   */
  abstract voteAgent(game: SFGame, voterId: string, isAgent: boolean): void;

  /**
   * Checks if there is no active vote and no win state is set.
   * This is assumed to indicate a failed vote
   * @param game
   */
  abstract voteFailed(game: SFGame): boolean;

  /**
   * Guess the location of the agent
   * @param game
   * @param guessedLocation
   */
  abstract guessLocation(game: SFGame, guessedLocation: string): void;

  abstract updateSettings(game: SFGame, settings: SettingsDto): void;
}
