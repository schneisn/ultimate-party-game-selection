import { SFGame } from '../domain/game.entity';
import { SFLocation } from '../domain/location.entity';

export abstract class ISFLocationService {
  /**
   * Chooses a random location for the game and selects possible locations
   * where the players could be at (for the agent).
   *
   * This function does not consider whether there are enough roles for
   * the amount of players in the game!
   * @param game
   */
  abstract selectGameLocation(game: SFGame): SFLocation;

  /**
   * Gets all available locations
   */
  abstract getLocations(): SFLocation[];
}
