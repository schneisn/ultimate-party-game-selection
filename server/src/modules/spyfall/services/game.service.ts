import { IGameRepository } from '../../common/game/repository/game.repository.interface';
import { NewGameDto } from '../../common/game/service/dto/new-game.dto';
import { CommonGameService } from '../../common/game/service/game.service';
import { AGENT } from './location.service';
import { ISFLocationService } from './location.service.interface';
import { SFPlayer } from '../domain/player.entity';
import { ISFPlayerService } from './player.service.interface';
import { SettingsDto } from './dto/settings.dto';
import { ISFGameService } from './game.service.interface';
import { SFGame, WinState } from '../domain/game.entity';
import { Vote } from '../domain/vote.entity';
import { Injectable } from '@nestjs/common';

@Injectable()
export class SFGameService extends CommonGameService<SFGame, SFPlayer>
  implements ISFGameService {
  constructor(
    private readonly playerService: ISFPlayerService,
    private readonly locationService: ISFLocationService,
    readonly gameRepository: IGameRepository<SFGame>,
  ) {
    super(gameRepository);
  }

  private static unreadyPlayers(game: SFGame): void {
    for (const player of game.players) {
      player.isReady = false;
    }
  }

  private static allPlayersReady(game: SFGame): boolean {
    return game.players.every((player) => player.isReady);
  }

  private static setGameWinState(game: SFGame, winState: WinState): void {
    SFGameService.unreadyPlayers(game);
    game.statePlaying = false;
    game.winState = winState;
  }

  getAllOpen(playerId: string): SFGame[] {
    return this.gameRepository.findAll().filter((game: SFGame) => {
      return (
        this.playerService.gameContainsPlayerId(game, playerId) ||
        game.statePlaying === false
      );
    });
  }

  newGame(playerName: string): NewGameDto {
    const game = new SFGame();
    game.maxNrPossibleLocations = 10;
    const player = this.playerService.addPlayer(game, null, playerName);
    this.gameRepository.save(game);
    return new NewGameDto(game, player);
  }

  setPlayerReadyState(game: SFGame, playerId: string, isReady: boolean): void {
    this.playerService.getPlayer(game, playerId).isReady = isReady;
    if (SFGameService.allPlayersReady(game)) {
      this.startGame(game);
    }
    this.gameRepository.save(game);
  }

  startAgentVote(game: SFGame, voterId: string, playerId: string): void {
    game.suspectedAgent = playerId;
    game.voteInitiator = voterId;
    this.voteAgent(game, voterId, true);
    this.gameRepository.save(game);
  }

  voteAgent(game: SFGame, voterId: string, isAgent: boolean): void {
    // Suspected agent may not vote no
    if (voterId === game.suspectedAgent && !isAgent) {
      return;
    }

    if (!isAgent) {
      // Reset vote
      game.yesVotes = [];
      game.voteInitiator = null;
      game.suspectedAgent = null;
      this.gameRepository.save(game);
      return;
    }

    if (game.yesVotes.some((vote) => vote.voterId === voterId)) {
      const voteIndex = game.yesVotes.findIndex(
        (vote) => vote.voterId === voterId,
      );
      game.yesVotes.splice(voteIndex, 1);
    }
    game.yesVotes.push(new Vote(voterId, isAgent));

    const nrConnectedPlayersWithoutSuspectedPlayer = game.players
      .filter((p) => p.id !== game.suspectedAgent)
      .filter((p) => p.isConnected).length;
    const nrVotesWithoutSuspectedPlayer = game.yesVotes.filter(
      (vote) => vote.voterId !== game.suspectedAgent,
    ).length;
    if (
      nrVotesWithoutSuspectedPlayer < nrConnectedPlayersWithoutSuspectedPlayer
    ) {
      this.gameRepository.save(game);
      return;
    }
    if (
      this.playerService.getPlayer(game, game.suspectedAgent).role === AGENT
    ) {
      SFGameService.setGameWinState(game, WinState.playersGuessedAgentCorrect);
    } else {
      SFGameService.setGameWinState(game, WinState.playersGuessedAgentWrong);
    }
    this.gameRepository.save(game);
  }

  voteFailed(game: SFGame): boolean {
    return !game.suspectedAgent && game.winState === null;
  }

  guessLocation(game: SFGame, guessedLocation: string): void {
    game.guessedLocation = guessedLocation;
    if (guessedLocation === game.location) {
      SFGameService.setGameWinState(game, WinState.agentGuessedLocationCorrect);
    } else {
      SFGameService.setGameWinState(game, WinState.agentGuessedLocationWrong);
    }
    this.gameRepository.save(game);
  }

  updateSettings(game: SFGame, settings: SettingsDto): void {
    game.maxNrPossibleLocations = Math.floor(
      Math.max(
        Math.min(
          settings.maxNrPossibleLocations,
          this.locationService.getLocations().length,
        ),
        2,
      ),
    );
    this.gameRepository.save(game);
  }

  private startGame(game: SFGame): void {
    game.statePlaying = true;
    game.winState = null;
    game.suspectedAgent = null;
    game.voteInitiator = null;
    game.yesVotes = [];
    this.shuffleLocationAndRoles(game);
  }

  private shuffleLocationAndRoles(game: SFGame): void {
    const location = this.locationService.selectGameLocation(game);
    const agentIndex = Math.floor(Math.random() * game.players.length);
    game.players.forEach(
      (player: SFPlayer) =>
        (player.role =
          game.players[agentIndex] === player
            ? AGENT
            : location.roles[
                Math.floor(Math.random() * location.roles.length)
              ]),
    );
  }
}
