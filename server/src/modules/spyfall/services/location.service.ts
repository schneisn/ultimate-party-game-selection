import { Injectable } from '@nestjs/common';
import { IFileService } from '../../file/file.service.interface';
import { ArrayUtils } from '../../../utils/array.utils';
import { SFGame } from '../domain/game.entity';
import { ISFLocationService } from './location.service.interface';
import { SFLocation } from '../domain/location.entity';
import { IGameRepository } from '../../common/game/repository/game.repository.interface';

/**
 * The role agent. It exists in every game
 */
export const AGENT = 'agent';

@Injectable()
export class SFLocationService implements ISFLocationService {
  private readonly locations: SFLocation[] = [];

  constructor(
    private readonly fileService: IFileService,
    private readonly gameRepository: IGameRepository,
  ) {
    this.locations = this.fileService.readJSONFromDir<SFLocation>(
      'assets/spyfall',
    );
  }

  getLocations(): SFLocation[] {
    return this.locations;
  }

  selectGameLocation(game: SFGame): SFLocation {
    const reducedLocations = ArrayUtils.shuffle(this.getLocations()).slice(
      0,
      game.maxNrPossibleLocations,
    );
    game.possibleLocations = reducedLocations.map(
      (loc: SFLocation) => loc.locationName,
    );
    const randomLocationIndex = Math.floor(
      Math.random() * reducedLocations.length,
    );
    const location = reducedLocations[randomLocationIndex];
    game.location = location.locationName;
    this.gameRepository.save(game);
    return location;
  }
}
