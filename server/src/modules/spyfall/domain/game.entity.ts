import { Game } from '../../common/game/domain/game.entity';
import { SFPlayer } from './player.entity';
import { Vote } from './vote.entity';

export enum WinState {
  agentGuessedLocationCorrect = 'agentGuessedLocationCorrect',
  agentGuessedLocationWrong = 'agentGuessedLocationWrong',
  playersGuessedAgentCorrect = 'playersGuessedAgentCorrect',
  playersGuessedAgentWrong = 'playersGuessedAgentWrong',
}

export class SFGame extends Game<SFPlayer> {
  /**
   * The location where the players are
   */
  location: string;
  /**
   * The locations that the players can guess
   * to be at
   */
  possibleLocations: string[] = [];
  /**
   * The maximum number of locations that the agent has to find
   * their actual location from
   */
  maxNrPossibleLocations: number;
  /**
   * Whether the players are in the lobby or playing
   */
  statePlaying: boolean = false;
  winState: WinState;
  /**
   * The location that the agent guessed to be at
   */
  guessedLocation: string;
  /**
   * The id of the player that initiated the vote for an agent
   */
  voteInitiator: string;
  /**
   * The id of the player that is suspected to be the agent
   */
  suspectedAgent: string;
  /**
   * The votes for the suspecting agent
   */
  yesVotes: Vote[];
}
