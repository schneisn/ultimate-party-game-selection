export class Vote {
  /**
   * The id of the voter
   */
  voterId: string;
  /**
   * Whether the players thinks the suspected player is the agent
   */
  isAgent: boolean;

  constructor(voterId: string, isAgent: boolean) {
    this.voterId = voterId;
    this.isAgent = isAgent;
  }
}
