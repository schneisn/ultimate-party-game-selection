export class SFLocation {
  readonly locationName: string;
  readonly roles: string[];

  constructor(locationName: string, roles: string[]) {
    this.locationName = locationName;
    this.roles = roles;
  }
}
