import { Player } from '../../common/player/domain/player.entity';

export class SFPlayer extends Player {
  role: string;
  isReady: boolean = false;
}
