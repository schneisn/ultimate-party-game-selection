import {
  MessageBody,
  SubscribeMessage,
  WebSocketGateway,
} from '@nestjs/websockets';
import { CommonGateway } from '../../common/gateway/common.gateway';
import { ISessionService } from '../../session/service/session.service.interface';
import { SetPlayerReadyStateRequest } from '../../common/gateway/event/request/set-player-ready-state.request';
import { SetPlayerReadyStateEvent } from '../../common/gateway/event/set-player-ready-state.event';
import { UpdateSettingsEvent } from '../../common/gateway/event/update-settings.event';
import { LoggerService } from '../../logger/logger.service';
import { ISFGameService } from '../services/game.service.interface';
import { ISFPlayerService } from '../services/player.service.interface';
import { AgentVoteEvent } from './event/agent-vote.event';
import { GuessLocationEvent } from './event/guess-location.event';
import { AgentVoteRequest } from './event/request/agent-vote.request';
import { GuessLocationRequest } from './event/request/guess-location.request';
import { StartAgentVoteRequest } from './event/request/start-agent-vote.request';
import { UpdateSettingsRequest } from './event/request/update-settings.request';
import { StartAgentVoteEvent } from './event/start-agent-vote.event';
import { VoteFailedEvent } from './event/vote-failed.event';
import { SFGame } from '../domain/game.entity';
import { Game } from '../../common/game/domain/game.entity';
import { Player } from '../../common/player/domain/player.entity';
import { UseInterceptors, UsePipes } from '@nestjs/common';
import { GetGamePipe } from '../../../pipes/get-game.pipe';
import { ConfigService } from '../../config/config.service';
import { GetPlayerInterceptor } from '../../../interceptors/get-player.interceptor';

@WebSocketGateway({ namespace: 'spyfall', cors: true })
export class SFGateway extends CommonGateway {
  constructor(
    readonly logger: LoggerService,
    readonly gameService: ISFGameService,
    readonly playerService: ISFPlayerService,
    readonly sessionService: ISessionService,
  ) {
    super('spyfall', logger, gameService, playerService, sessionService);
  }

  @UsePipes(GetGamePipe)
  @UseInterceptors(GetPlayerInterceptor)
  @SubscribeMessage(SetPlayerReadyStateEvent.id)
  onSetPlayerReadyState(
    @MessageBody() data: SetPlayerReadyStateRequest<SFGame>,
  ): void {
    this.gameService.setPlayerReadyState(
      data.game,
      data.player.id,
      data.isReady,
    );
    this.emitGame(data.game.id);
    this.emitOpenGames();
  }

  @UsePipes(GetGamePipe)
  @UseInterceptors(GetPlayerInterceptor)
  @SubscribeMessage(StartAgentVoteEvent.id)
  onStartAgentVote(@MessageBody() data: StartAgentVoteRequest): void {
    this.gameService.startAgentVote(
      data.game,
      data.player.id,
      data.suspectedAgent.id,
    );
    this.emitGame(data.game.id);
    this.emitOpenGames();
  }

  @UsePipes(GetGamePipe)
  @UseInterceptors(GetPlayerInterceptor)
  @SubscribeMessage(AgentVoteEvent.id)
  onAgentVote(@MessageBody() data: AgentVoteRequest): void {
    this.gameService.voteAgent(data.game, data.player.id, data.isAgent);
    if (this.gameService.voteFailed(data.game)) {
      this.publishEventToRoom(data.game.id, VoteFailedEvent.id);
    }
    this.emitGame(data.game.id);
    this.emitOpenGames();
  }

  @UsePipes(GetGamePipe)
  @SubscribeMessage(GuessLocationEvent.id)
  onGuessLocation(@MessageBody() data: GuessLocationRequest): void {
    this.gameService.guessLocation(data.game, data.location);
    this.emitGame(data.game.id);
    this.emitOpenGames();
  }

  @UsePipes(GetGamePipe)
  @SubscribeMessage(UpdateSettingsEvent.id)
  onUpdateSettings(@MessageBody() data: UpdateSettingsRequest): void {
    this.gameService.updateSettings(data.game, data.settings);
    this.emitGame(data.game.id);
  }

  protected transformGame(game: SFGame, playerId: string): Game<Player> {
    // Only remove agent info when players are in a round.
    // In the lobby or after the round this has to be included
    // to show which player was the agent.
    if (game.statePlaying) {
      game.players
        .filter((player) => player.id !== playerId)
        .forEach((player) => (player.role = null));
    }
    return game;
  }
}
