import { UpgsEvent } from '../../../common/gateway/event/upgs.event';

export class GuessLocationEvent extends UpgsEvent {
  static readonly id = 'GuessLocation';
}
