import { UpgsEvent } from '../../../common/gateway/event/upgs.event';

export class VoteFailedEvent extends UpgsEvent {
  static readonly id = 'VoteFailed';
}
