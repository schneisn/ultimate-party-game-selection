import { ValidateNested } from 'class-validator';
import { GameRequest } from '../../../../common/gateway/event/request/game.request';
import { IdRequestModel } from '../../../../common/gateway/event/request/model/id.request-model';
import { SFGame } from '../../../domain/game.entity';

export class StartAgentVoteRequest extends GameRequest<SFGame> {
  /**
   * The id of the suspected agent
   */
  @ValidateNested()
  suspectedAgent: IdRequestModel;
}
