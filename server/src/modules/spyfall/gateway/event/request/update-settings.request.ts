import { IsInt, Min, ValidateNested } from 'class-validator';
import { GameRequest } from '../../../../common/gateway/event/request/game.request';
import { SFGame } from '../../../domain/game.entity';

class Settings {
  @IsInt()
  @Min(1)
  maxNrPossibleLocations: number;
}

export class UpdateSettingsRequest extends GameRequest<SFGame> {
  @ValidateNested()
  settings: Settings;
}
