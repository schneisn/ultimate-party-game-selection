import { IsString } from 'class-validator';
import { GameRequest } from '../../../../common/gateway/event/request/game.request';
import { SFGame } from '../../../domain/game.entity';

export class GuessLocationRequest extends GameRequest<SFGame> {
  /**
   * The guessed location
   */
  @IsString()
  location: string;
}
