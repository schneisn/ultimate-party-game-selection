import { GameRequest } from '../../../../common/gateway/event/request/game.request';
import { SFGame } from '../../../domain/game.entity';

export class AgentVoteRequest extends GameRequest<SFGame> {
  /**
   * Whether the voter thinks the currently suspected player
   * is the agent
   */
  isAgent: boolean;
}
