import { UpgsEvent } from '../../../common/gateway/event/upgs.event';

export class StartAgentVoteEvent extends UpgsEvent {
  static readonly id = 'StartAgentVote';
}
