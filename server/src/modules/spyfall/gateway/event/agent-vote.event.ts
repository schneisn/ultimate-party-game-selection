import { UpgsEvent } from '../../../common/gateway/event/upgs.event';

export class AgentVoteEvent extends UpgsEvent {
  static readonly id = 'AgentVote';
}
