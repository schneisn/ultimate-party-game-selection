import { UpgsEvent } from '../../../common/gateway/event/upgs.event';

export class GuessAgentEvent extends UpgsEvent {
  static readonly id = 'GuessAgent';
}
