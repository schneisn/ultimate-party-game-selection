/**
 * A generic repository interface
 */
export abstract class IRepository<T> {
  /**
   * Retrieves an object with the given id
   * @param id
   * @throws Error if the object does not exist
   */
  abstract find(id: string): T;

  /**
   * Retrieves all objects
   */
  abstract findAll(): T[];

  /**
   * Creates or updates the given object
   * @param object
   */
  abstract save(object: T): void;

  /**
   * Deletes the object given the given id
   * @param id
   */
  abstract delete(id: string): void;
}
