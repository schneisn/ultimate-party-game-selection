import { Game } from '../../game/domain/game.entity';
import { Player } from '../domain/player.entity';
import { ICommonPlayerService } from './player.service.interface';
import { Injectable } from '@nestjs/common';
import { IGameRepository } from '../../game/repository/game.repository.interface';

/**
 * The subclasses must not have the injectable annotation!
 */
@Injectable()
export abstract class CommonPlayerService<G extends Game<P>, P extends Player>
  implements ICommonPlayerService<P> {
  protected constructor(readonly gameRepository: IGameRepository) {}

  getPlayers(game: G): P[] {
    return game.players;
  }

  getPlayer(game: G, playerId: string): P {
    return game.players.find((player) => player.id === playerId);
  }

  joinGame(game: G, playerId: string, playerName: string): P {
    game.scheduledRemovalDate = null;
    return this.addPlayer(game, playerId, playerName);
  }

  /**
   * Adds a player to the game and fills his hand cards
   * Has to be implemented in the child class because otherwise the player
   * that is created in this method and added to the game does not have the
   * correct type. The game also has the wrong type.
   * @param playerName
   * @param game
   * @param playerId
   */
  abstract addPlayer(game: G, playerId: string, playerName: string): P;

  gameContainsPlayerId(game: G, playerId: string): boolean {
    return game.players.some((player) => player.id === playerId);
  }

  getPlayerByName(game: G, name: string): P {
    name = name.trim();
    return game.players.find((player) => player.name === name);
  }

  disconnectPlayer(game: G, playerId: string): void {
    const playerIndex = game?.players?.findIndex((p) => p.id === playerId);
    if (playerIndex < 0) {
      return;
    }
    game?.players?.splice(playerIndex, 1);
  }

  kickPlayer(game: Game<P>, playerId: string): void {
    const playerIndex = game?.players?.findIndex((p) => p.id === playerId);
    if (playerIndex < 0) {
      return;
    }
    game?.players?.splice(playerIndex, 1);
    this.gameRepository.save(game);
  }

  /**
   * Adds a number to the player if there is a player with
   * the same name in the game. If there are multiple players
   * with the same name, they have different, incrementing numbers.
   * @param game
   * @param playerId
   * @param playerName
   * @protected
   */
  protected addNrToPlayerNameIfExists(
    game: G,
    playerId: string,
    playerName: string,
  ): string {
    const sameNamePlayer = this.getPlayerByName(game, playerName);
    if (!sameNamePlayer) {
      return playerName;
    }
    // Player rejoins game -> Use existing name
    if (sameNamePlayer.id === playerId) {
      return sameNamePlayer.name;
    }
    let playerNr = 1;
    let tempPlayerName;
    playerName = playerName.trim();
    do {
      playerNr++;
      tempPlayerName = playerName;
      tempPlayerName += ' ' + playerNr;
    } while (this.getPlayerByName(game, tempPlayerName));

    return playerName + ' ' + playerNr;
  }

  /**
   * Replace the player with the player id
   * @param game
   * @param playerId
   * @param playerName
   * @return The player that was replaced based on their id containing the new id and name
   * @protected
   */
  protected replaceExistingPlayer(
    game: G,
    playerId: string,
    playerName: string,
  ): P {
    const player = this.getPlayer(game, playerId);
    if (!player) {
      return null;
    }
    player.id = playerId;
    player.name = playerName;
    player.isConnected = true;
    return player;
  }

  /**
   * Checks whether there is only one player connected
   * @param game
   * @protected
   */
  protected atMostOnePlayerConnected(game: Game<Player>): boolean {
    return game.players.filter((player) => player.isConnected).length <= 1;
  }

  /**
   * Checks whether there are no more players connected
   * @param game
   * @protected
   */
  protected noPlayersAreConnected(game: Game<Player>): boolean {
    return game.players.filter((player) => player.isConnected).length < 1;
  }
}
