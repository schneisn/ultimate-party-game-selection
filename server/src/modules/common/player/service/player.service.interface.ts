import { Game } from '../../game/domain/game.entity';
import { Player } from '../domain/player.entity';

export abstract class ICommonPlayerService<P extends Player> {
  abstract getPlayer(game: Game<P>, playerId: string): P;

  abstract getPlayers(game: Game<P>): P[];

  /**
   * Adds a player with the given id and name to the game with the given id.
   * When overwriting the method from the base game service, remember to
   * think how you want to handle players with the same name.
   * The base game service adds a number after the player name
   *
   * @param game
   * @param playerId Used to take the place of a player.
   *        Creates a new id if this is null
   * @param playerName
   */
  abstract addPlayer(game: Game<P>, playerId: string, playerName: string): P;

  /**
   * Checks whether the given game contains the player with the given id
   * @param game
   * @param playerId
   */
  abstract gameContainsPlayerId(game: Game<P>, playerId: string): boolean;

  abstract getPlayerByName(game: Game<P>, name: string): P;

  /**
   * Removes the player with the given id from all games.
   * The default implementation deletes the player object from the game.
   *
   * @param game
   * @param playerId The id of the player to remove
   */
  abstract disconnectPlayer(game: Game<P>, playerId: string): void;

  /**
   * Completely deletes the player from the game.
   * @param game
   * @param playerId
   */
  abstract kickPlayer(game: Game<P>, playerId: string): void;

  /**
   * Join the given game with the given player id and the give name
   * @param game
   * @param playerName
   * @param playerId The client can send player id to re-join a
   *        previous game
   * When the player object still exists in the game, the newly joined
   * player can continue playing the existing player object
   */
  abstract joinGame(game: Game<P>, playerId: string, playerName: string): P;
}
