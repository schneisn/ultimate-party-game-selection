import { UpgsError } from '../../../../../common/error/upgs.error';

export class PlayerNotDefinedError extends UpgsError {
  constructor(playerId?: string) {
    super(
      'PLAYER_NOT_DEFINED',
      'Player does not exist or is null',
      'The player does not exist',
      playerId,
    );
  }
}
