import { UpgsError } from '../../../../../common/error/upgs.error';

export class PlayerExistsError extends UpgsError {
  constructor(playerId: any) {
    super(
      'PLAYER_EXISTS',
      'Player already exists with the given id',
      'The player already exists in the game',
      playerId,
    );
  }
}
