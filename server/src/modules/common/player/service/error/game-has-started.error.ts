import { UpgsError } from '../../../../../common/error/upgs.error';
import { Game } from '../../../game/domain/game.entity';
import { Player } from '../../domain/player.entity';

export class GameHasStartedError extends UpgsError {
  constructor(game: Game<Player>) {
    super(
      'GAME_HAS_ALREADY_STARTED',
      'Cannot add player, the game has already started',
      'You cannot join a running game',
      game,
    );
  }
}
