import { StringUtils } from '../../../../utils/string.utils';

export class Player {
  /**
   * A unique id for the player.
   * When the player is manually added by another player, the id is generated
   * when the object is created.
   */
  id: string;
  /**
   * The name of the player
   */
  name: string;
  /**
   * Tracks whether a player is currently connected to the server.
   * Useful when players are not supposed to be removed from the game
   * when they are disconnected from the server.
   */
  isConnected: boolean;
  /**
   * Whether the player is a real player that joined the game on his own
   * or the player was manually added to game by a real player.
   * If no id is passed to the constructor, it is true, otherwise false.
   */
  isComputer: boolean;

  constructor(id: string, playerName: string, isComputer: boolean = false) {
    this.id = id ? id : this.createId(playerName);
    this.isComputer = isComputer;
    this.name = playerName.trim();
    this.isConnected = !isComputer;
  }

  private createId(playerName: string): string {
    return (
      StringUtils.removeSpecialCharacters(
        StringUtils.removeBlanks(playerName),
      ).slice(0, 10) + Math.floor(Math.random() * Math.pow(10, 10)).toString()
    );
  }
}
