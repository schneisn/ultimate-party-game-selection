import {
  ClassSerializerInterceptor,
  UseFilters,
  UseGuards,
  UseInterceptors,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import {
  ConnectedSocket,
  MessageBody,
  OnGatewayConnection,
  OnGatewayDisconnect,
  SubscribeMessage,
  WebSocketServer,
} from '@nestjs/websockets';
import SocketIO, { Server, Socket } from 'socket.io';
import { ErrorFilter } from '../../../filters/error.filter';
import { ValidationErrorTransformFilter } from '../../../filters/validation-error-transform.filter';
import { WsLoggingInterceptor } from '../../../interceptors/ws-logging.interceptor';
import { LoggerService } from '../../logger/logger.service';
import { ISessionService } from '../../session/service/session.service.interface';
import { ICommonGameService } from '../game/service/game.service.interface';
import { Game } from '../game/domain/game.entity';
import { Player } from '../player/domain/player.entity';
import { ICommonPlayerService } from '../player/service/player.service.interface';
import { ConnectInfoEvent } from './event/connect-info.event';
import { CreateNewGameEvent } from './event/create-new-game.event';
import { DisconnectEvent } from './event/disconnect.event';
import { GetAllOpenGamesEvent } from './event/get-all-open-games.event';
import { GetGameEvent } from './event/get-game.event';
import { JoinGameEvent } from './event/join-game.event';
import { LeaveGameEvent } from './event/leave-game.event';
import { CreateNewGameRequest } from './event/request/create-new-game.request';
import { GetAllOpenGamesRequest } from './event/request/get-all-open-games.request';
import { GetGameRequest } from './event/request/get-game.request';
import { JoinGameRequest } from './event/request/join-game.request';
import { classToPlain } from 'class-transformer';
import { SerializableWsResponse } from './event/serializable-ws-response.class';
import { UpgsEvent } from './event/upgs.event';
import { cloneDeep } from 'lodash';
import { PlayerIdGuard } from '../../../guards/player-id.guard';
import { Public } from '../../../decorators/is-public.decorator';
import { GetGamePipe } from '../../../pipes/get-game.pipe';
import { ConfigService } from '../../config/config.service';
import { GetPlayerInterceptor } from '../../../interceptors/get-player.interceptor';
import { JoinRoomEvent } from './event/join-room.event';
import { LeaveRoomEvent } from './event/leave-room.event';
import { JoinRoomRequest } from './event/request/join-room.request';
import { LeaveRoomRequest } from './event/request/leave-room.request';
import { KickPlayerEvent } from './event/kick-player.event';
import { KickPlayerRequest } from './event/request/kick-player.request';
import { Cron } from '@nestjs/schedule';

/**
 * Rooms that the websocket clients can join
 */
enum Rooms {
  /**
   * The open games in the main menu
   */
  openGamesRoom = 'OpenGames',
}

@UseInterceptors(WsLoggingInterceptor, ClassSerializerInterceptor)
@UseFilters(ErrorFilter, ValidationErrorTransformFilter)
@UseGuards(PlayerIdGuard)
// @UsePipes(
//   new ValidationPipe({
//     disableErrorMessages: false,
//     dismissDefaultMessages: false,
//     validateCustomDecorators: true,
//     validationError: {
//       target: true,
//       value: true,
//     },
//   }),
// )
export class CommonGateway implements OnGatewayConnection, OnGatewayDisconnect {
  @WebSocketServer()
  private server: Server;

  constructor(
    readonly namespace: string,
    readonly logger: LoggerService,
    readonly gameService: ICommonGameService<Game<Player>, Player>,
    readonly playerService: ICommonPlayerService<Player>,
    readonly sessionService: ISessionService,
  ) {
    this.logger.setContext(CommonGateway.name);
  }

  @Public()
  @SubscribeMessage(JoinRoomEvent.id)
  onJoinRoom(
    @ConnectedSocket() client: Socket,
    @MessageBody() data: JoinRoomRequest,
  ): void {
    client.join(data.room);
  }

  @Public()
  @SubscribeMessage(LeaveRoomEvent.id)
  onLeaveRoom(
    @ConnectedSocket() client: Socket,
    @MessageBody() data: LeaveRoomRequest,
  ): void {
    client.leave(data.room);
  }

  @SubscribeMessage(GetGameEvent.id)
  onGetGame(
    @MessageBody() data: GetGameRequest,
  ): SerializableWsResponse<GetGameEvent> {
    const game = this.gameService.get(data.game.id);
    return new SerializableWsResponse(GetGameEvent.id, new GetGameEvent(game));
  }

  @Public()
  @UseInterceptors(GetPlayerInterceptor)
  @SubscribeMessage(GetAllOpenGamesEvent.id)
  onGetAllOpenGames(
    @ConnectedSocket() client: Socket,
    @MessageBody() data: GetAllOpenGamesRequest,
  ): SerializableWsResponse<GetAllOpenGamesEvent> {
    const allOpenGames = this.gameService.getAllOpen(data.player?.id);
    return new SerializableWsResponse(
      GetAllOpenGamesEvent.id,
      new GetAllOpenGamesEvent(allOpenGames),
    );
  }

  @Public()
  @SubscribeMessage(CreateNewGameEvent.id)
  onCreateNewGame(
    @ConnectedSocket() client: Socket,
    @MessageBody() data: CreateNewGameRequest,
  ): SerializableWsResponse<CreateNewGameEvent> {
    const newGameDto = this.gameService.newGame(data.player.name);
    client.join(newGameDto.game.id);
    this.sessionService.mapClientIdToPlayerId(client.id, newGameDto.player.id);
    this.emitOpenGames();
    return new SerializableWsResponse(
      CreateNewGameEvent.id,
      new CreateNewGameEvent(newGameDto.game, newGameDto.player),
    );
  }

  @Public()
  @UseInterceptors(GetPlayerInterceptor)
  @SubscribeMessage(JoinGameEvent.id)
  onJoin(
    @ConnectedSocket() client: Socket,
    @MessageBody(GetGamePipe) data: JoinGameRequest,
  ): SerializableWsResponse<JoinGameEvent> {
    const newPlayer = this.playerService.joinGame(
      data.game,
      data.player.id,
      data.player.name,
    );

    this.publishEventToRoom(
      data.game.id,
      ConnectInfoEvent.id,
      new ConnectInfoEvent(newPlayer),
    );
    this.emitGame(data.game.id);

    // Join game room after update is sent to other players in game
    client.join(data.game.id);
    this.sessionService.mapClientIdToPlayerId(client.id, newPlayer.id);
    return new SerializableWsResponse(
      JoinGameEvent.id,
      new JoinGameEvent(data.game, newPlayer),
    );
  }

  @UsePipes(GetGamePipe)
  @SubscribeMessage(KickPlayerEvent.id)
  onKickPlayer(@MessageBody() data: KickPlayerRequest): void {
    const kickedPlayer = this.playerService.getPlayer(data.game, data.playerId);
    const isConnected = kickedPlayer.isConnected;
    this.playerService.kickPlayer(data.game, data.playerId);
    // If the player is connected, remove from the game room and send a leave game event
    if (isConnected) {
      const clientIds = this.sessionService.getClientIds(kickedPlayer.id);
      clientIds.forEach((clientId) => {
        this.server.sockets[clientId].leave(data.game.id);
        this.publishEventToRoom(
          clientId,
          KickPlayerEvent.id,
          new KickPlayerEvent(),
        );
      });
    }
    this.emitGame(data.game.id);
  }

  @Public()
  @SubscribeMessage(LeaveGameEvent.id)
  onLeaveGame(
    @ConnectedSocket() client: Socket,
  ): SerializableWsResponse<LeaveGameEvent> {
    this.handleDisconnect(client, false);
    return new SerializableWsResponse(LeaveGameEvent.id, new LeaveGameEvent());
  }

  @Cron('0 */1 * * * *')
  removeGames(): void {
    this.logger.debug('Checking if games can be removed');
    this.gameService.removeGames();
    this.emitOpenGames();
  }

  handleConnection(client: Socket, ...args: any[]) {
    this.logger.log(`Client ${client.id} connected`);
  }

  handleDisconnect(
    @ConnectedSocket() client: Socket,
    endSession: boolean = true,
  ): void {
    this.logger.log(
      `Client ${client.id} disconnected`,
      this.handleDisconnect.name,
    );
    const disconnectedPlayerId = this.sessionService.getPlayerId(client.id);
    const game = this.gameService.getGameOfPlayer(disconnectedPlayerId);
    if (!game) {
      // The game is null if the player disconnects from the open games
      this.sessionService.removeClientId(client.id);
      return;
    }
    // Leave room here so the request to update the existing game is not send to this client.
    client.leave(game?.id);
    // Get player before removing because otherwise the player cannot be found anymore
    const disconnectedPlayer = this.playerService.getPlayer(
      game,
      disconnectedPlayerId,
    );
    if (!disconnectedPlayer.isConnected) {
      return;
    }
    endSession !== false &&
      this.sessionService.removePlayerId(disconnectedPlayer.id);
    disconnectedPlayer.isConnected = false;
    this.playerService.disconnectPlayer(game, disconnectedPlayer.id);
    // Emit disconnect before room is removed, so that the players in the room get the notification
    this.publishEventToRoom(
      game.id,
      DisconnectEvent.id,
      new DisconnectEvent(disconnectedPlayer),
    );
    // emitGame only works if the game still exists. If the game was deleted, emitGame does not emit
    // anything to the players, since they do not exist anymore (see implementation of emitGame).
    this.emitGame(game.id);
    // If the game was removed, remove the room
    try {
      this.gameService.get(game.id);
    } catch (e) {
      // Emit null-game to the room with the game id. This is normally not applicable because
      // otherwise players would see unwanted data
      this.emitGameToRoom(game.id, game.id);
      this.removeRoom(game.id);
    }
    this.emitOpenGames();
  }

  /**
   * Emits the game object to all clients in the adapter with the gameId.
   * This functions includes transforming the game object so that the players
   * do not receive unwanted data.
   * @param gameId the id of the game
   * @protected
   */
  protected emitGame(gameId: string): void {
    let g: Game<Player>;
    try {
      g = this.gameService.get(gameId);
    } catch (e) {
      // Ignore and return null
    }
    g &&
      this.logger.debug(`Emit game to all players in game ${gameId}`, null, g);
    this.emitGameToPlayers(g, g?.players);
  }

  /**
   * Emits the given game to the connected players in the namespace
   * of this gateway.
   * @param g the game. If null, the function does nothing
   * @param players
   * @protected
   */
  protected emitGameToPlayers(g: Game<Player>, players: Player[]): void {
    if (!g || !players) {
      return;
    }
    players
      .filter((p) => p.isConnected)
      .forEach((player) => {
        this.sessionService
          // TODO FIXME Fix namespace name
          .getClientIdsOfPlayerInNamespace(player.id, this.namespace)
          ?.forEach((clientId) => {
            this.logger.debug(`Emit game to client with id ${clientId}`);
            const transformedGame = this.transformGame(cloneDeep(g), player.id);
            this.publishEventToRoom(
              clientId,
              GetGameEvent.id,
              new GetGameEvent(transformedGame),
            );
          });
      });
  }

  /**
   * Emits the game object to the client with the given id.
   * If you want to emit the game to all players in a game, use emitGame.
   * @param gameId the id of the game
   * @param roomId the id of the room (game or client) to send the game to.
   *       If this is the id of a player, attributes in the game that are not supposed
   *       to be sent to the player are removed with transformGame
   * @protected
   */
  protected emitGameToRoom(gameId: string, roomId: string): void {
    let g: Game<Player>;
    try {
      g = this.gameService.get(gameId);
    } catch (e) {
      // Ignore and return null
    }
    g && this.logger.debug('Emit game to room ' + roomId, null, g);

    const playerId = this.sessionService.getPlayerId(roomId);
    const roomIsPlayerInExistingGame = playerId && g;
    if (roomIsPlayerInExistingGame) {
      const transformedGame = this.transformGame(cloneDeep(g), playerId);
      this.publishEventToRoom(
        roomId,
        GetGameEvent.id,
        new GetGameEvent(transformedGame),
      );
      return;
    }
    this.publishEventToRoom(roomId, GetGameEvent.id, new GetGameEvent(g));
  }

  /**
   * Allows transforming the game object that is emitted from the server to all clients
   * in a game. Useful when omitting or changing specific attributes or values that
   * a player is not allowed to see. If you want to exclude or expose attributes to all
   * clients, use @Exclude and @Expose (See https://github.com/typestack/class-transformer)
   * This does not modify the original game object.
   *
   * @param game A deep copy of the original game
   * @param playerId The id of the player that the game will be emitted to
   * @protected
   */
  protected transformGame(game: Game<Player>, playerId: string): Game<Player> {
    return game;
  }

  /**
   * Emits the games that are available to the respective players.
   * Different players see different game (e.g. a player that was
   * in a game previously can re-join a game, while other players
   * do not see this game)
   * @protected
   */
  protected emitOpenGames(): void {
    this.logger.debug('Emit open games');
    this.server
      .in(Rooms.openGamesRoom)
      .fetchSockets()
      .then((clientIds) => {
        clientIds.forEach((clientId) => {
          const playerId = this.sessionService.getPlayerId(clientId.id);
          const openGames = this.gameService.getAllOpen(playerId);
          // Transformation is not necessary here because the GetAllOpenGamesEvent excludes all sensitive data
          this.publishEventToRoom(
            clientId.id,
            GetAllOpenGamesEvent.id,
            new GetAllOpenGamesEvent(openGames),
          );
        });
      })
      .catch((error) => {
        this.logger.error(
          'Error emitting all open games',
          error.stack,
          null,
          error,
        );
      });
  }

  /**
   * Emits the event and data to the given room.
   * Uses the class transformer to exclude/expose annotated properties.
   * @param roomId
   * @param event
   * @param data
   * @protected
   */
  protected publishEventToRoom<T extends UpgsEvent>(
    roomId: string,
    event: string,
    data?: T,
  ): void {
    this.logger.debug(`Emit ${event} to room ${roomId}`);
    this.server.in(roomId).emit(event, classToPlain(data));
  }

  private removeRoom(roomId: string): void {
    this.logger.debug(`Remove room`, null, roomId);
    this.server
      .in(roomId)
      .fetchSockets()
      .then((socketIds) => {
        socketIds.forEach((socketId) => {
          this.server.sockets.sockets.get[socketId.id].leave(roomId);
          this.sessionService.removeClientId(socketId.id);
        });
      })
      .catch((error) => {
        this.logger.error('Remove room error', error.stack, null, error);
      });
  }
}
