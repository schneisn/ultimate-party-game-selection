import { GameEventModel } from './model/game.event-model';
import { PlayerEventModel } from './model/player.event-model';
import { UpgsEvent } from './upgs.event';

export class JoinGameEvent<
  G extends GameEventModel = GameEventModel
> extends UpgsEvent {
  static readonly id = 'JoinGame';
  game: G;
  player: PlayerEventModel;

  constructor(game: G, player: PlayerEventModel) {
    super();
    this.game = game;
    this.player = player;
  }
}
