import { UpgsEvent } from './upgs.event';

export class JoinRoomEvent extends UpgsEvent {
  static readonly id = 'JoinRoom';
}
