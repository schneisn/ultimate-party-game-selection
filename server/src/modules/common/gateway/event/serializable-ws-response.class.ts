import { WsResponse } from '@nestjs/websockets';
import { UpgsEvent } from './upgs.event';

/**
 * This class is required in order for the ClassSerializerInterceptor of nestjs to work.
 * Instead of returning a plain object of type WsResponse, which is an interface,
 * an instance of this class is returned.
 * The ClassSerializerInterceptor requires a class to be returned in order to process
 * the annotations @Exclude, @Expose, ...
 *
 * See https://github.com/nestjs/nest/issues/3417
 */
export class SerializableWsResponse<T extends UpgsEvent>
  implements WsResponse<T> {
  data: T;
  event: string;

  constructor(event: string, data: T) {
    this.data = data;
    this.event = event;
  }
}
