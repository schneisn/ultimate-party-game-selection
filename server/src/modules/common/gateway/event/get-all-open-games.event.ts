import { UpgsEvent } from './upgs.event';
import { OpenGameEventModel } from './model/open-game.event-model';
import { Type } from 'class-transformer';

export class GetAllOpenGamesEvent extends UpgsEvent {
  static readonly id = 'GetAllOpenGames';

  @Type(() => OpenGameEventModel)
  games: OpenGameEventModel[];

  constructor(games: OpenGameEventModel[]) {
    super();
    this.games = games;
  }
}
