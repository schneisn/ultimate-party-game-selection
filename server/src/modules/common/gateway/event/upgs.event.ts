/**
 * The object that is emitted from the server
 * by socket.emit(event: string, ...args) or
 * as data when the server responds to a client request.
 */
export abstract class UpgsEvent {
  /**
   * The identifier of the event
   */
  static readonly id: string;
}
