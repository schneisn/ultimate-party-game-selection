import { GameRequest } from './game.request';
import { Player } from '../../../player/domain/player.entity';
import { Game } from '../../../game/domain/game.entity';

export class StartNextRoundRequest<G extends Game<Player>> extends GameRequest<
  G
> {}
