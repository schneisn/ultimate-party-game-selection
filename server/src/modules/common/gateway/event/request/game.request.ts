import { ValidateNested } from 'class-validator';
import { IdRequestModel } from './model/id.request-model';
import { Game } from '../../../game/domain/game.entity';
import { Player } from '../../../player/domain/player.entity';

/**
 * Contains the attributes that are sent by all requests
 * in a game.
 */
export abstract class GameRequest<G extends Game<Player>> {
  /**
   * The game that the event addresses
   */
  @ValidateNested()
  game: G;

  /**
   * The player that sent the request
   */
  @ValidateNested()
  player: IdRequestModel;
}
