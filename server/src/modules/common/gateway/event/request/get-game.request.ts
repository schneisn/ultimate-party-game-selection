import { GameRequest } from './game.request';
import { Game } from '../../../game/domain/game.entity';
import { Player } from '../../../player/domain/player.entity';

export class GetGameRequest extends GameRequest<Game<Player>> {}
