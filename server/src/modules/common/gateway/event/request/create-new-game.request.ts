import { ValidateNested } from 'class-validator';
import { PlayerIdNameRequestModel } from './model/player-id-name.request-model';

export class CreateNewGameRequest {
  @ValidateNested()
  player: PlayerIdNameRequestModel;
}
