import { IsOptional, ValidateNested } from 'class-validator';
import { PlayerIdNameRequestModel } from './model/player-id-name.request-model';

export class GetAllOpenGamesRequest {
  /**
   * This player can be the old player object that was stored
   * in the local storage by the client with a previous client id.
   * This id can be used to check for games that still contain the
   * old player with the old id. The new client that sent this request
   * has a different id, since it is another socket connection.
   */
  @IsOptional()
  @ValidateNested()
  player: PlayerIdNameRequestModel;
}
