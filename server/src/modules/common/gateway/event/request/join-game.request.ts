import { ValidateNested } from 'class-validator';
import { PlayerIdNameRequestModel } from './model/player-id-name.request-model';
import { GameRequest } from './game.request';
import { Game } from '../../../game/domain/game.entity';
import { Player } from '../../../player/domain/player.entity';

export class JoinGameRequest extends GameRequest<Game<Player>> {
  @ValidateNested()
  player: PlayerIdNameRequestModel;
}
