import { IsNotEmpty } from 'class-validator';

export class LeaveRoomRequest {
  @IsNotEmpty()
  room: string;
}
