import { IsString } from 'class-validator';

/**
 * An object that only contains an id
 */
export class IdRequestModel {
  @IsString()
  id: string;
}
