import { IsString } from 'class-validator';
import { IdRequestModel } from './id.request-model';

export class PlayerIdNameRequestModel extends IdRequestModel {
  @IsString()
  name: string;
}
