import { IsNotEmpty } from 'class-validator';
import { GameRequest } from './game.request';
import { Game } from '../../../game/domain/game.entity';
import { Player } from '../../../player/domain/player.entity';

export class KickPlayerRequest extends GameRequest<Game<Player>> {
  @IsNotEmpty()
  playerId: string;
}
