import { IsBoolean } from 'class-validator';
import { GameRequest } from './game.request';
import { Player } from '../../../player/domain/player.entity';
import { Game } from '../../../game/domain/game.entity';

export class SetPlayerReadyStateRequest<
  G extends Game<Player>
> extends GameRequest<G> {
  @IsBoolean()
  isReady: boolean;
}
