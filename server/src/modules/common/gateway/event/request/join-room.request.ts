import { IsNotEmpty } from 'class-validator';

export class JoinRoomRequest {
  @IsNotEmpty()
  room: string;
}
