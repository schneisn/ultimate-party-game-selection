import { UpgsEvent } from './upgs.event';

export class StartNextRoundEvent extends UpgsEvent {
  static readonly id = 'StartNextRound';
}
