import { UpgsEvent } from './upgs.event';

export class SetPlayerReadyStateEvent extends UpgsEvent {
  static readonly id = 'SetPlayerReadyState';
}
