import { PlayerEventModel } from './model/player.event-model';
import { UpgsEvent } from './upgs.event';

/**
 * Another player has disconnected
 */
export class DisconnectEvent extends UpgsEvent {
  static readonly id = 'Disconnect';
  player: PlayerEventModel;

  constructor(player: PlayerEventModel) {
    super();
    this.player = player;
  }
}
