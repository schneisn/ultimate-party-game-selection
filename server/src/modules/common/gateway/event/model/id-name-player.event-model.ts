import { Exclude, Expose } from 'class-transformer';

/**
 * The dto of a player with only id and name.
 */
@Exclude()
export class IdNamePlayerEventModel {
  @Expose()
  id: string;

  @Expose()
  name: string;
}
