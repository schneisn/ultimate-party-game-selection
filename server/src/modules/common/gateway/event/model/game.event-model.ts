import { PlayerEventModel } from './player.event-model';

export class GameEventModel<P extends PlayerEventModel = PlayerEventModel> {
  readonly id: string;
  players: P[];
}
