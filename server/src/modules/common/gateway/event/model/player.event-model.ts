/**
 * A common model for the player object that is
 * sent to the client.
 */
export class PlayerEventModel {
  id: string;
  name: string;
  isConnected: boolean;
  isComputer: boolean;
}
