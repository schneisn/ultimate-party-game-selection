import { Exclude, Expose, Type } from 'class-transformer';
import { IdNamePlayerEventModel } from './id-name-player.event-model';

/**
 * The dto of an open game.
 * Only id and players are sent to client.
 */
@Exclude()
export class OpenGameEventModel {
  @Expose()
  id: string;

  @Expose()
  @Type(() => IdNamePlayerEventModel)
  players: IdNamePlayerEventModel[];

  /**
   * The gamemaster is needed for a werewolf game
   * TODO Rewrite this because it is only applicable to ww
   */
  @Expose()
  gamemaster?: IdNamePlayerEventModel;
}
