import { GameEventModel } from './model/game.event-model';
import { PlayerEventModel } from './model/player.event-model';
import { UpgsEvent } from './upgs.event';

export class CreateNewGameEvent extends UpgsEvent {
  static readonly id = 'CreateNewGame';
  game: GameEventModel;
  player: PlayerEventModel;

  constructor(game: GameEventModel, player: PlayerEventModel) {
    super();
    this.game = game;
    this.player = player;
  }
}
