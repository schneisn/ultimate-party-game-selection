import { UpgsEvent } from './upgs.event';

export class UpdateSettingsEvent extends UpgsEvent {
  static readonly id = 'UpdateSettings';
}
