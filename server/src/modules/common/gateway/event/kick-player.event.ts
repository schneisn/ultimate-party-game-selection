import { UpgsEvent } from './upgs.event';

export class KickPlayerEvent extends UpgsEvent {
  static readonly id = 'KickPlayer';
}
