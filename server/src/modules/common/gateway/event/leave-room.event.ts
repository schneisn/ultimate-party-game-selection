import { UpgsEvent } from './upgs.event';

export class LeaveRoomEvent extends UpgsEvent {
  static readonly id = 'LeaveRoom';
}
