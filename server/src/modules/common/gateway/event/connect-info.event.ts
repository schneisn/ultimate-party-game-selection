import { UpgsEvent } from './upgs.event';
import { IdNamePlayerEventModel } from './model/id-name-player.event-model';

export class ConnectInfoEvent extends UpgsEvent {
  static readonly id = 'ConnectInfo';
  player: IdNamePlayerEventModel;

  constructor(player: IdNamePlayerEventModel) {
    super();
    this.player = player;
  }
}
