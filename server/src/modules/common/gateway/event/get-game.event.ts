import { GameEventModel } from './model/game.event-model';
import { UpgsEvent } from './upgs.event';

export class GetGameEvent<
  G extends GameEventModel = GameEventModel
> extends UpgsEvent {
  static readonly id = 'GetGame';
  game: G;

  constructor(game: G) {
    super();
    this.game = game;
  }
}
