import { UpgsEvent } from './upgs.event';

export class LeaveGameEvent extends UpgsEvent {
  static readonly id = 'LeaveGame';
}
