/**
 * The error that the user receives
 */
export class ClientError {
  status: string;
  id: string;
  message: string;
  cause: any;

  constructor(status: string, id: string, message: string, cause: any) {
    this.status = status;
    this.id = id;
    this.message = message;
    this.cause = cause;
  }
}
