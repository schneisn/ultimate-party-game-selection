import { Player } from '../../player/domain/player.entity';

/**
 * The common game class that with properties and functionality that
 * all games share.
 *
 * When a property requires a getter or setter, it can be done as shown in
 * https://www.typescriptlang.org/docs/handbook/classes.html#accessors
 */
export abstract class Game<P extends Player> {
  readonly id: string;
  players: P[] = [];
  scheduledRemovalDate: Date;

  constructor() {
    this.id = Math.floor(Math.random() * Math.pow(10, 10)).toString();
  }
}
