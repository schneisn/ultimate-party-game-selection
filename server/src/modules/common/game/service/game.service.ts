import { Player } from '../../player/domain/player.entity';
import { IGameRepository } from '../repository/game.repository.interface';
import { NewGameDto } from './dto/new-game.dto';
import { ICommonGameService } from './game.service.interface';
import { Game } from '../domain/game.entity';
import { Injectable, Logger } from '@nestjs/common';

/**
 * The game service handles use cases regarding games.
 * The subclasses have to have the injectable annotation (probably because this class has a constructor)
 */
@Injectable()
export abstract class CommonGameService<G extends Game<P>, P extends Player>
  implements ICommonGameService<G, P> {
  private readonly logger = new Logger(CommonGameService.name);

  protected constructor(readonly gameRepository: IGameRepository<G>) {}

  abstract newGame(playerName: string): NewGameDto;

  get(gameId: string): G {
    return this.gameRepository.find(gameId);
  }

  getAll(): G[] {
    return this.gameRepository.findAll();
  }

  getAllOpen(playerId?: string): G[] {
    return this.getAll();
  }

  scheduleGameForRemoval(gameId: string): void {
    this.logger.log(`Scheduling game ${gameId} for removal`);
    const game = this.gameRepository.find(gameId);
    game.scheduledRemovalDate = new Date(Date.now() + 600000);
    this.gameRepository.save(game);
  }

  removeGame(gameId: string): void {
    this.gameRepository.delete(gameId);
  }

  getGameOfPlayer(playerId: string): G {
    return this.getAll().find((game) =>
      game.players.find((player) => player.id === playerId),
    );
  }

  removeGames(): void {
    const games = this.gameRepository.findAll();
    games
      .filter((game) => !!game.scheduledRemovalDate)
      .forEach((game) => {
        if (game.scheduledRemovalDate.getTime() < Date.now()) {
          this.gameRepository.delete(game.id);
        }
      });
  }
}
