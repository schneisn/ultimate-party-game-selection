import { Player } from '../../player/domain/player.entity';
import { NewGameDto } from './dto/new-game.dto';
import { Game } from '../domain/game.entity';

/**
 * An interface that all game services share.
 *
 * This interface is used by the common gateway.
 * The common gateway shares all common event-endpoints.
 */
export abstract class ICommonGameService<G extends Game<P>, P extends Player> {
  /**
   * Creates a new game and adds the player with the given id and name
   *
   * @return The new game and the player that created the game
   */
  abstract newGame(playerName: string): NewGameDto;

  /**
   * Gets the game with the given id
   * @param gameId The id of the game to get
   */
  abstract get(gameId: string): G;

  /**
   * Get all games
   */
  abstract getAll(): G[];

  /**
   * Get all games that can be joined by the player with
   * the given id.
   * By default, it gets all games.
   */
  abstract getAllOpen(playerId?: string): G[];

  /**
   * Gets the first game of a player
   * @param playerId
   */
  abstract getGameOfPlayer(playerId: string): G;

  /**
   * Schedules a game for removal.
   * @param gameId
   */
  abstract scheduleGameForRemoval(gameId: string): void;

  /**
   * Remove the game with the given id
   * @param gameId
   */
  abstract removeGame(gameId: string): void;

  /**
   * Removes games that are schedules for removal.
   */
  abstract removeGames(): void;
}
