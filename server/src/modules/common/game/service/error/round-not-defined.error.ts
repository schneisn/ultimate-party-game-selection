import { UpgsError } from '../../../../../common/error/upgs.error';
import { Player } from '../../../player/domain/player.entity';
import { Game } from '../../domain/game.entity';

export class RoundNotDefinedError extends UpgsError {
  constructor(game: Game<Player>) {
    super(
      'CURRENT_ROUND_NOT_DEFINED',
      'Game does not have a round',
      'The game does not have a round',
      game,
    );
  }
}
