import { Player } from '../../../player/domain/player.entity';
import { Game } from '../../domain/game.entity';

export class NewGameDto {
  game: Game<Player>;
  player: Player;

  constructor(game: Game<Player>, player: Player) {
    this.game = game;
    this.player = player;
  }
}
