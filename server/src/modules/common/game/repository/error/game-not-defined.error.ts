import { UpgsError } from '../../../../../common/error/upgs.error';

export class GameNotDefinedError extends UpgsError {
  constructor(gameId?: string) {
    super(
      'GAME_NOT_DEFINED',
      'Game is null or no game with the given id',
      'The game does not exist',
      gameId,
    );
  }
}
