import { Game } from './model/game.model';
import { IRepository } from '../../repository/repository.interface';

/**
 * Provides saving, retrieving and deleting of games
 */
export abstract class IGameRepository<
  G extends Game = Game
> extends IRepository<G> {}
