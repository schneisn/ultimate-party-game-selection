import { Player } from './player.model';

export class Game<P extends Player = Player> {
  id: string;
  players: P[];
}
