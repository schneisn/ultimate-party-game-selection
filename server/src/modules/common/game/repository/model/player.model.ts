export class Player {
  id: string;
  name: string;
  isConnected: boolean;
  isComputer: boolean;
}
