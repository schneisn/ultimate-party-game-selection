import { Injectable } from '@nestjs/common';
import { GameNotDefinedError } from './error/game-not-defined.error';
import { IGameRepository } from './game.repository.interface';
import { Game } from './model/game.model';
import { LoggerService } from '../../../logger/logger.service';
import { cloneDeep } from 'lodash';

/**
 * An in-memory implementation of the game repository interface
 */
@Injectable()
export class GameRepository<G extends Game> implements IGameRepository<G> {
  /**
   * Map of game-id to game
   * @private
   */
  private games: Map<string, G> = new Map();

  constructor(private readonly logger: LoggerService) {
    this.logger.setContext(GameRepository.name);
  }

  save(game: G): G {
    this.games.set(game.id, game);
    this.logger.debug(`Saved game ${game.id}`);
    return cloneDeep(game);
  }

  find(gameId: string): G {
    const game = this.games.get(gameId);
    if (!game) {
      throw new GameNotDefinedError(gameId);
    }
    this.logger.debug(`Found game ${gameId}`);
    return cloneDeep(game);
  }

  findAll(): G[] {
    const allGames = Array.from(this.games.values());
    this.logger.debug(`Found all games`);
    return cloneDeep(allGames);
  }

  delete(gameId: string): void {
    this.games.delete(gameId);
    this.logger.debug(`Deleted game ${gameId}`);
  }
}
