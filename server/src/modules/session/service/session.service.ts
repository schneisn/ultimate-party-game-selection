import { Injectable } from '@nestjs/common';
import { LoggerService } from '../../logger/logger.service';
import { ISessionService } from './session.service.interface';

@Injectable()
export class SessionService implements ISessionService {
  /**
   * Map of player id to active client ids.
   * Client ids have the format 'namespace'/'client id'.
   *
   * Every new tab in the browser is a new client id, but
   * they might have the same player id since they share
   * a common localStorage.
   * @protected
   */
  protected playerIdToClientIds: Map<string, string[]>;

  constructor(readonly logger: LoggerService) {
    this.logger.setContext(SessionService.name);
    this.playerIdToClientIds = new Map<string, string[]>();
  }

  getPlayerId(clientId: string): string {
    let playerId: string = null;
    this.playerIdToClientIds.forEach((cIds: string[], pId: string) => {
      // Sometimes, the clientIds are in the form 'namespace'/'id' and sometimes
      // only 'id'. Therefore, check only if the stored id ends with the given clientId.
      const isPlayerOfClientId = cIds.some((cId) => cId.endsWith(clientId));
      if (isPlayerOfClientId) {
        playerId = pId;
      }
    });
    return playerId;
  }

  getClientIds(playerId: string): string[] {
    return this.playerIdToClientIds.get(playerId);
  }

  getClientIdsOfPlayerInNamespace(
    playerId: string,
    namespace: string,
  ): string[] {
    return this.playerIdToClientIds
      .get(playerId)
      ?.filter((cId) => cId.startsWith(namespace));
  }

  mapClientIdToPlayerId(clientId: string, playerId: string) {
    if (!clientId || !playerId) {
      this.logger.warn('No client id or player id provided', null, {
        clientId,
        playerId,
      });
      return;
    }
    // If it already exists, skip
    if (this.getPlayerId(clientId) === playerId) {
      this.logger.warn(
        `Client id ${clientId} already mapped to player id ${playerId}`,
      );
      return;
    }
    // Remove old player ids that belong to this client (e.g. from previous games).
    // Otherwise, it is not known which player id is the current one
    this.removeClientId(clientId);
    const activeClientIds = this.playerIdToClientIds.get(playerId) || [];
    activeClientIds.push(clientId);
    this.playerIdToClientIds.set(playerId, activeClientIds);
    this.logger.debug(
      `Client id ${clientId} mapped to player id ${playerId}`,
      null,
      this.playerIdToClientIds,
    );
  }

  removePlayerId(playerId: string): void {
    this.playerIdToClientIds.delete(playerId);
    this.logger.debug(
      `Player id ${playerId} and all client ids deleted`,
      null,
      this.playerIdToClientIds,
    );
  }

  removeClientId(clientId: string): void {
    const playerId = this.getPlayerId(clientId);
    if (!playerId) {
      return;
    }
    const clientIds = this.getClientIds(playerId);
    const clientIdIndex = clientIds?.findIndex((cId) => cId === clientId);
    clientIds.splice(clientIdIndex, 1);
    this.logger.debug(
      `Client id ${clientId} -> Player id ${playerId} removed`,
      null,
      this.playerIdToClientIds,
    );
    if (clientIds.length < 1) {
      this.removePlayerId(playerId);
    }
  }
}
