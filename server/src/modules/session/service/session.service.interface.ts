/**
 * The SessionService tracks which client id belongs to which player.
 * One player can have multiple active client sessions,
 * but one client session can only belong to one player.
 */
export abstract class ISessionService {
  /**
   * Gets the corresponding player id to the given client id
   * @param clientId The client id can be of the form 'namespace/id' or only 'id'.
   *        The namespace is not considered.
   */
  abstract getPlayerId(clientId: string): string;

  /**
   * Gets the websocket client ids that belong to a player.
   * This is normally only one client id (= one open connection/game)
   * @param playerId
   */
  abstract getClientIds(playerId: string): string[];

  /**
   * Gets the websocket client ids that belong to the player
   * which are in the given namespace.
   * @param playerId
   * @param namespace
   */
  abstract getClientIdsOfPlayerInNamespace(
    playerId: string,
    namespace: string,
  ): string[];

  /**
   * Maps the client id to the player id. For consecutive requests, the
   * server knows to which player the client id belongs.
   * Removes existing associations between the client id and other player ids.
   * Otherwise, if a client with an existing player id creates a new game and
   * the new association between client id and new player id is stored,
   * one client id would be associated with multiple player ids.
   * @param clientId
   * @param playerId
   */
  abstract mapClientIdToPlayerId(clientId: string, playerId: string): void;

  /**
   * Removes the player id and all associated client ids
   * @param playerId
   */
  abstract removePlayerId(playerId: string): void;

  /**
   * Removes the given clientId from the first player id it belongs to.
   * If the client id is the last client id of the player, the player id
   * is also deleted.
   * If no associated player id is found, nothing is removed.
   * @param clientId
   */
  abstract removeClientId(clientId: string): void;
}
