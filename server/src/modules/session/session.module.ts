import { Global, Module } from '@nestjs/common';
import { ISessionService } from './service/session.service.interface';
import { SessionService } from './service/session.service';

@Global()
@Module({
  providers: [
    {
      provide: ISessionService,
      useClass: SessionService,
    },
  ],
  exports: [ISessionService],
})
export class SessionModule {}
