import {
  ConsoleLogger,
  Inject,
  Injectable,
  Scope,
} from '@nestjs/common';
import { ConfigService } from '../config/config.service';
import * as util from 'util';

/**
 * A custom logger.
 *
 * It uses the default nest logger statically to log to the console.
 *
 * The first arguments of the log/error/warn... methods have to be the
 * parameters of the methods in the LoggerService interface
 */
@Injectable({ scope: Scope.TRANSIENT })
export class LoggerService extends ConsoleLogger {
  constructor(@Inject('ConfigService') private configService: ConfigService) {
    // TODO Setting isTimestampEnabled to true appends "true" to all .log messages
    super(null, { timestamp: true });
  }

  /**
   * Logs a message. The data is only logged in dev mode
   * @param message
   * @param context
   * @param data
   */
  log<T>(message: any, context?: string, data?: T): void {
    super.log(
      this.getMessageString(message, data),
      this.getContextString(context, 'LOG'),
    );
  }

  /**
   * Use error when an error or unwanted behavior occurred
   * @param message
   * @param trace
   * @param context
   * @param data
   */
  error<T>(message: any, trace?: string, context?: string, data?: T): void {
    super.error(
      LoggerService.concatMessageAndDate(message, data),
      trace,
      this.getContextString(context, 'ERROR'),
    );
  }

  /**
   * Use debug when messages are only supposed to be logged in dev mode
   * @param message
   * @param context
   * @param data
   */
  debug<T>(message: any, context?: string, data?: T): void {
    if (!this.configService?.isDevelopment()) {
      return;
    }
    super.debug(
      this.getMessageString(message, data),
      this.getContextString(context, 'DEBUG'),
    );
  }

  /**
   * Use warn when warning the developer about possible risks
   * @param message
   * @param context
   * @param data
   */
  warn<T>(message: any, context?: string, data?: T) {
    super.warn(
      this.getMessageString(message, data),
      this.getContextString(context, 'WARN'),
    );
  }

  verbose(message: any, context?: string) {
    super.verbose(message, context);
  }

  /**
   * Creates a string from the message and the data.
   * Only adds the data in dev mode.
   * Do not log anything in this method via the custom logger!
   * @param message
   * @param data
   * @private
   */
  private getMessageString<T>(message: any, data?: T): string {
    if (this.configService?.isDevelopment()) {
      return `${message}`;
    }
    return LoggerService.concatMessageAndDate(message, data);
  }

  private static concatMessageAndDate<T>(message: any, data?: T): string {
    return `${message}${data ? ' ' + util.inspect(data, false, 2) : ''}`;
  }

  private getContextString(
    messageContext: string,
    defaultContext?: string,
  ): string {
    const msgCtx = messageContext ? `${messageContext}` : '';
    const messageContextStringWithLoggerContext = this.context
      ? `${this.context}${msgCtx ? ' ' : ''}${msgCtx}`
      : msgCtx;
    return messageContextStringWithLoggerContext.length < 1
      ? defaultContext
      : messageContextStringWithLoggerContext;
  }
}
