import { ICommonGameService } from '../../common/game/service/game.service.interface';
import { CAPlayer } from '../domain/player.entity';
import { CAGame } from '../domain/game.entity';
import { SettingsDto } from './dto/settings.dto';

export abstract class ICAGameService extends ICommonGameService<
  CAGame,
  CAPlayer
> {
  abstract setStatementCategories(game: CAGame, categories: string[]): void;

  abstract setCriticInput(
    game: CAGame,
    criticId: string,
    winnerId: string,
  ): void;

  abstract updateSettings(game: CAGame, settings: SettingsDto): void;
}
