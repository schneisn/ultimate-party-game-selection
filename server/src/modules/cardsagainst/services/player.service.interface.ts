import { ICommonPlayerService } from '../../common/player/service/player.service.interface';
import { CAGame } from '../domain/game.entity';
import { CAPlayer } from '../domain/player.entity';

export abstract class ICAPlayerService extends ICommonPlayerService<CAPlayer> {
  abstract allPlayersReady(game: CAGame): boolean;

  abstract setPlayerReadyState(
    game: CAGame,
    playerId: string,
    isReady: boolean,
  ): void;

  /**
   * Removes all hand cards of the given player and fill them again.
   * @param game
   * @param playerId
   */
  abstract refillHandCardsOfPlayer(game: CAGame, playerId: string): void;
}
