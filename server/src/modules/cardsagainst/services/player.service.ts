import { CommonPlayerService } from '../../common/player/service/player.service';
import { CAGame } from '../domain/game.entity';
import { ICAStatementService } from './statement.service.interface';
import { CAPlayer } from '../domain/player.entity';
import { ICAPlayerService } from './player.service.interface';
import { PlayerNotDefinedError } from '../../common/player/service/error/player-not-defined.error';
import { ICARoundService } from './round.service.interface';
import { ICAAnswerService } from './answer.service.interface';
import { ICAGameService } from './game.service.interface';
import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { CARound } from '../domain/round.entity';
import { RoundNotDefinedError } from '../../common/game/service/error/round-not-defined.error';
import { IGameRepository } from '../../common/game/repository/game.repository.interface';

@Injectable()
export class CAPlayerService extends CommonPlayerService<CAGame, CAPlayer>
  implements ICAPlayerService {
  constructor(
    @Inject(forwardRef(() => ICARoundService))
    private readonly roundService: ICARoundService,
    private readonly statementService: ICAStatementService,
    private readonly answerService: ICAAnswerService,
    private readonly gameService: ICAGameService,
    readonly gameRepository: IGameRepository,
  ) {
    super(gameRepository);
  }

  addPlayer(game: CAGame, playerId: string, playerName: string): CAPlayer {
    playerName = this.addNrToPlayerNameIfExists(game, playerId, playerName);
    const existingPlayer = this.replaceExistingPlayer(
      game,
      playerId,
      playerName,
    );
    const currentRound = this.roundService.getCurrentRound(game);
    const enoughCardsLeft =
      this.statementService.getTotalAmountAnswers(game.statementCategories) >
      this.statementService.getNrOfHandCardsAndAnswersInGame(game);
    if (existingPlayer) {
      if (
        currentRound &&
        !this.answerExists(currentRound) &&
        !this.playerAnswered(currentRound, playerId)
      ) {
        if (enoughCardsLeft) {
          this.roundService.fillHandCardsOfPlayer(game, existingPlayer);
        }
        if (playerId !== currentRound.criticId) {
          this.answerService.addAnswer(game, existingPlayer.id, []);
        }
      }
      this.gameRepository.save(game);
      return existingPlayer;
    }
    const newPlayer = new CAPlayer(null, playerName);
    game.players.push(newPlayer);

    const gameHasStartedAndEnoughCardsAreLeft = currentRound && enoughCardsLeft;
    if (gameHasStartedAndEnoughCardsAreLeft) {
      this.roundService.fillHandCardsOfPlayer(game, newPlayer);
      if (!this.answerExists(currentRound)) {
        this.answerService.addAnswer(game, newPlayer.id, []);
      }
    }
    this.gameRepository.save(game);
    return newPlayer;
  }

  setPlayerReadyState(game: CAGame, playerId: string, isReady: boolean): void {
    const player = game.players.find((p) => p.id === playerId);
    if (!player) {
      throw new PlayerNotDefinedError(playerId);
    }
    player.isReady = isReady;
    if (this.allPlayersReady(game)) {
      this.roundService.addRound(game);
    }
    this.gameRepository.save(game);
  }

  allPlayersReady(game: CAGame): boolean {
    return game.players
      .filter((player) => player.isConnected)
      .every((player) => player.isReady);
  }

  disconnectPlayer(game: CAGame, playerId: string): boolean {
    const player = this.getPlayer(game, playerId);
    if (!player) {
      throw new PlayerNotDefinedError(playerId);
    }
    const round = this.roundService.getCurrentRound(game);
    player.isConnected = false;
    player.isReady = false;
    if (!round) {
      super.disconnectPlayer(game, playerId);
    }
    if (this.noPlayersAreConnected(game)) {
      this.gameService.scheduleGameForRemoval(game.id);
      return;
    }
    const onePlayerInLobby = this.atMostOnePlayerConnected(game) && !round;
    if (this.allPlayersReady(game) && !onePlayerInLobby) {
      this.roundService.addRound(game);
    }

    const inScoreboard = round?.answers?.some((answer) => answer.hasWon);
    if (!inScoreboard) {
      // Do not remove answer when all players have answered and critic is already choosing
      // or scoreboard is shown
      if (
        round &&
        !this.allPlayersAnswered(round) &&
        !this.playerAnswered(round, playerId)
      ) {
        this.answerService.removeAnswer(round, playerId);
      }
      const playerIsCritic = round?.criticId === player.id;
      if (playerIsCritic) {
        this.changeCritic(game, round);
      }
    }
    this.gameRepository.save(game);
  }

  refillHandCardsOfPlayer(game: CAGame, playerId: string): void {
    const player = this.getPlayer(game, playerId);
    if (!player) {
      throw new PlayerNotDefinedError(playerId);
    }
    const currentRound = this.roundService.getCurrentRound(game);
    if (!currentRound) {
      throw new RoundNotDefinedError(game);
    }
    player.handCards = [];
    player.refilledCardsInRound = currentRound.number;
    this.roundService.fillHandCardsOfPlayer(game, player);
    this.gameRepository.save(game);
  }

  private changeCritic(game: CAGame, round: CARound): void {
    round.criticId = this.roundService.chooseCritic(game);
    const newCriticAnswers = this.answerService.getAnswer(
      round,
      round.criticId,
    );
    if (newCriticAnswers) {
      // Re-add hand-cards to player
      const answers = newCriticAnswers.answers;
      const critic = this.getPlayer(game, round.criticId);
      answers.forEach((answer) =>
        this.roundService.addHandCard(critic, answer),
      );
      this.answerService.removeAnswer(round, round.criticId);
    }
    this.gameRepository.save(game);
  }

  private answerExists(round: CARound): boolean {
    return round?.answers?.some((answer) => answer.answers.length >= 1);
  }

  private allPlayersAnswered(round: CARound): boolean {
    return round?.answers?.every((answer) => answer.answers.length > 0);
  }

  private playerAnswered(round: CARound, playerId: string): boolean {
    return round?.answers.some(
      (answer) => answer.playerId === playerId && answer.answers.length > 0,
    );
  }
}
