import { IGameRepository } from '../../common/game/repository/game.repository.interface';
import { NewGameDto } from '../../common/game/service/dto/new-game.dto';
import { CommonGameService } from '../../common/game/service/game.service';
import { ICAAnswerService } from './answer.service.interface';
import { CAPlayer } from '../domain/player.entity';
import { ICAPlayerService } from './player.service.interface';
import { ICARoundService } from './round.service.interface';
import { ICAStatementService } from './statement.service.interface';
import { ICAGameService } from './game.service.interface';
import { CAGame } from '../domain/game.entity';
import { Injectable } from '@nestjs/common';
import { SettingsDto } from './dto/settings.dto';

@Injectable()
export class CAGameService extends CommonGameService<CAGame, CAPlayer>
  implements ICAGameService {
  constructor(
    readonly playerService: ICAPlayerService,
    readonly statementService: ICAStatementService,
    readonly roundService: ICARoundService,
    readonly answerService: ICAAnswerService,
    readonly gameRepository: IGameRepository<CAGame>,
  ) {
    super(gameRepository);
  }

  newGame(playerName: string): NewGameDto {
    const game = new CAGame();
    game.statementCategories = this.statementService
      .getAllCategories()
      .filter((category) => category.preselected)
      // Only select english packs by default, since the language filter is single-select
      .filter((category) => category.language === 'english')
      .map((category) => category.name);
    game.replaceCardsRound = 1;
    const player = this.playerService.addPlayer(game, null, playerName);
    this.gameRepository.save(game);
    return new NewGameDto(game, player);
  }

  getAllOpen(): CAGame[] {
    return this.gameRepository
      .findAll()
      .filter((game: CAGame) => this.gameHasEnoughCards(game));
  }

  setStatementCategories(game: CAGame, categories: string[]): void {
    game.statementCategories = categories;
    this.gameRepository.save(game);
  }

  setCriticInput(game: CAGame, criticId: string, winnerId: string): void {
    this.roundService
      .getCurrentRound(game)
      .answers.find((answer) => answer.playerId === winnerId).hasWon = true;
    this.gameRepository.save(game);
  }

  updateSettings(game: CAGame, settings: SettingsDto): void {
    game.replaceCardsRound = Math.max(0, settings.replaceCardsRound);
    this.gameRepository.save(game);
  }

  private gameHasEnoughCards(game: CAGame): boolean {
    if (!this.roundService.getCurrentRound(game)) {
      return true;
    }
    const existingCards = this.statementService.getTotalAmountAnswers(
      game.statementCategories,
    );
    return (
      this.getAmountOfPlayedCards(game) +
        this.statementService.getNeededAnswers(
          game,
          this.roundService.getCurrentRound(game),
        ) <
      existingCards
    );
  }

  private getAmountOfPlayedCards(game: CAGame): number {
    let amountCards = this.statementService.getAmountHandCards(game);
    for (const round of game.rounds) {
      amountCards += round.answers.length;
    }
    return amountCards;
  }
}
