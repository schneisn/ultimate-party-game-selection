import { CAGame } from '../domain/game.entity';
import { CAStatementCategory } from '../domain/category.entity';
import { CARound } from '../domain/round.entity';

export abstract class ICAStatementService {
  abstract getUniqueStatement(game: CAGame): string;

  abstract getCategories(categoriesNames: string[]): CAStatementCategory[];

  /**
   * Returns all statement categories
   */
  abstract getAllCategories(): CAStatementCategory[];

  abstract getUniqueAnswer(game: CAGame): string;

  /**
   * Counts all the given answers in all rounds and the amount
   * of hand cards all players currently hold
   * @param game
   */
  abstract getNrOfHandCardsAndAnswersInGame(game: CAGame): number;

  /**
   * Counts the amount of hand cards all players in the game currently hold
   * @param game
   */
  abstract getAmountHandCards(game: CAGame): number;

  /**
   * Checks whether the players have enough hand cards for the given
   * round, which is the next round.
   * @param game
   * @param round
   */
  abstract enoughCardsForNextRound(game: CAGame, round: CARound): boolean;

  /**
   * Gets the nr of answers that are in the given categories
   * @param categories
   */
  abstract getNrAnswers(categories: string[]): number;

  /**
   * Counts how many blanks are in the statement of the round.
   * @param game
   * @param round
   */
  abstract getNeededAnswers(game: CAGame, round: CARound): number;

  /**
   * Counts how many answers exist in total in all given categories
   * @param categories
   */
  abstract getTotalAmountAnswers(categories: string[]): number;
}
