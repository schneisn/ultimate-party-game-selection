import { Injectable } from '@nestjs/common';
import { CAGame } from '../domain/game.entity';
import { ICAStatementService } from './statement.service.interface';
import { CARound } from '../domain/round.entity';
import { ICARoundService } from './round.service.interface';
import { CAPlayer } from '../domain/player.entity';
import { NoStatementCategorySelectedError } from './error/no-statement-category-selected.error';
import { CAAnswer } from '../domain/answer.entity';

@Injectable()
export class CARoundService implements ICARoundService {
  constructor(private statementService: ICAStatementService) {}

  addRound(game: CAGame): CARound {
    if (game.statementCategories.length < 1) {
      throw new NoStatementCategorySelectedError(game);
    }
    game.players.forEach((p) => (p.isReady = false));
    const statement = this.statementService.getUniqueStatement(game);
    const criticId = this.chooseCritic(game);
    const round = new CARound(game.rounds.length, statement, criticId);
    this.fillHandCards(game);
    if (!this.statementService.enoughCardsForNextRound(game, round)) {
      return;
    }
    this.addParticipatingPlayers(game, round);
    game.rounds.push(round);
  }

  addHandCard(player: CAPlayer, card: string): void {
    player.handCards.push(card);
  }

  removeHandCard(player: CAPlayer, card: string) {
    let index = 0;
    for (const cardText of player.handCards) {
      if (cardText === card) {
        player.handCards.splice(index, 1);
        return;
      }
      index++;
    }
  }

  chooseCritic(game: CAGame): string {
    if (game.rounds.length === 0) {
      return game.players[0].id;
    }
    const lastCriticId = this.getCurrentRound(game).criticId;
    let oldCriticIndex = game.players.findIndex((p) => p.id === lastCriticId);
    let newCriticIndex = ++oldCriticIndex % game.players.length;
    const atLeastOnePlayerIsConnected = game.players.some(
      (player) => player.isConnected,
    );
    while (
      !game.players[newCriticIndex].isConnected &&
      atLeastOnePlayerIsConnected
    ) {
      newCriticIndex = ++oldCriticIndex % game.players.length;
    }
    return game.players[newCriticIndex].id;
  }

  getCurrentRound(game: CAGame): CARound {
    if (game.rounds.length < 1) {
      return null;
    }
    return game.rounds[game.rounds.length - 1];
  }

  fillHandCardsOfPlayer(game: CAGame, player: CAPlayer) {
    const MAX_HAND_CARDS = 10;

    while (
      player.handCards.length < MAX_HAND_CARDS &&
      player.handCards.length < this.maxAverageAmountCardsPerPlayer(game)
    ) {
      const answer = this.statementService.getUniqueAnswer(game);
      if (!answer) {
        return;
      }
      this.addHandCard(player, answer);
    }
  }

  private fillHandCards(game: CAGame) {
    game.players.forEach((player) => this.fillHandCardsOfPlayer(game, player));
  }

  /**
   * Returns the average amount of cards a player can have at max
   * @param game
   * @private
   */
  private maxAverageAmountCardsPerPlayer(game: CAGame): number {
    const totalAmountAnswers = this.statementService.getNrAnswers(
      game.statementCategories,
    );
    return Math.floor(totalAmountAnswers / game.players.length);
  }

  private addParticipatingPlayers(game: CAGame, round: CARound): void {
    game.players
      .filter((player) => player.isConnected)
      .filter((player) => player.id !== round.criticId)
      .forEach((player) => round.answers.push(new CAAnswer(player.id, [])));
  }
}
