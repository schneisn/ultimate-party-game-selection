import { CARound } from '../domain/round.entity';
import { CAAnswer } from '../domain/answer.entity';
import { CAGame } from '../domain/game.entity';

export abstract class ICAAnswerService {
  abstract getAnswer(round: CARound, playerId: string): CAAnswer;

  /**
   * Adds an answer to the current round for the player with the given id
   * @param game
   * @param playerId
   * @param answers
   */
  abstract addAnswer(game: CAGame, playerId: string, answers: string[]): void;

  /**
   * Removes the answer of the player with the given id from the given round
   * @param round
   * @param playerId
   */
  abstract removeAnswer(round: CARound, playerId): void;
}
