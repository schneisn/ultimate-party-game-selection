import { Injectable } from '@nestjs/common';
import { IFileService } from '../../file/file.service.interface';
import { StatementCategoryNotDefinedError } from './error/statement-category-not-defined.error';
import { CAGame } from '../domain/game.entity';
import { CAStatementCategory } from '../domain/category.entity';
import { ICAStatementService } from './statement.service.interface';
import { CARound } from '../domain/round.entity';
import { RoundNotDefinedError } from '../../common/game/service/error/round-not-defined.error';
import { CAPlayer } from '../domain/player.entity';

@Injectable()
export class CAStatementService implements ICAStatementService {
  private readonly statementCategories: CAStatementCategory[] = [];

  constructor(readonly fileService: IFileService) {
    this.statementCategories = this.fileService.readJSONFromDir<
      CAStatementCategory
    >('assets/cardsagainst');
  }

  getNrAnswers(categories: string[]): number {
    return this.statementCategories
      .filter((cat) => categories.some((gameCat) => gameCat === cat.name))
      .reduce(
        (previousNrAnswers: number, category: CAStatementCategory) =>
          category.answers.length + previousNrAnswers,
        0,
      );
  }

  enoughCardsForNextRound(game: CAGame, nextRound: CARound): boolean {
    return game.players.every(
      (player) =>
        player.handCards.length >= this.getNeededAnswers(game, nextRound),
    );
  }

  getNeededAnswers(game: CAGame, round: CARound): number {
    if (!round) {
      throw new RoundNotDefinedError(game);
    }
    return round.statement.match(/___/g)
      ? round.statement.match(/___/g).length
      : 0;
  }

  getUniqueStatement(game: CAGame): string {
    let statement = this.chooseRandomStatement(game);
    if (!statement || this.noStatementLeft(game)) {
      return '';
    }
    while (this.gameContainsStatement(game, statement)) {
      statement = this.chooseRandomStatement(game);
    }
    return statement;
  }

  getCategories(categoriesNames: string[]): CAStatementCategory[] {
    return categoriesNames.map((categoryName) => {
      const category = this.statementCategories.find(
        (statementCategory) => statementCategory.name === categoryName,
      );
      if (!category) {
        throw new StatementCategoryNotDefinedError(categoryName);
      }
      return category;
    });
  }

  getAllCategories(): CAStatementCategory[] {
    return this.statementCategories;
  }

  getUniqueAnswer(game: CAGame): string {
    let answer = this.chooseRandomAnswer(game);
    if (!answer || this.noAnswersLeft(game)) {
      return null;
    }
    while (this.gameContainsAnswer(game, answer)) {
      answer = this.chooseRandomAnswer(game);
    }
    return answer;
  }

  getNrOfHandCardsAndAnswersInGame(game: CAGame): number {
    const answers = game.rounds.reduce(
      (nrAnswersInGame: number, round: CARound) =>
        nrAnswersInGame + round.answers?.length,
      0,
    );
    return answers + this.getAmountHandCards(game);
  }

  getAmountHandCards(game: CAGame): number {
    return game.players.reduce(
      (totalNrCards: number, player: CAPlayer) =>
        totalNrCards + player.handCards?.length,
      0,
    );
  }

  getTotalAmountAnswers(categories: string[]): number {
    return this.statementCategories.reduce(
      (totalNrAnswers: number, category: CAStatementCategory) =>
        totalNrAnswers + category.answers.length,
      0,
    );
  }

  private gameContainsStatement(game: CAGame, question: string): boolean {
    return game.rounds?.some((round) => round?.statement === question);
  }

  private gameContainsAnswer(game: CAGame, answer: string): boolean {
    const roundContainsAnswer = game.rounds.some((round) =>
      round.answers.some((a) => a.answers.some((ans) => ans === answer)),
    );
    const handCardsContainAnswer = game.players.some((player) =>
      player.handCards.some((handCard) => handCard === answer),
    );
    return roundContainsAnswer || handCardsContainAnswer;
  }

  /**
   * Gets the number of distinct statements in the given categories.
   * @param categories
   * @private
   */
  private getNrDistinctStatements(categories: string[]): number {
    return this.statementCategories
      .filter((cat) => categories.some((gameCat) => gameCat === cat.name))
      .reduce((previousNrCategories: number, category: CAStatementCategory) => {
        const distinctStatements = [...new Set(category.statements)];
        return distinctStatements.length + previousNrCategories;
      }, 0);
  }

  private noStatementLeft(game: CAGame): boolean {
    return (
      game.rounds.length >=
      this.getNrDistinctStatements(game.statementCategories)
    );
  }

  private noAnswersLeft(game: CAGame): boolean {
    return (
      this.getNrOfHandCardsAndAnswersInGame(game) >=
      this.getNrAnswers(game.statementCategories)
    );
  }

  private chooseRandomCategory(gameCategories: string[]): CAStatementCategory {
    const possibleCategories = this.getCategories(gameCategories);
    return possibleCategories[
      Math.floor(Math.random() * possibleCategories.length)
    ];
  }

  /**
   * Chooses a random statement from all valid categories.
   * Does not check whether the game contains the statement.
   * @param game
   * @private
   */
  private chooseRandomStatement(game: CAGame): string {
    const cat = this.chooseRandomCategory(game.statementCategories);
    return cat?.statements[Math.floor(Math.random() * cat.statements.length)];
  }

  /**
   * Chooses a random answer from all valid categories.
   * Does not check whether the game and players contain the answer.
   * @param game
   * @private
   */
  private chooseRandomAnswer(game: CAGame): string {
    const cat = this.chooseRandomCategory(game.statementCategories);
    return cat?.answers[Math.floor(Math.random() * cat.answers.length)];
  }
}
