import { CAGame } from '../domain/game.entity';
import { CARound } from '../domain/round.entity';
import { CAPlayer } from '../domain/player.entity';

export abstract class ICARoundService {
  /**
   * Selects a new statement, fills the hand cards of the players.
   * If there are not enough cards to play the next round,
   * the round is not added and null is returned.
   * @param game
   */
  abstract addRound(game: CAGame): CARound;

  /**
   * Fills the hand cards of the given player.
   * It makes sure that all players have the same amount of cards,
   * if possible.
   * @param game
   * @param player
   */
  abstract fillHandCardsOfPlayer(game: CAGame, player: CAPlayer): void;

  /**
   * Adds one card to the player.
   * @param player
   * @param card
   */
  abstract addHandCard(player: CAPlayer, card: string): void;

  /**
   * Removes the given card from the player.
   * If the player does not have this card, nothing is removed
   * @param player
   * @param card
   */
  abstract removeHandCard(player: CAPlayer, card: string): void;

  /**
   * Chooses a new critic for the round.
   * @param game
   * @return The id of the new critic
   */
  abstract chooseCritic(game: CAGame): string;

  abstract getCurrentRound(game: CAGame): CARound;
}
