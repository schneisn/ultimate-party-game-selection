import { UpgsError } from '../../../../common/error/upgs.error';

export class StatementCategoryNotDefinedError extends UpgsError {
  constructor(category: string) {
    super(
      'STATEMENT_CATEGORY_NOT_DEFINED',
      'No category with the given name exists',
      'There is no category with the given name',
      category,
    );
  }
}
