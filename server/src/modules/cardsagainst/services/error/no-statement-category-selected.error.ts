import { UpgsError } from '../../../../common/error/upgs.error';
import { CAGame } from '../../domain/game.entity';

export class NoStatementCategorySelectedError extends UpgsError {
  constructor(game: CAGame) {
    super(
      'STATEMENT_CATEGORY_NOT_SELECTED',
      'Game has no statement category',
      'You need to select a statement category to start playing',
      game,
    );
  }
}
