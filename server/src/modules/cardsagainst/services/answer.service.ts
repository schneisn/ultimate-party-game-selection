import { Injectable } from '@nestjs/common';
import { CARound } from '../domain/round.entity';
import { ICAAnswerService } from './answer.service.interface';
import { CAAnswer } from '../domain/answer.entity';
import { CAGame } from '../domain/game.entity';
import { RoundNotDefinedError } from '../../common/game/service/error/round-not-defined.error';
import { ICAPlayerService } from './player.service.interface';
import { ICARoundService } from './round.service.interface';
import { IGameRepository } from '../../common/game/repository/game.repository.interface';

@Injectable()
export class CAAnswerService implements ICAAnswerService {
  constructor(
    private readonly playerService: ICAPlayerService,
    private readonly roundService: ICARoundService,
    private readonly gameRepository: IGameRepository,
  ) {}

  getAnswer(round: CARound, playerId: string): CAAnswer {
    return round.answers.find((answer) => answer.playerId === playerId);
  }

  addAnswer(game: CAGame, playerId: string, answers: string[]): void {
    const currentRound = this.roundService.getCurrentRound(game);
    if (!currentRound) {
      throw new RoundNotDefinedError(game);
    }
    const playerAnswers = currentRound.answers.find(
      (a) => a.playerId === playerId,
    );
    if (playerAnswers) {
      playerAnswers.answers = answers;
    } else {
      currentRound.answers.push(new CAAnswer(playerId, answers));
    }
    answers.forEach((answer) =>
      this.roundService.removeHandCard(
        this.playerService.getPlayer(game, playerId),
        answer,
      ),
    );
    this.sortAnswersAlphabetically(currentRound);
    this.gameRepository.save(game);
  }

  removeAnswer(round: CARound, playerId): void {
    let index = 0;
    for (const answer of round.answers) {
      if (answer.playerId === playerId) {
        round.answers.splice(index, 1);
      }
      index++;
    }
  }

  private sortAnswersAlphabetically(round: CARound) {
    // Sort cards alphabetically so the critic does not know which player
    // gave which answer
    round.answers.sort((aRaw, bRaw) => {
      const a = aRaw.answers[0]?.toLocaleLowerCase();
      const b = bRaw.answers[0]?.toLocaleLowerCase();
      if (a < b) {
        return -1;
      }
      if (a > b) {
        return 1;
      }
      return 0;
    });
  }
}
