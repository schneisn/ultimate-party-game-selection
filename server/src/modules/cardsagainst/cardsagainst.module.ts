import { Module } from '@nestjs/common';
import { CAGateway } from './gateway/cardsagainst.gateway';
import { ICAStatementService } from './services/statement.service.interface';
import { CAStatementService } from './services/statement.service';
import { ICARoundService } from './services/round.service.interface';
import { CARoundService } from './services/round.service';
import { ICAPlayerService } from './services/player.service.interface';
import { CAPlayerService } from './services/player.service';
import { ICAGameService } from './services/game.service.interface';
import { CAGameService } from './services/game.service';
import { IGameRepository } from '../common/game/repository/game.repository.interface';
import { GameRepository } from '../common/game/repository/game.repository';
import { ICAAnswerService } from './services/answer.service.interface';
import { CAAnswerService } from './services/answer.service';

@Module({
  providers: [
    CAGateway,
    {
      provide: ICAAnswerService,
      useClass: CAAnswerService,
    },
    {
      provide: ICAGameService,
      useClass: CAGameService,
    },
    {
      provide: IGameRepository,
      useClass: GameRepository,
    },
    {
      provide: ICAPlayerService,
      useClass: CAPlayerService,
    },
    {
      provide: ICARoundService,
      useClass: CARoundService,
    },
    {
      provide: ICAStatementService,
      useClass: CAStatementService,
    },
  ],
})
export class CardsAgainstModule {}
