import {
  MessageBody,
  OnGatewayDisconnect,
  SubscribeMessage,
  WebSocketGateway,
} from '@nestjs/websockets';
import { ISessionService } from '../../session/service/session.service.interface';
import { CommonGateway } from '../../common/gateway/common.gateway';
import { SetPlayerReadyStateRequest } from '../../common/gateway/event/request/set-player-ready-state.request';
import { SetPlayerReadyStateEvent } from '../../common/gateway/event/set-player-ready-state.event';
import { LoggerService } from '../../logger/logger.service';
import { ICAGameService } from '../services/game.service.interface';
import { ICAPlayerService } from '../services/player.service.interface';
import { ICAStatementService } from '../services/statement.service.interface';
import { GetStatementCategoriesEvent } from './event/get-statement-categories.event';
import { NextAnswerEvent } from './event/next-answer.event';
import { NextAnswerRequest } from './event/request/next-answer.request';
import { SetCriticInputRequest } from './event/request/set-critic-input.request';
import { SetStatementCategoriesRequest } from './event/request/set-statement-categories.request';
import { SetCriticInputEvent } from './event/set-critic-input.event';
import { SetInputEvent } from './event/set-input.event';
import { SetStatementCategoriesEvent } from './event/set-statement-categories.event';
import { SerializableWsResponse } from '../../common/gateway/event/serializable-ws-response.class';
import { SetInputRequest } from './event/request/set-input.request';
import { ICAAnswerService } from '../services/answer.service.interface';
import { CAGame } from '../domain/game.entity';
import { UseInterceptors, UsePipes } from '@nestjs/common';
import { GetGamePipe } from '../../../pipes/get-game.pipe';
import { ConfigService } from '../../config/config.service';
import { UpdateSettingsEvent } from '../../common/gateway/event/update-settings.event';
import { UpdateSettingsRequest } from './event/request/update-settings.request';
import { RefillHandCardsRequest } from './event/request/refill-hand-cards.request';
import { RefillHandCardsEvent } from './event/refill-hand-cards.event';
import { Public } from '../../../decorators/is-public.decorator';
import { GetPlayerInterceptor } from '../../../interceptors/get-player.interceptor';

@WebSocketGateway({ namespace: 'cardsagainst', cors: true })
export class CAGateway extends CommonGateway {
  constructor(
    readonly logger: LoggerService,
    readonly gameService: ICAGameService,
    readonly playerService: ICAPlayerService,
    readonly sessionService: ISessionService,
    readonly statementService: ICAStatementService,
    readonly answerService: ICAAnswerService,
  ) {
    super('cardsagainst', logger, gameService, playerService, sessionService);
  }

  @Public()
  @SubscribeMessage(GetStatementCategoriesEvent.id)
  onGetStatementCategories(): SerializableWsResponse<
    GetStatementCategoriesEvent
  > {
    const categories = this.statementService.getAllCategories();
    return new SerializableWsResponse(
      GetStatementCategoriesEvent.id,
      new GetStatementCategoriesEvent(categories),
    );
  }

  @UsePipes(GetGamePipe)
  @SubscribeMessage(SetStatementCategoriesEvent.id)
  onSetStatementCategories(
    @MessageBody() data: SetStatementCategoriesRequest,
  ): void {
    this.gameService.setStatementCategories(
      data.game,
      data.statementCategories,
    );
    this.emitGame(data.game.id);
  }

  @UsePipes(GetGamePipe)
  @UseInterceptors(GetPlayerInterceptor)
  @SubscribeMessage(SetPlayerReadyStateEvent.id)
  onSetPlayerIsReady(
    @MessageBody() data: SetPlayerReadyStateRequest<CAGame>,
  ): void {
    this.playerService.setPlayerReadyState(
      data.game,
      data.player.id,
      data.isReady,
    );
    this.emitGame(data.game.id);
    this.emitOpenGames();
  }

  @UsePipes(GetGamePipe)
  @UseInterceptors(GetPlayerInterceptor)
  @SubscribeMessage(SetInputEvent.id)
  onInput(@MessageBody() data: SetInputRequest): void {
    this.answerService.addAnswer(data.game, data.player.id, data.answers);
    this.emitGame(data.game.id);
    this.emitOpenGames();
  }

  @UsePipes(GetGamePipe)
  @UseInterceptors(GetPlayerInterceptor)
  @SubscribeMessage(SetCriticInputEvent.id)
  onCriticInput(@MessageBody() data: SetCriticInputRequest): void {
    this.gameService.setCriticInput(data.game, data.player.id, data.winnerId);
    this.emitGame(data.game.id);
    this.emitOpenGames();
  }

  @UsePipes(GetGamePipe)
  @SubscribeMessage(NextAnswerEvent.id)
  onNextAnswer(@MessageBody() data: NextAnswerRequest): void {
    this.publishEventToRoom(
      data.game.id,
      NextAnswerEvent.id,
      new NextAnswerEvent(data.nextAnswer),
    );
  }

  @UsePipes(GetGamePipe)
  @UseInterceptors(GetPlayerInterceptor)
  @SubscribeMessage(RefillHandCardsEvent.id)
  onRefillHandCards(@MessageBody() data: RefillHandCardsRequest): void {
    this.playerService.refillHandCardsOfPlayer(data.game, data.player.id);
    this.emitGame(data.game.id);
  }

  @UsePipes(GetGamePipe)
  @SubscribeMessage(UpdateSettingsEvent.id)
  onUpdateSettings(@MessageBody() data: UpdateSettingsRequest): void {
    this.gameService.updateSettings(data.game, data.settings);
    this.emitGame(data.game.id);
  }
}
