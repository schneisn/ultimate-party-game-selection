import { UpgsEvent } from '../../../common/gateway/event/upgs.event';

export class NextAnswerEvent extends UpgsEvent {
  static readonly id = 'NextAnswer';
  nextAnswer: number;

  constructor(nextAnswer: number) {
    super();
    this.nextAnswer = nextAnswer;
  }
}
