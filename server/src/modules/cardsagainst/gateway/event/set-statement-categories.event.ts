import { UpgsEvent } from '../../../common/gateway/event/upgs.event';

export class SetStatementCategoriesEvent extends UpgsEvent {
  static readonly id = 'SetStatementCategories';
}
