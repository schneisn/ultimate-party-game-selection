import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class GetStatementCategoryModel {
  @Expose()
  name: string;

  @Expose()
  description: string;

  @Expose()
  language: string;

  @Expose()
  preselected: boolean;
}
