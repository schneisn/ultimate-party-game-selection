import { IsNumber } from 'class-validator';

export class Settings {
  @IsNumber()
  replaceCardsRound: number;
}
