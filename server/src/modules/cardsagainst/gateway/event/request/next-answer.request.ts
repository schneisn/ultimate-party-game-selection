import { IsNumber } from 'class-validator';
import { GameRequest } from '../../../../common/gateway/event/request/game.request';
import { CAGame } from '../../../domain/game.entity';

export class NextAnswerRequest extends GameRequest<CAGame> {
  @IsNumber()
  nextAnswer: number;
}
