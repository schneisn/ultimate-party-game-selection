import { IsString } from 'class-validator';
import { GameRequest } from '../../../../common/gateway/event/request/game.request';
import { CAGame } from '../../../domain/game.entity';

export class SetCriticInputRequest extends GameRequest<CAGame> {
  @IsString()
  winnerId: string;
}
