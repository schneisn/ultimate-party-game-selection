import { IsArray } from 'class-validator';
import { GameRequest } from '../../../../common/gateway/event/request/game.request';
import { CAGame } from '../../../domain/game.entity';

export class SetStatementCategoriesRequest extends GameRequest<CAGame> {
  @IsArray()
  statementCategories: string[];
}
