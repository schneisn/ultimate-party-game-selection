import { GameRequest } from '../../../../common/gateway/event/request/game.request';
import { CAGame } from '../../../domain/game.entity';

export class RefillHandCardsRequest extends GameRequest<CAGame> {}
