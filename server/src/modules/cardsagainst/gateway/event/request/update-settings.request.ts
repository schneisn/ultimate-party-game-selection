import { ValidateNested } from 'class-validator';
import { GameRequest } from '../../../../common/gateway/event/request/game.request';
import { Settings } from './model/settings.model';
import { CAGame } from '../../../domain/game.entity';

export class UpdateSettingsRequest extends GameRequest<CAGame> {
  @ValidateNested()
  settings: Settings;
}
