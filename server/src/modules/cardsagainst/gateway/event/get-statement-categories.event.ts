import { UpgsEvent } from '../../../common/gateway/event/upgs.event';
import { GetStatementCategoryModel } from './model/get-statement-category.model';
import { Type } from 'class-transformer';

export class GetStatementCategoriesEvent extends UpgsEvent {
  static readonly id = 'GetStatementCategories';

  @Type(() => GetStatementCategoryModel)
  statementCategories: GetStatementCategoryModel[];

  constructor(statementCategories: GetStatementCategoryModel[]) {
    super();
    this.statementCategories = statementCategories;
  }
}
