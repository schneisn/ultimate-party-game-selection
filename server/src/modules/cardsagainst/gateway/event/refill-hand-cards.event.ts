import { UpgsEvent } from '../../../common/gateway/event/upgs.event';

export class RefillHandCardsEvent extends UpgsEvent {
  static readonly id = 'RefillHandCards';
}
