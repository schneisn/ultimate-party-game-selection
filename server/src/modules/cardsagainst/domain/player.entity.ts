import { Player } from '../../common/player/domain/player.entity';

export class CAPlayer extends Player {
  isReady: boolean = false;
  handCards: string[] = [];
  /**
   * The player refilled their hand cards the last
   * time in this round. This is the index of the round,
   * not the actual number.
   */
  refilledCardsInRound: number;
}
