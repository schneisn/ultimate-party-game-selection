export class CAStatementCategory {
  name: string;
  description: string;
  language: string;
  preselected: boolean;
  statements: string[] = [];
  answers: string[] = [];
}
