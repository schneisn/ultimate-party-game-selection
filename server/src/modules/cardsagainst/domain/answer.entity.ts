export class CAAnswer {
  readonly playerId: string;
  answers: string[] = [];
  hasWon: boolean = false;

  constructor(playerId: string, answers: string[]) {
    this.playerId = playerId;
    this.answers = answers;
  }
}
