import { CAAnswer } from './answer.entity';

export class CARound {
  /**
   * The nr of the round. The first round is number 0.
   */
  readonly number: number;
  readonly statement: string;
  criticId: string = null;
  answers: CAAnswer[] = [];

  constructor(number: number, statement: string, criticId: string) {
    this.number = number;
    this.statement = statement;
    this.criticId = criticId;
  }
}
