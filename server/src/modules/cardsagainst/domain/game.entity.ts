import { Game } from '../../common/game/domain/game.entity';
import { CAPlayer } from './player.entity';
import { CARound } from './round.entity';

export class CAGame extends Game<CAPlayer> {
  /**
   * The rounds in the game
   */
  rounds: CARound[] = [];
  /**
   * The names of the statement categories that
   * are used in the game
   */
  statementCategories: string[];
  /**
   * How often players can replace all their cards
   */
  replaceCardsRound: number;
}
