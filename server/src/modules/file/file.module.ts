import { Global, Module } from '@nestjs/common';
import { FileService } from './file.service';
import { IFileService } from './file.service.interface';

@Global()
@Module({
  providers: [
    {
      provide: IFileService,
      useClass: FileService,
    },
  ],
  exports: [IFileService],
})
export class FileModule {}
