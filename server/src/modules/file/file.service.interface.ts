export abstract class IFileService {
  /**
   * Reads a json object from a file.
   * @param filePath The absolute file path of the file
   */
  abstract readJSONFromFile<T>(filePath: string): T;

  /**
   * Reads all json object from files in a directory
   * that are of type T and concatenates them.
   *
   * Takes the server directory as root path.
   * @param dirPath
   */
  abstract readJSONFromDir<T>(dirPath: string): T[];
}
