import { Injectable } from '@nestjs/common';
import { readdirSync, readFileSync } from 'fs';
import { join } from 'path';
import { IFileService } from './file.service.interface';

@Injectable()
export class FileService implements IFileService {
  readJSONFromFile<T>(filePath: string): T {
    const data = readFileSync(filePath);
    return JSON.parse(data.toString());
  }

  readJSONFromDir<T>(dirPath: string): T[] {
    let objects: T[] = [];
    // @ts-ignore
    dirPath = join(__baseDir, dirPath);
    const filePaths = readdirSync(dirPath);
    filePaths.forEach((filePath: string) => {
      if (filePath.endsWith('.json')) {
        const obj = this.readJSONFromFile<T>(join(dirPath, filePath));
        objects = objects.concat(obj);
      }
    });
    return objects;
  }
}
