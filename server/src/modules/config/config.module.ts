import { Global, Module } from '@nestjs/common';
import { ConfigModule as NestConfigModule } from '@nestjs/config';
import { ConfigService } from './config.service';

/**
 * The config module handles the configuration of the app.
 * It uses the given .env file to read the env vars.
 * The fallback .env file is the production
 */
@Global()
@Module({
  providers: [ConfigService],
  exports: [ConfigService],
  imports: [
    NestConfigModule.forRoot({
      // Uses sourceRoot of the application (see .nest-cli.json)
      envFilePath: [
        process.env.ENV_FILE_PATH,
        '.env.production', // Default is production
      ],
    }),
  ],
})
export class ConfigModule {}
