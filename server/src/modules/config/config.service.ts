import { Injectable, Logger, OnModuleInit } from '@nestjs/common';
import { ConfigService as NestConfigService } from '@nestjs/config';
import { NetworkInterfaceInfo, networkInterfaces } from 'os';
import { toString } from 'qrcode';

/**
 * Valid parameters that can be set in the .env file
 */
export enum configParams {
  /**
   * Used to define the used .env file
   */
  ENV_FILE_PATH = 'ENV_FILE_PATH',
  /**
   * The environment
   */
  NODE_ENV = 'NODE_ENV',
  /**
   * Use by some hosting providers.
   */
  PORT = 'PORT',
  /**
   * The host name of the server
   */
  SERVER_HOST = 'SERVER_HOST',
  /**
   * The port the server listens on
   */
  SERVER_PORT = 'SERVER_PORT',
  /**
   * Whether to show user instructions in the console
   */
  SHOW_USER_INSTRUCTIONS = 'SHOW_USER_INSTRUCTIONS',
}

/**
 * The accepted environments
 */
export enum env {
  PRODUCTION = 'production',
  DEVELOPMENT = 'development',
}

@Injectable()
export class ConfigService implements OnModuleInit {
  private readonly logger = new Logger(ConfigService.name);

  constructor(private readonly nestConfigService: NestConfigService) {}

  onModuleInit(): void {
    this.logEnvFile();
    this.logLocalIP();
    this.logUserInstructions();
  }

  /**
   * Get the environment of the .env
   */
  getEnv(): env {
    switch (
      this.nestConfigService.get<string>(configParams.NODE_ENV, env.PRODUCTION)
    ) {
      case env.PRODUCTION:
        return env.PRODUCTION;
      case env.DEVELOPMENT:
        return env.DEVELOPMENT;
      default:
        return env.PRODUCTION;
    }
  }

  isDevelopment(): boolean {
    return this.getEnv() === env.DEVELOPMENT;
  }

  /**
   * Returns the port as string.
   * Prioritizes the PORT env var over the
   * SERVER_PORT variable because some hosting providers
   * pass a random port as PORT env var.
   */
  getPort(): string {
    return process.env.PORT
      ? process.env.PORT
      : this.nestConfigService.get<string>(configParams.SERVER_PORT);
  }

  /**
   * Nestjs config cannot read booleans from .env file correctly.
   * Read the boolean as string and check whether it is 'true'
   */
  private getShowUserInstructions(): boolean {
    return (
      this.nestConfigService
        .get<string>(configParams.SHOW_USER_INSTRUCTIONS, 'false')
        .toLocaleLowerCase() === 'true'
    );
  }

  /**
   * Returns the ip and port in format ip:port
   */
  private getIPAndPort(): string {
    return this.getLocalNetworkIP() + ':' + this.getPort();
  }

  /**
   * Gets the ip in the current network.
   * @return null if there is no network
   */
  private getLocalNetworkIP(): string {
    const ip: NetworkInterfaceInfo = []
      .concat(...Object.values(networkInterfaces()))
      .find(
        (details: NetworkInterfaceInfo) =>
          details.family === 'IPv4' && !details.internal,
      );
    return ip ? ip.address : null;
  }

  private logEnvFile() {
    const envFilePath = this.nestConfigService.get<string>(
      configParams.ENV_FILE_PATH,
    );
    this.logger.log(
      envFilePath
        ? 'Using .env file: ' + envFilePath
        : 'No .env file specified. Using default .env.production file',
    );
  }

  /**
   * Logs the ip in the local network
   */
  private logLocalIP() {
    this.logger.log(
      'Server is available on your local network under: ' + this.getIPAndPort(),
    );
  }

  /**
   * Logs instructions how to start playing when it is started in production.
   */
  private logUserInstructions() {
    if (!this.getShowUserInstructions()) {
      return;
    }
    this.logBrowserInstruction();
    this.logQRCode();
    this.handleTerminalEvents();
  }

  private logBrowserInstruction(): void {
    const borderWidth = 2;
    const nrBlanks = 52;
    const totalWidth = borderWidth + nrBlanks;

    const borderIcon = ' ';
    const verticalBorder = borderIcon.repeat(borderWidth);
    const blanks = ' '.repeat(nrBlanks);
    const blankLine = verticalBorder + blanks + verticalBorder + '\n';

    let ipAndPort = this.getIPAndPort();
    const openBrowser = this.createTextLine(
      'Open your browser and put in',
      totalWidth,
      borderIcon,
      borderWidth,
    );
    ipAndPort = this.createTextLine(
      ipAndPort,
      totalWidth,
      borderIcon,
      borderWidth,
    );
    const inAddressBar = this.createTextLine(
      'in your address bar',
      totalWidth,
      borderIcon,
      borderWidth,
    );
    const orScanQRCode = this.createTextLine(
      'or scan the QR Code',
      totalWidth,
      borderIcon,
      borderWidth,
    );

    console.log(
      `${blankLine}` +
        `${blankLine}` +
        `${openBrowser}` +
        `${ipAndPort}` +
        `${inAddressBar}` +
        `${blankLine}` +
        `${orScanQRCode}`,
    );
  }

  private createTextLine(
    text: string,
    lineLength: number,
    borderIcon: string,
    borderWidth: number,
  ): string {
    const border = borderIcon.repeat(borderWidth);
    const blanks = ' '.repeat((lineLength - text.length - 2 * borderWidth) / 2);
    return border + blanks + text + blanks + border + '\n';
  }

  /**
   * Logs a qr code in the terminal to quickly access the website
   */
  private async logQRCode(): Promise<void> {
    try {
      console.log(
        await toString('http://' + this.getIPAndPort(), {
          errorCorrectionLevel: 'L',
          type: 'terminal',
        }),
      );
      console.log('\n');
    } catch (err) {
      console.error(err);
    }
  }

  /**
   * Handles terminal events such as resizing.
   * On resizing, the qr code needs to be printed again,
   * because it is deleted on re-render
   */
  private handleTerminalEvents(): void {
    let previousHeight = process.stdout.rows;
    let previousWidth = process.stdout.columns;
    let lastChangeTime = new Date();
    /**
     * Timer to set when the next render will happen
     */
    let timer: NodeJS.Timeout = null;
    process.stdout.on('resize', () => {
      if (process.stdout.columns === previousWidth) {
        // If width does not change, do not re-render qr code because
        // it is not removed on re-render
        return;
      }
      // Clear timer to render when the console is constantly
      // being resized. Only render when is "stands still" for two seconds
      const twoSecondsAgo = new Date();
      twoSecondsAgo.setSeconds(twoSecondsAgo.getSeconds() - 2);
      const wasRenderedInTheLastTwoSeconds = twoSecondsAgo < lastChangeTime;
      if (wasRenderedInTheLastTwoSeconds) {
        clearTimeout(timer);
      }
      // Update last change values
      previousHeight = process.stdout.rows;
      previousWidth = process.stdout.columns;
      lastChangeTime = new Date();
      timer = setTimeout(() => {
        this.logBrowserInstruction();
        this.logQRCode();
      }, 2000);
    });
  }
}
