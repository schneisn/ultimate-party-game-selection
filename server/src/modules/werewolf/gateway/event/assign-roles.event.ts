import { UpgsEvent } from '../../../common/gateway/event/upgs.event';

export class AssignRolesEvent extends UpgsEvent {
  static readonly id = 'AssignRoles';
}
