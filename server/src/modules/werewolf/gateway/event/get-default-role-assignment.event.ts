import { UpgsEvent } from '../../../common/gateway/event/upgs.event';
import { RoleEventModel } from './model/role.event-model';

export class GetDefaultRoleAssignmentEvent extends UpgsEvent {
  static readonly id = 'GetDefaultRoleAssignment';
  roles: RoleEventModel[];

  constructor(roles: RoleEventModel[]) {
    super();
    this.roles = roles;
  }
}
