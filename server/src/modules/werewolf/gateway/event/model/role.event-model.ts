import { Role } from '../../../domain/role.entity';

export class RoleEventModel {
  name: Role;
  amount: number;
}
