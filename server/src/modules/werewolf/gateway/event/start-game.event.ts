import { UpgsEvent } from '../../../common/gateway/event/upgs.event';

export class StartGameEvent extends UpgsEvent {
  static readonly id = 'StartGame';
}
