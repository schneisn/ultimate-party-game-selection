import { UpgsEvent } from '../../../common/gateway/event/upgs.event';

export class CloseVoteEvent extends UpgsEvent {
  static readonly id = 'CloseVote';
}
