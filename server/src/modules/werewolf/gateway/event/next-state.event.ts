import { UpgsEvent } from '../../../common/gateway/event/upgs.event';

export class NextStateEvent extends UpgsEvent {
  static readonly id = 'NextState';
}
