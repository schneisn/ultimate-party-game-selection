import { UpgsEvent } from '../../../common/gateway/event/upgs.event';
import { RoleEventModel } from './model/role.event-model';

export class GetRolesEvent extends UpgsEvent {
  static readonly id = 'GetRoles';
  roles: RoleEventModel[];

  constructor(roles: RoleEventModel[]) {
    super();
    this.roles = roles;
  }
}
