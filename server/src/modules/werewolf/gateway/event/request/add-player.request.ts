import { IsString } from 'class-validator';
import { GameRequest } from '../../../../common/gateway/event/request/game.request';
import { WWGame } from '../../../domain/game.entity';

export class AddPlayerRequest extends GameRequest<WWGame> {
  @IsString()
  playerName: string;
}
