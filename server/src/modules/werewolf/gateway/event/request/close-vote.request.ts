import { GameRequest } from '../../../../common/gateway/event/request/game.request';
import { StateEvent, WWGame } from '../../../domain/game.entity';

export class CloseVoteRequest extends GameRequest<WWGame> {
  voteType: StateEvent;
}
