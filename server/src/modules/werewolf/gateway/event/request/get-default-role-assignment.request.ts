import { GameRequest } from '../../../../common/gateway/event/request/game.request';
import { WWGame } from '../../../domain/game.entity';

export class GetDefaultRoleAssignmentRequest extends GameRequest<WWGame> {}
