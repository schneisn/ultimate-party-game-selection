import { ValidateNested } from 'class-validator';
import { GameRequest } from '../../../../common/gateway/event/request/game.request';
import { IdRequestModel } from '../../../../common/gateway/event/request/model/id.request-model';
import { WWGame } from '../../../domain/game.entity';

export class RemovePlayerRequest extends GameRequest<WWGame> {
  @ValidateNested()
  removePlayer: IdRequestModel;
}
