import { IsString } from 'class-validator';
import { GameRequest } from '../../../../common/gateway/event/request/game.request';
import { WWGame } from '../../../domain/game.entity';

export class VoteRequest extends GameRequest<WWGame> {
  @IsString()
  targetId: string;
}
