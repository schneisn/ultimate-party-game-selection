import { IsNumber, IsString } from 'class-validator';
import { Role } from '../../../../domain/role.entity';

export class RoleRequestModel {
  @IsString()
  name: Role;
  @IsNumber()
  amount: number;
}
