import { GameRequest } from '../../../../common/gateway/event/request/game.request';
import { RoleRequestModel } from './model/role.request-model';
import { WWGame } from '../../../domain/game.entity';

export class SetRolesAssignmentRequest extends GameRequest<WWGame> {
  roles: RoleRequestModel[];
}
