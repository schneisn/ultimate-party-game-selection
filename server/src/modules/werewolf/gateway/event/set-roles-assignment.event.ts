import { UpgsEvent } from '../../../common/gateway/event/upgs.event';

export class SetRolesAssignmentEvent extends UpgsEvent {
  static readonly id = 'SetRolesAssignment';
}
