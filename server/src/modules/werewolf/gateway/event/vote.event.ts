import { UpgsEvent } from '../../../common/gateway/event/upgs.event';

export class VoteEvent extends UpgsEvent {
  static readonly id = 'Vote';
}
