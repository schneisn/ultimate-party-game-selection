import { UpgsEvent } from '../../../common/gateway/event/upgs.event';

export class AddPlayerEvent extends UpgsEvent {
  static readonly id = 'AddPlayer';
}
