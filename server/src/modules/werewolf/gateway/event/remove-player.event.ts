import { UpgsEvent } from '../../../common/gateway/event/upgs.event';

export class RemovePlayerEvent extends UpgsEvent {
  static readonly id = 'RemovePlayer';
}
