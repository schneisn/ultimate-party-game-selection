import { UpgsEvent } from '../../../common/gateway/event/upgs.event';

export class ResetGameEvent extends UpgsEvent {
  static readonly id = 'ResetGame';
}
