import {
  MessageBody,
  SubscribeMessage,
  WebSocketGateway,
} from '@nestjs/websockets';
import { ISessionService } from '../../session/service/session.service.interface';
import { CommonGateway } from '../../common/gateway/common.gateway';
import { LoggerService } from '../../logger/logger.service';
import { IWWGameService } from '../services/game.service.interface';
import { IWWPlayerService } from '../services/player.service.interface';
import { IWWRoleService } from '../services/role.service.interface';
import { IWWStateService } from '../services/state.service.interface';
import { IWWVoteService } from '../services/vote.service.interface';
import { AddPlayerEvent } from './event/add-player.event';
import { AssignRolesEvent } from './event/assign-roles.event';
import { CloseVoteEvent } from './event/close-vote.event';
import { GetDefaultRoleAssignmentEvent } from './event/get-default-role-assignment.event';
import { GetRolesEvent } from './event/get-roles.event';
import { NextStateEvent } from './event/next-state.event';
import { RemovePlayerEvent } from './event/remove-player.event';
import { AddPlayerRequest } from './event/request/add-player.request';
import { AssignRolesRequest } from './event/request/assign-roles.request';
import { CloseVoteRequest } from './event/request/close-vote.request';
import { GetDefaultRoleAssignmentRequest } from './event/request/get-default-role-assignment.request';
import { NextStateRequest } from './event/request/next-state.request';
import { RemovePlayerRequest } from './event/request/remove-player.request';
import { ResetGameRequest } from './event/request/reset-game.request';
import { SetRolesAssignmentRequest } from './event/request/set-roles-assignment.request';
import { StartGameRequest } from './event/request/start-game.request';
import { VoteRequest } from './event/request/vote.request';
import { ResetGameEvent } from './event/reset-game.event';
import { SetRolesAssignmentEvent } from './event/set-roles-assignment.event';
import { StartGameEvent } from './event/start-game.event';
import { VoteEvent } from './event/vote.event';
import { SerializableWsResponse } from '../../common/gateway/event/serializable-ws-response.class';
import { cloneDeep } from 'lodash';
import { WWGame } from '../domain/game.entity';
import { Game } from '../../common/game/domain/game.entity';
import { Player } from '../../common/player/domain/player.entity';
import { Role } from '../domain/role.entity';
import { WWPlayer } from '../domain/player.entity';
import { UseInterceptors, UsePipes } from '@nestjs/common';
import { GetGamePipe } from '../../../pipes/get-game.pipe';
import { ConfigService } from '../../config/config.service';
import { GetPlayerInterceptor } from '../../../interceptors/get-player.interceptor';

@WebSocketGateway({ namespace: 'werewolf', cors: true })
export class WWGateway extends CommonGateway {
  constructor(
    readonly logger: LoggerService,
    readonly gameService: IWWGameService,
    readonly playerService: IWWPlayerService,
    readonly sessionService: ISessionService,
    readonly roleService: IWWRoleService,
    readonly voteService: IWWVoteService,
    readonly stateService: IWWStateService,
  ) {
    super('werewolf', logger, gameService, playerService, sessionService);
  }

  @UsePipes(GetGamePipe)
  @SubscribeMessage(StartGameEvent.id)
  onStartGame(@MessageBody() data: StartGameRequest): void {
    this.stateService.computeNextState(data.game);
    this.emitGame(data.game.id);
    this.emitOpenGames();
  }

  @UsePipes(GetGamePipe)
  @SubscribeMessage(GetDefaultRoleAssignmentEvent.id)
  onGetDefaultRoles(
    @MessageBody() data: GetDefaultRoleAssignmentRequest,
  ): SerializableWsResponse<GetDefaultRoleAssignmentEvent> {
    const roleAssignment = this.roleService.getDefaultRoleAssignment(data.game);
    return new SerializableWsResponse(
      GetDefaultRoleAssignmentEvent.id,
      new GetDefaultRoleAssignmentEvent(roleAssignment),
    );
  }

  @UsePipes(GetGamePipe)
  @SubscribeMessage(SetRolesAssignmentEvent.id)
  onSetRoles(@MessageBody() data: SetRolesAssignmentRequest): void {
    this.publishEventToRoom(
      data.game.id,
      GetRolesEvent.id,
      new GetRolesEvent(data.roles),
    );
  }

  @UsePipes(GetGamePipe)
  @SubscribeMessage(AssignRolesEvent.id)
  onAssignRoles(@MessageBody() data: AssignRolesRequest): void {
    this.stateService.assignRoles(data.game, data.roles);
    this.emitGame(data.game.id);
  }

  @UsePipes(GetGamePipe)
  @UseInterceptors(GetPlayerInterceptor)
  @SubscribeMessage(VoteEvent.id)
  onVote(@MessageBody() data: VoteRequest): void {
    this.voteService.vote(data.game, data.player.id, data.targetId);
    this.emitGame(data.game.id);
  }

  @UsePipes(GetGamePipe)
  @SubscribeMessage(CloseVoteEvent.id)
  onCloseVote(@MessageBody() data: CloseVoteRequest): void {
    this.stateService.closeVoteAndAddEventToCurrentState(
      data.game,
      data.voteType,
    );
    this.emitGame(data.game.id);
  }

  @UsePipes(GetGamePipe)
  @SubscribeMessage(NextStateEvent.id)
  onNextState(@MessageBody() data: NextStateRequest): void {
    this.stateService.computeNextState(data.game);
    this.emitGame(data.game.id);
  }

  @UsePipes(GetGamePipe)
  @SubscribeMessage(AddPlayerEvent.id)
  onAddPlayer(@MessageBody() data: AddPlayerRequest): void {
    this.playerService.addComputerPlayer(data.game, data.playerName);
    this.emitGame(data.game.id);
  }

  @UsePipes(GetGamePipe)
  @SubscribeMessage(RemovePlayerEvent.id)
  onRemovePlayer(@MessageBody() data: RemovePlayerRequest): void {
    this.playerService.disconnectPlayer(data.game, data.removePlayer.id);
    this.emitGame(data.game.id);
  }

  @UsePipes(GetGamePipe)
  @SubscribeMessage(ResetGameEvent.id)
  onResetGame(@MessageBody() data: ResetGameRequest): void {
    this.gameService.resetGame(data.game);
    this.emitGame(data.game.id);
  }

  protected emitGame(gameId: string) {
    let g: WWGame;
    try {
      g = this.gameService.get(gameId);
    } catch (e) {
      // Ignore and return null
    }
    g &&
      this.logger.debug('Emit game to all players in game ' + gameId, null, g);
    if (!g) {
      return;
    }
    const playersAndGm = cloneDeep(g.players).concat(g.gamemaster);
    this.emitGameToPlayers(g, playersAndGm);
  }

  protected transformGame(game: WWGame, playerId: string): Game<Player> {
    const player = this.playerService.getPlayer(game, playerId);
    if (!player || !player.isAlive) {
      return game;
    }
    switch (player.role) {
      case Role.GAME_MASTER:
        break;
      case Role.SEER:
        // TODO Only send the data when the seer has selected a player
        break;
      case Role.WEREWOLF:
        this.filterPlayers(game, playerId)
          .filter((p) => p.role !== Role.WEREWOLF)
          .forEach((p) => (p.role = null));
        break;
      default:
        this.filterPlayers(game, playerId).forEach((p) => (p.role = null));
    }
    return game;
  }

  private filterPlayers(game: WWGame, playerId: string): WWPlayer[] {
    return game.players
      .filter((p) => p.id !== playerId)
      .filter((p) => p.role !== Role.GAME_MASTER)
      .filter((p) => p.isAlive)
      .filter(
        (p) => this.stateService.getLovePartner(game, playerId)?.id !== p.id,
      );
  }
}
