export class WWVote {
  /**
   * The player that voted
   */
  voterId: string;
  /**
   * The target that was selected by the voter
   */
  targetId: string;

  constructor(voterId: string, targetId: string) {
    this.voterId = voterId;
    this.targetId = targetId;
  }
}
