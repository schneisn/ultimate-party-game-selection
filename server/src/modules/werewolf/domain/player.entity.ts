import { Player } from '../../common/player/domain/player.entity';
import { Role } from './role.entity';

export class WWPlayer extends Player {
  role: Role;
  isAlive: boolean = true;
}
