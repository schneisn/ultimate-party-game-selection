import { Game } from '../../common/game/domain/game.entity';
import { WWPlayer } from './player.entity';
import { WWGameState } from './state.entity';
import { WWVote } from './vote.entity';

/**
 * All events and states that occur during the game
 */
export enum GameState {
  /**
   * The armor is active.
   */
  ARMOR = 'armor',
  /**
   * The doppelganger is active.
   */
  DOPPELGANGER = 'doppelganger',
  /**
   * This event is only used server-internally to finish off players
   * permanently that were killed and not saved during the night.
   */
  KILL_PLAYERS_BEFORE_END_NIGHT = 'kill_players_before_end_night',
  /**
   * Shows who died during the night.
   */
  END_NIGHT = 'endnight',
  /**
   * The game is over. Shows the winner of the game.
   */
  GAME_OVER = 'gameover',
  /**
   * The healer is active.
   */
  HEALER = 'healer',
  /**
   * The hunter is active.
   */
  HUNTER = 'hunter',
  /**
   * Shows the player that the hunter killed.
   */
  SHOW_HUNTER_KILL = 'show_hunter_kill',
  /**
   * The players are in the lobby.
   */
  LOBBY = 'lobby',
  /**
   * The lovers are awake and see each others names.
   */
  LOVERS = 'lovers',
  /**
   * The players vote for the mayor.
   */
  MAYOR = 'votemayor',
  /**
   * The result of the mayor vote is shown.
   */
  MAYOR_ELECTION_RESULT = 'mayor_election_result',
  /**
   * The prostitute is active.
   */
  PROSTITUTE = 'prostitute',
  /**
   * The werewolves are active.
   */
  WEREWOLF = 'werewolf',
  /**
   * Show which player was targeted by the werewolves.
   */
  SHOW_WEREWOLF_KILL = 'show_werewolf_kill',
  /**
   * The seer is active.
   */
  SEER = 'seer',
  /**
   * Show if the selected player is a werewolf
   */
  SHOW_SEER_SELECTION = 'show_seer_selection',
  /**
   * The distribution of roles is defined
   */
  SELECT_ROLES = 'selectroles',
  /**
   * Shows an info that all players close their eyes and the night starts.
   */
  START_NIGHT = 'startnight',
  /**
   * All players select one person to die
   */
  TRIAL = 'trial',
  /**
   * Shows who was killed in the trial.
   */
  SHOW_TRIAL_KILL = 'show_trial_kill',
  /**
   * The witch is active
   */
  WITCH = 'witch',
}

export enum StateEvent {
  /**
   * The doppelganger selected a target that he becomes
   * when the target dies
   */
  DOPPELGANGER_SELECT = 'doppelganger_select',
  /**
   * The doppelganger becomes the target he selected
   */
  DOPPELGANGER_CHANGE = 'doppelganger_change',
  /**
   * The healer heals a target
   */
  HEALER_HEAL = 'healer_heal',
  /**
   * The hunter killed a target
   */
  HUNTER_KILL = 'hunter_kill',
  /**
   * The first lover is selected
   */
  LOVER_ONE = 'lover_one',
  /**
   * The second lover is selected
   */
  LOVER_TWO = 'lover_two',
  /**
   * A player was elected as mayor
   */
  MAYOR_ELECTED = 'mayor',
  /**
   * The prostitute selected the player where she wants to stay
   */
  PROSTITUTE_SELECT = 'prostitute_select',
  /**
   * A player was targeted by the werewolves.
   */
  WEREWOLF_KILL = 'werewolf_kill',
  /**
   * The seer selected a player to see their role
   */
  SEER_SELECT = 'seer_select',
  /**
   * A player was selected to be killed during trial
   */
  TRIAL_KILL = 'trial_kill',
  /**
   * The witch healed someone
   */
  WITCH_HEAL = 'witch_heal',
  /**
   * The witch killed someone
   */
  WITCH_KILL = 'witch_kill',
  /**
   * A player is permanently dead.
   */
  DEATH = 'death',
  /**
   * The player is a winner.
   */
  WINNER = 'winner',
  /**
   * The werewolves won the game.
   */
  WEREWOLVES_WON = 'werewolves_won',
  /**
   * The lovers won the game.
   */
  LOVERS_WON = 'lovers_won',
  /**
   * The citizens won the game.
   */
  CITIZENS_WON = 'citizens_won',
}

export class WWGame extends Game<WWPlayer> {
  gamemaster: WWPlayer;
  mayor: string;
  votes: WWVote[] = [];
  states: WWGameState[] = [];

  constructor() {
    super();
    this.states.push(new WWGameState(GameState.LOBBY, 1));
  }
}
