import { GameState } from './game.entity';
import { WWStateEvent } from './event.entity';

export class WWGameState {
  /**
   * Id of the state.
   */
  id: GameState;
  /**
   * The nr of the night. The first night is nr 1.
   */
  night: number;
  /**
   * Active actions that the players performed in this state.
   */
  events: WWStateEvent[];

  constructor(id: GameState, night: number) {
    this.id = id;
    this.night = night;
    this.events = [];
  }
}
