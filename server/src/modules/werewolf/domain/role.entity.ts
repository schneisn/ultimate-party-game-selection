export enum Role {
  ARMOR = 'armor',
  CHILD = 'child',
  CITIZEN = 'citizen',
  DOPPELGANGER = 'doppelganger',
  GAME_MASTER = 'gamemaster',
  HEALER = 'healer',
  HUNTER = 'hunter',
  PROSTITUTE = 'prostitute',
  SEER = 'seer',
  WEREWOLF = 'werewolf',
  WITCH = 'witch',
}

export class WWRole {
  name: Role;
  amount: number;
}
