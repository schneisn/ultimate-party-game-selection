import { StateEvent } from './game.entity';

/**
 * An event is an action that is performed in a state.
 */
export class WWStateEvent {
  /**
   * The id of the event
   */
  id: StateEvent;
  /**
   * The target of the event.
   * This is typically a player id,
   * but can also be undefined
   */
  targetId?: string;

  constructor(id: StateEvent, targetId: string) {
    this.id = id;
    this.targetId = targetId;
  }
}
