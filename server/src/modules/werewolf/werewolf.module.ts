import { Module } from '@nestjs/common';
import { WWGateway } from './gateway/werewolf.gateway';
import { IWWGameService } from './services/game.service.interface';
import { WWGameService } from './services/game.service';
import { IGameRepository } from '../common/game/repository/game.repository.interface';
import { GameRepository } from '../common/game/repository/game.repository';
import { IWWPlayerService } from './services/player.service.interface';
import { WWPlayerService } from './services/player.service';
import { IWWRoleService } from './services/role.service.interface';
import { WWRoleService } from './services/role.service';
import { IWWStateService } from './services/state.service.interface';
import { WWStateService } from './services/state.service';
import { IWWVoteService } from './services/vote.service.interface';
import { WWVoteService } from './services/vote.service';

@Module({
  providers: [
    WWGateway,
    {
      provide: IWWGameService,
      useClass: WWGameService,
    },
    {
      provide: IGameRepository,
      useClass: GameRepository,
    },
    {
      provide: IWWPlayerService,
      useClass: WWPlayerService,
    },
    {
      provide: IWWRoleService,
      useClass: WWRoleService,
    },
    {
      provide: IWWStateService,
      useClass: WWStateService,
    },
    {
      provide: IWWVoteService,
      useClass: WWVoteService,
    },
  ],
})
export class WerewolfModule {}
