import { CommonPlayerService } from '../../common/player/service/player.service';
import { GameState, WWGame } from '../domain/game.entity';
import { Role } from '../domain/role.entity';
import { WWPlayer } from '../domain/player.entity';
import { IWWPlayerService } from './player.service.interface';
import { IWWGameService } from './game.service.interface';
import { IWWStateService } from './state.service.interface';
import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { PlayerNotDefinedError } from '../../common/player/service/error/player-not-defined.error';
import { IGameRepository } from '../../common/game/repository/game.repository.interface';

@Injectable()
export class WWPlayerService extends CommonPlayerService<WWGame, WWPlayer>
  implements IWWPlayerService {
  constructor(
    private readonly gameService: IWWGameService,
    @Inject(forwardRef(() => IWWStateService))
    private readonly stateService: IWWStateService,
    readonly gameRepository: IGameRepository,
  ) {
    super(gameRepository);
  }

  addPlayer(game: WWGame, playerId: string, playerName: string): WWPlayer {
    playerName = this.addNrToPlayerNameIfExists(game, playerId, playerName);
    const existingPlayer = this.replaceExistingPlayer(
      game,
      playerId,
      playerName,
    );
    if (existingPlayer) {
      this.gameRepository.save(game);
      return existingPlayer;
    }
    const newPlayer = new WWPlayer(null, playerName);
    if (!game.gamemaster) {
      newPlayer.role = Role.GAME_MASTER;
      game.gamemaster = newPlayer;
    } else {
      game.players.push(newPlayer);
    }
    this.gameRepository.save(game);
    return newPlayer;
  }

  addComputerPlayer(game: WWGame, playerName: string): WWPlayer {
    const newPlayer = new WWPlayer(null, playerName, true);
    game.players.push(newPlayer);
    this.gameRepository.save(game);
    return newPlayer;
  }

  getPlayer(game: WWGame, playerId: string): WWPlayer {
    return game.gamemaster?.id === playerId
      ? game.gamemaster
      : super.getPlayer(game, playerId);
  }

  gameContainsPlayerId(game: WWGame, playerId: string): boolean {
    return (
      super.gameContainsPlayerId(game, playerId) ||
      game.gamemaster.id === playerId
    );
  }

  disconnectPlayer(game: WWGame, playerId: string) {
    const disconnectedPlayer = this.getPlayer(game, playerId);
    if (!disconnectedPlayer) {
      throw new PlayerNotDefinedError(playerId);
    }
    disconnectedPlayer.isConnected = false;
    if (this.noPlayersAreConnected(game)) {
      this.gameService.scheduleGameForRemoval(game.id);
      return;
    }
    if (this.stateService.getCurrentGameState(game).id === GameState.LOBBY) {
      if (game.gamemaster.id === disconnectedPlayer.id) {
        const nextGm = game.players.find((p) => p.isConnected);
        nextGm.role = Role.GAME_MASTER;
        game.gamemaster = nextGm;
        const nextGmIndex = game.players.findIndex((p) => p.id === nextGm.id);
        game.players.splice(nextGmIndex, 1);
      }

      super.disconnectPlayer(game, playerId);
    }
    this.gameRepository.save(game);
  }

  protected noPlayersAreConnected(game: WWGame): boolean {
    return (
      game.players.filter((player) => player.isConnected).length < 1 &&
      !game.gamemaster.isConnected
    );
  }
}
