import { Injectable } from '@nestjs/common';
import * as _ from 'lodash';
import { WWGame } from '../domain/game.entity';
import { WWPlayer } from '../domain/player.entity';
import { Role, WWRole } from '../domain/role.entity';
import { IWWRoleService } from './role.service.interface';

@Injectable()
export class WWRoleService implements IWWRoleService {
  /**
   * The order of roles that are added to the game for the first 15 players
   * @private
   */
  private roleSuggestion: Role[] = [
    Role.CITIZEN,
    Role.CITIZEN,
    Role.WEREWOLF,
    Role.WITCH,
    Role.ARMOR,
    Role.WEREWOLF,
    Role.CHILD,
    Role.PROSTITUTE,
    Role.WEREWOLF,
    Role.SEER,
    Role.HEALER,
    Role.WEREWOLF,
    Role.HUNTER,
    Role.DOPPELGANGER,
    Role.WEREWOLF,
  ];

  /**
   * When more than 15 players are in the game, all special roles are distributed.
   * The remaining players will be citizens or werewolves
   * @private
   */
  private citCitWer: Role[] = [Role.CITIZEN, Role.CITIZEN, Role.WEREWOLF];

  /**
   * Used to synchronize the distribution of roles in a game
   * while the game master changes the distribution
   */
  private defaultRoleAssignment: WWRole[] = [
    {
      name: Role.CITIZEN,
      amount: 0,
    },
    {
      name: Role.WEREWOLF,
      amount: 0,
    },
    {
      name: Role.WITCH,
      amount: 0,
    },
    {
      name: Role.ARMOR,
      amount: 0,
    },
    {
      name: Role.CHILD,
      amount: 0,
    },
    {
      name: Role.PROSTITUTE,
      amount: 0,
    },
    {
      name: Role.SEER,
      amount: 0,
    },
    {
      name: Role.HEALER,
      amount: 0,
    },
    {
      name: Role.HUNTER,
      amount: 0,
    },
    {
      name: Role.DOPPELGANGER,
      amount: 0,
    },
  ];

  /**
   * Provides a reasonable distribution of roles in a game.
   * It considers the amount of players to compute it.
   * @param game The game to compute the role assignment for
   */
  getDefaultRoleAssignment(game: WWGame): WWRole[] {
    let roleAssignment = _.cloneDeep(this.defaultRoleAssignment);
    // Assign a reasonable mix of roles to the first 15 players
    const maxIterations =
      game.players.length > this.roleSuggestion.length
        ? this.roleSuggestion.length
        : game.players.length;
    for (let i = 0; i < maxIterations; i++) {
      roleAssignment = this.setRoleAmount(
        roleAssignment,
        this.roleSuggestion[i],
        this.getRoleAmount(roleAssignment, this.roleSuggestion[i]) + 1,
      );
    }

    // Assign citizen/citizen/werewolf to the rest of the players
    const restAmountPlayers = game.players.length - this.roleSuggestion.length;
    if (restAmountPlayers > 0) {
      for (let j = 0; j < restAmountPlayers; j++) {
        roleAssignment = this.setRoleAmount(
          roleAssignment,
          this.citCitWer[j % 3],
          this.getRoleAmount(roleAssignment, this.citCitWer[j % 3]) + 1,
        );
      }
    }
    return roleAssignment;
  }

  getRoleAmount(roles, roleName): number {
    for (const role of roles) {
      if (role.name === roleName) {
        return role.amount;
      }
    }
    return -1;
  }

  getPlayerWithRole(game: WWGame, role: Role): WWPlayer {
    if (role === Role.GAME_MASTER) {
      return game.gamemaster;
    }

    // Prefer alive players
    for (const player of game.players) {
      if (player.isAlive && player.role === role) {
        return player;
      }
    }
    for (const player of game.players) {
      if (player.role === role) {
        return player;
      }
    }
    return null;
  }

  getPlayersWithRole(game: WWGame, role: Role): WWPlayer[] {
    if (role === Role.GAME_MASTER) {
      return [game.gamemaster];
    }

    return game.players.filter((player) => player.role === role);
  }

  private setRoleAmount(
    roles: WWRole[],
    roleName: string,
    amount: number,
  ): WWRole[] {
    for (const role of roles) {
      if (role.name === roleName) {
        role.amount = amount;
        break;
      }
    }
    return roles;
  }
}
