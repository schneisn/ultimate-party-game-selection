import { Injectable } from '@nestjs/common';
import { LoggerService } from '../../logger/logger.service';
import { NoTargetError } from './error/no-target.error';
import { GameState, StateEvent, WWGame } from '../domain/game.entity';
import { WWPlayer } from '../domain/player.entity';
import { IWWPlayerService } from './player.service.interface';
import { Role, WWRole } from '../domain/role.entity';
import { IWWRoleService } from './role.service.interface';
import { IWWVoteService } from './vote.service.interface';
import { InvalidGameStateError } from './error/invalid-game-state.error';
import { NotEnoughVotesError } from './error/not-enough-votes.error';
import { UndefinedGameStateError } from './error/undefined-game-state.error';
import { WWStateEvent } from '../domain/event.entity';
import { WWGameState } from '../domain/state.entity';
import { IWWStateService } from './state.service.interface';
import { IGameRepository } from '../../common/game/repository/game.repository.interface';

@Injectable()
export class WWStateService implements IWWStateService {
  constructor(
    private readonly logger: LoggerService,
    private readonly playerService: IWWPlayerService,
    private readonly voteService: IWWVoteService,
    private readonly roleService: IWWRoleService,
    private readonly gameRepository: IGameRepository,
  ) {}

  private static getVotePowerOfGoodPlayers(game: WWGame): number {
    let amount = 0;
    for (const player of game.players) {
      if (player.role !== Role.WEREWOLF && player.isAlive) {
        amount++;
        if (player.id === game.mayor) {
          amount += 0.5;
        }
      }
    }
    return amount;
  }

  private static getVotePowerOfWerewolves(game: WWGame): number {
    let amount = 0;
    for (const player of game.players) {
      if (player.role === Role.WEREWOLF && player.isAlive) {
        amount++;
        if (player.id === game.mayor) {
          amount += 0.5;
        }
      }
    }
    return amount;
  }

  private static werewolvesWon(game: WWGame): boolean {
    return (
      WWStateService.getVotePowerOfGoodPlayers(game) <=
      WWStateService.getVotePowerOfWerewolves(game)
    );
  }

  private static citizensWon(game: WWGame): boolean {
    return WWStateService.getVotePowerOfWerewolves(game) === 0;
  }

  getCurrentGameState(game: WWGame): WWGameState {
    return game.states[game.states.length - 1];
  }

  computeNextState(game: WWGame): void {
    const currentState = this.getCurrentGameState(game);
    if (!currentState) {
      return null;
    }
    const trialExistsInCurrentNight = this.getStatesOfCurrentNight(game).some(
      (state) => state.id === GameState.TRIAL,
    );
    switch (currentState.id) {
      case GameState.LOBBY:
        this.addGameState(game, GameState.SELECT_ROLES);
        break;
      case GameState.SELECT_ROLES:
        this.addGameState(game, GameState.MAYOR);
        break;
      case GameState.MAYOR:
        this.addGameState(game, GameState.MAYOR_ELECTION_RESULT);
        break;
      case GameState.MAYOR_ELECTION_RESULT:
        if (!trialExistsInCurrentNight && !this.isStartOfFirstNight(game)) {
          this.addGameState(game, GameState.TRIAL);
          break;
        }
        this.addGameState(game, GameState.START_NIGHT);
        break;
      case GameState.START_NIGHT:
        if (!this.isStartOfFirstNight(game)) {
          if (
            this.roleIsDefined(game, Role.PROSTITUTE) &&
            this.roleIsAlive(game, Role.PROSTITUTE)
          ) {
            this.addGameState(game, GameState.PROSTITUTE);
            break;
          }
          this.addGameState(game, GameState.WEREWOLF);
          break;
        }
        if (this.roleIsDefined(game, GameState.ARMOR)) {
          this.addGameState(game, GameState.ARMOR);
          break;
        }
      // fallthrough
      case GameState.ARMOR:
        if (this.loversExist(game)) {
          this.addGameState(game, GameState.LOVERS);
          break;
        }
      // fallthrough
      case GameState.LOVERS:
        if (
          this.roleIsDefined(game, GameState.DOPPELGANGER) &&
          this.isStartOfFirstNight(game)
        ) {
          this.addGameState(game, GameState.DOPPELGANGER);
          break;
        }
      // fallthrough
      case GameState.DOPPELGANGER:
        if (
          this.roleIsDefined(game, GameState.PROSTITUTE) &&
          this.roleIsAlive(game, Role.PROSTITUTE)
        ) {
          this.addGameState(game, GameState.PROSTITUTE);
          break;
        }
      // fallthrough
      case GameState.PROSTITUTE:
        if (this.roleIsDefined(game, GameState.WEREWOLF)) {
          this.addGameState(game, GameState.WEREWOLF);
          break;
        }
      // fallthrough
      case GameState.WEREWOLF:
        this.addGameState(game, GameState.SHOW_WEREWOLF_KILL);
        break;
      case GameState.SHOW_WEREWOLF_KILL:
        if (
          this.roleIsDefined(game, GameState.SEER) &&
          this.roleIsAlive(game, Role.SEER)
        ) {
          this.addGameState(game, GameState.SEER);
          break;
        }
      // fallthrough
      case GameState.SEER:
        if (
          this.roleIsDefined(game, GameState.SEER) &&
          this.roleIsAlive(game, Role.SEER)
        ) {
          this.addGameState(game, GameState.SHOW_SEER_SELECTION);
          break;
        }
      // fallthrough
      case GameState.SHOW_SEER_SELECTION:
        const witch = this.roleService.getPlayerWithRole(game, Role.WITCH);
        if (witch && witch.isAlive && this.witchHasFreeSkill(game)) {
          this.addGameState(game, GameState.WITCH);
          break;
        }
      // fallthrough
      case GameState.WITCH:
        if (
          this.roleIsDefined(game, Role.HEALER) &&
          this.roleIsAlive(game, Role.HEALER)
        ) {
          this.addGameState(game, GameState.HEALER);
          break;
        }
      // fallthrough
      case GameState.HEALER:
        this.addGameState(game, GameState.KILL_PLAYERS_BEFORE_END_NIGHT);
      // fallthrough
      case GameState.KILL_PLAYERS_BEFORE_END_NIGHT:
        this.killPlayersOfNight(game);
        this.addGameState(game, GameState.END_NIGHT);
        break;
      case GameState.END_NIGHT:
        if (this.roleWasKilledInThisNight(game, Role.HUNTER)) {
          this.addGameState(game, GameState.HUNTER);
          break;
        }
        this.changeDoppelgangerRole(game);
        if (this.isGameOver(game)) {
          this.addGameOverState(game);
          break;
        }
        if (!game.mayor) {
          this.addGameState(game, GameState.MAYOR);
          break;
        }
        this.addGameState(game, GameState.TRIAL);
        break;
      case GameState.HUNTER:
        this.addGameState(game, GameState.SHOW_HUNTER_KILL);
        break;
      case GameState.SHOW_HUNTER_KILL:
        this.changeDoppelgangerRole(game);
        if (this.isGameOver(game)) {
          this.addGameOverState(game);
          break;
        }
        if (!game.mayor) {
          this.addGameState(game, GameState.MAYOR);
          break;
        }
        if (!trialExistsInCurrentNight) {
          this.addGameState(game, GameState.TRIAL);
          break;
        }
        this.addGameState(game, GameState.START_NIGHT);
        break;
      case GameState.TRIAL:
        this.addGameState(game, GameState.SHOW_TRIAL_KILL);
        break;
      case GameState.SHOW_TRIAL_KILL:
        const event = this.getLatestEvent(game, StateEvent.TRIAL_KILL);
        const target = this.playerService.getPlayer(game, event.targetId);
        if (target.role === Role.HUNTER) {
          this.addGameState(game, GameState.HUNTER);
          break;
        }
        this.changeDoppelgangerRole(game);
        if (this.isGameOver(game)) {
          this.addGameOverState(game);
          break;
        }
        if (!game.mayor) {
          this.addGameState(game, GameState.MAYOR);
          break;
        }
        this.addGameState(game, GameState.START_NIGHT);
        break;
      default:
        throw new UndefinedGameStateError(currentState);
    }
    this.gameRepository.save(game);
  }

  closeVoteAndAddEventToCurrentState(game: WWGame, voteType: StateEvent): void {
    // TODO Also select lovers via voteService.getVoteWinner (by making it usable for multiple vote winners)
    if (this.getCurrentGameState(game)?.id === GameState.ARMOR) {
      this.selectLovers(game);
      this.voteService.resetVotes(game);
      this.gameRepository.save(game);
      return;
    }
    if (!voteType) {
      this.logger.warn('vote type is undefined');
    }
    const voteWinner = this.voteService.getVoteWinner(game);
    this.addEventToCurrentState(game, voteType, voteWinner?.id);
    this.voteService.resetVotes(game);
    this.gameRepository.save(game);
    this.closingVoteAdvancesState(game, voteType) &&
      this.computeNextState(game);
  }

  assignRoles(game: WWGame, roles: WWRole[]): void {
    for (const role of roles) {
      for (let i = 0; i < role.amount; i++) {
        let player =
          game.players[Math.floor(Math.random() * game.players.length)];
        while (player.role) {
          player =
            game.players[Math.floor(Math.random() * game.players.length)];
        }
        player.role = role.name;
      }
    }
    this.computeNextState(game);
  }

  getLovePartner(game: WWGame, playerId: string): WWPlayer {
    return game.players.find(
      (player) => this.playerIsLover(game, player.id) && player.id !== playerId,
    );
  }

  playerIsLover(game: WWGame, playerId: string): boolean {
    const loverOneEntry = this.getLatestEvent(game, StateEvent.LOVER_ONE);
    const loverTwoEntry = this.getLatestEvent(game, StateEvent.LOVER_TWO);
    return (
      (loverOneEntry && loverOneEntry.targetId === playerId) ||
      (loverTwoEntry && loverTwoEntry.targetId === playerId)
    );
  }

  private addEventToCurrentState(
    game: WWGame,
    eventId: StateEvent,
    targetId?: string,
  ): void {
    const currentState = this.getCurrentGameState(game);
    if (!currentState) {
      throw new UndefinedGameStateError(currentState);
    }
    const newEvent = new WWStateEvent(eventId, targetId);
    currentState.events.push(newEvent);
    this.logger.debug('New event ', null, newEvent);
    this.logger.debug('Updated state ', null, currentState);

    const target = this.playerService.getPlayer(game, targetId);
    switch (eventId) {
      case StateEvent.MAYOR_ELECTED:
        game.mayor = targetId;
        break;
      case StateEvent.HUNTER_KILL:
      // fallthrough
      case StateEvent.TRIAL_KILL:
        this.killPlayerDuringDay(game, target);
        break;
    }
  }

  /**
   * When the events of the witch have no valid player as target, the witch
   * still has free skills (kill or heal).
   * @param game
   * @private
   */
  private witchHasFreeSkill(game: WWGame): boolean {
    const healedPlayer = this.playerService.getPlayer(
      game,
      this.getLatestEvent(game, StateEvent.WITCH_HEAL)?.targetId,
    );
    const killedPlayer = this.playerService.getPlayer(
      game,
      this.getLatestEvent(game, StateEvent.WITCH_KILL)?.targetId,
    );
    return !healedPlayer || !killedPlayer;
  }

  /**
   * Checks whether any player with the given role was killed in this night
   * @param game
   * @param role
   */
  private roleWasKilledInThisNight(game: WWGame, role: Role): boolean {
    const players = this.roleService.getPlayersWithRole(game, role);
    return players.some((player) =>
      this.playerWasKilledInThisNight(game, player),
    );
  }

  private roleIsDefined(game: WWGame, role: string): boolean {
    return game.players.some((player) => player.role === role);
  }

  /**
   * When all roles had their turn the players are set to alive = false
   * and all dependent players are killed
   */
  private killPlayersOfNight(game: WWGame): void {
    game.players
      .filter((player) => this.playerWasKilledInThisNight(game, player))
      .forEach((player) => this.killPlayerInNight(game, player));
  }

  /**
   * Kills a single player in the night
   * @param game
   * @param killedPlayer
   * @private
   */
  private killPlayerInNight(game: WWGame, killedPlayer: WWPlayer): void {
    if (!killedPlayer.isAlive) {
      return;
    }
    killedPlayer.isAlive = false;
    if (killedPlayer.id === game.mayor) {
      game.mayor = null;
    }
    this.addEventToCurrentState(game, StateEvent.DEATH, killedPlayer.id);
    this.killDependentPlayersInNight(game, killedPlayer);
  }

  /**
   * Kill all players that depend on the given player.
   * During night, the prostitute and the lovers die together with the killed player.
   * @param game
   * @param killedPlayer
   */
  private killDependentPlayersInNight(
    game: WWGame,
    killedPlayer: WWPlayer,
  ): void {
    if (!killedPlayer) {
      return;
    }
    if (this.prostituteDies(game, killedPlayer.id)) {
      const prostitute = this.roleService.getPlayerWithRole(
        game,
        Role.PROSTITUTE,
      );
      this.killPlayerInNight(game, prostitute);
    }
    if (this.playerIsLover(game, killedPlayer.id)) {
      const lover = this.getLovePartner(game, killedPlayer.id);
      this.killPlayerInNight(game, lover);
    }
  }

  /**
   * Checks whether the prostitute dies when the player with the given id died.
   * @param game
   * @param killedPlayerId
   */
  private prostituteDies(game: WWGame, killedPlayerId: string): boolean {
    const prostitute = this.roleService.getPlayerWithRole(
      game,
      Role.PROSTITUTE,
    );
    const prostituteSelectEvent = this.getLatestEvent(
      game,
      StateEvent.PROSTITUTE_SELECT,
    );
    return (
      prostitute &&
      prostituteSelectEvent &&
      this.playerWasKilledByWerewolvesInCurrentNight(game, killedPlayerId) &&
      prostituteSelectEvent.targetId === killedPlayerId
    );
  }

  /**
   * Checks whether the player was killed by werewolves in most recent night
   * @param game
   * @param killedPlayerId
   * @private
   */
  private playerWasKilledByWerewolvesInCurrentNight(
    game: WWGame,
    killedPlayerId: string,
  ): boolean {
    const currentNightNr = this.getCurrentNightNumber(game);
    return game.states
      .slice()
      .reverse()
      .filter((s) => s.night === currentNightNr)
      .some((state) =>
        state.events.some(
          (ev) =>
            ev.id === StateEvent.WEREWOLF_KILL &&
            ev.targetId === killedPlayerId,
        ),
      );
  }

  private getStatesOfCurrentNight(game: WWGame): WWGameState[] {
    const currentNight = this.getCurrentGameState(game)?.night;
    return game.states
      .slice()
      .reverse()
      .filter((state) => state.night === currentNight);
  }

  /**
   * Iterates reverse over the last events of the night and checks if there
   * is a kill event.
   * If there is a kill event, it return true.
   * If there is a heal event, it returns false.
   * If there is no event such event, it returns false.
   * If the player is null, it returns false.
   * @param game
   * @param player
   */
  private playerWasKilledInThisNight(game: WWGame, player: WWPlayer): boolean {
    if (!player) {
      return false;
    }
    for (const state of this.getStatesOfCurrentNight(game)) {
      const latestEventWithPlayer = state.events
        .slice()
        .reverse()
        .find((ev) => ev.targetId === player.id);
      if (latestEventWithPlayer) {
        if (this.eventKillsPlayerInNight(game, latestEventWithPlayer)) {
          return true;
        }
        if (
          latestEventWithPlayer.id === StateEvent.WITCH_HEAL ||
          latestEventWithPlayer.id === StateEvent.HEALER_HEAL
        ) {
          return false;
        }
      }
    }
    return false;
  }

  /**
   * Gets the latest event with the given id from any event
   * @param game
   * @param eventId
   */
  private getLatestEvent(game: WWGame, eventId: StateEvent): WWStateEvent {
    return game.states
      .slice()
      .reverse()
      .find((state) => state.events.find((ev) => ev.id === eventId))
      ?.events.find((ev) => ev.id === eventId);
  }

  /**
   * Checks whether the given event is an event where the targeted player dies
   * @param game
   * @param event
   */
  private eventKillsPlayerInNight(game: WWGame, event: WWStateEvent): boolean {
    return (
      (event.id === StateEvent.WEREWOLF_KILL &&
        this.playerService.getPlayer(game, event.targetId).role !==
          Role.PROSTITUTE) ||
      event.id === StateEvent.HUNTER_KILL ||
      event.id === StateEvent.WITCH_KILL ||
      event.id === StateEvent.DEATH
    );
  }

  private addGameState(game: WWGame, entryId: GameState): WWGameState {
    const newEntry = new WWGameState(
      entryId,
      this.getNightNumber(game, entryId),
    );
    this.logger.debug('New State entry', null, newEntry);
    game.states.push(newEntry);
    return newEntry;
  }

  /**
   * Check whether the game continues to the next state after the voting round is closed.
   * @param game
   * @param voteType
   * @private
   */
  private closingVoteAdvancesState(
    game: WWGame,
    voteType: StateEvent,
  ): boolean {
    return (
      voteType !== StateEvent.WITCH_HEAL ||
      (voteType === StateEvent.WITCH_HEAL && !this.witchHasFreeSkill(game))
    );
  }

  private getNightNumber(game: WWGame, entryId: GameState): number {
    let entryNight = 1;
    if (game.states.length < 2) {
      return entryNight;
    }
    // Every action is generally happening in the same night as the action before
    entryNight = game.states[game.states.length - 1].night;
    if (!this.isStartOfFirstNight(game) && entryId === GameState.WEREWOLF) {
      entryNight = game.states[game.states.length - 1].night + 1;
    }
    return entryNight;
  }

  private getCurrentNightNumber(game: WWGame): number {
    return this.getNightNumber(game, game.states[game.states.length - 1].id);
  }

  /**
   * Werewolf is the first action in a new night; except first night
   * To determine if it is the first night, count the amount of werewolf-entries
   * in the game states. If there is none, it is the first night.
   * @param game
   * @private
   */
  private isStartOfFirstNight(game: WWGame): boolean {
    return !game.states.find((state) => state.id === GameState.WEREWOLF);
  }

  /**
   * The doppelganger takes on the role of this selected target when the
   * target died.
   * @param game
   */
  private changeDoppelgangerRole(game: WWGame): void {
    const doppelganger = this.roleService.getPlayerWithRole(
      game,
      Role.DOPPELGANGER,
    );
    if (!doppelganger) {
      return;
    }
    const doppelgangerSelectEvent = this.getLatestEvent(
      game,
      StateEvent.DOPPELGANGER_SELECT,
    );
    if (!doppelgangerSelectEvent) {
      return;
    }
    const target = this.playerService.getPlayer(
      game,
      doppelgangerSelectEvent.targetId,
    );
    if (!target) {
      throw new NoTargetError(game);
    }
    if (doppelganger && doppelganger.isAlive && !target.isAlive) {
      doppelganger.role = target.role;
      this.addEventToCurrentState(
        game,
        StateEvent.DOPPELGANGER_CHANGE,
        target.id,
      );
    }
  }

  private loversExist(game: WWGame): boolean {
    return game.states.some((state) =>
      state.events.some(
        (ev) =>
          ev.id === StateEvent.LOVER_ONE || ev.id === StateEvent.LOVER_TWO,
      ),
    );
  }

  /**
   * Selects the two lovers by adding two events to the the current state.
   * // TODO Replace with vote event
   * @param game
   */
  private selectLovers(game: WWGame): void {
    if (this.getCurrentGameState(game).id !== GameState.ARMOR) {
      throw new InvalidGameStateError(game);
    }
    if (game.votes.length < 2) {
      throw new NotEnoughVotesError(game);
    }
    const loverOne = this.playerService.getPlayer(game, game.votes[0].targetId);
    this.addEventToCurrentState(game, StateEvent.LOVER_ONE, loverOne.id);
    const loverTwo = this.playerService.getPlayer(game, game.votes[1].targetId);
    this.addEventToCurrentState(game, StateEvent.LOVER_TWO, loverTwo.id);
    this.computeNextState(game);
  }

  /**
   * Kills a player an all dependent players during the day.
   * During the day. the prostitute does not die together with the killed player.
   * @param game
   * @param target
   * @private
   */
  private killPlayerDuringDay(game: WWGame, target: WWPlayer): void {
    if (!target) {
      throw new NoTargetError(game);
    }
    if (!target.isAlive) {
      return;
    }
    target.isAlive = false;
    if (target.id === game.mayor) {
      game.mayor = null;
    }
    this.addEventToCurrentState(game, StateEvent.DEATH, target.id);
    if (this.playerIsLover(game, target.id)) {
      const lover = this.getLovePartner(game, target.id);
      this.killPlayerDuringDay(game, lover);
    }
  }

  /**
   * Checks whether the player with the given role is alive.
   * @param game
   * @param role
   * @return False if there is no player with this role
   */
  private roleIsAlive(game: WWGame, role: Role): boolean {
    return this.roleService.getPlayerWithRole(game, role)?.isAlive;
  }

  private isGameOver(game: WWGame): boolean {
    const werewolvesWon = WWStateService.werewolvesWon(game);
    const citizensWon = WWStateService.citizensWon(game);
    const loversWon = this.onlyLoversAlive(game);
    return werewolvesWon || citizensWon || loversWon;
  }

  private addGameOverState(game: WWGame): WWGameState {
    const winState = this.addGameState(game, GameState.GAME_OVER);
    if (this.onlyLoversAlive(game)) {
      this.addEventToCurrentState(game, StateEvent.LOVERS_WON);
      game.players
        .filter((p) => this.playerIsLover(game, p.id))
        .map((lover) =>
          this.addEventToCurrentState(game, StateEvent.WINNER, lover.id),
        );
    } else if (WWStateService.werewolvesWon(game)) {
      this.addEventToCurrentState(game, StateEvent.WEREWOLVES_WON);
      game.players
        .filter((p) => p.role === Role.WEREWOLF)
        .map((werewolf) =>
          this.addEventToCurrentState(game, StateEvent.WINNER, werewolf.id),
        );
    } else if (WWStateService.citizensWon(game)) {
      this.addEventToCurrentState(game, StateEvent.CITIZENS_WON);
      game.players
        .filter((p) => p.role !== Role.WEREWOLF)
        .map((werewolf) =>
          this.addEventToCurrentState(game, StateEvent.WINNER, werewolf.id),
        );
    }
    return winState;
  }

  private onlyLoversAlive(game: WWGame): boolean {
    return !game.players.some(
      (player) =>
        (!this.playerIsLover(game, player.id) && player.isAlive) ||
        (this.playerIsLover(game, player.id) && !player.isAlive),
    );
  }
}
