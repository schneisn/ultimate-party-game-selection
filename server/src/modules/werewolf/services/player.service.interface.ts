import { ICommonPlayerService } from '../../common/player/service/player.service.interface';
import { WWGame } from '../domain/game.entity';
import { WWPlayer } from '../domain/player.entity';

export abstract class IWWPlayerService extends ICommonPlayerService<WWPlayer> {
  abstract addComputerPlayer(game: WWGame, playerName: string): WWPlayer;
}
