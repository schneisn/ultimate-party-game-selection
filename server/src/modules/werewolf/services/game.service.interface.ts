import { ICommonGameService } from '../../common/game/service/game.service.interface';
import { WWPlayer } from '../domain/player.entity';
import { WWGame } from '../domain/game.entity';

export abstract class IWWGameService extends ICommonGameService<
  WWGame,
  WWPlayer
> {
  abstract resetGame(game: WWGame): void;
}
