import { WWGame } from '../domain/game.entity';
import { WWPlayer } from '../domain/player.entity';
import { Role, WWRole } from '../domain/role.entity';

export abstract class IWWRoleService {
  abstract getDefaultRoleAssignment(game: WWGame): WWRole[];

  abstract getRoleAmount(roles, roleName): number;

  /**
   * Returns the first player with the given role.
   * Alive players are prioritized.
   * @param game
   * @param role
   */
  abstract getPlayerWithRole(game: WWGame, role: Role): WWPlayer;

  /**
   * Returns all players with the given role.
   * @param game
   * @param role
   */
  abstract getPlayersWithRole(game: WWGame, role: Role): WWPlayer[];
}
