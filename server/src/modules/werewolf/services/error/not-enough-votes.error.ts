import { UpgsError } from '../../../../common/error/upgs.error';
import { WWGame } from '../../domain/game.entity';

export class NotEnoughVotesError extends UpgsError {
  constructor(game: WWGame) {
    super(
      'NOT_ENOUGH_VOTES',
      'Not enough votes to close the voting round',
      'You have not voted for enough players',
      game,
    );
  }
}
