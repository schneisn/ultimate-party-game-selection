import { UpgsError } from '../../../../common/error/upgs.error';
import { WWGame } from '../../domain/game.entity';

export class InvalidGameStateError extends UpgsError {
  constructor(game: WWGame) {
    super(
      'INVALID_GAME_STATE',
      'The current game state is invalid for the given event',
      'Invalid game state for current action',
      game,
    );
  }
}
