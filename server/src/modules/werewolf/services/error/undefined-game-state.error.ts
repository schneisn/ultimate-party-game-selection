import { UpgsError } from '../../../../common/error/upgs.error';
import { WWGameState } from '../../domain/state.entity';

export class UndefinedGameStateError extends UpgsError {
  constructor(entry: WWGameState) {
    super(
      'UNDEFINED_GAME_STATE',
      'Game state is undefined',
      'The current game state is invalid. The action cannot be performed',
      entry,
    );
  }
}
