import { UpgsError } from '../../../../common/error/upgs.error';
import { WWGame } from '../../domain/game.entity';

export class NoTargetError extends UpgsError {
  constructor(game: WWGame) {
    super(
      'NO_TARGET',
      'No target was provided',
      'No target was selected',
      game,
    );
  }
}
