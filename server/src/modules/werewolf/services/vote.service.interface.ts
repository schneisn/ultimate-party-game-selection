import { WWGame } from '../domain/game.entity';
import { WWPlayer } from '../domain/player.entity';

export abstract class IWWVoteService {
  /**
   * The player with the given id votes for the player with the target id
   * @param game
   * @param voterId
   * @param targetId
   */
  abstract vote(game: WWGame, voterId: string, targetId: string): void;

  /**
   * Gets the player with the most votes.
   * If there is no player with the id that has the most votes, null is returned.
   * This is the case when the vote allows selecting no player
   * @param game
   * @private
   */
  abstract getVoteWinner(game: WWGame): WWPlayer;

  abstract resetVotes(game: WWGame): void;
}
