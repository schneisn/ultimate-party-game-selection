import { IGameRepository } from '../../common/game/repository/game.repository.interface';
import { NewGameDto } from '../../common/game/service/dto/new-game.dto';
import { CommonGameService } from '../../common/game/service/game.service';
import { WWPlayer } from '../domain/player.entity';
import { IWWPlayerService } from './player.service.interface';
import { WWGameState } from '../domain/state.entity';
import { IWWGameService } from './game.service.interface';
import { GameState, WWGame } from '../domain/game.entity';
import { Injectable } from '@nestjs/common';

@Injectable()
export class WWGameService extends CommonGameService<WWGame, WWPlayer>
  implements IWWGameService {
  constructor(
    private readonly playerService: IWWPlayerService,
    readonly gameRepository: IGameRepository<WWGame>,
  ) {
    super(gameRepository);
  }

  getAllOpen(playerId: string): WWGame[] {
    return this.gameRepository
      .findAll()
      .filter(
        (game) =>
          this.playerService.gameContainsPlayerId(game, playerId) ||
          game.states.length < 2,
      );
  }

  newGame(playerName: string): NewGameDto {
    const game = new WWGame();
    const player = this.playerService.addPlayer(game, null, playerName);
    this.gameRepository.save(game);
    return new NewGameDto(game, player);
  }

  getGameOfPlayer(playerId: string): WWGame {
    for (const game of this.gameRepository.findAll()) {
      for (const player of game.players) {
        if (player.id === playerId) {
          return game;
        }
      }
      if (game.gamemaster?.id === playerId) {
        return game;
      }
    }
    return null;
  }

  resetGame(game: WWGame): void {
    game.states = [];
    game.states.push(new WWGameState(GameState.LOBBY, 1));
    game.mayor = null;
    game.votes = [];
    game.players.forEach((player: WWPlayer) => {
      player.isAlive = true;
      player.role = null;
    });
    this.gameRepository.save(game);
  }
}
