import { Injectable } from '@nestjs/common';
import { GameState, WWGame } from '../domain/game.entity';
import { IWWPlayerService } from './player.service.interface';
import { WWVote } from '../domain/vote.entity';
import { IWWVoteService } from './vote.service.interface';
import { WWPlayer } from '../domain/player.entity';
import { IGameRepository } from '../../common/game/repository/game.repository.interface';

@Injectable()
export class WWVoteService implements IWWVoteService {
  constructor(
    private readonly playerService: IWWPlayerService,
    private readonly gameRepository: IGameRepository,
  ) {}

  vote(game: WWGame, voterId: string, targetId: string): void {
    const existingVote = this.getExistingVote(game, voterId, targetId);
    if (existingVote) {
      this.deleteVote(game, voterId, targetId);
      return;
    }
    const existingVotes = this.getExistingVotes(game, voterId);
    const maxNrVotes = this.getMaxNrVotesForVoterRole(game, voterId);
    if (maxNrVotes === 1) {
      this.replaceVote(game, voterId, targetId);
    } else if (existingVotes.length < maxNrVotes) {
      this.newVote(game, voterId, targetId);
    }
    this.gameRepository.save(game);
  }

  getVoteWinner(game: WWGame): WWPlayer {
    const votes = new Map<string, number>(); // Map<targetId, nrVotes>

    // Check if gm has voted. Gm vote is veto
    for (const vote of game.votes) {
      if (vote.voterId === game.gamemaster.id) {
        return this.playerService.getPlayer(game, vote.targetId);
      }
    }

    for (const vote of game.votes) {
      const incAmount = vote.voterId === game.mayor ? 1.5 : 1;
      const count = votes.get(vote.targetId)
        ? votes.get(vote.targetId) + incAmount
        : incAmount;
      votes.set(vote.targetId, count);
    }

    let winnerId: string = null;
    for (const targetId of votes.keys()) {
      if (!votes.get(winnerId) || votes.get(targetId) > votes.get(winnerId)) {
        winnerId = targetId;
      }
    }
    return this.playerService.getPlayer(game, winnerId);
  }

  resetVotes(game: WWGame): void {
    game.votes = [];
  }

  private deleteVote(game: WWGame, voterId: string, targetId: string): void {
    game.votes.forEach((vote: WWVote, index: number) => {
      if (vote.voterId === voterId && vote.targetId === targetId) {
        game.votes.splice(index, 1);
        return;
      }
    });
  }

  private getExistingVote(
    game: WWGame,
    voterId: string,
    targetId: string,
  ): WWVote {
    return this.getExistingVotes(game, voterId).find(
      (vote) => vote.targetId === targetId,
    );
  }

  /**
   * Checks how many players can be selected by the current role.
   *
   * The role is selected by checking the history, not by checking the role
   * of the voter, because if the voter is the gamemaster, the role will
   * not be correct
   * @param game
   * @param voterId
   * @private
   */
  private getMaxNrVotesForVoterRole(game: WWGame, voterId: string): number {
    // TODO Directly access history here instead of using history service due to circular dependency
    return game.states.slice().reverse()[0].id === GameState.ARMOR ? 2 : 1;
  }

  private newVote(game: WWGame, voterId: string, targetId: string): void {
    game.votes.push(new WWVote(voterId, targetId));
  }

  private getExistingVotes(game: WWGame, voterId: string): WWVote[] {
    return game.votes.filter((vote) => vote.voterId === voterId);
  }

  private replaceVote(game: WWGame, voterId: string, targetId: string): void {
    game.votes.forEach((vote: WWVote, index: number) => {
      if (vote.voterId === voterId) {
        game.votes.splice(index, 1);
      }
    });
    this.newVote(game, voterId, targetId);
  }
}
