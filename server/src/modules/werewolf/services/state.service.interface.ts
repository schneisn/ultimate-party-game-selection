import { StateEvent, WWGame } from '../domain/game.entity';
import { WWRole } from '../domain/role.entity';
import { WWPlayer } from '../domain/player.entity';
import { WWGameState } from '../domain/state.entity';

export abstract class IWWStateService {
  abstract getCurrentGameState(game: WWGame): WWGameState;

  /**
   * Computes and sets the next state depending on the current state
   * and the current voting result
   * @param game
   */
  abstract computeNextState(game: WWGame): void;

  /**
   * Computes the winner of the votes, adds the respective event
   * to the current state
   * @param game
   * @param voteType Tells the server which event was meant with the vote
   */
  abstract closeVoteAndAddEventToCurrentState(
    game: WWGame,
    voteType: StateEvent,
  ): void;

  abstract assignRoles(game: WWGame, roles: WWRole[]): void;

  abstract getLovePartner(game: WWGame, playerId: string): WWPlayer;

  abstract playerIsLover(game: WWGame, playerId: string): boolean;
}
