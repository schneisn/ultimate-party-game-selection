import { Module } from '@nestjs/common';
import { PGateway } from './gateway/privacy.gateway';
import { IPGameService } from './services/game.service.interface';
import { PGameService } from './services/game.service';
import { IGameRepository } from '../common/game/repository/game.repository.interface';
import { GameRepository } from '../common/game/repository/game.repository';
import { IPGuessService } from './services/guess.service.interface';
import { PGuessService } from './services/guess.service';
import { IPPlayerService } from './services/player.service.interface';
import { PPlayerService } from './services/player.service';
import { IPQuestionService } from './services/question.service.interface';
import { PQuestionService } from './services/question.service';
import { IPRoundService } from './services/round.service.interface';
import { PRoundService } from './services/round.service';

@Module({
  providers: [
    PGateway,
    {
      provide: IPGameService,
      useClass: PGameService,
    },
    {
      provide: IGameRepository,
      useClass: GameRepository,
    },
    {
      provide: IPGuessService,
      useClass: PGuessService,
    },
    {
      provide: IPPlayerService,
      useClass: PPlayerService,
    },
    {
      provide: IPQuestionService,
      useClass: PQuestionService,
    },
    {
      provide: IPRoundService,
      useClass: PRoundService,
    },
  ],
})
export class PrivacyModule {}
