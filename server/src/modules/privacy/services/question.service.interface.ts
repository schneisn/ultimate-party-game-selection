import { PGame } from '../domain/game.entity';

export abstract class IPQuestionService {
  /**
   * Sets the names of the question categories that are played with in the game
   * @param game
   * @param questionCategories
   */
  abstract setQuestionCategories(
    game: PGame,
    questionCategories: string[],
  ): void;

  /**
   * Retrieves a question that was not part of the game before.
   * @param questionCategories
   * @param game
   * @return null when there is no question left, otherwise the question as string
   */
  abstract getUniqueQuestion(questionCategories: string[], game: PGame): string;

  /**
   * Checks whether there are any questions left that can be played
   * in a game
   * @param questionCategories The question categories the game is played with
   * @param existingQuestions The questions that are already in the game
   * @return True if there are questions left for another round, false otherwise
   */
  abstract areQuestionsLeft(
    questionCategories: string[],
    existingQuestions: string[],
  ): boolean;

  /**
   * Gets the names of all categories
   */
  abstract getAllCategoryNames(): string[];
}
