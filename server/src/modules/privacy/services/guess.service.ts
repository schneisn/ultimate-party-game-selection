import { Injectable } from '@nestjs/common';
import { RoundNotDefinedError } from '../../common/game/service/error/round-not-defined.error';
import { PGame } from '../domain/game.entity';
import { IPRoundService } from './round.service.interface';
import { IPGuessService } from './guess.service.interface';
import { PGuess, SelectedCard } from '../domain/guess.entity';
import { PRound } from '../domain/round.entity';
import { IGameRepository } from '../../common/game/repository/game.repository.interface';

@Injectable()
export class PGuessService implements IPGuessService {
  constructor(
    private readonly roundService: IPRoundService,
    private readonly gameRepository: IGameRepository,
  ) {}

  setInput(
    game: PGame,
    playerId: string,
    selectedCard: SelectedCard,
    guess: number,
  ): void {
    const currentRound = this.roundService.getCurrentRound(game);
    if (!currentRound) {
      throw new RoundNotDefinedError(game);
    }
    if (!guess) {
      guess = -1;
    } else if (guess < 1 || game.players.length < 2) {
      guess = 1;
    } else if (guess >= game.players.length) {
      guess = game.players.length - 1;
    }

    const newGuess = new PGuess(playerId, guess, selectedCard);
    const guessInputIndex = currentRound.guessInputs.findIndex(
      (gi) => gi.playerId === playerId,
    );
    if (guessInputIndex > -1) {
      // Player has already guessed -> Update existing guess
      currentRound.guessInputs[guessInputIndex] = newGuess;
    } else {
      // Player has not yet guessed
      currentRound.guessInputs.push(newGuess);
    }

    this.computeScoreIfRoundIsComplete(currentRound);
    this.gameRepository.save(game);
  }

  removeInput(game: PGame, playerId: string): void {
    const currentRound = this.roundService.getCurrentRound(game);
    if (!currentRound) {
      throw new RoundNotDefinedError(game);
    }
    const playersGuessIndex = currentRound.guessInputs.findIndex(
      (gi) => gi.playerId === playerId,
    );
    if (playersGuessIndex < 0) {
      return;
    }
    currentRound.guessInputs.splice(playersGuessIndex, 1);
    this.computeScoreIfRoundIsComplete(currentRound);
  }

  private computeScoreIfRoundIsComplete(currentRound: PRound): void {
    if (this.roundService.isRoundComplete(currentRound)) {
      currentRound.isLocked = true;
      this.roundService.computeScore(currentRound);
    }
  }
}
