import { PlayerNotDefinedError } from '../../common/player/service/error/player-not-defined.error';
import { CommonPlayerService } from '../../common/player/service/player.service';
import { IPGameService } from './game.service.interface';
import { PGame } from '../domain/game.entity';
import { PPlayer } from '../domain/player.entity';
import { IPPlayerService } from './player.service.interface';
import { IPGuessService } from './guess.service.interface';
import { IPRoundService } from './round.service.interface';
import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { IGameRepository } from '../../common/game/repository/game.repository.interface';

@Injectable()
export class PPlayerService extends CommonPlayerService<PGame, PPlayer>
  implements IPPlayerService {
  constructor(
    private readonly gameService: IPGameService,
    @Inject(forwardRef(() => IPRoundService))
    private readonly roundService: IPRoundService,
    private readonly guessService: IPGuessService,
    readonly gameRepository: IGameRepository,
  ) {
    super(gameRepository);
  }

  addPlayer(game: PGame, playerId: string, playerName: string): PPlayer {
    playerName = this.addNrToPlayerNameIfExists(game, playerId, playerName);
    const existingPlayer = this.replaceExistingPlayer(
      game,
      playerId,
      playerName,
    );
    if (existingPlayer) {
      this.gameRepository.save(game);
      return existingPlayer;
    }
    const newPlayer = new PPlayer(null, playerName);
    game.players.push(newPlayer);
    this.gameRepository.save(game);
    return newPlayer;
  }

  setPlayerReadyState(game: PGame, playerId: string, isReady: boolean): void {
    const player = game.players.find((p) => p.id === playerId);
    if (!player) {
      throw new PlayerNotDefinedError(playerId);
    }
    player.isReady = isReady;
    if (this.allPlayersReady(game)) {
      this.resetReadyPlayers(game);
      this.roundService.newRound(game);
    }
    this.gameRepository.save(game);
  }

  disconnectPlayer(game: PGame, playerId: string) {
    const player = this.getPlayer(game, playerId);
    if (!player) {
      throw new PlayerNotDefinedError(playerId);
    }
    player.isConnected = false;
    player.isReady = false;
    if (this.noPlayersAreConnected(game)) {
      this.gameService.scheduleGameForRemoval(game.id);
      return;
    }
    const currentRound = this.roundService.getCurrentRound(game);
    if (!currentRound) {
      super.disconnectPlayer(game, playerId);
    } else {
      const participatedInRound = this.roundService.playerParticipatesInRound(
        currentRound,
        player,
      );
      if (!currentRound.isLocked && participatedInRound) {
        this.guessService.removeInput(game, playerId);
        this.roundService.resetCurrentRound(game);
      }
      if (this.atMostOnePlayerConnected(game)) {
        this.gameService.scheduleGameForRemoval(game.id);
        return;
      }
      this.gameRepository.save(game);
      return;
    }
    if (this.allPlayersReady(game) && !this.atMostOnePlayerConnected(game)) {
      this.resetReadyPlayers(game);
      this.roundService.newRound(game);
    }
    this.gameRepository.save(game);
  }

  private allPlayersReady(game: PGame): boolean {
    const round = this.roundService.getCurrentRound(game);
    // Lobby
    if (!round) {
      return game.players.every((player) => player.isReady);
    }
    // Check the ready state of the players in a round
    return round.guessInputs?.every((guess) => {
      const player = this.getPlayer(game, guess.playerId);
      return player.isReady || !player.isConnected;
    });
  }

  private resetReadyPlayers(game: PGame): void {
    game.players.forEach((player) => (player.isReady = false));
  }
}
