import { PGame } from '../domain/game.entity';
import { PRound } from '../domain/round.entity';
import { PPlayer } from '../domain/player.entity';

export abstract class IPRoundService {
  abstract newRound(game: PGame): PRound;

  abstract getCurrentRound(game: PGame): PRound;

  abstract resetCurrentRound(game: PGame): void;

  /**
   * Checks whether the round is complete. A round is complete
   * when all players that are connected have guessed
   * @param round
   */
  abstract isRoundComplete(round: PRound): boolean;

  abstract computeScore(round: PRound): void;

  /**
   * Checks whether the player is actively playing in a round.
   * Waiting for the next round or being disconnected does not
   * count as actively playing.
   * @param round
   * @param player
   */
  abstract playerParticipatesInRound(round: PRound, player: PPlayer): boolean;
}
