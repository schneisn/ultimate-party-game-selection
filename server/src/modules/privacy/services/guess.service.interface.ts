import { PGame } from '../domain/game.entity';
import { SelectedCard } from '../domain/guess.entity';

export abstract class IPGuessService {
  /**
   * Adds or updates the guess input of the player with the given id.
   * If all players have guessed, the score for this round is computed.
   * @param game
   * @param playerId
   * @param selectedCard
   * @param guess
   */
  abstract setInput(
    game: PGame,
    playerId: string,
    selectedCard: SelectedCard,
    guess: number,
  ): void;

  /**
   * Removes the guess input of the player with the given id from
   * the current round. If all other players have answered, the
   * score is computed.
   * @param game
   * @param playerId
   */
  abstract removeInput(game: PGame, playerId: string): void;
}
