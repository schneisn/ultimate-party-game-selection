import { Injectable } from '@nestjs/common';
import { PGame } from '../domain/game.entity';
import { PGuess, SelectedCard } from '../domain/guess.entity';
import { IPQuestionService } from './question.service.interface';
import { PRound } from '../domain/round.entity';
import { IPRoundService } from './round.service.interface';
import { PPlayer } from '../domain/player.entity';
import { IGameRepository } from '../../common/game/repository/game.repository.interface';

@Injectable()
export class PRoundService implements IPRoundService {
  constructor(
    private readonly questionService: IPQuestionService,
    private readonly gameRepository: IGameRepository,
  ) {}

  newRound(game: PGame): PRound {
    const question = this.questionService.getUniqueQuestion(
      game.questionCategories,
      game,
    );
    const round = new PRound(game.rounds.length, question);
    const gameQuestions = game.rounds
      .map((rnd) => rnd.question)
      .concat(question);
    round.isLastRound = !this.questionService.areQuestionsLeft(
      game.questionCategories,
      gameQuestions,
    );
    this.addParticipatingPlayers(game, round);
    game.rounds.push(round);
    this.gameRepository.save(game);
    return round;
  }

  getCurrentRound(game: PGame): PRound {
    return game.rounds.length > 0 ? game.rounds[game.rounds.length - 1] : null;
  }

  resetCurrentRound(game: PGame): void {
    const round = this.getCurrentRound(game);
    round.guessInputs = [];
    this.addParticipatingPlayers(game, round);
  }

  isRoundComplete(round: PRound): boolean {
    if (!round || round.guessInputs.length < 1) {
      return false;
    }
    return round.guessInputs.every((guess) => guess.guess > 0);
  }

  computeScore(round: PRound): void {
    const totalYes = this.getTotalYes(round);
    round.guessInputs
      .filter((guessIn) => guessIn.guess === totalYes)
      .forEach((guessIn) => (guessIn.score = 3));
    round.guessInputs
      .filter(
        (guessIn) =>
          guessIn.guess === totalYes - 1 || guessIn.guess === totalYes + 1,
      )
      .forEach((guessIn) => (guessIn.score = 1));
  }

  playerParticipatesInRound(round: PRound, player: PPlayer): boolean {
    return round?.guessInputs.some(
      (guessInput) => guessInput.playerId === player?.id,
    );
  }

  private getTotalYes(round: PRound): number {
    return round?.guessInputs.reduce(
      (total: number, current: PGuess) =>
        total + (current.selectedCard === SelectedCard.YES ? 1 : 0),
      0,
    );
  }

  /**
   * Defines which players are allowed to participate in a round by adding dummy guesses to the round.
   * @param game
   * @param round
   * @private
   */
  private addParticipatingPlayers(game: PGame, round: PRound): void {
    game.players
      .filter((player) => player.isConnected)
      .forEach((player) => round.guessInputs.push(new PGuess(player.id, -1)));
  }
}
