import { IGameRepository } from '../../common/game/repository/game.repository.interface';
import { NewGameDto } from '../../common/game/service/dto/new-game.dto';
import { CommonGameService } from '../../common/game/service/game.service';
import { PPlayer } from '../domain/player.entity';
import { IPPlayerService } from './player.service.interface';
import { IPQuestionService } from './question.service.interface';
import { IPRoundService } from './round.service.interface';
import { IPGameService } from './game.service.interface';
import { PGame } from '../domain/game.entity';
import { Injectable } from '@nestjs/common';

@Injectable()
export class PGameService extends CommonGameService<PGame, PPlayer>
  implements IPGameService {
  constructor(
    readonly playerService: IPPlayerService,
    readonly roundService: IPRoundService,
    readonly questionService: IPQuestionService,
    readonly gameRepository: IGameRepository<PGame>,
  ) {
    super(gameRepository);
  }

  newGame(playerName: string): NewGameDto {
    const game = new PGame(this.questionService.getAllCategoryNames());
    const player = this.playerService.addPlayer(game, null, playerName);
    this.gameRepository.save(game);
    return new NewGameDto(game, player);
  }

  resetGame(game: PGame): void {
    game.rounds = [];
  }
}
