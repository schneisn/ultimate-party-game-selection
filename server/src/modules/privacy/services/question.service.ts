import { Injectable } from '@nestjs/common';
import { IFileService } from '../../file/file.service.interface';
import { PGame } from '../domain/game.entity';
import { QuestionCategoryNotDefinedError } from './error/question-category-not-defined.error';
import { QuestionCategory } from '../domain/category.entity';
import { IPQuestionService } from './question.service.interface';
import { IGameRepository } from '../../common/game/repository/game.repository.interface';

@Injectable()
export class PQuestionService implements IPQuestionService {
  private readonly questionCategories: QuestionCategory[] = [];

  constructor(
    private readonly fileService: IFileService,
    private readonly gameRepository: IGameRepository,
  ) {
    this.questionCategories = this.fileService.readJSONFromDir<
      QuestionCategory
    >('assets/privacy');
  }

  private static gameContainsQuestion(game: PGame, question: string): boolean {
    return game.rounds.some((round) => round.question === question);
  }

  getUniqueQuestion(questionCategories: string[], game: PGame): string {
    const gameQuestions = game.rounds.map((rnd) => rnd.question);
    if (!this.areQuestionsLeft(questionCategories, gameQuestions)) {
      return null;
    }
    let question = this.chooseRandomQuestion(questionCategories);
    while (PQuestionService.gameContainsQuestion(game, question)) {
      question = this.chooseRandomQuestion(questionCategories);
    }
    return question;
  }

  setQuestionCategories(game: PGame, questionCategories: string[]): void {
    game.questionCategories = questionCategories;
    this.gameRepository.save(game);
  }

  areQuestionsLeft(
    questionCategories: string[],
    existingQuestions: string[],
  ): boolean {
    const questionsOfQuestionCategories = this.questionCategories
      .filter((qc) => questionCategories.includes(qc.name))
      .reduce(
        (questions: string[], qc: QuestionCategory) =>
          questions.concat(qc.questions),
        [],
      );
    return questionsOfQuestionCategories.length > existingQuestions.length;
  }

  getAllCategoryNames(): string[] {
    const categoryNames: string[] = [];
    for (const category of this.questionCategories) {
      categoryNames.push(category.name);
    }
    return categoryNames;
  }

  private chooseRandomQuestion(questionCategories: string[]): string {
    const randomCategoryName =
      questionCategories[Math.floor(Math.random() * questionCategories.length)];
    const randomCategory = this.getCategory(randomCategoryName);
    if (!randomCategory) {
      throw new QuestionCategoryNotDefinedError(randomCategoryName);
    }
    return randomCategory.questions[
      Math.floor(Math.random() * randomCategory.questions.length)
    ];
  }

  private getCategory(categoryName: string): QuestionCategory {
    return this.questionCategories.find(
      (category) => category.name === categoryName,
    );
  }
}
