import { ICommonPlayerService } from '../../common/player/service/player.service.interface';
import { PGame } from '../domain/game.entity';
import { PPlayer } from '../domain/player.entity';

export abstract class IPPlayerService extends ICommonPlayerService<PPlayer> {
  abstract setPlayerReadyState(
    game: PGame,
    playerId: string,
    isReady: boolean,
  ): void;
}
