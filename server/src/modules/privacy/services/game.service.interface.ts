import { ICommonGameService } from '../../common/game/service/game.service.interface';
import { PPlayer } from '../domain/player.entity';
import { PGame } from '../domain/game.entity';

export abstract class IPGameService extends ICommonGameService<PGame, PPlayer> {
  abstract resetGame(game: PGame): void;
}
