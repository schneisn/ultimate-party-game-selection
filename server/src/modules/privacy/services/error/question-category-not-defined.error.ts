import { UpgsError } from '../../../../common/error/upgs.error';

export class QuestionCategoryNotDefinedError extends UpgsError {
  constructor(questionCategory: string) {
    super(
      'QUESTION_CATEGORY_NOT_DEFINED',
      'Question category does not exist',
      'This category does not exist',
      questionCategory,
    );
  }
}
