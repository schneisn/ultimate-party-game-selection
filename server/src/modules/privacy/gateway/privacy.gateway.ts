import {
  ConnectedSocket,
  MessageBody,
  SubscribeMessage,
  WebSocketGateway,
} from '@nestjs/websockets';
import { ISessionService } from '../../session/service/session.service.interface';
import { CommonGateway } from '../../common/gateway/common.gateway';
import { SetPlayerReadyStateRequest } from '../../common/gateway/event/request/set-player-ready-state.request';
import { StartNextRoundRequest } from '../../common/gateway/event/request/start-next-round.request';
import { SetPlayerReadyStateEvent } from '../../common/gateway/event/set-player-ready-state.event';
import { StartNextRoundEvent } from '../../common/gateway/event/start-next-round.event';
import { LoggerService } from '../../logger/logger.service';
import { IPGameService } from '../services/game.service.interface';
import { IPGuessService } from '../services/guess.service.interface';
import { IPPlayerService } from '../services/player.service.interface';
import { IPQuestionService } from '../services/question.service.interface';
import { IPRoundService } from '../services/round.service.interface';
import { GetQuestionCategoriesEvent } from './event/get-question-categories.event';
import { SetInputRequest } from './event/request/set-input.request';
import { SetQuestionCategoriesRequest } from './event/request/set-question-categories.request';
import { SetInputEvent } from './event/set-input.event';
import { SetQuestionCategoriesEvent } from './event/set-question-categories.event';
import { SerializableWsResponse } from '../../common/gateway/event/serializable-ws-response.class';
import SocketIO from 'socket.io';
import { ResetRoundEvent } from './event/reset-round.event';
import { PGame } from '../domain/game.entity';
import { UseInterceptors, UsePipes } from '@nestjs/common';
import { GetGamePipe } from '../../../pipes/get-game.pipe';
import { ConfigService } from '../../config/config.service';
import { GetPlayerInterceptor } from '../../../interceptors/get-player.interceptor';

@WebSocketGateway({ namespace: 'privacy', cors: true })
export class PGateway extends CommonGateway {
  constructor(
    readonly logger: LoggerService,
    readonly gameService: IPGameService,
    readonly playerService: IPPlayerService,
    readonly sessionService: ISessionService,
    readonly questionService: IPQuestionService,
    readonly roundService: IPRoundService,
    readonly guessService: IPGuessService,
  ) {
    super('privacy', logger, gameService, playerService, sessionService);
  }

  @SubscribeMessage(GetQuestionCategoriesEvent.id)
  onQuestionCategory(): SerializableWsResponse<GetQuestionCategoriesEvent> {
    const categoryNames = this.questionService.getAllCategoryNames();
    return new SerializableWsResponse(
      GetQuestionCategoriesEvent.id,
      new GetQuestionCategoriesEvent(categoryNames),
    );
  }

  @UsePipes(GetGamePipe)
  @SubscribeMessage(SetQuestionCategoriesEvent.id)
  onSetQuestionCategories(
    @MessageBody() data: SetQuestionCategoriesRequest,
  ): void {
    this.questionService.setQuestionCategories(
      data.game,
      data.questionCategories,
    );
    this.emitGame(data.game.id);
  }

  @UsePipes(GetGamePipe)
  @UseInterceptors(GetPlayerInterceptor)
  @SubscribeMessage(SetInputEvent.id)
  onInput(@MessageBody() data: SetInputRequest): void {
    this.guessService.setInput(
      data.game,
      data.player.id,
      data.selectedCard,
      data.guess,
    );
    this.emitGame(data.game.id);
  }

  @UsePipes(GetGamePipe)
  @UseInterceptors(GetPlayerInterceptor)
  @SubscribeMessage(SetPlayerReadyStateEvent.id)
  onReady(@MessageBody() data: SetPlayerReadyStateRequest<PGame>): void {
    this.playerService.setPlayerReadyState(
      data.game,
      data.player.id,
      data.isReady,
    );
    this.emitGame(data.game.id);
  }

  @SubscribeMessage(StartNextRoundEvent.id)
  onStartNextRound(
    @ConnectedSocket() client,
    @MessageBody(GetGamePipe) data: StartNextRoundRequest<PGame>,
  ): void {
    this.roundService.newRound(data.game);
    this.emitGame(data.game.id);
  }

  handleDisconnect(client: SocketIO.Socket) {
    const disconnectedPlayerId = this.sessionService.getPlayerId(client.id);
    const game = this.gameService.getGameOfPlayer(disconnectedPlayerId);
    if (!game) {
      return;
    }
    const currentRound = this.roundService.getCurrentRound(game);
    const disconnectedPlayer = this.playerService.getPlayer(
      game,
      disconnectedPlayerId,
    );
    // Check if the player participated in the round before removing the guess of the player
    const playerParticipatedInRound = this.roundService.playerParticipatesInRound(
      currentRound,
      disconnectedPlayer,
    );
    // First handle the normal disconnect. Otherwise if the score of the round is computed,
    // because the last player that had yet to answer left the game, the player will see
    // the reset-round message popping up for a millisecond because it would be sent before
    // the game-update.
    super.handleDisconnect(client);
    if (disconnectedPlayer && playerParticipatedInRound) {
      this.publishEventToRoom(
        game.id,
        ResetRoundEvent.id,
        new ResetRoundEvent(disconnectedPlayer),
      );
    }
  }
}
