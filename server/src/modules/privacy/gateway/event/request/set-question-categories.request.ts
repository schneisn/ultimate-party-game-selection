import { IsArray } from 'class-validator';
import { GameRequest } from '../../../../common/gateway/event/request/game.request';
import { PGame } from '../../../domain/game.entity';

export class SetQuestionCategoriesRequest extends GameRequest<PGame> {
  @IsArray()
  questionCategories: string[];
}
