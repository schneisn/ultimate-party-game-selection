import { IsNumber, IsOptional, IsString } from 'class-validator';
import { GameRequest } from '../../../../common/gateway/event/request/game.request';
import { SelectedCard } from '../../../domain/guess.entity';
import { PGame } from '../../../domain/game.entity';

export class SetInputRequest extends GameRequest<PGame> {
  @IsOptional()
  @IsString()
  selectedCard: SelectedCard;

  @IsOptional()
  @IsNumber()
  guess: number;
}
