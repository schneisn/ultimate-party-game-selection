import { UpgsEvent } from '../../../common/gateway/event/upgs.event';

export class GetQuestionCategoriesEvent extends UpgsEvent {
  static readonly id = 'GetQuestionCategories';
  questionCategories: string[];

  constructor(questionCategories: string[]) {
    super();
    this.questionCategories = questionCategories;
  }
}
