import { UpgsEvent } from '../../../common/gateway/event/upgs.event';

export class SetInputEvent extends UpgsEvent {
  static readonly id = 'SetInput';
}
