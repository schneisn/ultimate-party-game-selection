import { UpgsEvent } from '../../../common/gateway/event/upgs.event';

export class SetQuestionCategoriesEvent extends UpgsEvent {
  static readonly id = 'SetQuestionCategories';
}
