import { UpgsEvent } from '../../../common/gateway/event/upgs.event';
import { PlayerEventModel } from '../../../common/gateway/event/model/player.event-model';

export class ResetRoundEvent extends UpgsEvent {
  static readonly id = 'ResetRound';
  player: PlayerEventModel;

  constructor(player: PlayerEventModel) {
    super();
    this.player = player;
  }
}
