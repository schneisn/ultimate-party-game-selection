import { Game } from '../../common/game/domain/game.entity';
import { PPlayer } from './player.entity';
import { PRound } from './round.entity';

export class PGame extends Game<PPlayer> {
  rounds: PRound[] = [];
  /**
   * The selected questionCategories
   */
  questionCategories: string[] = [];

  constructor(questionCategories: string[]) {
    super();
    this.questionCategories = questionCategories;
  }
}
