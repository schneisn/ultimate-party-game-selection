import { Exclude } from 'class-transformer';

export enum SelectedCard {
  YES = 'yes',
  NO = 'no',
}

export class PGuess {
  readonly playerId: string;
  /**
   * The guess how many players have answered yes
   */
  guess: number;
  @Exclude()
  selectedCard: SelectedCard;
  score: number;

  constructor(playerId: string, guess: number, selectedCard?: SelectedCard) {
    this.playerId = playerId;
    this.guess = guess;
    this.selectedCard = selectedCard ? selectedCard : null;
    this.score = 0;
  }
}
