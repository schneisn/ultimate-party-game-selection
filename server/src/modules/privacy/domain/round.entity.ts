import { PGuess } from './guess.entity';
import { Expose } from 'class-transformer';

export class PRound {
  readonly number: number;
  readonly question: string;
  guessInputs: PGuess[];
  isLastRound: boolean;
  isLocked: boolean;

  constructor(number: number, question: string, isLastRound: boolean = false) {
    this.number = number;
    this.question = question;
    this.guessInputs = [];
    this.isLastRound = isLastRound;
    this.isLocked = false;
  }

  @Expose()
  get yesVotes(): number {
    return this.guessInputs.reduce(
      (previousValue: number, guess: PGuess) =>
        previousValue + (guess.selectedCard === 'yes' ? 1 : 0),
      0,
    );
  }
}
