import {
  MessageBody,
  SubscribeMessage,
  WebSocketGateway,
} from '@nestjs/websockets';
import { ISessionService } from '../../session/service/session.service.interface';
import { CommonGateway } from '../../common/gateway/common.gateway';
import { SetPlayerReadyStateRequest } from '../../common/gateway/event/request/set-player-ready-state.request';
import { SetPlayerReadyStateEvent } from '../../common/gateway/event/set-player-ready-state.event';
import { UpdateSettingsEvent } from '../../common/gateway/event/update-settings.event';
import { LoggerService } from '../../logger/logger.service';
import { ICAnswerService } from '../services/answer.service.interface';
import { ICGameService } from '../services/game.service.interface';
import { ICPlayerService } from '../services/player.service.interface';
import { ICRoundService } from '../services/round.service.interface';
import { ICVoteService } from '../services/vote.service.interface';
import { AddAnswersEvent } from './event/add-answers.event';
import { AnswerTimeoutCountdownEvent } from './event/answer-timeout-countdown.event';
import { CollectAnswersEvent } from './event/collect-answers.event';
import { AddAnswerRequest } from './event/request/add-answer.request';
import { UpdateCategoriesRequest } from './event/request/update-categories.request';
import { UpdateSettingsRequest } from './event/request/update-settings.request';
import { VoteInvalidAnswersRequest } from './event/request/vote-invalid-answers.request';
import { UpdateCategoriesEvent } from './event/update-categories.event';
import { VoteInvalidAnswersEvent } from './event/vote-invalid-answers.event';
import { CGame } from '../domain/game.entity';
import { UseInterceptors, UsePipes } from '@nestjs/common';
import { GetGamePipe } from '../../../pipes/get-game.pipe';
import { ConfigService } from '../../config/config.service';
import { GetPlayerInterceptor } from '../../../interceptors/get-player.interceptor';

@WebSocketGateway({ namespace: 'categories', cors: true })
export class CGateway extends CommonGateway {
  constructor(
    readonly logger: LoggerService,
    readonly gameService: ICGameService,
    readonly playerService: ICPlayerService,
    readonly sessionService: ISessionService,
    readonly answerService: ICAnswerService,
    readonly roundService: ICRoundService,
    readonly voteService: ICVoteService,
  ) {
    super('categories', logger, gameService, playerService, sessionService);
  }

  @UsePipes(GetGamePipe)
  @UseInterceptors(GetPlayerInterceptor)
  @SubscribeMessage(SetPlayerReadyStateEvent.id)
  onSetPlayerIsReady(
    @MessageBody() data: SetPlayerReadyStateRequest<CGame>,
  ): void {
    this.playerService.setPlayerReadyState(
      data.game,
      data.player.id,
      data.isReady,
    );
    this.emitGame(data.game.id);
    this.emitOpenGames();
  }

  @UsePipes(GetGamePipe)
  @UseInterceptors(GetPlayerInterceptor)
  @SubscribeMessage(AddAnswersEvent.id)
  onAddAnswer(@MessageBody() data: AddAnswerRequest): void {
    const isFirstAnswer =
      this.roundService.getCurrentRound(data.game).answers.length === 0;
    // Add 2000ms to the timer because the actual lock time is also 2 seconds after the games answer timeout
    const collectAnswersTimeInMs = data.game.answerTimeout * 1000 + 2000;
    this.answerService.addAnswers(data.game, data.player.id, data.answers);
    if (!isFirstAnswer) {
      return;
    }
    setTimeout(() => {
      this.publishEventToRoom(data.game.id, CollectAnswersEvent.id);
      // Poll until the service locked the round and then send the game with
      // all answers to clients
      const lockedTimer = setInterval(() => {
        // Emit game when round is locked
        const upToDateGame = this.gameService.get(data.game.id);
        if (this.roundService.isRoundLocked(upToDateGame)) {
          clearInterval(lockedTimer);
          this.emitGame(upToDateGame.id);
        }
      }, 200);
    }, collectAnswersTimeInMs);
    const countdownTimer = setInterval(() => {
      const remainingSeconds = this.roundService.getRemainingTimeInSeconds(
        data.game,
      );
      this.publishEventToRoom(
        data.game.id,
        AnswerTimeoutCountdownEvent.id,
        new AnswerTimeoutCountdownEvent(remainingSeconds),
      );
      if (remainingSeconds <= 0) {
        clearInterval(countdownTimer);
      }
    }, 1000);
  }

  @UsePipes(GetGamePipe)
  @SubscribeMessage(UpdateCategoriesEvent.id)
  onUpdateCategories(@MessageBody() data: UpdateCategoriesRequest): void {
    this.gameService.updateCategories(data.game, data.categories);
    this.emitGame(data.game.id);
  }

  @UsePipes(GetGamePipe)
  @SubscribeMessage(UpdateSettingsEvent.id)
  onUpdateSettings(@MessageBody() data: UpdateSettingsRequest): void {
    this.gameService.updateSettings(data.game, data.settings);
    this.emitGame(data.game.id);
  }

  @UsePipes(GetGamePipe)
  @UseInterceptors(GetPlayerInterceptor)
  @SubscribeMessage(VoteInvalidAnswersEvent.id)
  onVoteInvalidAnswers(@MessageBody() data: VoteInvalidAnswersRequest): void {
    this.voteService.voteInvalidAnswers(
      data.game,
      data.player.id,
      data.invalidAnswers,
    );
    this.emitGame(data.game.id);
  }
}
