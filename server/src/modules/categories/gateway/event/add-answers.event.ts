import { UpgsEvent } from '../../../common/gateway/event/upgs.event';

export class AddAnswersEvent extends UpgsEvent {
  static readonly id = 'AddAnswers';
}
