import { UpgsEvent } from '../../../common/gateway/event/upgs.event';

export class UpdateCategoriesEvent extends UpgsEvent {
  static readonly id = 'UpdateCategories';
}
