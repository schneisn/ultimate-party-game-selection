import { ValidateNested } from 'class-validator';
import { GameRequest } from '../../../../common/gateway/event/request/game.request';
import { Settings } from './model/settings.model';
import { CGame } from '../../../domain/game.entity';

export class UpdateSettingsRequest extends GameRequest<CGame> {
  @ValidateNested()
  settings: Settings;
}
