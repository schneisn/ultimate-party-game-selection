import { IsBoolean } from 'class-validator';

export class Settings {
  @IsBoolean()
  gameModeNoScore: boolean;
}
