import { IsString } from 'class-validator';

export class CAnswer {
  @IsString()
  category: string;

  @IsString()
  input: string;
}
