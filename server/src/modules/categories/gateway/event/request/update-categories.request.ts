import { IsArray } from 'class-validator';
import { GameRequest } from '../../../../common/gateway/event/request/game.request';
import { CGame } from '../../../domain/game.entity';

export class UpdateCategoriesRequest extends GameRequest<CGame> {
  @IsArray()
  categories: string[];
}
