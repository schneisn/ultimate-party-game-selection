import { ValidateNested } from 'class-validator';
import { GameRequest } from '../../../../common/gateway/event/request/game.request';
import { CAnswer } from './model/answer.request-model';
import { CGame } from '../../../domain/game.entity';

export class VoteInvalidAnswersRequest extends GameRequest<CGame> {
  @ValidateNested()
  invalidAnswers: CAnswer[];
}
