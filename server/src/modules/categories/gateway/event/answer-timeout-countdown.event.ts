import { UpgsEvent } from '../../../common/gateway/event/upgs.event';

export class AnswerTimeoutCountdownEvent extends UpgsEvent {
  static readonly id = 'AnswerTimeoutCountdown';
  remainingTimeInSeconds: number;

  constructor(remainingTimeInSeconds: number) {
    super();
    this.remainingTimeInSeconds = remainingTimeInSeconds;
  }
}
