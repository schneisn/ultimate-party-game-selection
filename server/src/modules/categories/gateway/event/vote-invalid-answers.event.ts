import { UpgsEvent } from '../../../common/gateway/event/upgs.event';

export class VoteInvalidAnswersEvent extends UpgsEvent {
  static readonly id = 'VoteInvalidAnswers';
}
