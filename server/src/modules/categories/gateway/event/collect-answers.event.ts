import { UpgsEvent } from '../../../common/gateway/event/upgs.event';

export class CollectAnswersEvent extends UpgsEvent {
  static readonly id = 'CollectAnswers';
}
