import { CGame } from '../domain/game.entity';
import { VoteInvalidAnswerDto } from './dto/vote-invalid-answer.dto';

export abstract class ICVoteService {
  abstract voteInvalidAnswers(
    game: CGame,
    playerId: string,
    invalidAnswers: VoteInvalidAnswerDto[],
  ): void;

  abstract removePlayerVotes(game: CGame, playerId: string): void;
}
