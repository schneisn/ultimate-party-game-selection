import { ICommonPlayerService } from '../../common/player/service/player.service.interface';
import { CGame } from '../domain/game.entity';
import { CPlayer } from '../domain/player.entity';

export abstract class ICPlayerService extends ICommonPlayerService<CPlayer> {
  abstract setPlayerReadyState(
    game: CGame,
    playerId: string,
    isReady: boolean,
  ): void;
}
