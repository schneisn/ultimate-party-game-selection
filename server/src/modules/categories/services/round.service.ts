import { Injectable } from '@nestjs/common';
import { RoundNotDefinedError } from '../../common/game/service/error/round-not-defined.error';
import { ICRoundService } from './round.service.interface';
import { CGame } from '../domain/game.entity';
import { CRound } from '../domain/round.entity';
import { CAnswer } from '../domain/answer.entity';
import { ICPlayerService } from './player.service.interface';

@Injectable()
export class CRoundService implements ICRoundService {
  constructor(private readonly playerService: ICPlayerService) {}

  newRound(game: CGame): CRound {
    const round = new CRound(this.getRandomLetter(game));
    game.rounds.push(round);
    return round;
  }

  getCurrentRound(game: CGame): CRound {
    return game.rounds[game.rounds.length - 1];
  }

  setLockTime(game: CGame): CRound {
    const round = this.getCurrentRound(game);
    if (!round) {
      throw new RoundNotDefinedError(game);
    }
    const lockTime = new Date();
    // 2 extra seconds because otherwise players would see the counter starting at 28 seconds remaining
    lockTime.setSeconds(lockTime.getSeconds() + game.answerTimeout + 2);
    round.lockTime = lockTime;
    return round;
  }

  lockCurrentRound(game: CGame): CRound {
    const round = this.getCurrentRound(game);
    if (!round) {
      throw new RoundNotDefinedError(game);
    }
    round.isLocked = true;
    if (game.gameModeNoScore) {
      // Compute score here because there is no voting for invalid questions
      this.computeScore(game);
    }
    return round;
  }

  isRoundLocked(game: CGame): boolean {
    const currentRound = this.getCurrentRound(game);
    if (!currentRound) {
      throw new RoundNotDefinedError(game);
    }
    return currentRound.isLocked;
  }

  getRemainingTimeInSeconds(game: CGame): number {
    const currentRound = this.getCurrentRound(game);
    if (!currentRound) {
      throw new RoundNotDefinedError(game);
    }
    const currentTime = new Date();
    const lockTime = currentRound.lockTime;
    const timeDifInMs = lockTime.getTime() - currentTime.getTime();
    return Math.abs(Math.floor(timeDifInMs / 1000));
  }

  computeScore(game: CGame): void {
    const round = this.getCurrentRound(game);
    this.markInvalidAnswers(game);
    this.markDuplicates(round.answers);
    this.scoreAnswers(round.answers);
    round.isCounted = true;
  }

  private getRandomLetter(game: CGame): string {
    let newLetter = this.getLetter();
    // Only consider the last 20 rounds when checking for duplicates because
    // there are only 26 letters to choose from and we do not want the
    // same order of letters every time
    const roundsToConsider = game.rounds.slice().reverse().slice(0, 20);
    while (roundsToConsider.find((round) => round.letter === newLetter)) {
      newLetter = this.getLetter();
    }
    return newLetter;
  }

  private getLetter(): string {
    return String.fromCharCode(65 + Math.floor(Math.random() * 26));
  }

  private markInvalidAnswers(game: CGame): void {
    const answers = this.getCurrentRound(game).answers;
    answers.forEach((answer) => {
      const nrActivePlayersInRound = this.getNrActivePlayersInRound(game);
      const nrVotesForInvalidAnswer = nrActivePlayersInRound / 2;
      answer.isValid =
        (!answer.invalidVotes ||
          answer.invalidVotes.length < nrVotesForInvalidAnswer) &&
        answer.input !== '';
    });
  }

  private getNrActivePlayersInRound(game: CGame): number {
    const round = this.getCurrentRound(game);
    return new Set(
      round.answers
        .map((ans: CAnswer) => {
          return this.playerService.getPlayer(game, ans.playerId).isConnected
            ? ans.playerId
            : null;
        })
        .filter((p) => p),
    ).size;
  }

  private markDuplicates(answers: CAnswer[]): void {
    answers.forEach((answer1) => {
      answers.forEach((answer2) => {
        const isDuplicate =
          answer1.playerId !== answer2.playerId &&
          answer1.category === answer2.category &&
          answer1.input.toLocaleLowerCase() ===
            answer2.input.toLocaleLowerCase();
        if (isDuplicate) {
          answer1.isDuplicate = true;
          answer2.isDuplicate = true;
        }
      });
    });
  }

  private scoreAnswers(answers: CAnswer[]): void {
    answers.forEach((answer: CAnswer) => {
      if (!answer.isValid || answer.input === '') {
        answer.score = 0;
        return;
      }
      if (answer.isDuplicate) {
        answer.score = 1;
        return;
      }
      answer.score = 2;
    });
  }
}
