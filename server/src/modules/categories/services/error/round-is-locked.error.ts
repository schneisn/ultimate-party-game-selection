import { UpgsError } from '../../../../common/error/upgs.error';
import { CRound } from '../../domain/round.entity';

export class RoundIsLockedError extends UpgsError {
  constructor(round: CRound) {
    super(
      'ROUND_LOCKED',
      'Cannot add answer. Round is locked',
      'You cannot answer anymore',
      round,
    );
  }
}
