import { CGame } from '../domain/game.entity';
import { AddAnswerDto } from './dto/add-answer.dto';

export abstract class ICAnswerService {
  /**
   * Saves the given answers for the player with the given id.
   *
   * If the player is the first player to submit answer in this round,
   * the time when the round is locked will be set.
   * @param game
   * @param playerId
   * @param answers
   */
  abstract addAnswers(
    game: CGame,
    playerId: string,
    answers: AddAnswerDto[],
  ): void;
}
