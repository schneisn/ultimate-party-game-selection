import { IGameRepository } from '../../common/game/repository/game.repository.interface';
import { NewGameDto } from '../../common/game/service/dto/new-game.dto';
import { CommonGameService } from '../../common/game/service/game.service';
import { CPlayer } from '../domain/player.entity';
import { ICPlayerService } from './player.service.interface';
import { SettingsDto } from './dto/settings.dto';
import { ICGameService } from './game.service.interface';
import { CGame } from '../domain/game.entity';
import { Injectable } from '@nestjs/common';

@Injectable()
export class CGameService extends CommonGameService<CGame, CPlayer>
  implements ICGameService {
  constructor(
    private readonly playerService: ICPlayerService,
    readonly gameRepository: IGameRepository<CGame>,
  ) {
    super(gameRepository);
  }

  newGame(playerName: string): NewGameDto {
    const game = new CGame();
    const player = this.playerService.addPlayer(game, null, playerName);
    this.gameRepository.save(game);
    return new NewGameDto(game, player);
  }

  updateCategories(game: CGame, categories: string[]): void {
    game.categories = categories.filter((c) => c).map((c) => c.trim());
    this.gameRepository.save(game);
  }

  updateSettings(game: CGame, settings: SettingsDto): void {
    game.gameModeNoScore = settings.gameModeNoScore;
    this.gameRepository.save(game);
  }
}
