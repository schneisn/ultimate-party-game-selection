import { CommonPlayerService } from '../../common/player/service/player.service';
import { CGame } from '../domain/game.entity';
import { CPlayer } from '../domain/player.entity';
import { ICPlayerService } from './player.service.interface';
import { ICRoundService } from './round.service.interface';
import { ICVoteService } from './vote.service.interface';
import { ICGameService } from './game.service.interface';
import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { PlayerNotDefinedError } from '../../common/player/service/error/player-not-defined.error';
import { IGameRepository } from '../../common/game/repository/game.repository.interface';

@Injectable()
export class CPlayerService extends CommonPlayerService<CGame, CPlayer>
  implements ICPlayerService {
  constructor(
    @Inject(forwardRef(() => ICRoundService))
    private readonly roundService: ICRoundService,
    private readonly voteService: ICVoteService,
    private readonly gameService: ICGameService,
    readonly gameRepository: IGameRepository,
  ) {
    super(gameRepository);
  }

  addPlayer(game: CGame, playerId: string, playerName: string): CPlayer {
    playerName = this.addNrToPlayerNameIfExists(game, playerId, playerName);
    const existingPlayer = this.replaceExistingPlayer(
      game,
      playerId,
      playerName,
    );
    if (existingPlayer) {
      this.gameRepository.save(game);
      return existingPlayer;
    }
    const player = new CPlayer(null, playerName);
    game.players.push(player);
    this.gameRepository.save(game);
    return player;
  }

  setPlayerReadyState(game: CGame, playerId: string, isReady: boolean): void {
    const player = this.getPlayer(game, playerId);
    player.isReady = isReady;
    if (this.allPlayersReady(game)) {
      this.roundService.newRound(game);
      game.players.forEach((p) => (p.isReady = false));
    }
    this.gameRepository.save(game);
  }

  disconnectPlayer(game: CGame, playerId: string) {
    const player = this.getPlayer(game, playerId);
    if (!player) {
      throw new PlayerNotDefinedError(playerId);
    }
    player.isConnected = false;
    player.hasVoted = false;
    this.voteService.removePlayerVotes(game, player.id);
    if (!this.roundService.getCurrentRound(game)) {
      super.disconnectPlayer(game, playerId);
    }
    if (this.canCloseGame(game)) {
      this.gameService.scheduleGameForRemoval(game.id);
      return;
    }
    this.gameRepository.save(game);
  }

  protected replaceExistingPlayer(
    game: CGame,
    playerId: string,
    playerName: string,
  ): CPlayer {
    const player = super.replaceExistingPlayer(game, playerId, playerName);
    game.rounds.forEach((round) =>
      round.answers.forEach((answer) => {
        answer.playerId === playerId ? (answer.playerId = playerId) : null;
        answer.invalidVotes?.forEach((vote) =>
          vote.playerId === playerId ? (vote.playerId = playerId) : null,
        );
      }),
    );
    return player;
  }

  private allPlayersReady(game: CGame): boolean {
    return game.players.every(
      (player) => player.isReady || !player.isConnected,
    );
  }

  private canCloseGame(game: CGame): boolean {
    return (
      game.players.length < 1 ||
      (game.rounds.length > 0 && game.players.length < 2) ||
      game.players.every((p) => !p.isConnected)
    );
  }
}
