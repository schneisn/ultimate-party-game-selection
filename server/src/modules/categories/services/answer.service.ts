import { Injectable } from '@nestjs/common';
import { RoundNotDefinedError } from '../../common/game/service/error/round-not-defined.error';
import { CGame } from '../domain/game.entity';
import { ICPlayerService } from './player.service.interface';
import { ICRoundService } from './round.service.interface';
import { RoundIsLockedError } from './error/round-is-locked.error';
import { ICAnswerService } from './answer.service.interface';
import { AddAnswerDto } from './dto/add-answer.dto';
import { AddAnswerMapper } from './dto/add-answer.mapper';
import { CAnswer } from '../domain/answer.entity';
import { IGameRepository } from '../../common/game/repository/game.repository.interface';

@Injectable()
export class CAnswerService implements ICAnswerService {
  constructor(
    private readonly playerService: ICPlayerService,
    private readonly roundService: ICRoundService,
    private readonly gameRepository: IGameRepository<CGame>,
  ) {}

  addAnswers(game: CGame, playerId: string, answerDtos: AddAnswerDto[]): void {
    // Give players 3000 ms extra to send answers to server due to delay
    const lockTimeInFutureInMs = game.answerTimeout * 1000 + 3000;
    const currentRound = this.roundService.getCurrentRound(game);
    if (!currentRound) {
      throw new RoundNotDefinedError(game);
    }
    if (this.roundService.isRoundLocked(game)) {
      throw new RoundIsLockedError(currentRound);
    }
    // The first answer added starts the timer to lock the round
    if (currentRound.answers.length === 0) {
      this.roundService.setLockTime(game);
      setTimeout(() => {
        // Get updated game from repo to prevent overriding changes
        // that occurred before timeout triggered
        const upToDateGame = this.gameRepository.find(game.id);
        this.roundService.lockCurrentRound(upToDateGame);
        this.gameRepository.save(upToDateGame);
      }, lockTimeInFutureInMs);
    }
    const answers = answerDtos.map((answerDto) =>
      new AddAnswerMapper().toCAnswer(answerDto, playerId),
    );
    this.updateExistingAnswersAndRemoveFromNewAnswer(
      playerId,
      currentRound.answers,
      answers,
    );
    currentRound.answers.push(...answers);
    this.fillMissingCategoryAnswers(game, playerId);
    this.playerService.getPlayer(game, playerId).hasAnswered = true;
    this.gameRepository.save(game);
  }

  /**
   * Updates the input of existing answers
   * @param playerId
   * @param existingAnswers
   * @param newAnswers
   * @private
   */
  private updateExistingAnswersAndRemoveFromNewAnswer(
    playerId: string,
    existingAnswers: CAnswer[],
    newAnswers: CAnswer[],
  ): void {
    existingAnswers
      .filter((a) => a.playerId === playerId)
      .forEach((existingAnswer: CAnswer) => {
        newAnswers.forEach((newAnswer: CAnswer, index: number) => {
          if (existingAnswer.category === newAnswer.category) {
            existingAnswer.input = newAnswer.input;
            newAnswers.splice(index, 1);
          }
        });
      });
  }

  /**
   * Adds an empty input for any category where no answer was sent
   * @param game
   * @param playerId
   * @private
   */
  private fillMissingCategoryAnswers(game: CGame, playerId: string) {
    const currentRound = this.roundService.getCurrentRound(game);
    game.categories.forEach((category: string) => {
      if (!this.playerAnsweredCategory(game, category, playerId)) {
        currentRound.answers.push(new CAnswer(category, '', playerId));
      }
    });
  }

  private playerAnsweredCategory(
    game: CGame,
    category: string,
    playerId: string,
  ): boolean {
    const currentRound = this.roundService.getCurrentRound(game);
    return !!currentRound.answers
      .filter((a) => a.playerId === playerId)
      .find((a) => a.category === category);
  }
}
