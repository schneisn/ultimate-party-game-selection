export class VoteInvalidAnswerDto {
  category: string;
  input: string;
}
