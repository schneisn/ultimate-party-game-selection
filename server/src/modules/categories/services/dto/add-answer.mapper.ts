import { CAnswer } from '../../domain/answer.entity';
import { AddAnswerDto } from './add-answer.dto';

export class AddAnswerMapper {
  toCAnswer(addAnswerDto: AddAnswerDto, playerId: string): CAnswer {
    return new CAnswer(addAnswerDto.category, addAnswerDto.input, playerId);
  }
}
