import { CRound } from '../domain/round.entity';
import { CGame } from '../domain/game.entity';

export abstract class ICRoundService {
  abstract getCurrentRound(game: CGame): CRound;

  abstract newRound(game: CGame): CRound;

  abstract setLockTime(game: CGame): CRound;

  abstract lockCurrentRound(game: CGame): CRound;

  abstract isRoundLocked(game: CGame): boolean;

  abstract getRemainingTimeInSeconds(game: CGame): number;

  abstract computeScore(game: CGame): void;
}
