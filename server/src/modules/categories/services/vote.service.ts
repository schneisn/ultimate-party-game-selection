import { Injectable } from '@nestjs/common';
import { RoundNotDefinedError } from '../../common/game/service/error/round-not-defined.error';
import { CAnswer } from '../domain/answer.entity';
import { CGame } from '../domain/game.entity';
import { VoteInvalidAnswerDto } from './dto/vote-invalid-answer.dto';
import { CVote } from '../domain/vote.entity';
import { ICVoteService } from './vote.service.interface';
import { ICPlayerService } from './player.service.interface';
import { ICRoundService } from './round.service.interface';
import { IGameRepository } from '../../common/game/repository/game.repository.interface';

@Injectable()
export class CVoteService implements ICVoteService {
  constructor(
    private readonly playerService: ICPlayerService,
    private readonly roundService: ICRoundService,
    private readonly gameRepository: IGameRepository,
  ) {}

  voteInvalidAnswers(
    game: CGame,
    playerId: string,
    invalidAnswers: VoteInvalidAnswerDto[],
  ): void {
    const currentRound = this.roundService.getCurrentRound(game);
    if (!currentRound) {
      throw new RoundNotDefinedError(game);
    }
    invalidAnswers.forEach((invalidAnswer: CAnswer) => {
      currentRound.answers.forEach((answer: CAnswer) => {
        if (
          answer.category === invalidAnswer.category &&
          answer.input === invalidAnswer.input
        ) {
          if (!answer.invalidVotes) {
            answer.invalidVotes = [];
          }
          if (!answer.invalidVotes.find((a) => a.playerId === playerId)) {
            answer.invalidVotes.push(new CVote(playerId));
          }
        }
      });
    });
    this.playerService.getPlayer(game, playerId).hasVoted = true;
    if (this.allActivePlayersInRoundVoted(game)) {
      this.roundService.computeScore(game);
      game.players.forEach((player) => (player.hasVoted = false));
    }
    this.gameRepository.save(game);
  }

  removePlayerVotes(game: CGame, playerId: string): void {
    this.roundService.getCurrentRound(game)?.answers.forEach((answer) => {
      const playerVoteIndex = answer.invalidVotes?.findIndex(
        (v) => v.playerId === playerId,
      );
      if (playerVoteIndex && playerVoteIndex >= 0) {
        answer.invalidVotes.splice(playerVoteIndex, 1);
      }
    });
  }

  private allActivePlayersInRoundVoted(game: CGame): boolean {
    return game.players
      .filter(
        (player) => this.playerIsInRound(game, player.id) && player.isConnected,
      )
      .every((player) => player.hasVoted);
  }

  private playerIsInRound(game: CGame, playerId: string): boolean {
    return this.roundService
      .getCurrentRound(game)
      .answers.some((a) => a.playerId === playerId);
  }
}
