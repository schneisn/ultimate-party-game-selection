import { ICommonGameService } from '../../common/game/service/game.service.interface';
import { CPlayer } from '../domain/player.entity';
import { SettingsDto } from './dto/settings.dto';
import { CGame } from '../domain/game.entity';

export abstract class ICGameService extends ICommonGameService<CGame, CPlayer> {
  abstract updateCategories(game: CGame, categories: string[]): void;

  abstract updateSettings(game: CGame, settings: SettingsDto): void;
}
