import { Game } from '../../common/game/domain/game.entity';
import { CPlayer } from './player.entity';
import { CRound } from './round.entity';

export class CGame extends Game<CPlayer> {
  categories: string[] = [];
  rounds: CRound[] = [];
  /**
   * Time to answer after the first player answered in seconds
   */
  answerTimeout: number = 30;
  /**
   * Play without points
   */
  gameModeNoScore: boolean;
}
