import { DateUtils } from '../../../utils/date.utils';
import { CAnswer } from './answer.entity';

export class CRound {
  letter: string;
  /**
   * The time when the round is locked.
   * This is used to compute the remaining seconds until no player
   * can answer anymore
   */
  lockTime: Date;
  /**
   * Whether the round is locked.
   * This is used to check whether players can still answer
   */
  isLocked: boolean;
  /**
   * True when the score is computed
   */
  isCounted: boolean = false;
  answers: CAnswer[] = [];
  // TODO Replace the startTime with a countdown because his will not work when the client has a different time than the server
  /**
   * The start time of the round
   */
  startTime: Date;

  constructor(letter: string) {
    this.startTime = DateUtils.createDateInSeconds(5);
    this.letter = letter;
  }
}
