import { Player } from '../../common/player/domain/player.entity';

export class CPlayer extends Player {
  isReady: boolean = false;
  hasVoted: boolean = false;
  hasAnswered: boolean = false;
}
