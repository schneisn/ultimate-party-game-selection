import { CVote } from './vote.entity';

export class CAnswer {
  category: string;
  input: string;
  playerId: string;
  isValid: boolean;
  isDuplicate: boolean;
  score: number;
  invalidVotes: CVote[] = [];

  constructor(category: string, input: string, playerId: string) {
    this.category = category;
    this.input = input;
    this.playerId = playerId;
  }
}
