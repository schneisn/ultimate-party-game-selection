import { Module } from '@nestjs/common';
import { CGateway } from './gateway/categories.gateway';
import { CAnswerService } from './services/answer.service';
import { CGameService } from './services/game.service';
import { CPlayerService } from './services/player.service';
import { CRoundService } from './services/round.service';
import { CVoteService } from './services/vote.service';
import { GameRepository } from '../common/game/repository/game.repository';
import { IGameRepository } from '../common/game/repository/game.repository.interface';
import { ICVoteService } from './services/vote.service.interface';
import { ICRoundService } from './services/round.service.interface';
import { ICAnswerService } from './services/answer.service.interface';
import { ICGameService } from './services/game.service.interface';
import { ICPlayerService } from './services/player.service.interface';

@Module({
  providers: [
    CGateway,
    {
      provide: ICAnswerService,
      useClass: CAnswerService,
    },
    {
      provide: ICGameService,
      useClass: CGameService,
    },
    {
      provide: IGameRepository,
      useClass: GameRepository,
    },
    {
      provide: ICPlayerService,
      useClass: CPlayerService,
    },
    {
      provide: ICRoundService,
      useClass: CRoundService,
    },
    {
      provide: ICVoteService,
      useClass: CVoteService,
    },
  ],
})
export class CategoriesModule {}
