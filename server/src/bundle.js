// This file is used for pkg to include environment variables
// because passing a .env file while packaging the app does
// not work.
// See https://github.com/vercel/pkg/issues/358

// Define the env variables here
process.env.NODE_ENV = 'pkg';
process.env.SERVER_HOST = 'localhost';
process.env.SERVER_PORT = 5000;

// This calls the main bootstrap function
require('./main.js');
