import {
  ArgumentsHost,
  BadRequestException,
  Catch,
  Global,
} from '@nestjs/common';
import { BaseWsExceptionFilter } from '@nestjs/websockets';
import { LoggerService } from '../modules/logger/logger.service';
import { UpgsValidationError } from './error/validation.error';

/**
 * Transforms the bad request exception thrown from the validation pipe
 * into a WsException.
 * The validation factory in the validation pipe does not work somehow...
 *
 * This is needed for the client to receive a proper error.
 * See https://stackoverflow.com/questions/60749135/nestjs-validationpipe-in-websocketgateway-returns-internal-server-error
 */
@Global()
@Catch(BadRequestException)
export class ValidationErrorTransformFilter extends BaseWsExceptionFilter {
  constructor(private logger: LoggerService) {
    super();
    this.logger.setContext(ValidationErrorTransformFilter.name);
  }

  catch(exception: BadRequestException, host: ArgumentsHost) {
    const properError = new UpgsValidationError(exception);
    this.logger.error(exception, exception.stack, null, properError);
    super.catch(properError, host);
  }
}
