import { BadRequestException } from '@nestjs/common';
import { UpgsError } from '../../common/error/upgs.error';

/**
 * Wraps the bad request exception that is thrown by the validator pipe
 * in a ws exception, so that the user receives a proper error message.
 */
export class UpgsValidationError extends UpgsError {
  constructor(error: BadRequestException) {
    super(
      'VALIDATION_ERROR',
      `Invalid data sent from client`,
      'Invalid data sent to server',
      error,
    );
  }
}
