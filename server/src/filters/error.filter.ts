import { ArgumentsHost, Catch, Global } from '@nestjs/common';
import { WsArgumentsHost } from '@nestjs/common/interfaces';
import { BaseWsExceptionFilter, WsException } from '@nestjs/websockets';
import { LoggerService } from '../modules/logger/logger.service';

/**
 * Logs the thrown error and passes it to the BaseWsExceptionFilter
 */
@Global()
@Catch()
export class ErrorFilter extends BaseWsExceptionFilter<WsException> {
  constructor(private logger: LoggerService) {
    super();
    this.logger.setContext(ErrorFilter.name);
  }

  catch(exception: WsException, host: ArgumentsHost) {
    const ctx: WsArgumentsHost = host.switchToWs();
    this.logger.error(exception, exception.stack, null, ctx.getData());
    super.catch(exception, host);
  }
}
