import { Module } from '@nestjs/common';
import { ConfigModule } from './modules/config/config.module';
import { FileModule } from './modules/file/file.module';
import { LoggerModule } from './modules/logger/logger.module';
import { SessionModule } from './modules/session/session.module';
import { CardsAgainstModule } from './modules/cardsagainst/cardsagainst.module';
import { CategoriesModule } from './modules/categories/categories.module';
import { WhoAmIModule } from './modules/whoAmI/who-am-i.module';
import { PrivacyModule } from './modules/privacy/privacy.module';
import { WerewolfModule } from './modules/werewolf/werewolf.module';
import { SpyfallModule } from './modules/spyfall/spyfall.module';
import { AppController } from './app.controller';
import { ServeStaticModule } from '@nestjs/serve-static';
import { ServerResponse } from 'http';
import { join } from 'path';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
  controllers: [AppController],
  imports: [
    ServeStaticModule.forRoot({
      exclude: ['/api*'],
      rootPath: join(__dirname, '..', 'client', 'build'),
      // Comments for the options are documented here:
      // https://github.com/nestjs/nest/blob/master/packages/platform-express/interfaces/serve-static-options.interface.ts
      serveStaticOptions: {
        setHeaders: (res: ServerResponse) => {
          // Use recommended max-age of 2 years
          // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Strict-Transport-Security
          res.setHeader(
            'Strict-Transport-Security',
            'max-age:63072000; includeSubDomains',
          );
        },
        // 1000*60*60*24 = 86400000 = 1 Day
        maxAge: 86400000,
      },
    }),
    ScheduleModule.forRoot(),
    ConfigModule,
    FileModule,
    LoggerModule,
    SessionModule,
    CardsAgainstModule,
    CategoriesModule,
    PrivacyModule,
    SpyfallModule,
    WerewolfModule,
    WhoAmIModule,
  ],
})
export class AppModule {}
