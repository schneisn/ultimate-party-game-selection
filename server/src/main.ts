import { INestApplication } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { IoAdapter } from '@nestjs/platform-socket.io';
import * as compression from 'compression';
import { AppModule } from './app.module';
import {
  configParams,
  ConfigService,
  env,
} from './modules/config/config.service';
import { LoggerService } from './modules/logger/logger.service';
import { resolve } from 'path';

// Set directory with the main file as root for the app.
// https://stackoverflow.com/questions/10265798/determine-project-root-from-a-running-node-js-application
// @ts-ignore
global.__baseDir = resolve(__dirname);

/**
 * Entry point for application
 */
export async function bootstrap() {
  const app: INestApplication = await NestFactory.create(AppModule);
  // Needed for pkg to work
  // https://stackoverflow.com/questions/55985763/nestjs-server-does-not-serve-socket-io-client
  app.useWebSocketAdapter(new IoAdapter(app));
  // https://docs.nestjs.com/techniques/logger#using-the-logger-for-application-logging
  const loggerService = await app.resolve(LoggerService);
  app.useLogger(loggerService);
  // Use compression
  // https://docs.nestjs.com/techniques/compression
  const configService = app.get(ConfigService);
  const environment = configService.getEnv();
  if (environment !== env.DEVELOPMENT) {
    loggerService.log('Using compression for response bodies');
    app.use(compression());
  }
  loggerService.log(`Using env file ${environment}`);
  // loggerService.log('Enable cors');
  // app.enableCors();
  const port = configService.getPort();
  if (!port) {
    throw Error('Port is undefined. The port can be set in the .env file');
  }
  await app.listen(port);
  loggerService.log(`Server is listening on ${await app.getUrl()}`);
}

bootstrap();
