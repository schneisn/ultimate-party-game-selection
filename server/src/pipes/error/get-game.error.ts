import { UpgsError } from '../../common/error/upgs.error';

export class GetGameError extends UpgsError {
  constructor() {
    super(
      'GET_GAME_ERROR',
      `No game id in request body`,
      'Request to server did not contain a game id',
      null,
    );
  }
}
