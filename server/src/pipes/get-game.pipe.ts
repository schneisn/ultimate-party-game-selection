import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common';
import { LoggerService } from '../modules/logger/logger.service';
import { GameRequest } from '../modules/common/gateway/event/request/game.request';
import { IGameRepository } from '../modules/common/game/repository/game.repository.interface';
import { Game } from '../modules/common/game/domain/game.entity';
import { Player } from '../modules/common/player/domain/player.entity';
import { GetGameError } from './error/get-game.error';

/**
 * The GetGamePipe retrieves the game object from the repository.
 * The id of the game is sent in the data of the ws request.
 *
 * This cannot not be declared as global, since the DI of the repo needs to happen
 * in the module, that has the respective GameRepository as provider.
 */
@Injectable()
export class GetGamePipe
  implements
    PipeTransform<GameRequest<Game<Player>>, GameRequest<Game<Player>>> {
  constructor(
    private readonly logger: LoggerService,
    private readonly gameRepo: IGameRepository<Game<Player>>,
  ) {
    this.logger.setContext(GetGamePipe.name);
  }

  transform(
    requestBody: GameRequest<Game<Player>>,
    metadata: ArgumentMetadata,
  ): GameRequest<Game<Player>> {
    const game = requestBody?.game;
    if (!game?.id) {
      throw new GetGameError();
    }
    try {
      requestBody.game = this.gameRepo.find(game.id);
    } catch (e) {
      this.logger.error('Error getting game from request', e.stack, null, e);
      throw e;
    }
    return requestBody;
  }
}
