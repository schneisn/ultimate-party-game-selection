# Server

This is the readme for the server. The server is build with [NestJS](https://nestjs.com/).

## Scripts

Checkout example scripts at

https://github.com/nestjs/nest-cli/issues/320

* `build`: Uses nest-cli (.nestcli.json) to build. Also copies assets. Only running tsc does not copy assets
* `start`: Start server without hot reload
* `start:dev`: Start server with hot reload using nodemon
* `prestart:prod`: Compiles all files
* `start:prod`: Starts the compiled server
* `test`: Run jest tests
* `webpack`: Watches for file changes using webpack. Does not start the server.

## Files

* `.nestcli.json`: NestJS config file, e.g. used to config the build.  
  The sourceRoot is not `src` because otherwise the `.env` files could not be copied (
  See e.g. https://github.com/nestjs/nest-cli/issues/567).
* `nodemon.json`: Config file for running server with nodemon for development. Uses hot reload.
* `webpack.config.js`: Alternative for nodemon.  
  Used for hot module replacement when building without nest-cli (https://docs.nestjs.com/recipes/hot-reload).  
  Uses server/main.hmr.ts for running the server.

## Config

See root readme.  
