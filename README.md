# Ultimate Party Game Selection

<p>
  <img width="200" src="https://assets.gitlab-static.net/uploads/-/system/project/avatar/8585075/icon-512x512.png" alt="Ultimate Party Game Selection"/>
</p>

A fun collection of famous party games that can be played from anywhere - notebook, tablet, phone or even your toaster.

- [Cards against Humanity](https://cardsagainsthumanity.com/)
- [Categories](https://www.thespruce.com/categories-411167)
- [Privacy](https://boardgamegeek.com/boardgame/181866/privacy-quickie)
- [Spyfall](https://boardgamegeek.com/boardgame/166384/spyfall)
- [Werewolf](https://www.playwerewolf.co/rules/)
- [Who Am I](https://icebreakerideas.com/who-am-i-game/)

If you have any idea for more games, feel free to open an issue!

## How does this stuff work

The front-end (client directory) is a react-app created
with [create-react-app](https://github.com/facebook/create-react-app).  
The back-end (server directory) is a node-js server with [NestJs](https://nestjs.com/) as a framework.

### Installation

In root, client and server directory, run

```bash
$ npm install
```

Additionally, to be able to build the server, install [Nest CLI](https://docs.nestjs.com/cli/overview)

```bash
$ npm install -g @nestjs/cli
```

### Running the app

```bash
# development
$ npm run start

# watch mode with hot reload
$ npm run start:dev

# production mode
$ npm run start:prod

# packaging the app into an executable
$ npm run pkg
```

#### Configuration

The app can be configured using the `.env` files.  
The file for the front-end is in the `client` directory. The file for the server is in `server` directory. A basic
configuration is defined in the `.env.template` file. You can overwrite the parameters by creating your own .env file,
e.g. `.myconfig.env` and defining the parameters to change. The application can then be started by
running `ENV_FILE_PATH=./path/to/.myconfig.env npm run start` (or any other npm script).

#### Development

##### Tricks & Known Issues

* Gateways cannot have the same class name
* Two gateways cannot have the same namespace
* Importing a globally exported service might lead to the error
  `service.xxx is not a function`. Remove the import.
* The following error in the CI pipeline (env-var CI=true)
  ```
  chunk 2 [mini-css-extract-plugin]
         Conflicting order. Following module has been added:
  ...
  ```
  can be solved by sorting imports.
* If the server fails silently with a message like `[nodemon] clean exit - waiting for changes before restart`, it might
  be due to missing forwardRef() definitions in the services. This happens a lot with inherited services
  (e.g. PlayerServices).
* If there is a circular dependency like A -> B -> C -> A, the forwardRef() has to be defined in all modules (A, B, C).
  The injected services that cause the CD in modules B and C do not need to have the forwardRef in the imports.
* Nestjs sometimes behaves weird when adding decorators to inherited classes when the decorator already exists on the
  superclass

##### Debugging

Type in the browser-console: `localStorage.debug = 'socket.io-client:socket*';`

More info about debugging and logging: https://socket.io/docs/logging-and-debugging/

### Building executable

The `package.json` in the root directory offers a `pkg` script, which can be used to build an executable for windows,
linux and macOS.

## Contributions

Always welcome 😃

## License

MIT
