/**
 * The keys for the items that are stored in the local storage
 */
export enum LocalStorageItem {
    /**
     * Defines the debug log level for socket.io
     */
    DEBUG = 'debug',
    /**
     * The locally stored player with name and id
     */
    PLAYER = 'player',
    READ_ENV_INFO = 'readEnvironmentInfo'
}

/**
 * LocalStorage handles saving and retrieving objects from the localStorage
 */
export class LocalStorage {

    /**
     * Saves an item as stringified json to the local storage
     * @param item
     * @param data
     */
    static setJSONItem<T>(item: LocalStorageItem, data: T): void {
        localStorage.setItem(item.toString(), JSON.stringify(data));
    }

    static getJSONItem<T>(item: LocalStorageItem): T {
        return JSON.parse(localStorage.getItem(item.toString())) as T;
    }

    static getString(item: LocalStorageItem): string {
        return localStorage.getItem(item.toString());
    }

}
