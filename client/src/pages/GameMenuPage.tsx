import React from 'react';
import {useHistory, useLocation, useRouteMatch} from 'react-router-dom';
import useNameGuard from '../hooks/UseNameGuard';
import GameMenu from '../component/common/component/gameMenu/GameMenu';

const ChangeNamePage: React.FunctionComponent = () => {

    useNameGuard();

    const history = useHistory();
    const match = useRouteMatch();
    const location = useLocation();

    return <GameMenu history={history} match={match} location={location}/>;
}

export default ChangeNamePage;
