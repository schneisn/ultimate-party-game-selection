import React from 'react';
import {useHistory, useLocation, useRouteMatch} from 'react-router-dom';
import useNameGuard from '../hooks/UseNameGuard';
import {WhoAmI} from '../component/whoAmI/WhoAmI';

const WhoAmIPage: React.FunctionComponent = () => {

    useNameGuard();

    const history = useHistory();
    const match = useRouteMatch();
    const location = useLocation();

    return <WhoAmI history={history} match={match} location={location}/>;
}

export default WhoAmIPage;
