import React from 'react';
import {useHistory, useLocation, useRouteMatch} from 'react-router-dom';
import useNameGuard from '../hooks/UseNameGuard';
import {Privacy} from '../component/privacy/Privacy';

const PrivacyPage: React.FunctionComponent = () => {

    useNameGuard();

    const history = useHistory();
    const match = useRouteMatch();
    const location = useLocation();

    return <Privacy history={history} match={match} location={location}/>;
}

export default PrivacyPage;
