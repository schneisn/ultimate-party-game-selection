import React from 'react';
import {useHistory, useLocation, useRouteMatch} from 'react-router-dom';
import useNameGuard from '../hooks/UseNameGuard';
import {Werewolf} from '../component/werewolf/Werewolf';

const WerewolfPage: React.FunctionComponent = () => {

    useNameGuard();

    const history = useHistory();
    const match = useRouteMatch();
    const location = useLocation();

    return <Werewolf history={history} match={match} location={location}/>;
}

export default WerewolfPage;
