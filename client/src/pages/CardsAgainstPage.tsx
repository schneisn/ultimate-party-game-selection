import React from 'react';
import {useHistory, useLocation, useRouteMatch} from 'react-router-dom';
import useNameGuard from '../hooks/UseNameGuard';
import {CardsAgainst} from '../component/cardsAgainst/CardsAgainst';

const CardsAgainstPage: React.FunctionComponent = () => {

    useNameGuard();

    const history = useHistory();
    const match = useRouteMatch();
    const location = useLocation();

    return <CardsAgainst history={history} match={match} location={location}/>;
}

export default CardsAgainstPage;
