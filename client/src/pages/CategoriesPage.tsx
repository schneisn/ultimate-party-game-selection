import React from 'react';
import {useHistory, useLocation, useRouteMatch} from 'react-router-dom';
import useNameGuard from '../hooks/UseNameGuard';
import {Categories} from '../component/categories/Categories';

const CategoriesPage: React.FunctionComponent = () => {

    useNameGuard();

    const history = useHistory();
    const match = useRouteMatch();
    const location = useLocation();

    return <Categories history={history} match={match} location={location}/>;
}

export default CategoriesPage;
