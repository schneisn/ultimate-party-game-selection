import React from 'react';
import {useHistory, useLocation, useRouteMatch} from 'react-router-dom';
import useNameGuard from '../hooks/UseNameGuard';
import {Spyfall} from '../component/spyfall/Spyfall';

const SpyfallPage: React.FunctionComponent = () => {

    useNameGuard();

    const history = useHistory();
    const match = useRouteMatch();
    const location = useLocation();

    return <Spyfall history={history} match={match} location={location}/>;
}

export default SpyfallPage;
