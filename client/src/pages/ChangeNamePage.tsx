import React from 'react';
import {useHistory, useLocation, useRouteMatch} from 'react-router-dom';
import useNameGuard from '../hooks/UseNameGuard';
import {ChangeName} from '../component/common/component/changeName/ChangeName';

const ChangeNamePage: React.FunctionComponent = () => {

    useNameGuard();

    const history = useHistory();
    const match = useRouteMatch();
    const location = useLocation();

    return <ChangeName history={history} match={match} location={location}/>;
}

export default ChangeNamePage;
