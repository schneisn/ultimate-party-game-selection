import React from 'react';
import {useHistory} from 'react-router-dom';
import {MainMenu} from '../component/common/component/mainMenu/MainMenu';
import useNameGuard from '../hooks/UseNameGuard';

const MainMenuPage: React.FunctionComponent = () => {

    useNameGuard();

    const history = useHistory();
    const goTo = (url: string) => history.push(url);

    return <MainMenu onClick={goTo}/>;
}

export default MainMenuPage;
