export interface RequestOptions {
    method: 'GET' | 'POST';
    url: string;
}

export class Http {

    /**
     * Send a request with the given options.
     * // TODO For now, there is no http endpoint that responds with data. Expand when necessary
     * @param options
     */
    static sendRequest = (options: RequestOptions) => {
        fetch(options.url)
            .catch(e => console.error(e));
    }

}
