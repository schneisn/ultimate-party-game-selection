import io from 'socket.io-client';
import {AbstractEvent} from '../component/common/event/AbstractEvent';
import {Logger} from '../logger/logger';
import {Config} from '../config/config';

/**
 * A wrapper for the socket.io socket
 */
export class Socket {

    /**
     * The actual socket io client socket
     */
    private readonly socket: SocketIOClient.Socket;

    /**
     * Creates a new socket
     * @param path The path. Has to start with /
     * @param options https://socket.io/docs/server-api/
     *                https://socket.io/docs/client-connection-lifecycle/
     */
    constructor(path: string = '/', options?: SocketIOClient.ConnectOpts) {
        // See https://socket.io/docs/v3/client-api/index.html for options
        const opt: SocketIOClient.ConnectOpts = {
            ...options,
            reconnectionAttempts: 100,
            reconnectionDelay: 1000,
            reconnectionDelayMax: 3000,
        }
        this.socket = io(Config.getServerOrigin() + path, opt);
    }

    /**
     * Logs that a callback is being called. Then calls the callback.
     * @param eventId
     * @param callback
     * @private
     */
    logThenCallback<T>(eventId: string, callback: (data: T) => void): (data: T) => void {
        return function x(data: T) {
            Logger.log(`On`, eventId, data);
            callback(data);
        }
    }

    /**
     * Emits the event with the given id.
     * If event is not provided, emit null.
     * @param eventId The name of the event to emit
     * @param event The data of the event
     */
    emit<E extends AbstractEvent>(eventId: string, event?: E): SocketIOClient.Socket {
        Logger.log<E>(`Emit to socket id <${this.socket.nsp}>`, eventId, event);
        return this.socket.emit(eventId, event ? event : null);
    }

    /**
     * Adds a listener for a particular event. Calling multiple times will add multiple listeners
     * @param eventId
     * @param callback
     * @param log
     */
    on<T>(eventId: string, callback: (data: T) => void, log: boolean = true): SocketIOClient.Emitter {
        return this.socket.on(eventId, log ? this.logThenCallback<T>(eventId, callback) : callback);
    }

    /**
     * Removes an event listener from the socket server. Only use this when you are sure
     * that there is no other component listening to this event.
     * @param eventId
     * @param callbackFn The function to remove. Null means remove all functions
     */
    removeEventListener(eventId: string, callbackFn?: Function): SocketIOClient.Emitter {
        return this.socket.removeEventListener(eventId, callbackFn);
    }

    /**
     * Disconnects the socket
     */
    disconnect(): SocketIOClient.Socket {
        return this.socket.disconnect();
    }

}
