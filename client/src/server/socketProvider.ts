import {Socket} from './socket';

/**
 * A wrapper for the socket.io socket
 */
export class SocketProvider {

    /**
     * Singletons of the different sockets. Each socket is for a different game.
     * @private
     */
    private static readonly socketMap: Map<string, Socket> = new Map<string, Socket>();

    /**
     * Returns an existing singleton socket of the given game.
     * @param game
     */
    static getSocket(game: string): Socket {
        game = '/' + game;
        if (this.socketMap.has(game)) {
            return this.socketMap.get(game);
        }
        const newSocket = new Socket(game);
        this.socketMap.set(game, newSocket);
        return newSocket;
    }
}
