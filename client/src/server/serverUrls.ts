export enum Urls {
    Root = '/',
    CardsAgainst = '/cardsagainst',
    Categories = '/categories',
    ChangeName = '/changename',
    GameMenu = '/:game',
    Privacy = '/privacy',
    Spyfall = '/spyfall',
    Werewolf = '/werewolf',
    WhoAmI = '/whoami'
}

/**
 * The paths of the games in the url.
 */
export enum Path {
    CardsAgainst = 'cardsagainst',
    Categories = 'categories',
    ChangeName = 'changename',
    Privacy = 'privacy',
    Spyfall = 'spyfall',
    Werewolf = 'werewolf',
    WhoAmI = 'whoami'
}
