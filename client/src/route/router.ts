export enum Routes {
    GITLAB = 'https://gitlab.com/schneisn/ultimate-party-game-selection/',
}

export class Router {
    static redirect(url: string): void {
        window.location.href = url;
    }
}
