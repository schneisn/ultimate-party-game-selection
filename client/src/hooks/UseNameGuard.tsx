import {useEffect} from 'react';
import {useHistory} from 'react-router-dom';
import {Urls} from '../server/serverUrls';
import {LocalStorage, LocalStorageItem} from '../store/localStorage';
import {StoredPlayer} from '../store/model/StoredPlayer';
import {Logger} from '../logger/logger';
import {UrlUtils} from '../component/common/utils/url';

/**
 * Checks if the player has set a name.
 */
const useNameGuard = (): void => {

    const history = useHistory();

    useEffect(() => {
        const player = LocalStorage.getJSONItem<StoredPlayer>(LocalStorageItem.PLAYER);
        if (!player?.name) {
            Logger.log(`Player does not have a name. Redirect to ${Urls.ChangeName}`);
            UrlUtils.goTo(history, Urls.ChangeName);
        }
    }, [history]);
}

export default useNameGuard;
