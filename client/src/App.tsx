import React from 'react';
import {BrowserRouter, Redirect, Route, Switch} from 'react-router-dom';
import styles from './App.module.scss';
import {EnvironmentInfo} from './component/common/component/environmentInfo/EnvironmentInfo';
import {Loader} from './component/common/component/loader/Loader';
import {RegisterServiceWorker} from './component/common/component/registerServiceWorker/RegisterServiceWorker';
import {Urls} from './server/serverUrls';

const MainMenuPage = React.lazy(() => import('./pages/MainMenuPage'));
const ChangeNamePage = React.lazy(() => import('./pages/ChangeNamePage'));
const GameMenuPage = React.lazy(() => import ('./pages/GameMenuPage'));
const CardsAgainstPage = React.lazy(() => import('./pages/CardsAgainstPage'));
const CategoriesPage = React.lazy(() => import('./pages/CategoriesPage'));
const PrivacyPage = React.lazy(() => import('./pages/PrivacyPage'));
const SpyfallPage = React.lazy(() => import('./pages/SpyfallPage'));
const WerewolfPage = React.lazy(() => import('./pages/WerewolfPage'));
const WhoAmIPage = React.lazy(() => import('./pages/WhoAmIPage'));

export class App extends React.Component {

    render() {
        return (
            <div className={styles.App}>
                <a className={styles.SkipLink} href={'#main'}>Skip to main</a>
                <BrowserRouter>
                    <React.Suspense fallback={<Loader additionalWrapperClasses={styles.Loader}/>}>
                        <Switch>
                            <Route exact path={Urls.Root} render={() => <MainMenuPage/>}/>
                            <Route exact path={Urls.ChangeName} component={() => <ChangeNamePage/>}/>
                            <Route exact path={Urls.GameMenu} render={() => <GameMenuPage/>}/>
                            <Route exact path={Urls.CardsAgainst + '/:id'} render={() => <CardsAgainstPage/>}/>
                            <Route exact path={Urls.Categories + '/:id'} render={() => <CategoriesPage/>}/>
                            <Route exact path={Urls.Privacy + '/:id'} render={() => <PrivacyPage/>}/>
                            <Route exact path={Urls.Spyfall + '/:id'} render={() => <SpyfallPage/>}/>
                            <Route exact path={Urls.Werewolf + '/:id'} render={() => <WerewolfPage/>}/>
                            <Route exact path={Urls.WhoAmI + '/:id'} render={() => <WhoAmIPage/>}/>
                            <Redirect to={Urls.Root}/>
                        </Switch>
                    </React.Suspense>
                </BrowserRouter>
                <EnvironmentInfo/>
                <RegisterServiceWorker/>
            </div>
        );
    }
}

export default App;
