import {Config} from '../config/config';

export class Logger {

    /**
     * Logs a message to the standard console.
     * Does not log in production
     * @param message
     */
    static log<T>(...message: Array<T | string>): void {
        if (!Config.isDev()) {
            return;
        }
        console.log(`[${new Date().toLocaleString()}] ${Logger.concMessages<T>(...message)}`);
    }

    /**
     * Logs an error to the standard console.
     * @param message
     */
    static error<T>(...message: Array<T | string>): void {
        console.trace(`[${new Date().toLocaleString()}] Error: ${Logger.concMessages<T>(...message)}`);
    }

    private static concMessages<T>(...messages: Array<T | string>): string {
        let completeMsg: string = '';
        messages.forEach((msg: T | string) =>
            completeMsg += msg ? JSON.stringify(msg, null, 2)?.slice(1, -1) + ' ' : '');
        return completeMsg;
    }
}
