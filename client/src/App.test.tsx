import {render, screen, waitFor} from '@testing-library/react';
import '@testing-library/jest-dom';
import React from 'react';
import App from './App';

describe('App Component', () => {
    it('should render correctly', async () => {
        render(<App/>);
        await waitFor(() => expect(screen.getByText('Party Game Selection')).toBeInTheDocument());
    });
});
