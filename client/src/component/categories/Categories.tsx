import React from 'react';
import {RouteComponentProps} from 'react-router-dom';
import {Urls} from '../../server/serverUrls';
import {BaseGame} from '../common/component/baseGame/BaseGame';
import {generateConnectToastStyles} from '../common/component/baseGame/component/connectToast/utils/styles';
import {generateBaseGameStyles} from '../common/component/baseGame/utils/styles';
import {generateModalStyles} from '../common/component/modal/utils/styles';
import {generatePopoverStyles} from '../common/component/popover/utils/styles';
import {Answer} from './component/answer/Answer';
import {AnswerVote} from './component/answerVote/AnswerVote';
import {Lobby} from './component/lobby/Lobby';
import {Scoreboard} from './component/scoreboard/Scoreboard';
import {IGame} from './model/IGame';
import {IPlayer} from './model/IPlayer';
import styles from './styles/color.module.scss';
import {getCurrentRound} from './utils/round';
import {UrlUtils} from '../common/utils/url';
import {ICommonPlayer} from '../common/model/ICommonPlayer';
import {IConnectionReadyPlayer} from '../common/model/IConnectionReadyPlayer';
import {playerIsGameMaster} from '../common/utils/player';
import {Card} from '../common/component/card/Card';
import {PlayerInfo} from '../common/component/playerInfo/PlayerInfo';
import {Modal} from '../common/component/modal/Modal';

export class Categories extends BaseGame<IGame, IPlayer> {

    constructor(props: Readonly<RouteComponentProps>) {
        super(props);
        const options = {
            gameName: 'Categories',
            gamePath: UrlUtils.getPathFromString(this.props.match.path),
            socketNamespace: Urls.Categories,
            styles: generateBaseGameStyles(
                generateModalStyles(styles.CPrimary, styles.CSecondary, styles.CTertiary),
                generateConnectToastStyles(styles.CConnect, styles.CDisconnect),
                null,
                styles.CBackground,
                generatePopoverStyles(styles.CTertiaryIcon, styles.CTertiary)
            )
        };
        this.state = {
            ...this.state,
            options: options
        };
    }

    renderGame(): JSX.Element {
        const currentRound = getCurrentRound(this.state.game);
        if (!currentRound) {
            return <Lobby game={this.state.game} player={this.state.player} socket={this.state.socket}/>;
        }
        if (!currentRound.isLocked) {
            return <Answer game={this.state.game} player={this.state.player} socket={this.state.socket}/>;
        }
        if (!currentRound.isCounted) {
            return <AnswerVote game={this.state.game} player={this.state.player} socket={this.state.socket}/>;
        }
        if (currentRound.isCounted) {
            return <Scoreboard game={this.state.game} player={this.state.player} socket={this.state.socket}/>;
        }
        return null;
    }

    protected getPlayerModal(): JSX.Element {
        const {isPlayerModalOpen, game} = this.state;
        const playerElements = game?.players
            .map((teamMate: ICommonPlayer & Partial<IConnectionReadyPlayer>) => {
                if ((playerIsGameMaster(game, teamMate.id))) {
                    teamMate.isGameMaster = true;
                }
                return teamMate;
            })
            .map(teamMate => {
                return <Card key={teamMate.id}>
                    <PlayerInfo player={teamMate}/>
                </Card>
            });

        return <Modal modalTitle={'Players'}
                      isOpen={isPlayerModalOpen}
                      showFooter={false}
                      onToggle={this.togglePlayerModal}>
            {playerElements}
        </Modal>
    }
}
