import {IAnswer} from './IAnswer';

export interface IRound {
    letter: string;
    isLocked: boolean;
    isCounted: boolean;
    answers: IAnswer[];
    startTime: Date;
}
