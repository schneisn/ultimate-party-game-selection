import {IVote} from './IVote';

export interface IAnswer {
    category: string;
    input: string;
    playerId: string;
    isValid: boolean;
    isDuplicate: boolean;
    score: number;
    invalidVotes: IVote[];
}
