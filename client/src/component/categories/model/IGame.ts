import {ICommonGame as ICommonGame} from '../../common/model/ICommonGame';
import {IPlayer} from './IPlayer';
import {IRound} from './IRound';

export interface IGame extends ICommonGame<IPlayer> {
    categories: string[];
    rounds: IRound[];
    answerTimeout: number;
    gameModeNoScore: boolean;
}
