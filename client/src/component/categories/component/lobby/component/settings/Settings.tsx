import React from 'react';
import {Socket} from '../../../../../../server/socket';
import {CheckboxWithLabel} from '../../../../../common/component/checkboxWithLabel/CheckboxWithLabel';
import {CommonSettings} from '../../../../../common/component/commonSettings/CommonSettings';
import {UpdateSettingsEvent} from '../../../../../common/component/commonSettings/event/UpdateSettingsEvent';
import {playerIsGameMaster} from '../../../../../common/utils/player';
import {IGame} from '../../../../model/IGame';
import {IPlayer} from '../../../../model/IPlayer';
import styles from './Settings.module.scss';
import classNames from 'classnames';

interface ICategoriesSettings {
    gameModeNoScore: boolean;
}

interface IProps {
    game: IGame;
    player: IPlayer;
    socket: Socket;
}

export class Settings extends React.Component<IProps> {

    render() {
        return (
            <CommonSettings additionalClassNames={styles.CSecondary}>
                <CheckboxWithLabel additionalClassNames={classNames(styles.SettingsCard, styles.CTertiary)}
                                   onClick={this.onTogglePointsMode}
                                   disabled={!playerIsGameMaster(this.props.game, this.props.player.id)}
                                   checked={this.props.game.gameModeNoScore}>
                    Play without points
                </CheckboxWithLabel>
            </CommonSettings>
        );
    }

    private getGameSettings(): ICategoriesSettings {
        return {
            gameModeNoScore: this.props.game.gameModeNoScore
        }
    }

    private onTogglePointsMode = (): void => {
        const updatedSettings = this.getGameSettings();
        updatedSettings.gameModeNoScore = !updatedSettings.gameModeNoScore;
        this.props.socket.emit(UpdateSettingsEvent.id, new UpdateSettingsEvent(this.props.game.id, updatedSettings));
    }
}
