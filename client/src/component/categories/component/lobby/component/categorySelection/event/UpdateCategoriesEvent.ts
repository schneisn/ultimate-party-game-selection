import {GameEvent} from '../../../../../../common/event/GameEvent';

export class UpdateCategoriesEvent extends GameEvent {
    static readonly id = 'UpdateCategories';
    categories: string[]

    constructor(gameId: string, categories: string[]) {
        super(gameId);
        this.categories = categories;
    }
}
