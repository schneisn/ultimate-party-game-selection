import React from 'react';
import {Socket} from '../../../../../../server/socket';
import {HeaderContainer} from '../../../../../common/component/headerContainer/HeaderContainer';
import {IconType} from '../../../../../common/component/icon/Icon';
import {IconContent} from '../../../../../common/component/iconContent/IconContent';
import {SmallCard} from '../../../../../common/component/smallCard/SmallCard';
import {TextAreaWithButton} from '../../../../../common/component/textAreaWithButton/TextAreaWithButton';
import {IGame} from '../../../../model/IGame';
import {IPlayer} from '../../../../model/IPlayer';
import styles from './CategorySelection.module.scss';
import {UpdateCategoriesEvent} from './event/UpdateCategoriesEvent';

interface IProps {
    game: IGame;
    player: IPlayer;
    socket: Socket;
}

interface IState {
    category: string;
}

export class CategorySelection extends React.Component<IProps, IState> {

    constructor(props: Readonly<IProps>) {
        super(props);
        this.state = {
            category: ''
        }
    }

    render() {
        return (
            <div>
                <HeaderContainer header={'Categories'}
                                 additionalClassNames={styles.CSecondary}>
                    {this.getCategoryList()}
                    <TextAreaWithButton additionalTextAreaClasses={styles.CategoryInput}
                                        additionalButtonClasses={styles.CTertiary}
                                        textPlaceholder={'Add a new category'}
                                        textValue={this.state.category}
                                        onChange={this.onInputChange}
                                        onSubmit={this.updateCategories}
                                        buttonText={this.categoryExists() ? 'Category already exists' : 'Add category'}
                                        buttonIsDisabled={this.confirmIsDisabled()}
                    />
                </HeaderContainer>
            </div>
        );
    }

    private getCategoryList(): JSX.Element[] {
        return this.props.game.categories.map((category: string) => {
            const content = <IconContent iconRight={{
                icon: IconType.trash,
                additionalIconClassnames: styles.TrashIcon,
                onIconClick: () => this.handleRemoveCategoryClick(category)
            }}>
                {category}
            </IconContent>;
            return <SmallCard key={category}
                              additionalClassNames={styles.CTertiary}>
                {content}
            </SmallCard>;
        });
    }

    private handleRemoveCategoryClick = (category: string): void => {
        const categories = this.props.game.categories;
        const categoryIndex = categories.findIndex(cat => cat === category);
        categories.splice(categoryIndex, 1);
        this.props.socket.emit(UpdateCategoriesEvent.id, new UpdateCategoriesEvent(this.props.game.id, categories));
    }

    private onInputChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
        this.setState({
            category: event.currentTarget.value
        })
    }

    private updateCategories = (): void => {
        if (this.state.category.trim().length < 1 || this.categoryExists()) {
            return;
        }
        const categories = this.props.game.categories;
        categories.push(this.state.category);
        this.props.socket.emit(UpdateCategoriesEvent.id, new UpdateCategoriesEvent(this.props.game.id, categories));
        this.setState({category: ''});
    }

    private categoryExists(): boolean {
        return !!this.props.game.categories.find(cat => cat === this.state.category.trim());
    }

    private confirmIsDisabled(): boolean {
        return this.state.category.length < 1 || this.categoryExists();
    }
}
