import React from 'react';
import {Socket} from '../../../../server/socket';
import {CommonLobby} from '../../../common/component/commonLobby/CommonLobby';
import {generateCommonLobbyStyles} from '../../../common/component/commonLobby/utils/styles';
import {getGameMaster} from '../../../common/utils/player';
import {IGame} from '../../model/IGame';
import {IPlayer} from '../../model/IPlayer';
import styles from '../../styles/color.module.scss';
import {CategorySelection} from './component/categorySelection/CategorySelection';
import {Settings} from './component/settings/Settings';

interface IProps {
    game: IGame;
    player: IPlayer;
    socket: Socket;
}

export class Lobby extends React.Component<IProps> {

    render(): JSX.Element {
        return <>
            <CommonLobby description={'Wait for more players and add categories'}
                         buttonIsDisabled={this.readyButtonIsDisabled()}
                         buttonText={this.getReadyButtonText()}
                         game={this.props.game}
                         gameMasterId={getGameMaster(this.props.game).id}
                         player={this.props.player}
                         socket={this.props.socket}
                         styles={generateCommonLobbyStyles(styles.CPrimary, null, styles.CSecondary, styles.CTertiary)}>
                <CategorySelection game={this.props.game} player={this.props.player} socket={this.props.socket}/>
                <Settings game={this.props.game} player={this.props.player} socket={this.props.socket}/>
            </CommonLobby>
        </>;
    }

    private getReadyButtonText(): string {
        if (this.gameHasEnoughPlayers() && !this.gameHasCategories()) {
            return 'Add more categories to start!';
        }
        return null;
    }

    private readyButtonIsDisabled(): boolean {
        return !this.gameHasEnoughPlayers() || !this.gameHasCategories();
    }

    private gameHasEnoughPlayers(): boolean {
        return this.props.game.players.length > 1;
    }

    private gameHasCategories(): boolean {
        return this.props.game.categories.length > 0;
    }
}
