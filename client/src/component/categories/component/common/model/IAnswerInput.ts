export interface IAnswerInput {
    category: string;
    input: string;
}
