import React from 'react';
import {Socket} from '../../../../server/socket';
import {WideButton} from '../../../common/component/wideButton/WideButton';
import {Form} from '../../../common/component/form/Form';
import {InfoCard} from '../../../common/component/infoCard/InfoCard';
import {StringUtils} from '../../../common/utils/string';
import {IGame} from '../../model/IGame';
import {IPlayer} from '../../model/IPlayer';
import styles from '../../styles/color.module.scss';
import {getPlayerAnswersInCurrentRound} from '../../utils/answer';
import {getCurrentRound} from '../../utils/round';
import {IAnswerInput} from '../common/model/IAnswerInput';
import {AnswerTimeout} from './component/answerTimeout/AnswerTimeout';
import {CategoryInput} from './component/categoryInput/CategoryInput';
import {CollectAnswersInfo} from './component/collectAnswersInfo/CollectAnswersInfo';
import {Countdown} from './component/countdown/Countdown';
import {AddAnswersEvent} from './event/AddAnswersEvent';
import {CollectAnswersEvent} from './event/CollectAnswersEvent';
import {DateUtils} from '../../../common/utils/date';

interface IProps {
    game: IGame;
    player: IPlayer;
    socket: Socket;
}

interface IState {
    answers: IAnswerInput[];
    /**
     * Whether all answers are being collected (aka server is waiting for clients to send answers)
     */
    isCollected: boolean;
    /**
     * The countdown timer until the round starts
     */
    renderRoundTimer: NodeJS.Timer;
    /**
     * Whether to show the countdown until the round starts
     */
    showCountdown: boolean;
}

export class Answer extends React.Component<IProps, IState> {

    constructor(props: Readonly<IProps>) {
        super(props);
        this.state = {
            answers: getPlayerAnswersInCurrentRound(this.props.game, this.props.player.id),
            isCollected: false,
            renderRoundTimer: null,
            showCountdown: DateUtils.isInFuture(getCurrentRound(this.props.game).startTime)
        }
    }

    componentDidMount() {
        if (this.state.showCountdown) {
            const renderRound = setTimeout(() => {
                this.setState({
                    showCountdown: false,
                    renderRoundTimer: null
                });
            }, (this.getTimeUntilStartInSeconds() + 1) * 1000);
            this.setState({renderRoundTimer: renderRound});
        }
        this.props.socket.on(CollectAnswersEvent.id, () => {
            this.setState({isCollected: true});
            this.sendAnswers();
        });
    }

    componentWillUnmount() {
        clearTimeout(this.state.renderRoundTimer);
        this.props.socket.removeEventListener(CollectAnswersEvent.id);
    }

    render() {
        if (this.state.showCountdown) {
            return <Countdown timeout={this.getTimeUntilStartInSeconds()}/>
        }
        if (this.state.isCollected) {
            return <CollectAnswersInfo/>;
        }
        return (
            <div>
                {this.getAnswerTimeout()}
                <InfoCard titleText={getCurrentRound(this.props.game).letter}
                          additionalClassNames={styles.CPrimary}>
                    Find a word for every category. After the first player answered, you have {this.props.game.answerTimeout} seconds left to answer
                </InfoCard>
                {this.getConfirmInputButton()}
                <Form>
                    {this.listCategoryInputs()}
                </Form>
                {this.getConfirmInputButton()}
            </div>
        );
    }

    private onConfirmInput = () => {
        if (!this.inputExists() || !this.inputChanged()) {
            return;
        }
        this.sendAnswers();
    }

    private sendAnswers(): void {
        const answers = this.state.answers;
        answers.forEach(answer => answer.input = answer.input.trim());
        this.props.socket.emit(AddAnswersEvent.id, new AddAnswersEvent(this.props.game.id, answers));
    }

    private onCategoryInputChange = (category: string, input: string) => {
        const answers = this.state.answers;
        const categoryAnswer = answers.find(answer => answer.category === category);
        if (categoryAnswer) {
            categoryAnswer.input = input;
        } else {
            answers.push({category: category, input: input});
        }
        this.setState({answers})
    }

    private getCategoryInput(category: string): string {
        const catAnswer = this.state.answers.find(answer => answer.category === category);
        return catAnswer ? catAnswer.input : '';
    }

    private listCategoryInputs(): JSX.Element[] {
        return this.props.game.categories.map((cat) => {
            return <CategoryInput key={cat}
                                  category={cat}
                                  onInputChange={this.onCategoryInputChange}
                                  input={this.getCategoryInput(cat)}
                                  disabled={this.state.isCollected}
                                  letter={getCurrentRound(this.props.game).letter}/>;
        })
    }

    private getConfirmInputButton(): JSX.Element {
        return <WideButton additionalClassNames={styles.CTertiary}
                           onClick={this.onConfirmInput}
                           disabled={!this.inputExists() || !this.inputChanged()}>
            Confirm Input. You can still edit your answers afterwards
        </WideButton>;
    }

    private inputExists(): boolean {
        return this.state.answers.some(answer => answer.input && StringUtils.removeBlanks(answer.input).length > 0);
    }

    private inputChanged(): boolean {
        const existingPlayerAnswers = getPlayerAnswersInCurrentRound(this.props.game, this.props.player.id);
        return this.state.answers.length !== existingPlayerAnswers.length || !this.state.answers.every(answer => {
            return existingPlayerAnswers.find(exAnswer => exAnswer.category === answer.category && exAnswer.input === answer.input.trim());
        });
    }

    private getAnswerTimeout(): JSX.Element {
        return <AnswerTimeout socket={this.props.socket} timeout={this.props.game.answerTimeout} isRunning={false}/>;
    }

    private getTimeUntilStartInSeconds(): number {
        const secondsUntilStart = DateUtils.getTimeDifFromNowInSec(getCurrentRound(this.props.game).startTime);
        // Ensure countdown is max. 5 seconds (= 4 + 1), even when client time is different than server time
        return secondsUntilStart <= 4 ? secondsUntilStart : 4;
    }
}
