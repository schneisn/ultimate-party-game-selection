import {AbstractEvent} from '../../../../common/event/AbstractEvent';

export class AnswerTimeoutCountdownEvent extends AbstractEvent {
    static readonly id = 'AnswerTimeoutCountdown';
    remainingTimeInSeconds: number;
}
