import {AbstractEvent} from '../../../../common/event/AbstractEvent';

export class CollectAnswersEvent extends AbstractEvent {
    static readonly id = 'CollectAnswers';
}
