import {GameEvent} from '../../../../common/event/GameEvent';
import {IAnswerInput} from '../../common/model/IAnswerInput';

export class AddAnswersEvent extends GameEvent {
    static readonly id = 'AddAnswers';
    answers: IAnswerInput[];

    constructor(gameId: string, answers: IAnswerInput[]) {
        super(gameId);
        this.answers = answers;
    }
}
