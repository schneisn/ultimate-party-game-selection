import React from 'react';
import {InfoCard} from '../../../../../common/component/infoCard/InfoCard';
import styles from '../../../../styles/color.module.scss';

export class CollectAnswersInfo extends React.PureComponent {
    render() {
        return (
            <>
                <InfoCard titleText={'Collecting answers'}
                          additionalClassNames={styles.CPrimary}>
                    Collecting the answers from all players takes a few seconds...
                </InfoCard>
            </>
        );
    }
}
