import React from 'react';
import {Socket} from '../../../../../../server/socket';
import {Counter, ICounterProps, ICounterState} from '../../../../../common/component/counter/Counter';
import {SmallCard} from '../../../../../common/component/smallCard/SmallCard';
import {AnswerTimeoutCountdownEvent} from '../../event/AnswerTimeoutCountdownEvent';
import styles from './AnswerTimeout.module.scss';

interface IProps extends ICounterProps {
    socket: Socket;
}

interface IState extends ICounterState {
    isVisible: boolean;
}

/**
 * The timeout until no player can answer anymore.
 * The remaining time is updated by the server, to allow joining
 * players to see the correct time.
 * Using a time-stamp set by the server could lead to
 * de-synchronization issues between server
 * and client when the client has a different local time than the server
 */
export class AnswerTimeout extends Counter<IProps, IState> {

    constructor(props: Readonly<IProps>) {
        super(props);
        this.state = {
            ...this.state,
            isVisible: false
        }
    }

    componentDidMount() {
        super.componentDidMount();
        this.props.socket.on(AnswerTimeoutCountdownEvent.id, (data: AnswerTimeoutCountdownEvent) => {
            this.setState({
                isVisible: true,
                remainingSeconds: data.remainingTimeInSeconds
            });
        });
    }

    componentWillUnmount() {
        super.componentWillUnmount();
        this.props.socket.removeEventListener(AnswerTimeoutCountdownEvent.id);
    }

    render() {
        if (!this.state.isVisible) {
            return null;
        }
        const countdown = this.secondsToCountdown(this.state.remainingSeconds);
        return (
            <div className={styles.AnswerTimeout}>
                <SmallCard additionalClassNames={styles.TimeoutCard}>
                    Remaining time: {countdown.minutes > 0 && countdown.minutes + 'm'} {countdown.seconds}s
                </SmallCard>
            </div>
        );
    }
}
