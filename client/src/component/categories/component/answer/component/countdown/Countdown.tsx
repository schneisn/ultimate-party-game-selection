import classNames from 'classnames';
import React from 'react';
import {Card} from '../../../../../common/component/card/Card';
import {Counter} from '../../../../../common/component/counter/Counter';
import {InfoCard} from '../../../../../common/component/infoCard/InfoCard';
import styles from './Countdown.module.scss';

export class Countdown extends Counter {

    render() {
        const countdown = this.secondsToCountdown(this.state.remainingSeconds);
        return (
            <>
                <InfoCard titleText={'Ready.. Set.. Go..'}
                          additionalClassNames={styles.CPrimary}>
                    The round is about to start
                </InfoCard>
                <Card additionalClassNames={classNames(styles.Countdown, styles.CSecondary)}>
                    {countdown.seconds + 1}
                </Card>
            </>
        );
    }
}
