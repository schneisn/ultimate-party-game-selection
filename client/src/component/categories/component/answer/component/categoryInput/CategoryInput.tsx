import classNames from 'classnames';
import React from 'react';
import {HeaderContainer} from '../../../../../common/component/headerContainer/HeaderContainer';
import {TextArea} from '../../../../../common/component/textArea/TextArea';
import styles from './CategoryInput.module.scss';

interface IProps {
    category: string;
    input: string;
    disabled: boolean;
    letter: string;
    onInputChange: (category: string, input: string) => void;
}

export class CategoryInput extends React.Component<IProps> {

    render() {
        return (
            <HeaderContainer header={this.props.category} additionalClassNames={classNames(styles.CSecondary, styles.CategoryInput)}>
                <TextArea disabled={this.props.disabled}
                          placeholder={this.props.letter + '...'}
                          value={this.props.input}
                          onChange={(event) => this.props.onInputChange(this.props.category, event.currentTarget.value)}
                          maxLength={200}/>
            </HeaderContainer>
        );
    }
}
