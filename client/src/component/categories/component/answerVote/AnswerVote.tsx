import React from 'react';
import {Socket} from '../../../../server/socket';
import {WideButton} from '../../../common/component/wideButton/WideButton';
import {CheckboxWithLabel} from '../../../common/component/checkboxWithLabel/CheckboxWithLabel';
import {HeaderContainer} from '../../../common/component/headerContainer/HeaderContainer';
import {InfoCard} from '../../../common/component/infoCard/InfoCard';
import {StringUtils} from '../../../common/utils/string';
import {IAnswer} from '../../model/IAnswer';
import {IGame} from '../../model/IGame';
import {IPlayer} from '../../model/IPlayer';
import styles from '../../styles/color.module.scss';
import {getCurrentRound, playerIsInRound} from '../../utils/round';
import {IAnswerInput} from '../common/model/IAnswerInput';
import {VoteInvalidAnswersEvent} from './event/VoteInvalidAnswersEvent';
import {Emoji} from '../../../common/component/emoji/Emoji.';

interface IProps {
    game: IGame;
    player: IPlayer;
    socket: Socket;
}

interface IState {
    invalidAnswers: IAnswerInput[];
}

export class AnswerVote extends React.Component<IProps, IState> {

    constructor(props: Readonly<IProps>) {
        super(props);
        this.state = {
            invalidAnswers: []
        }
    }

    private static getDistinctInputs(answers: IAnswer[]): string[] {
        return Array.from(new Set(answers.map(a => a.input)));
    }

    render() {
        return (
            <div>
                <InfoCard titleText={'Vote'}
                          additionalClassNames={styles.CPrimary}>
                    <div>Click on the answers that you think are <Emoji symbol={'❗'}/><b>not correct</b><Emoji symbol={'❗'}/></div>
                    <div>When more than 50 % of players say an answer it not correct, it will not award any points</div>
                </InfoCard>
                {this.getWaitForOtherPlayers()}
                {this.getAnswers()}
            </div>
        );
    }

    private onAnswerClick(category: string, input: string): void {
        const invalidAnswers = this.state.invalidAnswers;
        const existingAnswerIndex = invalidAnswers.findIndex(answer => answer.category === category && answer.input === input);
        if (existingAnswerIndex >= 0) {
            invalidAnswers.splice(existingAnswerIndex, 1);
        } else {
            invalidAnswers.push({category: category, input: input});
        }
        this.setState({invalidAnswers: invalidAnswers});
    }

    private getAnswers(): JSX.Element {
        return !this.props.player.hasVoted && playerIsInRound(this.props.game, this.props.player.id) &&
          <>
              {this.getCategories()}
            <WideButton onClick={this.onConfirmVoteClick} additionalClassNames={styles.CTertiary}>
              Confirm Votes
            </WideButton>
          </>;
    }

    private getCategories(): JSX.Element[] {
        return this.props.game.categories.map((category) => {
            return <HeaderContainer key={category}
                                    header={StringUtils.toFirstUpperCase(category)}
                                    additionalClassNames={styles.CSecondary}>
                {this.getCategoryAnswers(category)}
            </HeaderContainer>;
        })
    }

    private getCategoryAnswers(category: string): JSX.Element[] {
        const categoryAnswers = getCurrentRound(this.props.game).answers.filter(a => a.category === category);
        const answers = AnswerVote.getDistinctInputs(categoryAnswers).filter(input => input && input !== '').map((input: string) => {
            return <CheckboxWithLabel key={input}
                                      additionalClassNames={styles.CTertiary}
                                      checked={this.answerIsSelected(category, input)}
                                      checkedClassNames={styles.Selected}
                                      onClick={() => this.onAnswerClick(category, input)}>
                {input}
            </CheckboxWithLabel>;
        });
        if (!answers || answers.length < 1) {
            return [<i key={'no-answer'}>No Answers</i>];
        }
        return answers;
    }

    private answerIsSelected(category: string, input: string): boolean {
        return this.state.invalidAnswers.some(answer => answer.category === category && answer.input === input);
    }

    private onConfirmVoteClick = (): void => {
        this.props.socket.emit(VoteInvalidAnswersEvent.id, new VoteInvalidAnswersEvent(this.props.game.id, this.state.invalidAnswers));
    }

    private getWaitForOtherPlayers(): JSX.Element {
        const playerHasVoted = this.props.player.hasVoted;
        return (playerHasVoted || !playerIsInRound(this.props.game, this.props.player.id)) &&
          <InfoCard titleText={'Wait until the other players have voted'}
                    additionalClassNames={styles.CSecondary}>
              {this.props.game.players
                  .filter(p => !p.hasVoted && playerIsInRound(this.props.game, p.id))
                  .map(p => <div key={p.id} className={styles.EllipseText}>{p.name}</div>)}
          </InfoCard>;
    }
}
