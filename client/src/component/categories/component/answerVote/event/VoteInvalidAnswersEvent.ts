import {GameEvent} from '../../../../common/event/GameEvent';
import {IAnswerInput} from '../../common/model/IAnswerInput';

export class VoteInvalidAnswersEvent extends GameEvent {
    static readonly id = 'VoteInvalidAnswers';
    invalidAnswers: IAnswerInput[];

    constructor(gameId: string, invalidAnswers: IAnswerInput[]) {
        super(gameId);
        this.invalidAnswers = invalidAnswers;
    }
}
