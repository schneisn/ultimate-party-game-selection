import React from 'react';
import {Socket} from '../../../../server/socket';
import {IGame} from '../../model/IGame';
import {IPlayer} from '../../model/IPlayer';
import {Result} from './component/result/Result';
import {Scores} from './component/scores/Scores';

interface IProps {
    game: IGame;
    player: IPlayer;
    socket: Socket;
}

interface IState {
    showAllAnswers: boolean;
}

export class Scoreboard extends React.Component<IProps, IState> {

    constructor(props: Readonly<IProps>) {
        super(props);
        this.state = {
            showAllAnswers: true
        }
    }

    render() {
        if (this.state.showAllAnswers) {
            return <Result game={this.props.game}
                           player={this.props.player}
                           socket={this.props.socket}
                           onGoToScoreButtonClick={this.onNavButtonClick}/>;
        }
        return <Scores game={this.props.game}
                       onGoToResultsButtonClick={this.onNavButtonClick}
                       player={this.props.player}
                       socket={this.props.socket}/>;
    }

    private onNavButtonClick = (): void => {
        this.setState({showAllAnswers: !this.state.showAllAnswers});
    }
}
