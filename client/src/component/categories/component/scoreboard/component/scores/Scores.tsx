import React from 'react';
import {Socket} from '../../../../../../server/socket';
import {WideButton} from '../../../../../common/component/wideButton/WideButton';
import {CommonScoreboard, IScore} from '../../../../../common/component/commonScoreboard/CommonScoreboard';
import {generateCommonScoreboardStyles} from '../../../../../common/component/commonScoreboard/utils/styles';
import {IAnswer} from '../../../../model/IAnswer';
import {IGame} from '../../../../model/IGame';
import {IPlayer} from '../../../../model/IPlayer';
import {IRound} from '../../../../model/IRound';
import styles from '../../../../styles/color.module.scss';
import {getCurrentRound} from '../../../../utils/round';

interface IProps {
    game: IGame;
    onGoToResultsButtonClick: () => void;
    player: IPlayer;
    socket: Socket;
}

export class Scores extends React.Component<IProps> {

    render() {
        return (<>
                <CommonScoreboard game={this.props.game}
                                  player={this.props.player}
                                  socket={this.props.socket}
                                  showIsReady={true}
                                  scores={this.getScores()}
                                  styles={generateCommonScoreboardStyles(styles.CPrimary, styles.CSecondary, styles.CTertiary)}/>
                <WideButton onClick={this.props.onGoToResultsButtonClick}
                            additionalClassNames={styles.CTertiary}>
                    Go back to see all answers
                </WideButton>
            </>
        );
    }

    private getScores(): IScore[] {
        return this.props.game.players.map((player: IPlayer) => {
            return {
                totalScore: this.getPlayerTotalScore(player.id),
                roundScore: this.getPlayerCurrentRoundScore(player.id),
                player: player
            }
        });
    }

    private getPlayerCurrentRoundScore(playerId: string): number {
        return this.getPlayerRoundScore(playerId, getCurrentRound(this.props.game));
    }

    private getPlayerRoundScore(playerId: string, round: IRound): number {
        return round.answers.filter(a => a.playerId === playerId)
            .reduce((sum: number, answer: IAnswer) => sum + answer.score, 0);
    }

    private getPlayerTotalScore(playerId: string): number {
        return this.props.game.rounds.reduce((sum: number, round: IRound) => sum + this.getPlayerRoundScore(playerId, round), 0);
    }
}
