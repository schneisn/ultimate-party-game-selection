import classNames from 'classnames';
import React from 'react';
import {Socket} from '../../../../../../server/socket';
import {WideButton} from '../../../../../common/component/wideButton/WideButton';
import {ReadyButton} from '../../../../../common/component/commonLobby/component/readyButton/ReadyButton';
import {DisconnectedIcon} from '../../../../../common/component/disconnectedIcon/DisconnectedIcon';
import {HeaderContainer} from '../../../../../common/component/headerContainer/HeaderContainer';
import {IconType} from '../../../../../common/component/icon/Icon';
import {InfoCard} from '../../../../../common/component/infoCard/InfoCard';
import {Popover} from '../../../../../common/component/popover/Popover';
import {generatePopoverStyles} from '../../../../../common/component/popover/utils/styles';
import {SmallCard} from '../../../../../common/component/smallCard/SmallCard';
import {getPlayer} from '../../../../../common/utils/player';
import {StringUtils} from '../../../../../common/utils/string';
import {IAnswer} from '../../../../model/IAnswer';
import {IGame} from '../../../../model/IGame';
import {IPlayer} from '../../../../model/IPlayer';
import {getCurrentRound} from '../../../../utils/round';
import {playerIsReady} from '../../utils/scoreboardUtils';
import styles from './Result.module.scss';

interface IProps {
    game: IGame;
    onGoToScoreButtonClick: () => void;
    player: IPlayer;
    socket: Socket;
}

export class Result extends React.Component<IProps> {

    private static getUniqueAnswerKey(answer: IAnswer): string {
        return StringUtils.stringToId(answer.playerId + '-' + answer.category + '-' + answer.input);
    }

    render() {
        return (
            <div>
                <InfoCard titleText={'Results'} additionalClassNames={styles.CPrimary}>
                    Here you see all answers from all players
                </InfoCard>
                {this.getAllAnswers()}
                {this.props.game.gameModeNoScore ?
                    <ReadyButton game={this.props.game}
                                 player={this.props.player}
                                 socket={this.props.socket}
                                 additionalClasses={styles.CTertiary}/> :
                    <WideButton onClick={this.props.onGoToScoreButtonClick} additionalClassNames={styles.CTertiary}>
                        Go to scoreboard
                    </WideButton>}
            </div>
        );
    }

    private getAllAnswers(): JSX.Element {
        const categories = this.props.game.categories;
        return <>
            {categories.map((category: string) => {
                return <HeaderContainer key={category} header={category} additionalClassNames={styles.CSecondary}>
                    {this.getCategoryAnswers(category)}
                </HeaderContainer>;
            })}
        </>;
    }

    private getCategoryAnswers(category: string): JSX.Element[] {
        const {game} = this.props;

        const categoryAnswers = getCurrentRound(game).answers.filter(a => a.category === category);
        return categoryAnswers.map((answer: IAnswer) => {
            return <SmallCard key={Result.getUniqueAnswerKey(answer)}
                              additionalClassNames={classNames(styles.CategoryAnswer, styles.CTertiary)}>
                <div>
                    {this.getValidityIcon(answer)}
                </div>
                <div className={styles.Result}>
                    <div className={styles.PlayerInfo}>
                        <div className={styles.PlayerNameAndConnection}>
                            <div className={styles.EllipseText}>
                                <b>{getPlayer(game, answer.playerId)?.name}</b>
                            </div>
                            {!getPlayer<IPlayer>(game, answer.playerId)?.isConnected && <DisconnectedIcon/>}
                        </div>
                        {game.gameModeNoScore && <div className={styles.ReadyState}>
                            {playerIsReady(game, answer.playerId) ? 'Ready' : 'Not Ready'}
                        </div>}
                    </div>
                    <div className={styles.Answer}>
                        {answer.input ? answer.input : <i>No Answer</i>}
                    </div>
                </div>
            </SmallCard>;
        });
    }

    private getValidityIcon(answer: IAnswer): JSX.Element {
        if (this.props.game.gameModeNoScore) {
            return null;
        }
        let icon = IconType.check;
        let text: string = 'Valid answer';
        if (answer.isDuplicate) {
            icon = IconType.exclamation;
            text = 'Duplicate answer';
        }
        if (!answer.isValid || answer.input === '') {
            icon = IconType.times;
            text = 'Invalid answer';
        }
        return <Popover id={'icon' + Result.getUniqueAnswerKey(answer)}
                        icon={icon}
                        customStyles={generatePopoverStyles(styles.ValidityIcon, styles.CTertiary)}>
            {text}
        </Popover>
    }
}
