import {IGame} from '../../../model/IGame';

export function playerIsReady(game: IGame, playerId: string): boolean {
    return game.players.find(player => player.id === playerId)?.isReady;
}
