import {IAnswer} from '../model/IAnswer';
import {IGame} from '../model/IGame';
import {getCurrentRound} from './round';

export function getPlayerAnswersInCurrentRound(game: IGame, playerId: string): IAnswer[] {
    return getCurrentRound(game).answers.filter(a => a.playerId === playerId);
}
