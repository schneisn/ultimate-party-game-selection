import {IGame} from '../model/IGame';
import {IRound} from '../model/IRound';

export function getCurrentRound(game: IGame): IRound {
    return game.rounds[game.rounds.length - 1];
}

/**
 * Checks whether the player participated in the current round.
 * The player participated in the round if there is an answer
 * from the player
 * @param game
 * @param playerId
 */
export function playerIsInRound(game: IGame, playerId: string): boolean {
    return !!getCurrentRound(game).answers.find(a => a.playerId === playerId);
}
