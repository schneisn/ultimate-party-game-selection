import React from 'react';
import {RouteComponentProps} from 'react-router-dom';
import {Urls} from '../../server/serverUrls';
import {BaseGame} from '../common/component/baseGame/BaseGame';
import {generateConnectToastStyles} from '../common/component/baseGame/component/connectToast/utils/styles';
import {generateBaseGameStyles} from '../common/component/baseGame/utils/styles';
import {generateModalStyles} from '../common/component/modal/utils/styles';
import {generatePopoverStyles} from '../common/component/popover/utils/styles';
import {Lobby} from './component/lobby/Lobby';
import {Round} from './component/round/Round';
import {IGame} from './model/IGame';
import {IPlayer} from './model/IPlayer';
import styles from './styles/color.module.scss';
import {getCurrentRound} from './utils/round';
import {UrlUtils} from '../common/utils/url';
import Scoreboard from './component/scoreboard/Scoreboard';
import {ICommonPlayer} from '../common/model/ICommonPlayer';
import {IConnectionReadyPlayer} from '../common/model/IConnectionReadyPlayer';
import {playerIsGameMaster} from '../common/utils/player';
import {Card} from '../common/component/card/Card';
import {PlayerInfo} from '../common/component/playerInfo/PlayerInfo';
import {Modal} from '../common/component/modal/Modal';

export class Privacy extends BaseGame<IGame, IPlayer> {

    constructor(props: Readonly<RouteComponentProps>) {
        super(props);
        const options = {
            gameName: 'Privacy',
            gamePath: UrlUtils.getPathFromString(this.props.match.path),
            socketNamespace: Urls.Privacy,
            styles: generateBaseGameStyles(
                generateModalStyles(styles.PPrimary, styles.PSecondary, styles.PTertiary),
                generateConnectToastStyles(styles.PConnect, styles.PDisconnect),
                null,
                styles.PBackground,
                generatePopoverStyles(styles.PTertiaryIcon, styles.PTertiary)
            )
        };
        this.state = {
            ...this.state,
            options: options
        };
    }

    renderGame() {
        const currentRound = getCurrentRound(this.state.game);
        if (!currentRound) {
            return <Lobby socket={this.state.socket} game={this.state.game} player={this.state.player}/>;
        }
        if (currentRound.isLocked) {
            return <Scoreboard socket={this.state.socket} game={this.state.game} player={this.state.player}/>;
        }
        return <Round socket={this.state.socket} game={this.state.game} player={this.state.player}/>;
    }

    protected getPlayerModal(): JSX.Element {
        const {isPlayerModalOpen, game} = this.state;
        const playerElements = game?.players
            .map((teamMate: ICommonPlayer & Partial<IConnectionReadyPlayer>) => {
                if ((playerIsGameMaster(game, teamMate.id))) {
                    teamMate.isGameMaster = true;
                }
                return teamMate;
            })
            .map(teamMate => {
                return <Card key={teamMate.id}>
                    <PlayerInfo player={teamMate}/>
                </Card>
            });

        return <Modal modalTitle={'Players'}
                      isOpen={isPlayerModalOpen}
                      showFooter={false}
                      onToggle={this.togglePlayerModal}>
            {playerElements}
        </Modal>
    }
}
