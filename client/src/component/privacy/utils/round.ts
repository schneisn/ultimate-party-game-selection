import {IGame} from '../model/IGame';
import {IRound} from '../model/IRound';

export function getCurrentRound(game: IGame): IRound {
    return game?.rounds[game.rounds.length - 1];
}

export function playerParticipatesInRound(currentRound: IRound, playerId: string): boolean {
    return currentRound?.guessInputs.some(guess => guess.playerId === playerId);
}
