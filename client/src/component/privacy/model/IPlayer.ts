import {IConnectionReadyPlayer} from '../../common/model/IConnectionReadyPlayer';

export interface IPlayer extends IConnectionReadyPlayer {
}
