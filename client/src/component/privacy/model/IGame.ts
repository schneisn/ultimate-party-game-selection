import {ICommonGame} from '../../common/model/ICommonGame';
import {IPlayer} from './IPlayer';
import {IRound} from './IRound';

export interface IGame extends ICommonGame<IPlayer> {
    questionCategories: string[];
    rounds: IRound[];
}
