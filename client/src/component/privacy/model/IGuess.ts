export interface IGuess {
    playerId: string;
    guess: number;
    score: number;
}
