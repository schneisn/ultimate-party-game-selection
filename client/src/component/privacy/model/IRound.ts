import {IGuess} from './IGuess';

export interface IRound {
    number: number;
    guessInputs: IGuess[];
    question: string;
    isLastRound: boolean;
    isLocked: boolean;
    yesVotes: number;
}
