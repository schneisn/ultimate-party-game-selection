import React from 'react';
import {Socket} from '../../../../server/socket';
import {WideButton} from '../../../common/component/wideButton/WideButton';
import {CommonScoreboard, IScore} from '../../../common/component/commonScoreboard/CommonScoreboard';
import {generateCommonScoreboardStyles} from '../../../common/component/commonScoreboard/utils/styles';
import {Emoji} from '../../../common/component/emoji/Emoji.';
import {Icon, IconType} from '../../../common/component/icon/Icon';
import {SmallCard} from '../../../common/component/smallCard/SmallCard';
import {IGame} from '../../model/IGame';
import {IPlayer} from '../../model/IPlayer';
import {getCurrentRound} from '../../utils/round';
import {StartNextRoundEvent} from './event/StartNextRoundEvent';
import styles from './Scoreboard.module.scss';
import classNames from 'classnames';
import {UrlUtils} from '../../../common/utils/url';
import {RouteComponentProps, withRouter} from 'react-router-dom';

interface IProps extends RouteComponentProps {
    game: IGame;
    player: IPlayer;
    socket: Socket;
}

class Scoreboard extends React.Component<IProps> {

    render() {
        const currentRound = getCurrentRound(this.props.game);

        return <>
            <CommonScoreboard game={this.props.game}
                              player={this.props.player}
                              socket={this.props.socket}
                              scores={this.getScores()}
                              description={this.getDescription()}
                              showIsReady={false}
                              customHeaderRound={'Guess'}
                              customHeaderTotal={'Points'}
                              styles={generateCommonScoreboardStyles(styles.PPrimary, styles.PSecondary, styles.PTertiary)}
                              showButton={false}
                              additionalContent={this.getTotalYesVotesText()}/>
            {this.getNextRoundButton()}
            {currentRound.isLastRound && <>
              <SmallCard additionalClassNames={styles.LastRoundInfo}>
                This was the last round. There are no more questions <Emoji symbol={'😢'}/>
              </SmallCard>
              <WideButton onClick={() => UrlUtils.goHierarchyUp(this.props.history)}
                          additionalClassNames={styles.PTertiary}>
                No more questions left. Click to leave the game
              </WideButton>
            </>}
        </>
    }

    private getGameMaster(): IPlayer {
        return this.props.game.players.find(player => player.isConnected && this.playerParticipatedInRound(player.id));
    }

    private playerCanSeeNextRoundButton(): boolean {
        return this.getGameMaster()?.id === this.props.player.id;
    }

    private getNextRoundButton(): JSX.Element {
        const {game} = this.props;
        const currentRound = getCurrentRound(this.props.game);
        const playerCanSeeButton = !currentRound.isLastRound && this.playerCanSeeNextRoundButton();
        return playerCanSeeButton ?
            <WideButton additionalClassNames={classNames(styles.StickyButton, styles.PTertiary)}
                        onClick={() => this.props.socket.emit(StartNextRoundEvent.id, new StartNextRoundEvent(game.id))}>
                Start next round
            </WideButton> :
            <SmallCard additionalClassNames={styles.PSecondary}>{this.getGameMaster().name} will start the next round</SmallCard>;
    }

    private getDescription(): JSX.Element {
        const currentRound = getCurrentRound(this.props.game);
        return <b>{currentRound.question}</b>;
    }

    private getScores(): IScore[] {
        return this.props.game.players.filter(player => this.playerParticipatedInRound(player.id)).map(player => {
            return {
                player: player,
                roundScore: this.getRoundScore(player.id),
                totalScore: this.getTotalScoreForPlayer(player.id)
            }
        });
    }

    private getRoundScore(playerId: string): JSX.Element {
        switch (this.getRoundScoreForPlayer(playerId)) {
            case 1:
                return this.getOneOffGuess(playerId);
            case 3:
                return this.getCorrectGuess(playerId);
            default:
                return this.getIncorrectGuess(playerId);
        }
    }

    private getCorrectGuess(playerId: string): JSX.Element {
        return <>{this.getPlayerGuess(playerId)} <Icon icon={IconType.check} additionalClasses={styles.GuessCorrect}/></>;
    }

    private getOneOffGuess(playerId: string): JSX.Element {
        return <>{this.getPlayerGuess(playerId)} <Icon icon={IconType.exclamation} additionalClasses={styles.GuessOneOff}/></>;
    }

    private getIncorrectGuess(playerId: string): JSX.Element {
        return <>{this.getPlayerGuess(playerId)} <Icon icon={IconType.times} additionalClasses={styles.GuessIncorrect}/></>;
    }

    private getPlayerGuess(playerId: string): number {
        const currentRound = getCurrentRound(this.props.game);
        return currentRound.guessInputs.find(gi => gi.playerId === playerId)?.guess;
    }

    private playerParticipatedInRound(playerId: string): boolean {
        return getCurrentRound(this.props.game).guessInputs.some(gi => gi.playerId === playerId);
    }

    private getRoundScoreForPlayer(playerId: string): number {
        const currentRound = getCurrentRound(this.props.game);
        for (let i = 0; i < currentRound.guessInputs.length; i++) {
            if (currentRound.guessInputs[i].playerId === playerId) {
                return currentRound.guessInputs[i].score;
            }
        }
    }

    private getTotalScoreForPlayer(playerId: string): number {
        let score = 0;
        for (const round of this.props.game.rounds) {
            for (const guess of round.guessInputs) {
                if (guess.playerId === playerId) {
                    score += guess.score;
                }
            }
        }
        return score;
    }

    private getYesCount(): number {
        return getCurrentRound(this.props.game).yesVotes;
        // return getCurrentRound(this.props.game).guessInputs.reduce((total: number, guess: IGuess) => total + (guess.selectedCard === 'yes' ? 1 : 0), 0);
    }

    private getTotalYesVotesText(): JSX.Element {
        return <SmallCard additionalClassNames={styles.PSecondary}>
            {this.getYesCount() + ' Player' + (this.getYesCount() > 1 || this.getYesCount() < 1 ? 's have ' : ' has ') + 'answered YES'}
        </SmallCard>;
    }
}

export default withRouter(Scoreboard);
