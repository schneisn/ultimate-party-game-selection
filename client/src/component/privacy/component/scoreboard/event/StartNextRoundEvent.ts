import {GameEvent} from '../../../../common/event/GameEvent';

export class StartNextRoundEvent extends GameEvent {
    static readonly id = 'StartNextRound';
}
