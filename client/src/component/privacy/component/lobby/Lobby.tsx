import React from 'react';
import {Socket} from '../../../../server/socket';
import {CommonLobby} from '../../../common/component/commonLobby/CommonLobby';
import {generateCommonLobbyStyles} from '../../../common/component/commonLobby/utils/styles';
import {getGameMaster, playerIsGameMaster} from '../../../common/utils/player';
import {IGame} from '../../model/IGame';
import {IPlayer} from '../../model/IPlayer';
import styles from '../../styles/color.module.scss';
import {GetQuestionCategoriesEvent} from './event/GetQuestionCategoriesEvent';
import {IGetQuestionCategories} from './event/in/IGetQuestionCategories';
import {SetQuestionCategoriesEvent} from './event/SetQuestionCategoriesEvent';
import {CheckboxList, ICheckboxListItem} from '../../../common/component/checkboxList/CheckboxList';
import {generateCheckboxListStyles} from '../../../common/component/checkboxList/utils/stylesUtils';

interface IProps {
    game: IGame;
    player: IPlayer;
    socket: Socket;
}

interface IState {
    questionCategories: string[];
}

export class Lobby extends React.Component<IProps, IState> {

    constructor(props: Readonly<IProps>) {
        super(props);
        this.state = {
            questionCategories: []
        }
    }

    componentDidMount() {
        this.props.socket.emit(GetQuestionCategoriesEvent.id, new GetQuestionCategoriesEvent(this.props.game.id));
        this.props.socket.on(GetQuestionCategoriesEvent.id, (data: IGetQuestionCategories) => {
            this.setState({
                questionCategories: data.questionCategories
            });
        });
    };

    componentWillUnmount() {
        this.props.socket.removeEventListener(GetQuestionCategoriesEvent.id);
    }

    render() {
        const {player, socket, game} = this.props;
        return (
            <div>
                <CommonLobby
                    styles={generateCommonLobbyStyles(styles.PPrimary, null, styles.PSecondary, styles.PTertiary)}
                    game={game}
                    gameMasterId={getGameMaster(game)?.id}
                    player={player}
                    socket={socket}
                    buttonIsDisabled={this.buttonIsDisabled()}
                    buttonText={this.getButtonText()}>
                    <CheckboxList listHeader={'Choose categories'}
                                  items={this.listQuestionCategories()}
                                  listStyles={generateCheckboxListStyles(styles.PSecondary, styles.PTertiary)}
                                  onCheckboxSelectNone={this.selectNoCategory}
                                  onCheckboxSelectAll={this.selectAllCategories}
                                  showCheckAllIcon={true}
                                  checkAllIsDisabled={!playerIsGameMaster(game, player.id)}
                                  filterIsDisabled={!playerIsGameMaster(game, player.id)}/>
                </CommonLobby>
            </div>
        )
    }

    private listQuestionCategories(): ICheckboxListItem[] {
        return this.state.questionCategories?.map((categoryName: string) => {
            return {
                id: categoryName,
                content: categoryName,
                checked: this.categoryIsSelected(categoryName),
                checkable: playerIsGameMaster(this.props.game, this.props.player.id),
                onClick: () => (this.setCategoryIsSelected(categoryName))
            }
        });
    };

    private categoryIsSelected(categoryName: string): boolean {
        return !!this.props.game.questionCategories.find(selectedCategory => selectedCategory === categoryName);
    }

    private setCategoryIsSelected = (categoryName: string) => {
        const gameQuestionCategory = this.props.game.questionCategories;
        let index = 0;
        let gameContainedCategory = false;
        for (const gameCat of gameQuestionCategory) {
            if (gameCat === categoryName) {
                gameContainedCategory = true;
                gameQuestionCategory.splice(index, 1);
                break;
            }
            index++;
        }
        if (!gameContainedCategory) {
            gameQuestionCategory.push(categoryName);
        }

        this.emitCategories(gameQuestionCategory);
    };

    private buttonIsDisabled(): boolean {
        return !this.hasEnoughPlayers() || this.props.game.questionCategories.length <= 0;
    }

    private getButtonText(): string {
        if (this.hasEnoughPlayers() && this.props.game.questionCategories.length <= 0) {
            return 'Need to select at least one question category!';
        }
        return null;
    }

    private hasEnoughPlayers(): boolean {
        return this.props.game && this.props.game.players.length >= 2;
    }

    private selectNoCategory = () => {
        this.emitCategories([]);
    }

    private selectAllCategories = () => {
        this.emitCategories(this.state.questionCategories);
    }

    private emitCategories = (gameQuestionCategory: string[]) => {
        this.props.socket.emit(SetQuestionCategoriesEvent.id, new SetQuestionCategoriesEvent(this.props.game.id, gameQuestionCategory));
    }
}
