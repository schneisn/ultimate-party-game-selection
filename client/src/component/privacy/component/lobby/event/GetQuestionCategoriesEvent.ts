import {GameEvent} from '../../../../common/event/GameEvent';

export class GetQuestionCategoriesEvent extends GameEvent {
    static readonly id = 'GetQuestionCategories';
}
