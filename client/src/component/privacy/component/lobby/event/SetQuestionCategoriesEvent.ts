import {GameEvent} from '../../../../common/event/GameEvent';

export class SetQuestionCategoriesEvent extends GameEvent {
    static readonly id = 'SetQuestionCategories';
    questionCategories: string[];

    constructor(gameId: string, questionCategories: string[]) {
        super(gameId);
        this.questionCategories = questionCategories;
    }
}
