import React from 'react';
import {WideButton} from '../../../../../common/component/wideButton/WideButton';
import {HeaderContainer} from '../../../../../common/component/headerContainer/HeaderContainer';
import {IGuess} from '../../../../model/IGuess';
import {IRound} from '../../../../model/IRound';
import styles from '../../../../styles/color.module.scss';

interface IProps {
    backToCards: () => void;
    round: IRound;
    handleGuessInput: (i: number) => void;
}

export class GuessInput extends React.Component<IProps> {

    render() {
        return <>
            <HeaderContainer header={'Guess how many players have answered YES'}
                             additionalClassNames={styles.PSecondary}>
                {this.createGuessInputCards()}
            </HeaderContainer>
            <WideButton onClick={this.props.backToCards}
                        additionalClassNames={styles.PTertiary}>
                Back
            </WideButton>
        </>
    }

    private createGuessInputCards(): JSX.Element[] {
        return this.props.round.guessInputs.slice(0, -1).map((guess: IGuess, index: number) => {
            const nr = index + 1;
            return <WideButton key={nr}
                               additionalClassNames={styles.PTertiary}
                               onClick={() => this.props.handleGuessInput(nr)}>
                {nr}
            </WideButton>;
        });
    };
}
