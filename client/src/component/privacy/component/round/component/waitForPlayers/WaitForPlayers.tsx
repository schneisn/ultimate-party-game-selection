import React from 'react';
import {Socket} from '../../../../../../server/socket';
import {WideButton} from '../../../../../common/component/wideButton/WideButton';
import {SmallCard} from '../../../../../common/component/smallCard/SmallCard';
import {IGame} from '../../../../model/IGame';
import {IPlayer} from '../../../../model/IPlayer';
import {getCurrentRound} from '../../../../utils/round';
import {SetInputEvent} from '../../event/SetInputEvent';
import styles from './WaitForPlayers.module.scss';

interface IProps {
    game: IGame;
    player: IPlayer;
    socket: Socket;
}

export class WaitForPlayers extends React.Component<IProps> {
    render() {
        return <>
            <SmallCard additionalClassNames={styles.PSecondary}>
                <b>Wait for other players</b>
                {this.getListOfGuessingPlayers()}
            </SmallCard>
            <WideButton additionalClassNames={styles.PTertiary}
                        onClick={this.emitResetGuessInput}>
                Reset answer
            </WideButton>
        </>
    }

    private getListOfGuessingPlayers(): JSX.Element {
        const playerList = this.playersWhoDidNotGuess().map((player) => <div key={player.id}>{player.name}</div>);
        return <div>{playerList}</div>;
    }

    private playersWhoDidNotGuess(): IPlayer[] {
        const guessPlayers = [];
        const currentRound = getCurrentRound(this.props.game);
        for (const player of this.props.game.players) {
            for (const guess of currentRound.guessInputs) {
                if (guess.playerId === player.id && guess.guess < 1) {
                    guessPlayers.push(player);
                }
            }
        }
        return guessPlayers;
    };

    private emitResetGuessInput = () => {
        const {game, socket} = this.props;
        socket.emit(SetInputEvent.id, new SetInputEvent(game.id, null, null));
    }
}
