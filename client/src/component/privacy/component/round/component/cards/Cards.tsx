import classNames from 'classnames';
import React from 'react';
import styles from './Cards.module.scss';
import {StringUtils} from '../../../../../common/utils/string';
import {WideButton} from '../../../../../common/component/wideButton/WideButton';

enum CardValue {
    Yes = 'yes',
    No = 'no'
}

interface IProps {
    handleYesNo: (selection: string) => void;
}

interface IState {
    isClicked: boolean;
}

export class Cards extends React.Component<IProps, IState> {

    constructor(props: Readonly<IProps>) {
        super(props);
        this.state = {
            isClicked: false
        };
    }

    render() {
        const {isClicked} = this.state;
        return (
            <div className={styles.Cards}>
                <div className={styles.Card}>
                    <WideButton additionalClassNames={classNames(styles.PTertiary, styles.Yes, isClicked && styles.RotateAnimation)}
                                onClick={() => this.onClick(CardValue.Yes)}>
                        {StringUtils.toFirstUpperCase(CardValue.Yes)}
                    </WideButton>
                </div>
                <div className={styles.CardSpace}/>
                <div className={styles.Card}>
                    <WideButton additionalClassNames={classNames(styles.PTertiary, styles.No, isClicked && styles.RotateAnimation)}
                                onClick={() => this.onClick(CardValue.No)}>
                        {StringUtils.toFirstUpperCase(CardValue.No)}
                    </WideButton>
                </div>
            </div>
        )
    }

    private onClick = (selection: CardValue) => {
        const animationTime = 300;
        this.setState({isClicked: true});
        setTimeout(() => {
            this.props.handleYesNo && this.props.handleYesNo(selection);
        }, animationTime);
    }
}
