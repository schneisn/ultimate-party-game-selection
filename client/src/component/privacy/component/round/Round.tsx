import React from 'react';
import {Socket} from '../../../../server/socket';
import {Expire} from '../../../common/component/expire/Expire';
import {InfoCard} from '../../../common/component/infoCard/InfoCard';
import {SmallCard} from '../../../common/component/smallCard/SmallCard';
import {Position, Toast} from '../../../common/component/toast/Toast';
import {IGame} from '../../model/IGame';
import {IPlayer} from '../../model/IPlayer';
import {getCurrentRound, playerParticipatesInRound} from '../../utils/round';
import {Cards} from './component/cards/Cards';
import {GuessInput} from './component/guessInput/GuessInput';
import {WaitForPlayers} from './component/waitForPlayers/WaitForPlayers';
import {IResetRound} from './event/in/IResetRound';
import {ResetRoundEvent} from './event/ResetRoundEvent';
import {SetInputEvent} from './event/SetInputEvent';
import styles from './Round.module.scss';

interface IInput {
    selectedCard: string;
    guess: number;
}

interface IProps {
    game: IGame;
    player: IPlayer;
    socket: Socket;
}

interface IState {
    disconnectedPlayer: IPlayer;
    state: number;
    input: IInput;
}

export class Round extends React.Component<IProps, IState> {

    constructor(props: Readonly<IProps>) {
        super(props);
        this.state = {
            disconnectedPlayer: null,
            state: 0,
            input: {
                selectedCard: '',
                guess: 0,
            }
        };
    }

    componentDidMount() {
        this.props.socket.on(ResetRoundEvent.id, (data: IResetRound) => this.resetRound(data.player));
    }

    componentWillUnmount() {
        this.props.socket.removeEventListener(ResetRoundEvent.id);
    }

    render() {
        const {player, game} = this.props;
        const currentRound = getCurrentRound(game);
        if (!playerParticipatesInRound(currentRound, player.id)) {
            return <>
                <InfoCard titleText={'Question'}
                          additionalClassNames={styles.PPrimary}>
                    {currentRound.question}
                </InfoCard>
                <SmallCard additionalClassNames={styles.PSecondary}>Wait for next round to join</SmallCard>
            </>;
        }
        return <>
            {this.state.disconnectedPlayer && this.getDisconnectedPlayerToast()}
            {this.getQuestion()}
            {this.getState()}
        </>;
    }

    private getDisconnectedPlayerToast(): JSX.Element {
        return <Toast position={Position.Bottom}>
            <Expire onExpire={() => this.setState({disconnectedPlayer: null})}>
                <SmallCard additionalClassNames={styles.CardPlayerDisconnected}>
                    <span>{this.state.disconnectedPlayer.name} disconnected</span>
                    <span>The round has to be played again</span>
                </SmallCard>
            </Expire>
        </Toast>;
    }

    /**
     * The player participates in the round and has already voted
     * @private
     */
    private hasToWaitForPlayers(): boolean {
        const currentRound = getCurrentRound(this.props.game);
        return currentRound.guessInputs.some(guess => guess.playerId === this.props.player.id && guess.guess > 0);
    }

    private resetRound = (disconnectedPlayer: IPlayer) => {
        this.setState({
            state: 0,
            disconnectedPlayer: disconnectedPlayer
        });
    }

    private handleYesNo = (selection: string) => {
        this.setState({
            state: 1,
            input: {
                selectedCard: selection,
                guess: 0,
            }
        });
    };

    private handleGuessInput = (input: number): void => {
        const {socket, game} = this.props;
        this.setState({
            input: {
                selectedCard: this.state.input.selectedCard,
                guess: input,
            }
        });
        socket.emit(SetInputEvent.id, new SetInputEvent(game.id, this.state.input.selectedCard, input));
    };

    private getQuestion(): JSX.Element {
        const currentRound = getCurrentRound(this.props.game);
        return <InfoCard titleText={'Question'}
                         additionalClassNames={styles.PPrimary}>
            {currentRound.question}
        </InfoCard>;
    };

    private backToCards = () => {
        this.setState({
            state: 0
        })
    }

    private getState(): JSX.Element {
        if (this.hasToWaitForPlayers()) {
            return <WaitForPlayers game={this.props.game} player={this.props.player} socket={this.props.socket}/>;
        }

        switch (this.state.state) {
            case 0:
                return <Cards handleYesNo={this.handleYesNo}/>;
            case 1:
                return <GuessInput handleGuessInput={this.handleGuessInput}
                                   round={getCurrentRound(this.props.game)}
                                   backToCards={this.backToCards}/>;
            default:
                return <div>Loading...</div>
        }
    };
}
