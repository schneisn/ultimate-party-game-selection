import {GameEvent} from '../../../../common/event/GameEvent';

export class SetInputEvent extends GameEvent {
    static readonly id = 'SetInput';
    selectedCard: string;
    guess: number;

    constructor(gameId: string, selectedCard: string, guess: number) {
        super(gameId);
        this.selectedCard = selectedCard;
        this.guess = guess;
    }
}
