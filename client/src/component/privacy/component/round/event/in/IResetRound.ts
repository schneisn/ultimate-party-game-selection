import {IPlayer} from '../../../../model/IPlayer';

export interface IResetRound {
    player: IPlayer;
}
