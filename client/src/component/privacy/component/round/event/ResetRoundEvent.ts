import {AbstractEvent} from '../../../../common/event/AbstractEvent';

export class ResetRoundEvent extends AbstractEvent {
    static readonly id = 'ResetRound';
}
