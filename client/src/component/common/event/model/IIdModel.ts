export class IIdModel {
    id: string;

    constructor(id: string) {
        this.id = id;
    }
}
