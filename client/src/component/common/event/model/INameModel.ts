export class INameModel {
    name: string;

    constructor(name: string) {
        this.name = name;
    }
}
