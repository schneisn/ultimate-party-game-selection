import {IIdModel} from './IIdModel';

export class IIdNameModel extends IIdModel {
    name: string;

    constructor(id: string, name: string) {
        super(id);
        this.name = name;
    }
}
