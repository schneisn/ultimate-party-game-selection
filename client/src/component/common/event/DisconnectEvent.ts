import {AbstractEvent} from './AbstractEvent';

/**
 * Another player lost connection to the game
 */
export class DisconnectEvent extends AbstractEvent {
    static readonly id = 'Disconnect';
}
