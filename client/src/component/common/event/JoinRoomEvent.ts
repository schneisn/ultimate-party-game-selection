import {AbstractEvent} from './AbstractEvent';

export class JoinRoomEvent extends AbstractEvent {
    static readonly id = 'JoinRoom';
    room: string;

    constructor(room: string) {
        super();
        this.room = room;
    }
}
