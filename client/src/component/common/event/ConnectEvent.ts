import {AbstractEvent} from './AbstractEvent';

export class ConnectEvent extends AbstractEvent {
    static readonly id = 'connect';
}
