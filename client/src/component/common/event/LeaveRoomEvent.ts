import {AbstractEvent} from './AbstractEvent';

export class LeaveRoomEvent extends AbstractEvent {
    static readonly id = 'LeaveRoom';
    room: string;

    constructor(room: string) {
        super();
        this.room = room;
    }
}
