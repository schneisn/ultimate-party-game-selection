import {IIdModel} from './model/IIdModel';
import {AbstractEvent} from './AbstractEvent';
import {Logger} from '../../../logger/logger';

/**
 * The game events contains the id of the game that
 * the event refers to
 */
export abstract class GameEvent extends AbstractEvent {
    /**
     * The game to which this request belongs
     */
    game: IIdModel;

    constructor(gameId: string) {
        super();
        if (!gameId) {
            Logger.error('Game id not defined');
        }
        this.game = new IIdModel(gameId);
    }
}
