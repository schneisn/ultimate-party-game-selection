import {AbstractEvent} from './AbstractEvent';

export class ReconnectEvent extends AbstractEvent {
    static readonly id = 'reconnect';
}
