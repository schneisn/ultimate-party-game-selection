import {AbstractEvent} from './AbstractEvent';

export class ExceptionEvent extends AbstractEvent {
    static readonly id = 'exception';
}
