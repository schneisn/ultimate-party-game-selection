/**
 * An event that is sent from the client to the server
 */
export abstract class AbstractEvent {
    /**
     * The unique id of the event
     */
    static readonly id: string;
}
