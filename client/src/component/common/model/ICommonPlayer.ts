/**
 * The player object with the basic attributes id and name
 */
export interface ICommonPlayer {
    id: string;
    name: string;
}
