import {ICommonPlayer} from './ICommonPlayer';

export interface IConnectionPlayer extends ICommonPlayer {
    isConnected: boolean;
}
