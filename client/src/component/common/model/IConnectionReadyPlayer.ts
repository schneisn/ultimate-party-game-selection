import {ICommonPlayer} from './ICommonPlayer';

/**
 * A player with isReady and isConnected attributes
 */
export interface IConnectionReadyPlayer extends ICommonPlayer {
    /**
     * Whether the player is ready to start/continue the game.
     * Default is false.
     */
    isReady: boolean;
    /**
     * Whether the player is connected to the server.
     * Default is false.
     */
    isConnected: boolean;
    /**
     * Whether the player is game master (has more permissions than other players).
     * Default is false.
     */
    isGameMaster?: boolean;
}
