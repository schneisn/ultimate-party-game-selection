import {ICommonPlayer} from './ICommonPlayer';

export interface ICommonGame<P extends ICommonPlayer = ICommonPlayer> {
    id: string;
    players: P[];
}
