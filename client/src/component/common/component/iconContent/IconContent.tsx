import {IconDefinition} from '@fortawesome/fontawesome-svg-core';
import classNames from 'classnames';
import React from 'react';
import {Icon} from '../icon/Icon';
import styles from './IconContent.module.scss';

export interface IIconDefinition {
    icon?: IconDefinition;
    additionalIconClassnames?: string;
    /**
     * Text shown on hover
     */
    iconTitle?: string;
    onIconClick?: () => void;
}

interface IProps {
    additionalClassNames?: string;
    additionalContentClassnames?: string;
    iconLeft?: IIconDefinition;
    iconRight?: IIconDefinition;
}

/**
 * A div used for layout. Icons are on left and right, content is in the middle.
 */
export class IconContent extends React.Component<IProps> {

    private static getIcon(iconDef: IIconDefinition): JSX.Element {
        if (!iconDef?.icon) {
            return <div className={classNames(styles.IconContainer)}/>;
        }
        const {additionalIconClassnames, icon, onIconClick, iconTitle} = iconDef;
        return <div className={classNames(styles.IconContainer)}>
            <Icon additionalClasses={classNames(styles.Icon,
                additionalIconClassnames)}
                  icon={icon}
                  onClick={onIconClick}
                  title={iconTitle}/>
        </div>;
    }

    render() {
        const {additionalClassNames, additionalContentClassnames, children, iconLeft, iconRight} = this.props;
        return (
            <div className={classNames(styles.IconAndContent, additionalClassNames)}>
                {IconContent.getIcon(iconLeft)}
                <div className={classNames(styles.Content, additionalContentClassnames)}>
                    {children}
                </div>
                {IconContent.getIcon(iconRight)}
            </div>
        );
    }
}
