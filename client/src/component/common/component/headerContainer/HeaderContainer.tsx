import classNames from 'classnames';
import React from 'react';
import {Card} from 'reactstrap';
import styles from './HeaderContainer.module.scss';

interface IProps {
    additionalClassNames?: string;
    header?: string | JSX.Element;
}

/**
 * A container for items with a header text
 */
export class HeaderContainer extends React.PureComponent<IProps> {
    render(): JSX.Element {
        const {additionalClassNames, children, header} = this.props;
        return <Card className={classNames(styles.HeaderContainer, additionalClassNames)}>
            {header &&
            <b>
                {header}
            </b>}
            {children &&
            <div className={styles.Children}>
                {children}
            </div>}
        </Card>;
    }
}
