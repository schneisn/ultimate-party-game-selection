import classNames from 'classnames';
import React from 'react';
import {Card, CardBody, CardTitle} from 'reactstrap';
import styles from './InfoCard.module.scss';

interface IProps {
    additionalClassNames?: string;
    titleText?: string | JSX.Element;
}

/**
 * A card to display information, which cannot be clicked.
 *
 * It has a card title and a card body
 */
export class InfoCard extends React.Component<IProps> {

    render() {
        return <>
            <Card className={classNames(styles.InfoCard, this.props.additionalClassNames)}>
                {this.props.titleText &&
                <CardTitle className={styles.InfoCardTitle}>
                    {this.props.titleText}
                </CardTitle>}
                <CardBody>
                    {this.props.children}
                </CardBody>
            </Card>
        </>;
    }
}
