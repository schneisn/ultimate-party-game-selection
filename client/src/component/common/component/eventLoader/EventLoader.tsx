import React from 'react';
import {Loader} from '../loader/Loader';
import styles from './EventLoader.module.scss';

interface IProps {
    isLoading: boolean;
}

/**
 * A wrapper for a component that shows a loader while the client waits for an
 * answer from the server for the given event.
 */
export class EventLoader extends React.Component<IProps> {
    render() {
        if (this.props.isLoading) {
            return <Loader additionalSpinnerClasses={styles.EventLoader}/>;
        }

        return this.props.children;
    }
}
