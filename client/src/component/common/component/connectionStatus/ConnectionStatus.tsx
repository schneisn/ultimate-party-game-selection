import React from 'react';
import {Socket} from '../../../../server/socket';
import {ConnectEvent} from '../../event/ConnectEvent';
import {ReconnectEvent} from '../../event/ReconnectEvent';
import {Expire} from '../expire/Expire';
import {SmallCard} from '../smallCard/SmallCard';
import {Toast} from '../toast/Toast';
import {ClientDisconnectEvent} from '../baseGame/event/ClientDisconnectEvent';
import styles from './ConnectionStatus.module.scss';
import {ConnectErrorEvent} from './event/ConnectErrorEvent';
import {ConnectTimeoutEvent} from './event/ConnectTimeoutEvent';
import {ReconnectAttemptEvent} from './event/ReconnectAttemptEvent';

const STATUS_TIMEOUT = 2000;

interface IProps {
    socket: Socket;
}

interface IState {
    additionalClasses: string;
    timer: NodeJS.Timer;
    isVisible: boolean;
    previousEvent: string;
    statusText: string;
}

/**
 * Shows a notification when your connection to the server changes
 */
export class ConnectionStatus extends React.Component<IProps, IState> {

    constructor(props: Readonly<IProps>) {
        super(props);
        this.state = {
            additionalClasses: null,
            timer: null,
            isVisible: false,
            previousEvent: null,
            statusText: null
        }
    }

    componentDidMount() {
        this.registerEvent(ConnectEvent.id, 'Connected to server', styles.Connect);
        this.registerEvent(ClientDisconnectEvent.id, 'Connection to the server lost', styles.Error, 15000);
        this.registerEvent(ConnectTimeoutEvent.id, 'Connection timeout', styles.Error);
        this.registerEvent(ConnectErrorEvent.id, 'Connection error', styles.Error);
        this.registerEvent(ReconnectAttemptEvent.id, 'Attempting reconnect to server', styles.Reconnect);
        this.registerEvent(ReconnectEvent.id, 'Successfully reconnected to server', styles.Connect);
    }

    componentWillUnmount() {
        this.props.socket.removeEventListener(ConnectEvent.id);
        this.props.socket.removeEventListener(ClientDisconnectEvent.id);
        this.props.socket.removeEventListener(ConnectTimeoutEvent.id);
        this.props.socket.removeEventListener(ConnectErrorEvent.id);
        this.props.socket.removeEventListener(ReconnectAttemptEvent.id);
        this.props.socket.removeEventListener(ReconnectEvent.id);
        clearTimeout(this.state.timer);
    }

    render() {
        if (!this.state.isVisible) {
            return null;
        }
        return (
            <Expire timeout={STATUS_TIMEOUT}>
                <Toast>
                    <SmallCard additionalClassNames={this.state.additionalClasses}>
                        {this.state.statusText}
                    </SmallCard>
                </Toast>
            </Expire>
        )
    }

    private registerEvent(event: string, text: string, additionalClasses: string, timeOut: number = STATUS_TIMEOUT): void {
        this.props.socket.on(event, () => {
            // Do not show connect status when loading the page
            if (event === ConnectEvent.id && !this.state.previousEvent) {
                return;
            }

            clearTimeout(this.state.timer);
            const timer = setTimeout(() => {
                this.setState({
                    additionalClasses: null,
                    timer: null,
                    isVisible: false,
                    statusText: null
                });
            }, timeOut);
            this.setState({
                additionalClasses: additionalClasses,
                timer: timer,
                isVisible: true,
                previousEvent: event,
                statusText: text
            });
        });
    }
}
