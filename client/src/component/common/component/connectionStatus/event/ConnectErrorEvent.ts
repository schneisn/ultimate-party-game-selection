import {AbstractEvent} from '../../../event/AbstractEvent';

export class ConnectErrorEvent extends AbstractEvent {
    static readonly id = 'connect_error';
}
