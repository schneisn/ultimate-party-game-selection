import {AbstractEvent} from '../../../event/AbstractEvent';

export class ReconnectAttemptEvent extends AbstractEvent {
    static readonly id = 'reconnect_attempt';
}
