import {AbstractEvent} from '../../../event/AbstractEvent';

export class ConnectTimeoutEvent extends AbstractEvent {
    static readonly id = 'connect_timeout';
}
