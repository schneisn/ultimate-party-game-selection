import classNames from 'classnames';
import React from 'react';
import Spinner from 'reactstrap/lib/Spinner';
import styles from './Loader.module.scss';

interface IProps {
    additionalWrapperClasses?: string;
    additionalSpinnerClasses?: string;
}

export class Loader extends React.PureComponent<IProps> {
    render() {
        return (
            <div className={classNames(styles.SpinnerWrapper, this.props.additionalWrapperClasses)}>
                <Spinner animation="border" role="status" className={classNames(styles.Spinner, this.props.additionalSpinnerClasses)}>
                    Loading...
                </Spinner>
            </div>
        );
    }
}
