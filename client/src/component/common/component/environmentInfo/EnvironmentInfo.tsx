import React from 'react';
import {Toast, ToastBody, ToastHeader} from 'reactstrap';
import {LocalStorage, LocalStorageItem} from '../../../../store/localStorage';
import styles from './EnvironmentInfo.module.scss';

interface IState {
    isOpen: boolean;
}

/**
 * Information about the environment the app runs in. Can be configured using the .env files
 */
export class EnvironmentInfo extends React.Component<{}, IState> {

    constructor(props: {}) {
        super(props);
        this.state = {
            isOpen: LocalStorage.getJSONItem<boolean>(LocalStorageItem.READ_ENV_INFO) !== true
        }
    }

    render(): JSX.Element {
        const {isOpen} = this.state;
        const readInfo = LocalStorage.getJSONItem<boolean>(LocalStorageItem.READ_ENV_INFO) === true;
        if (!import.meta.env.VITE_REACT_APP_ENV_INFO || readInfo) {
            return null;
        }
        return <Toast isOpen={isOpen}
                      className={styles.SuggestionToast}>
            <ToastHeader toggle={this.toggleIsOpen}>
                Important information
            </ToastHeader>
            <ToastBody>
                <span dangerouslySetInnerHTML={{__html: import.meta.env.VITE_REACT_APP_ENV_INFO}}/>
            </ToastBody>
        </Toast>
    }

    private toggleIsOpen = () => {
        LocalStorage.setJSONItem(LocalStorageItem.READ_ENV_INFO, true);
        this.setState({isOpen: !this.state.isOpen});
    }
}
