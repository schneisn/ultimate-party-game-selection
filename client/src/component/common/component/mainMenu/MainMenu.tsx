import React from 'react';
import {NavLink} from 'reactstrap';
import {Urls} from '../../../../server/serverUrls';
import {InfoCard} from '../infoCard/InfoCard';
import {WideButton} from '../wideButton/WideButton';
import {Icon, IconType} from '../icon/Icon';
import Navbar from '../navbar/Navbar';
import styles from './MainMenu.module.scss';

interface IExternalGame {
    name: string;
    href: string;
}

interface IProps {
    onClick?: (link: string) => void;
}

export class MainMenu extends React.Component<IProps> {

    private externalGames: IExternalGame[] = [
        {
            name: 'Skribbl.io',
            href: 'https://skribbl.io/'
        },
        {
            name: 'Codenames',
            href: 'https://www.horsepaste.com/'
        },
        {
            name: 'Gartic Phone',
            href: 'https://garticphone.com/'
        },
        {
            name: 'Curve Fever Pro',
            href: 'https://curvefever.pro/'
        },
        {
            name: 'Taboo',
            href: 'https://playtaboo.com/playpage'
        },
        {
            name: 'UNO',
            href: 'https://pizz.uno/'
        },
        {
            name: 'More io games',
            href: 'https://iogames.space/'
        },
    ]

    render(): JSX.Element {
        return <>
            <Navbar showBackIcon={false}/>
            <main id={'main'} role={'main'}>
                <div className={styles.MainMenu}>
                    <InfoCard titleText={'Party Game Selection'} additionalClassNames={styles.MainMenuInfoCard}>
                        Which game do you want to play?
                    </InfoCard>
                    <WideButton additionalClassNames={styles.GameOne}
                                onClick={() => this.props.onClick(Urls.CardsAgainst)}>
                        Cards against Everything
                    </WideButton>
                    <WideButton additionalClassNames={styles.GameTwo}
                                onClick={() => this.props.onClick(Urls.Categories)}>
                        Categories
                    </WideButton>
                    <WideButton additionalClassNames={styles.GameThree}
                                onClick={() => this.props.onClick(Urls.Privacy)}>
                        Privacy
                    </WideButton>
                    <WideButton additionalClassNames={styles.GameFour}
                                onClick={() => this.props.onClick(Urls.Spyfall)}>
                        Spyfall
                    </WideButton>
                    <WideButton additionalClassNames={styles.GameFive}
                                onClick={() => this.props.onClick(Urls.Werewolf)}>
                        Werewolf
                    </WideButton>
                    <WideButton additionalClassNames={styles.GameSix}
                                onClick={() => this.props.onClick(Urls.WhoAmI)}>
                        Who am I
                    </WideButton>
                    <div className={styles.ExternalGamesHeader}>
                        More Games
                    </div>
                    {this.externalGames.map(game => <NavLink key={game.name}
                                                             href={game.href}
                                                             target={'_blank'}
                                                             rel={'noopener noreferrer'}
                                                             role={'button'}
                                                             title={`Open ${game.name}`}
                                                             className={styles.ExternalGame}>
                        {game.name}
                        <Icon icon={IconType.externalLink} additionalClasses={styles.ExternalLinkIcon}/>
                    </NavLink>)}
                </div>
            </main>
        </>
    }
}

