import classNames from 'classnames';
import React from 'react';
import {Card as ReactCard} from 'reactstrap';
import styles from './Card.module.scss';

interface IProps {
    additionalClassNames?: string;
    disabled?: boolean;
    hidden?: boolean;
    onClick?: () => void;
}

/**
 * Wraps the react strap card
 */
export class Card extends React.Component<IProps> {
    render() {
        const {additionalClassNames, onClick, children, hidden, disabled} = this.props;
        return (
            <ReactCard className={classNames(styles.Card, additionalClassNames)}
                       onClick={onClick}
                       aria-disabled={disabled}
                       hidden={hidden}>
                {children}
            </ReactCard>
        );
    }
}
