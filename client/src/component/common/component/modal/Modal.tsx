import classNames from 'classnames';
import React from 'react';
import {Modal as ReactModal, ModalBody, ModalFooter, ModalHeader} from 'reactstrap';
import {WideButton} from '../wideButton/WideButton';
import styles from './Modal.module.scss';

export interface IModalStyles {
    headerClasses?: string;
    modalBodyClasses?: string;
    buttonClasses?: string;
    modalClasses?: string;
}

interface IProps {
    isOpen?: boolean;
    modalTitle?: string;
    onConfirm?: () => void;
    onToggle?: (event?: React.MouseEvent<unknown>) => void;
    /**
     * Whether the footer is shown.
     * Default is true
     */
    showFooter?: boolean;
    styles?: IModalStyles;
}

/**
 * A modal.
 *
 * The children are the modal body
 */
export class Modal extends React.Component<IProps> {

    render() {
        return (
            <ReactModal className={classNames(styles.Modal, this.props.styles?.modalClasses)}
                        contentClassName={styles.ModalContent}
                        isOpen={this.props.isOpen}
                        toggle={this.onToggle}
                        centered={true}>
                <ModalHeader className={classNames(styles.Header, this.props.styles?.headerClasses)}
                             toggle={this.onToggle}>
                    {this.props.modalTitle}
                </ModalHeader>
                <ModalBody className={this.props.styles?.modalBodyClasses}>
                    {this.props.children}
                </ModalBody>
                {this.props.showFooter !== false && <ModalFooter className={classNames(styles.Footer, this.props.styles?.modalBodyClasses)}>
                  <WideButton additionalClassNames={this.props.styles?.buttonClasses}
                              onClick={this.onConfirm}>
                    Confirm
                  </WideButton>
                  <WideButton additionalClassNames={this.props.styles?.buttonClasses}
                              onClick={this.onToggle}>
                    Cancel
                  </WideButton>
                </ModalFooter>}
            </ReactModal>
        );
    }

    private onConfirm = (event: React.MouseEvent<unknown, MouseEvent>): void => {
        this.onToggle(event);
        this.props.onConfirm && this.props.onConfirm();
    }

    private onToggle = (event: React.MouseEvent<unknown, MouseEvent>): void => {
        this.props.onToggle && this.props.onToggle(event);
    }
}
