import {IModalStyles} from '../Modal';

export function generateModalStyles(headerClasses: string, modalBodyClasses: string, buttonClasses: string, modalClasses?: string): IModalStyles {
    return {
        headerClasses: headerClasses,
        modalBodyClasses: modalBodyClasses,
        buttonClasses: buttonClasses,
        modalClasses: modalClasses,
    }
}
