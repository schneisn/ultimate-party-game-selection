import React from 'react';
import {Collapse as ReactCollapse} from 'reactstrap';
import {WideButton} from '../wideButton/WideButton';

interface IProps {
    buttonClasses?: string;
    /**
     * The text of the button when the collapse is open
     */
    openButtonText?: string | JSX.Element;
    /**
     * The text of the button when the collapse is hidden
     */
    hiddenButtonText?: string | JSX.Element;
    onButtonClick?: () => void;
    /**
     * Whether the button is disabled.
     * Default is false.
     */
    buttonIsDisabled?: boolean;
}

interface IState {
    isHidden: boolean;
}

export class Collapse extends React.Component<IProps, IState> {

    constructor(props: IProps) {
        super(props);
        this.state = {
            isHidden: true
        }
    }

    render() {
        return (
            <>
                <WideButton onClick={this.onButtonClick}
                            additionalClassNames={this.props.buttonClasses}
                            disabled={this.props.buttonIsDisabled}>
                    {this.getButtonText()}
                </WideButton>
                <ReactCollapse isOpen={!this.state.isHidden}>
                    {this.props.children}
                </ReactCollapse>
            </>
        )
    }

    private onButtonClick = (): void => {
        this.setState({isHidden: !this.state.isHidden});
        this.props.onButtonClick && this.props.onButtonClick();
    }

    private getButtonText(): string | JSX.Element {
        if (this.state.isHidden) {
            return this.props.hiddenButtonText ? this.props.hiddenButtonText : 'Show';
        }
        return this.props.openButtonText ? this.props.openButtonText : 'Hide';
    }
}
