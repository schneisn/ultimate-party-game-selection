import React from 'react';
import {HeaderContainer} from '../headerContainer/HeaderContainer';

interface IProps {
    additionalClassNames?: string;
}

/**
 * Common component for settings
 */
export class CommonSettings extends React.Component<IProps> {
    render() {
        return (
            <HeaderContainer header={'Settings'}
                             additionalClassNames={this.props.additionalClassNames}>
                {this.props.children}
            </HeaderContainer>
        );
    }
}
