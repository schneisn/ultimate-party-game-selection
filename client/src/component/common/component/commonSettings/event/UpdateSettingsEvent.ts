import {GameEvent} from '../../../event/GameEvent';

export class UpdateSettingsEvent<S> extends GameEvent {
    static readonly id = 'UpdateSettings';
    settings: S;

    constructor(gameId: string, settings: S) {
        super(gameId);
        this.settings = settings;
    }
}
