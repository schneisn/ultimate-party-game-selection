import classNames from 'classnames';
import React from 'react';
import styles from './Toast.module.scss';

export enum Position {
    Top,
    Bottom
}

interface IProps {
    /**
     * The position of the toast, default is top.
     */
    position?: Position;
}

/**
 * The children are positioned at the defined position in the screen
 */
export class Toast extends React.PureComponent<IProps> {
    render() {
        return <div className={classNames(
            styles.Toast,
            this.props.position === Position.Bottom ? styles.Bottom : styles.Top
        )}>
            {this.props.children}
        </div>
    };
}
