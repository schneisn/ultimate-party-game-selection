import React from 'react';
import styles from './GameDescription.module.scss';
import {IPopoverStyles, Popover} from '../popover/Popover';
import {IconType} from '../icon/Icon';

interface IProps {
    gameDescription?: string;
    gameDescriptionStyles?: IPopoverStyles;
}

/**
 * A popover that show the game description
 */
export class GameDescription extends React.Component<IProps> {

    render() {
        const {gameDescriptionStyles, gameDescription} = this.props;
        return (
            <div className={styles.GameDescriptionIcon}>
                <Popover id={'gameDescription'}
                         icon={IconType.infoCircle}
                         iconTitle={'Game description'}
                         customStyles={gameDescriptionStyles}>
                    {gameDescription}
                </Popover>
            </div>
        );
    }
}
