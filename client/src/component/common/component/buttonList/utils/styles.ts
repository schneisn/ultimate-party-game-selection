import {IButtonListStyles} from '../ButtonList';

export function generateButtonListStyles(containerClasses?: string, itemClasses?: string): IButtonListStyles {
    return {
        containerClasses,
        itemClasses
    }
}
