import React from 'react';
import {WideButton} from '../wideButton/WideButton';
import {HeaderContainer} from '../headerContainer/HeaderContainer';
import styles from './ButtonList.module.scss';
import classNames from 'classnames';

export interface IButtonListStyles {
    containerClasses?: string;
    itemClasses?: string;
}

/**
 * The items that are displayed in the list
 */
export interface IButtonListItem {
    content: JSX.Element | string;
    onClick?: () => void;
}

interface IProps {
    listHeader?: string;
    items: IButtonListItem[];
    styles?: IButtonListStyles;
    /**
     * The content to display when there are not items.
     * Default is 'no data'
     */
    emptyItemsContent?: JSX.Element | string;
}

/**
 * A list with buttons
 */
export class ButtonList extends React.Component<IProps> {

    render() {
        return (
            <HeaderContainer additionalClassNames={this.props.styles?.containerClasses}
                             header={this.props.listHeader}>
                {this.getItemCards()}
            </HeaderContainer>
        );
    }

    private getItemCards(): JSX.Element[] {
        if (!this.props.items || this.props.items.length < 1) {
            return [<div key={'no-data'} className={styles.NoData}>{this.props.emptyItemsContent || 'No Data'}</div>];
        }

        return this.props.items.map((item: IButtonListItem, index: number) => {
            return <WideButton key={index}
                               additionalClassNames={classNames(styles.ButtonItem, this.props.styles?.itemClasses)}
                               onClick={() => this.onItemClick(index)}>
                {item.content}
            </WideButton>
        });
    }

    private onItemClick = (index: number): void => {
        this.props.items[index].onClick && this.props.items[index].onClick();
    }
}
