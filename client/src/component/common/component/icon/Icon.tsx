import {IconProp} from '@fortawesome/fontawesome-svg-core';
import {
    faArrowLeft,
    faAward,
    faCheck,
    faCheckDouble,
    faCopy,
    faCross,
    faCrosshairs,
    faEraser,
    faExclamation,
    faExclamationTriangle,
    faExternalLinkAlt,
    faEye,
    faEyeSlash,
    faFilter,
    faHeart,
    faInfoCircle,
    faLanguage,
    faMinusSquare,
    faRecycle,
    faSkullCrossbones,
    faSquare,
    faTheaterMasks,
    faTimes,
    faTrash,
    faUserCheck,
    faUserEdit,
    faUserTag,
    faUserTie
} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import classNames from 'classnames';
import React from 'react';
import styles from './Icon.module.scss';
import {Button} from '../button/Button';

/**
 * The types of icons used
 */
export class IconType {
    static arrowLeft = faArrowLeft;
    static award = faAward;
    static check = faCheck;
    static checkDouble = faCheckDouble;
    static copy = faCopy;
    static cross = faCross;
    static crosshairs = faCrosshairs;
    static eraser = faEraser;
    static exclamation = faExclamation;
    static exclamationTriangle = faExclamationTriangle;
    static externalLink = faExternalLinkAlt;
    static eye = faEye;
    static eyeSlash = faEyeSlash;
    static filter = faFilter;
    static heart = faHeart;
    static infoCircle = faInfoCircle;
    static language = faLanguage;
    static minusSquare = faMinusSquare;
    static recycle = faRecycle;
    static skullCrossbones = faSkullCrossbones;
    static square = faSquare;
    static theaterMask = faTheaterMasks;
    static times = faTimes;
    static trash = faTrash;
    static userCheck = faUserCheck;
    static userEdit = faUserEdit;
    static userTag = faUserTag;
    static userTie = faUserTie;
}

interface IProps {
    additionalClasses?: string;
    icon: IconProp;
    id?: string;
    /**
     * Whether the icon is disabled.
     * Default is false.
     */
    disabled?: boolean;
    /**
     * An on click event.
     * If this event is defined, the icon has the role button
     * @param event
     */
    onClick?: (event: React.MouseEvent) => void;
    /**
     * Text shown on hover
     */
    title?: string;
}

/**
 * Displays a fontawesome icon
 */
export class Icon extends React.PureComponent<IProps> {
    render() {
        const {onClick, disabled} = this.props;
        if (onClick) {
            // TODO The button around the icon does not trigger the onClick event
            // and the onclick cannot be activated using keyboard when it is used by a popover
            return <Button additionalClassNames={
                classNames(styles.IconButton,
                    this.props.additionalClasses)}
                           onClick={this.props.onClick}
                           disabled={disabled}>
                <FontAwesomeIcon id={this.props.id}
                                 icon={this.props.icon}
                                 title={this.props.title}/>
            </Button>
        }

        return <span className={styles.Icon}>
            <FontAwesomeIcon id={this.props.id}
                             className={this.props.additionalClasses}
                             icon={this.props.icon}
                             title={this.props.title}/>
        </span>;
    }
}
