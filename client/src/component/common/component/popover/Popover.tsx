import {IconProp} from '@fortawesome/fontawesome-svg-core';
import classNames from 'classnames';
import * as Popper from 'popper.js';
import React from 'react';
import {Popover as ReactPopover, PopoverBody, PopoverHeader} from 'reactstrap';
import {StringUtils} from '../../utils/string';
import {Icon} from '../icon/Icon';
import styles from './Popover.module.scss';

export interface IPopoverStyles {
    iconClasses?: string;
    popoverClasses?: string;
}

interface IProps {
    /**
     * Required id to identify the element where
     * the popover is attached to
     */
    id: string;
    /**
     * A custom element can be used instead of an icon
     */
    customElement?: JSX.Element
    /**
     * The type of icon that is used for the icon.
     * Instead of this, a custom element can be provided
     */
    icon?: IconProp;
    /**
     * Text shown on hovering the icon or the popover
     */
    iconTitle?: string;
    /**
     * Overrides whether the popover is open/closed.
     * Default is false.
     */
    isOpen?: boolean;
    /**
     * onClick event on the icon. The custom element has to
     * provide its own onClick event
     * @param event
     */
    onClick?: (event: React.MouseEvent) => void;
    /**
     * Placement of the popover.
     * Default is top
     */
    placement?: Popper.Placement;
    /**
     * Customize the appearance of the popover
     */
    customStyles?: IPopoverStyles;
    /**
     * The title of the popover
     */
    title?: string;
}

interface IState {
    isOpen: boolean;
}

export class Popover extends React.Component<IProps, IState> {

    constructor(props: Readonly<IProps>) {
        super(props);
        this.state = {
            isOpen: this.props.isOpen
        }
    }

    componentDidUpdate(prevProps: Readonly<IProps>, prevState: Readonly<IState>, snapshot?: any) {
        if (this.props === prevProps) {
            return;
        }
        this.setState({isOpen: this.props.isOpen});
    }

    componentDidMount() {
        document.addEventListener('click', this.closePopover);
    }

    componentWillUnmount() {
        document.removeEventListener('click', this.closePopover);
    }

    render() {
        const {placement, customElement, icon, title, customStyles, iconTitle, children} = this.props;
        return (
            <>
                {icon ?
                    <Icon id={this.getId()}
                          additionalClasses={classNames(styles.PopoverIcon, customStyles?.iconClasses)}
                          icon={icon}
                          title={iconTitle}
                          onClick={this.onIconClick}/> :
                    <span id={this.getId()} className={styles.PopoverIcon}
                          role={'button'}
                          title={iconTitle}>
                        {customElement}
                    </span>}
                <ReactPopover innerClassName={classNames(styles.NoSelect, customStyles?.popoverClasses)}
                              placement={placement || 'top'}
                              isOpen={this.state.isOpen}
                              target={this.getId()}
                              toggle={this.toggleIsOpen}
                              title={iconTitle}>
                    {title && <PopoverHeader>
                        {title}
                    </PopoverHeader>}
                    {children && <PopoverBody className={styles.NoSelect}>
                        {children}
                    </PopoverBody>}
                </ReactPopover>
            </>
        );
    }

    private closePopover = (): void => {
        this.setState({isOpen: false});
    }

    /**
     * Creates an id that is usable by the popover to show the popover at the
     * correct location.
     * The id must begin with a letter and cannot contain special characters
     * @private
     */
    private getId(): string {
        return StringUtils.removeBlanks(StringUtils.removeSpecialCharacters(Popover.name + this.props.id));
    }

    private onIconClick = (event: React.MouseEvent): void => {
        this.props.onClick && this.props.onClick(event);
        // event.preventDefault();
        this.toggleIsOpen();
    }

    private toggleIsOpen = (): void => {
        this.setState({isOpen: !this.state.isOpen});
    }
}
