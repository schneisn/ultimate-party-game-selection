import {IPopoverStyles} from '../Popover';

export function generatePopoverStyles(iconClasses?: string, popoverClasses?: string): IPopoverStyles {
    return {
        iconClasses,
        popoverClasses
    }
}
