import React from 'react';

interface IProps {
    symbol: string;
    label?: string;
    onClick?: (event: React.MouseEvent<HTMLSpanElement>) => void;
}

/**
 * Displays an emoji in-line
 */
export class Emoji extends React.Component<IProps> {
    render() {
        return (
            <span role={'img'}
                  aria-label={this.props.label || ''}
                  aria-hidden={!this.props.label}
                  onClick={(event: React.MouseEvent<HTMLSpanElement>) => this.onClick(event)}>
                {this.props.symbol}
            </span>
        );
    }

    private onClick = (event: React.MouseEvent<HTMLSpanElement>): void => {
        this.props.onClick && this.props.onClick(event);
    }
}
