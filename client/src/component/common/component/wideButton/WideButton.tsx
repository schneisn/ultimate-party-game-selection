import classNames from 'classnames';
import React from 'react';
import styles from './WideButton.module.scss';
import {Button} from '../button/Button';

interface IProps {
    additionalClassNames?: string;
    /**
     * Whether the button is disabled or not
     */
    disabled?: boolean;
    id?: string;
    /**
     * Whether the button is hidden.
     * Default is false.
     */
    hidden?: boolean;
    onClick?: (event: React.MouseEvent<any, MouseEvent>) => void;
    /**
     * Whether to prevent default event on click.
     * Default is true.
     */
    preventDefaultOnClick?: boolean;
    /**
     * Text shown on hover
     */
    title?: string;
}

/**
 * A button that takes up all of the available width
 */
export class WideButton extends React.Component<IProps> {

    render(): JSX.Element {
        return (
            <Button id={this.props.id}
                    additionalClassNames={classNames(styles.WideButton, this.props.additionalClassNames)}
                    disabled={this.props.disabled}
                    onClick={this.props.onClick}
                    title={this.props.title}
                    hidden={this.props.hidden}>
                {this.props.children}
            </Button>
        )
    }

}
