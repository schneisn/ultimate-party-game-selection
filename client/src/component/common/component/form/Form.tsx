import React from 'react';
import {Form as ReactForm} from 'reactstrap';

interface IProps {
    hidden?: boolean;
    onSubmit?: (event: React.FormEvent<HTMLFormElement>) => void;
    /**
     * Whether to prevent the default action of the event.
     * Default is true
     */
    preventDefault?: boolean;
}

/**
 * Form to group input.
 * Wraps the reactstrap form.
 */
export class Form extends React.Component<IProps> {

    render() {
        return (
            <ReactForm onSubmit={this.onSubmit} hidden={this.props.hidden}>
                {this.props.children}
            </ReactForm>
        );
    }

    private onSubmit = (event: React.FormEvent<HTMLFormElement>): void => {
        if (this.props.preventDefault === undefined || this.props.preventDefault) {
            event?.preventDefault();
        }
        if (this.props.onSubmit) {
            this.props.onSubmit(event);
        }
    }
}
