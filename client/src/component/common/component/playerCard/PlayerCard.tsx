import classNames from 'classnames';
import React from 'react';
import {IConnectionReadyPlayer} from '../../model/IConnectionReadyPlayer';
import {Card} from '../card/Card';
import {PlayerInfo} from '../playerInfo/PlayerInfo';
import styles from './PlayerCard.module.scss';

interface IProps {
    additionalClassNames?: string;
    player: IConnectionReadyPlayer;
    /**
     * Whether to show if the player is ready.
     * Default is false
     */
    showReadyState?: boolean;
}

/**
 * A card that shows the player name and optionally additional information
 */
export class PlayerCard extends React.Component<IProps> {

    render(): JSX.Element {
        const {additionalClassNames, player, showReadyState} = this.props;
        return <Card key={player.id}
                     additionalClassNames={classNames(styles.PlayerCard, additionalClassNames)}>
            <PlayerInfo player={player} showReadyState={showReadyState}/>
        </Card>
    }
}
