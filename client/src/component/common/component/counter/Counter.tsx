import React from 'react';

interface Countdown {
    hours: number;
    minutes: number;
    seconds: number;
}

export interface ICounterProps {
    /**
     * The timeout in seconds
     */
    timeout: number;
    /**
     * Whether the timer is running.
     * Default is true
     */
    isRunning?: boolean;
}

export interface ICounterState {
    timer: NodeJS.Timer;
    remainingSeconds: number;
}

/**
 * A counter that counts down the time each second
 */
export class Counter<ICustomCounterProps extends ICounterProps = ICounterProps, ICustomCounterState extends ICounterState = ICounterState>
    extends React.Component<ICustomCounterProps, ICustomCounterState> {

    constructor(props: Readonly<ICustomCounterProps>) {
        super(props);
        this.state = {
            ...this.state,
            timer: null,
            remainingSeconds: this.props.timeout
        }
    }

    componentDidMount() {
        this.startTimer();
    }

    componentWillUnmount() {
        clearTimeout(this.state.timer);
    }

    render() {
        const countdown = this.secondsToCountdown(this.state.remainingSeconds);
        return (
            <span>
                {countdown.minutes > 0 && countdown.minutes + 'm'} {countdown.seconds}s
            </span>
        );
    }

    protected secondsToCountdown(secs: number): Countdown {
        let hours = Math.floor(secs / (60 * 60));

        let divisorForMinutes = secs % (60 * 60);
        let minutes = Math.floor(divisorForMinutes / 60);

        let divisorForSeconds = divisorForMinutes % 60;
        let seconds = Math.ceil(divisorForSeconds);

        return {
            hours: hours,
            minutes: minutes,
            seconds: seconds
        }
    }

    private startTimer(): void {
        if (!this.state.timer && this.state.remainingSeconds > 0) {
            this.setState({
                timer: setInterval(this.countDown, 1000)
            })
        }
    }

    private countDown = (): void => {
        if (this.props.isRunning === false) {
            return;
        }
        let seconds = this.state.remainingSeconds - 1;
        this.setState({
            remainingSeconds: seconds,
        });
        if (seconds === 0) {
            clearInterval(this.state.timer);
        }
    }
}
