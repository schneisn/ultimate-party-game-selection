import React from 'react';
import {IConnectionReadyPlayer} from '../../model/IConnectionReadyPlayer';
import {Badge} from '../badge/Badge';
import {DisconnectedIcon} from '../disconnectedIcon/DisconnectedIcon';
import {Emoji} from '../emoji/Emoji.';
import {Popover} from '../popover/Popover';
import styles from './PlayerInfo.module.scss';
import classNames from 'classnames';
import {ICommonPlayer} from '../../model/ICommonPlayer';

interface IProps {
    additionalClassnames?: string;
    additionalNameClassnames?: string;
    player: ICommonPlayer & Partial<IConnectionReadyPlayer>;
    /**
     * Whether to show if the player is not connected.
     * Default is true.
     */
    showConnectionState?: boolean;
    /**
     * Whether to show if the player is ready.
     * Default is false.
     */
    showReadyState?: boolean;
}

/**
 * A card that shows the player name and optionally additional information
 */
export class PlayerInfo extends React.Component<IProps> {

    render(): JSX.Element {
        const {additionalClassnames, additionalNameClassnames, player, showConnectionState, showReadyState} = this.props;
        return <div className={classNames(styles.PlayerInfo, additionalClassnames)}>
            <div className={classNames(styles.NameConnectionStatus, additionalNameClassnames)}>
                <div className={styles.EllipseText}>
                    {player.isGameMaster && <Popover id={'gameMaster'}
                                                     customElement={<Emoji symbol={'👑 '} onClick={(event => event.stopPropagation())}/>}
                                                     title={'Gamemaster'}
                                                     iconTitle={'Gamemaster'}/>}
                    {player.name}
                </div>
                {showConnectionState !== false && !player.isConnected && <DisconnectedIcon/>}
            </div>
            {showReadyState &&
            <div className={styles.ReadyState}>
                {player.isReady ?
                    <Badge additionalClassNames={styles.Ready}>Ready</Badge> :
                    <Badge additionalClassNames={styles.NotReady}>Not Ready</Badge>}
            </div>}
        </div>
    }
}
