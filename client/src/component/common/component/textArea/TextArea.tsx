import classNames from 'classnames';
import React from 'react';
import {Input as ReactInput, Label} from 'reactstrap';
import {IdUtils} from '../../utils/id';
import styles from './TextArea.module.scss';

interface IProps {
    /**
     * Additional classes for styling
     */
    additionalClasses?: string;
    /**
     * Whether to automatically focus on render.
     * Default is false
     */
    autoFocus?: boolean;
    /**
     * Whether the input is disabled.
     * Default is false
     */
    disabled?: boolean;
    /**
     * Whether the input is hidden.
     * Default is false
     */
    hidden?: boolean;
    /**
     * The label for the text area
     */
    label?: string;
    /**
     * Placeholder to display in the input when there
     * is no value
     */
    placeholder?: string;
    /**
     * Max length of input. Default is 50
     */
    maxLength?: number;
    /**
     * Min length of input. Default is 1
     */
    minLength?: number;
    /**
     * On input change event
     * @param event
     */
    onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
    onSubmit?: (event: React.FormEvent<HTMLInputElement>) => void;
    inputRef?: React.Ref<HTMLInputElement>;
    /**
     * Whether the input is required.
     * Default is false
     */
    required?: boolean;
    /**
     * The editable value of the input
     */
    value?: string;
}

/**
 * Wraps the reactstrap input with type text
 */
export class TextArea extends React.Component<IProps> {
    render() {
        const id = IdUtils.createUuid();
        return (
            <>
                <Label for={id} hidden={!this.props.label}>
                    {this.props.label || 'Input'}
                </Label>
                <ReactInput id={id}
                            type={'text'}
                            autoFocus={this.props.autoFocus}
                            className={classNames(styles.Input, this.props.additionalClasses)}
                            hidden={this.props.hidden}
                            disabled={this.props.disabled}
                            required={this.props.required}
                            placeholder={this.props.placeholder}
                            maxLength={this.props.maxLength ? this.props.maxLength : 50}
                            minLength={this.props.minLength ? this.props.minLength : 1}
                            onChange={this.props.onChange}
                            value={this.props.value}
                            onSubmit={this.props.onSubmit}
                            innerRef={this.props.inputRef}
                />
            </>
        );
    }
}
