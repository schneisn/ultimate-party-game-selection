import classNames from 'classnames';
import React from 'react';
import {FormGroup, Label} from 'reactstrap';
import {Checkbox} from '../checkbox/Checkbox';
import styles from './CheckboxWithLabel.module.scss';

interface IProps {
    additionalClassNames?: string;
    /**
     * Whether the card is checked. The styling of a checked
     * card can be customized with checkedClassNames
     */
    checked?: boolean;
    /**
     * The style to apply when the card is checked
     */
    checkedClassNames?: string;
    /**
     * Whether the checkbox is disabled.
     * Default is false
     */
    disabled?: boolean;
    /**
     * onClick event for the checkbox card
     * @param event
     */
    onClick?: (event: React.ChangeEvent<HTMLInputElement>) => void;
    /**
     * Whether to show an icon to indicate whether the card is selected or not.
     * Default is true
     */
    showIcon?: boolean;
}

/**
 * A checkbox with label that can be selected
 */
export class CheckboxWithLabel extends React.Component<IProps> {
    render() {
        const {showIcon, disabled, onClick, children, checkedClassNames, checked, additionalClassNames} = this.props;
        return <FormGroup check
                          disabled={disabled}
                          className={
                              classNames(styles.CheckboxCardFormGroup,
                                  additionalClassNames,
                                  checked && checkedClassNames)}>
            <Label check
                   disabled={disabled}
                // If no icon is shown, the div has to be focusable nonetheless. This is done with tabindex=0.
                   tabIndex={showIcon === false ? 0 : undefined}
                   role={showIcon === false ? 'button' : undefined}
                   className={classNames(styles.Label, disabled && styles.Disabled)}>
                <div className={classNames(showIcon !== false && styles.CheckboxContainer)}>
                    <Checkbox additionalClassNames={styles.Checkbox}
                              checked={checked}
                              hidden={showIcon === false}
                              disabled={disabled}
                              onClick={onClick}/>
                </div>
                <div className={styles.LabelContent}>
                    {children}
                </div>
            </Label>
        </FormGroup>;
    }
}
