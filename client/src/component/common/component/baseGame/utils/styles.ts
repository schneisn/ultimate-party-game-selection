import {IModalStyles} from '../../modal/Modal';
import {IPopoverStyles} from '../../popover/Popover';
import {IBaseGameStyles} from '../BaseGame';
import {IConnectToastStyles} from '../component/connectToast/ConnectToast';

export function generateBaseGameStyles(modalStyles?: IModalStyles,
                                       connectToastStyles?: IConnectToastStyles,
                                       gameClasses?: string,
                                       bodyClasses?: string,
                                       gameDescriptionStyles?: IPopoverStyles): IBaseGameStyles {
    return {
        bodyClasses,
        modalStyles,
        connectToastStyles,
        gameClasses,
        gameDescriptionStyles
    }
}
