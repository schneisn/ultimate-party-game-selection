import {GameEvent} from '../../../event/GameEvent';

export class GetGameEvent extends GameEvent {
    static readonly id = 'GetGame';
}
