import {AbstractEvent} from '../../../event/AbstractEvent';

/**
 * The client lost connection to the server
 */
export class ClientDisconnectEvent extends AbstractEvent {
    static readonly id = 'disconnect';
}
