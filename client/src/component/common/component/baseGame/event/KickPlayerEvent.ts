import {GameEvent} from '../../../event/GameEvent';

export class KickPlayerEvent extends GameEvent {
    static readonly id = 'KickPlayer';
    playerId: string;

    constructor(gameId: string, playerId: string) {
        super(gameId);
        this.playerId = playerId;
    }
}
