export interface IException {
    status: string;
    message: string;
}
