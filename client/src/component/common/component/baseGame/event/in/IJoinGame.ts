export interface IJoinGame<Game, Player> {
    game: Game;
    player: Player;
}
