import {GameEvent} from '../../../event/GameEvent';
import {LocalStorage, LocalStorageItem} from '../../../../../store/localStorage';
import {IIdNameModel} from '../../../event/model/IIdNameModel';

export class JoinGameEvent extends GameEvent {
    static readonly id = 'JoinGame';
    player: IIdNameModel;

    constructor(gameId: string) {
        super(gameId);
        const localStoragePlayer = LocalStorage.getJSONItem<IIdNameModel>(LocalStorageItem.PLAYER);
        this.player = {
            id: localStoragePlayer?.id,
            name: localStoragePlayer?.name
        };
    }
}
