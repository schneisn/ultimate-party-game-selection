import {AbstractEvent} from '../../../event/AbstractEvent';

export class LeaveGameEvent extends AbstractEvent {
    static readonly id = 'LeaveGame';
}
