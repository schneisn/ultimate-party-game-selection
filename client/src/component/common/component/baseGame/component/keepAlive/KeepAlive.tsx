import React from 'react';
import {Config} from '../../../../../../config/config';
import {Http} from '../../../../../../server/http';

interface IState {
    keepAliveTimer: NodeJS.Timer;
}

/**
 * Sends regular http requests to the server.
 */
export class KeepAlive extends React.Component <{}, IState> {

    constructor(props: Readonly<{}>) {
        super(props);
        this.state = {
            keepAliveTimer: null
        }
    }

    componentDidMount() {
        this.startTimer();
    }

    componentWillUnmount() {
        clearInterval(this.state.keepAliveTimer);
    }

    render(): JSX.Element {
        return null;
    }

    private startTimer() {
        // 60000 = 60s * 1000ms
        const oneMinuteInterval = 60000;
        const timer = setInterval(() => {
            if (document.hasFocus()) {
                Http.sendRequest({method: 'GET', url: `${Config.getServerOrigin()}/api/keep-alive`});
            }
        }, oneMinuteInterval);
        this.setState({
            keepAliveTimer: timer
        });
    }
}
