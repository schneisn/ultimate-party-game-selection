import {AbstractEvent} from '../../../../../event/AbstractEvent';

export class ConnectInfoEvent extends AbstractEvent {
    static readonly id = 'ConnectInfo';
}
