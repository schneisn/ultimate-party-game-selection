import {ICommonPlayer} from '../../../../../../model/ICommonPlayer';

export interface IConnectInfo {
    player: ICommonPlayer;
}
