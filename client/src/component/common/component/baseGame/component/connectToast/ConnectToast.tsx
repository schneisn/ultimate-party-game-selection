import classNames from 'classnames';
import React from 'react';
import {Socket} from '../../../../../../server/socket';
import {LocalStorage, LocalStorageItem} from '../../../../../../store/localStorage';
import {StoredPlayer} from '../../../../../../store/model/StoredPlayer';
import {DisconnectEvent} from '../../../../event/DisconnectEvent';
import {ICommonPlayer} from '../../../../model/ICommonPlayer';
import {Expire} from '../../../expire/Expire';
import {SmallCard} from '../../../smallCard/SmallCard';
import {Toast} from '../../../toast/Toast';
import styles from './ConnectToast.module.scss';
import {ConnectInfoEvent} from './event/ConnectInfoEvent';
import {IConnectInfo} from './event/in/IConnectInfo';
import {IDisconnect} from './event/in/IDisconnect';

const TOAST_TIMEOUT = 2000;

enum ConnectionType {
    Connection = 'connection',
    Disconnection = 'disconnection'
}

interface IConnection {
    type: ConnectionType;
    player: ICommonPlayer;
    timer: NodeJS.Timer;
}

export interface IConnectToastStyles {
    /**
     * Additional classes for connect toast
     */
    connectClasses?: string;
    /**
     * Additional classes for disconnect toast
     */
    disconnectClasses?: string;
}

interface IProps {
    player: ICommonPlayer;
    socket: Socket;
    styles?: IConnectToastStyles;
}

interface IState {
    connections: IConnection[];
}

/**
 * Shows a notification when another player connects or disconnects
 */
export class ConnectToast extends React.Component<IProps, IState> {

    constructor(props: Readonly<IProps>) {
        super(props);
        this.state = {
            connections: []
        }
    }

    private static getConnectionKey(id: string, conn: IConnection): string {
        return id + '-' + conn.player.id + '-' + conn.type;
    }

    componentDidMount() {
        this.props.socket.on(ConnectInfoEvent.id, (data: IConnectInfo) => this.addConnection(data.player, ConnectionType.Connection));
        this.props.socket.on(DisconnectEvent.id, (data: IDisconnect) => this.addConnection(data.player, ConnectionType.Disconnection));
    }

    componentWillUnmount() {
        this.state.connections.forEach(conn => {
            clearTimeout(conn.timer);
        });
        this.props.socket.removeEventListener(ConnectInfoEvent.id);
        this.props.socket.removeEventListener(DisconnectEvent.id);
    }

    render() {
        return <Toast>
            {this.getConnectionList()}
        </Toast>;
    }

    /**
     * Adds a connection to the state that is automatically removed after x seconds
     * @param player
     * @param type
     */
    private addConnection(player: ICommonPlayer, type: ConnectionType): void {
        if (this.newPlayerIsPlayer(player)) {
            return;
        }
        // If the player connects twice in a short time, it must be removed from the list
        // before it can be added again to prevent two elements with the same keys in the list.
        if (this.connectionIsAlreadyDisplayed(player.id, type)) {
            this.removeConnection(player.id, type);
        }
        const timer = setTimeout(() => {
            this.removeConnection(player.id, type);
        }, TOAST_TIMEOUT);
        const newConnection: IConnection = {
            type: type,
            player: player,
            timer: timer
        }
        const connections = this.state.connections;
        connections.unshift(newConnection);
        this.setState({
            connections: connections
        });
    }

    private removeConnection = (playerId: string, type: ConnectionType): void => {
        const {connections} = this.state;
        const playerConIndex = connections.findIndex(conn => conn.player.id === playerId && conn.type === type);
        if (playerConIndex > -1) {
            clearTimeout(connections[playerConIndex].timer);
            connections.splice(playerConIndex, 1);
        }
        this.setState({
            connections: connections
        });
    }

    /**
     * This only works if the players does not get a new id on every reconnect,
     * which happens e.g. in the lobby.
     * @param playerId
     * @param type
     * @private
     */
    private connectionIsAlreadyDisplayed(playerId: string, type: ConnectionType): boolean {
        return this.state.connections.some(conn => conn.player.id === playerId && conn.type === type);
    }

    private newPlayerIsPlayer(newPlayer: ICommonPlayer): boolean {
        // Props player might still contain the player object of a previous game, because
        // the player just joined and the BaseGame did not update the player yet.
        return newPlayer && this.props.player.id && newPlayer.id === LocalStorage.getJSONItem<StoredPlayer>(LocalStorageItem.PLAYER).id;
    }

    private getConnectionList(): JSX.Element {
        const conns = this.state.connections.map(conn => {
            return (
                <Expire timeout={TOAST_TIMEOUT} key={ConnectToast.getConnectionKey('expire', conn)}>
                    <SmallCard key={ConnectToast.getConnectionKey('card', conn)}
                               additionalClassNames={classNames(
                                   styles.ConnectToast,
                                   conn.type === ConnectionType.Connection ? this.props.styles?.connectClasses : this.props.styles?.disconnectClasses
                               )}>
                        <div className={styles.EllipseText}>{conn.player.name}</div>
                        <div>{(conn.type === ConnectionType.Connection ? 'connected' : 'disconnected')}</div>
                    </SmallCard>
                </Expire>
            );
        });
        return <div>{conns}</div>
    }
}
