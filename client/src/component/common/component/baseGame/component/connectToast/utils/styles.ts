import {IConnectToastStyles} from '../ConnectToast';

export function generateConnectToastStyles(connectClasses: string, disconnectClasses: string): IConnectToastStyles {
    return {
        connectClasses,
        disconnectClasses,
    }
}
