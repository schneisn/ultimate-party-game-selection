import {IPlayer} from '../../../../../../../privacy/model/IPlayer';

export interface IDisconnect {
    player: IPlayer;
}
