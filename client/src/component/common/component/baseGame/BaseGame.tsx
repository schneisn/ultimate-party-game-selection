import classNames from 'classnames';
import React from 'react';
import {RouteComponentProps} from 'react-router-dom';
import {Socket} from '../../../../server/socket';
import {SocketProvider} from '../../../../server/socketProvider';
import {LocalStorage, LocalStorageItem} from '../../../../store/localStorage';
import {StoredPlayer} from '../../../../store/model/StoredPlayer';
import {ConnectEvent} from '../../event/ConnectEvent';
import {ExceptionEvent} from '../../event/ExceptionEvent';
import {ReconnectEvent} from '../../event/ReconnectEvent';
import {ICommonGame} from '../../model/ICommonGame';
import {ICommonPlayer} from '../../model/ICommonPlayer';
import {StylesUtils} from '../../utils/styles';
import {Loader} from '../loader/Loader';
import {IModalStyles, Modal} from '../modal/Modal';
import {IPopoverStyles} from '../popover/Popover';
import styles from './BaseGame.module.scss';
import {ConnectionStatus} from '../connectionStatus/ConnectionStatus';
import {ConnectToast, IConnectToastStyles} from './component/connectToast/ConnectToast';
import {Error} from '../error/Error';
import {InfoCard} from '../infoCard/InfoCard';
import Navbar from '../navbar/Navbar';
import {KeepAlive} from './component/keepAlive/KeepAlive';
import {ClientDisconnectEvent} from './event/ClientDisconnectEvent';
import {GetGameEvent} from './event/GetGameEvent';
import {IGetGame} from './event/in/IGetGame';
import {IJoinGame} from './event/in/IJoinGame';
import {JoinGameEvent} from './event/JoinGameEvent';
import {LeaveGameEvent} from './event/LeaveGameEvent';
import {Config} from '../../../../config/config';
import {Emoji} from '../emoji/Emoji.';
import {UrlUtils} from '../../utils/url';
import {WideButton} from '../wideButton/WideButton';
import {GameDescription} from '../gameDescription/GameDescription';
import {GameUtils} from '../../utils/game';
import {Path, Urls} from '../../../../server/serverUrls';
import {NavItem, NavLink} from 'reactstrap';
import {Icon, IconType} from '../icon/Icon';
import {PlayerInfo} from '../playerInfo/PlayerInfo';
import {IConnectionReadyPlayer} from '../../model/IConnectionReadyPlayer';
import {playerIsGameMaster} from '../../utils/player';
import {IConnectionPlayer} from '../../model/IConnectionPlayer';
import {Card} from '../card/Card';
import {IconContent} from '../iconContent/IconContent';
import {KickPlayerEvent} from './event/KickPlayerEvent';

export interface IBaseGameStyles {
    /**
     * Styling for the body when the game is mounted
     */
    bodyClasses?: string;
    /**
     * Styling for the container of a game
     */
    gameClasses?: string;
    /**
     * Styling for the nav bar modal
     */
    modalStyles?: IModalStyles;
    /**
     * Styles for the connect and disconnect toasts
     */
    connectToastStyles?: IConnectToastStyles;
    /**
     * Styles for the game description icon
     */
    gameDescriptionStyles?: IPopoverStyles;
}

interface IBaseGameOptions {
    /**
     * The visible name of the game.
     */
    gameName: string;
    /**
     * The part in the url path with the game name.
     */
    gamePath: Path;
    /**
     * The server endpoint for the game (werewolf, cardsagainst, ...).
     * Starts with '/'
     */
    socketNamespace: Urls;
    styles?: IBaseGameStyles;
}

interface IBaseGameState<Game, Player> {
    /**
     * The game object.
     */
    game: Game;
    /**
     * When the component is mounted and the game is loaded
     * from the server, this is true.
     */
    loading: boolean;
    isPlayerModalOpen: boolean;
    options: IBaseGameOptions;
    player: Player;
    socket: Socket;
}

interface UrlParams {
    id: string;
}

/**
 * The base game that all games extend.
 * It provides common functionality, views and events.
 */
export class BaseGame<Game extends ICommonGame<Player>, Player extends IConnectionPlayer> extends React.Component<RouteComponentProps, IBaseGameState<Game, Player>> {

    constructor(props: Readonly<RouteComponentProps>) {
        super(props);
        const game = this.props.match.path.split('/')[1];
        this.state = {
            ...this.state,
            game: null,
            loading: true,
            isPlayerModalOpen: false,
            player: LocalStorage.getJSONItem<Player>(LocalStorageItem.PLAYER) || null,
            // TODO Move socket out of state
            socket: SocketProvider.getSocket(game)
        };
    }

    componentDidMount() {
        const {id} = this.props.match.params as UrlParams;
        const {socket} = this.state;
        this.registerSocketEvents();
        StylesUtils.addBodyClasses(this.state.options.styles.bodyClasses);
        if (!Config.isDev()) {
            window.onbeforeunload = () => {
                return 'Are you sure you want to leave the game?';
            }
        }
        // TODO Only emit when not coming from game menu AND being the one that created the game
        socket.emit(JoinGameEvent.id, new JoinGameEvent(id));
    }

    componentWillUnmount() {
        this.removeSocketEventListeners();
        this.emitLeaveGame();
        window.onbeforeunload = null;
        StylesUtils.removeBodyClasses(this.state.options.styles.bodyClasses);
    }

    render(): JSX.Element {
        const {id} = this.props.match.params as UrlParams;
        const {player, socket, game, loading, options} = this.state;
        return (
            <>
                <Navbar showBackIcon={true}
                        onBackIconClick={this.onBackIconClick}
                        showConfirmModal={!!game}
                        modalStyles={options.styles?.modalStyles}
                        noCollapseNavItems={!loading && game && this.getNavItems()}/>
                {!loading && game && this.getPlayerModal()}
                <div className={classNames(styles.Game, options.styles?.gameClasses)}>
                    <ConnectionStatus socket={socket}/>
                    <ConnectToast styles={options.styles?.connectToastStyles}
                                  player={player}
                                  socket={socket}/>
                    {loading && !game && <Loader additionalWrapperClasses={styles.Loader}/>}
                    {!loading && !game && this.get404Card(id)}
                    {game && <>
                      <KeepAlive/>
                      <Error socket={socket}/>
                      <GameDescription gameDescription={GameUtils.getGameDescription(options.gamePath)}
                                       gameDescriptionStyles={options.styles?.gameDescriptionStyles}/>
                      <main id={'main'} role={'main'}>
                          {game && player &&
                          this.renderGame()}
                      </main>
                    </>}
                </div>
            </>
        )
    }

    /**
     * Renders the actual game.
     * Overwrite in child classes to render the game
     */
    protected renderGame(): JSX.Element {
        console.error('Game is not implemented');
        return null;
    }

    /**
     * Getting the player from the game only works if he is part of the players array in the game object.
     * This does not work for e.g. the game master in werewolf. In this case, override this function and
     * get the player from the game yourself.
     */
    protected getPlayerFromGame(game: Game): Player {
        if (!game) {
            return null;
        }
        const localPlayer = LocalStorage.getJSONItem<StoredPlayer>(LocalStorageItem.PLAYER);
        return game?.players?.find((p: ICommonPlayer) => p.id === localPlayer.id) as Player;
    }

    /**
     * Updates the game and the player in the state and the
     * player in the local storage. Keeps the name of the player in the local storage,
     * because the player might have another name in the game than in the local storage.
     * @param game
     * @param player
     */
    protected updateGameAndPlayer = (game: Game, player: Player): void => {
        if (!player) {
            this.setState({game});
            return;
        }
        if (!game) {
            // This has to be reset when the game was closed due to too many players leaving
            window.onbeforeunload = null;
        }
        // Keep your name of the local storage. The server changes the name in the game if multiple
        // players have the same name
        const oldPlayer = LocalStorage.getJSONItem<StoredPlayer>(LocalStorageItem.PLAYER);
        LocalStorage.setJSONItem<StoredPlayer>(LocalStorageItem.PLAYER, new StoredPlayer(player.id, oldPlayer.name));
        this.setState({game, player});
    }

    protected getNavItems(): JSX.Element {
        return (
            <>
                <NavItem>
                    <NavLink title={'Manage players'}
                             href={''}
                             onClick={this.togglePlayerModal}>
                        Players <Icon icon={IconType.userEdit}/>
                    </NavLink>
                </NavItem>
            </>
        );
    }

    protected getPlayerModal(): JSX.Element {
        const {isPlayerModalOpen, game, player} = this.state;
        const playerElements = game?.players
            .map((teamMate: ICommonPlayer & Partial<IConnectionReadyPlayer>) => {
                if ((playerIsGameMaster(game, teamMate.id))) {
                    teamMate.isGameMaster = true;
                }
                return teamMate;
            })
            .map(teamMate => {
                const icon = !playerIsGameMaster(game, player.id) || teamMate.isGameMaster ? null : {
                    icon: IconType.trash,
                    iconTitle: 'Kick player',
                    additionalIconClassnames: styles.KickIcon,
                    onIconClick: () => this.onKickPlayerClick(teamMate.id),
                };
                return <Card key={teamMate.id}
                             additionalClassNames={styles.PlayerCard}>
                    <IconContent iconRight={icon}>
                        <PlayerInfo player={teamMate}/>
                    </IconContent>
                </Card>
            });

        return <Modal modalTitle={'Players'}
                      isOpen={isPlayerModalOpen}
                      showFooter={false}
                      onToggle={this.togglePlayerModal}>
            {playerElements}
        </Modal>
    }

    private onKickPlayerClick = (playerId: string): void => {
        const {socket, game} = this.state;
        socket.emit(KickPlayerEvent.id, new KickPlayerEvent(game.id, playerId));
    }

    protected togglePlayerModal = (event: React.MouseEvent<unknown>): void => {
        event.preventDefault();
        this.setState({
            isPlayerModalOpen: !this.state.isPlayerModalOpen
        });
    }

    private get404Card(gameId: string): JSX.Element {
        return <>
            <InfoCard titleText={'404'}>
                <div>
                    There is no game with the id {gameId}
                </div>
                <div className={styles.NotFoundInfo}>
                    Maybe there were not enough players and the game was closed <Emoji symbol={'🤷‍♂️'}/>
                </div>
            </InfoCard>
            <WideButton additionalClassNames={styles.BackToMenuButton}
                        onClick={() => UrlUtils.goHierarchyUp(this.props.history)}>
                Go back to menu
            </WideButton>
        </>;
    }

    private registerSocketEvents(): void {
        const {socket} = this.state;
        socket.on(JoinGameEvent.id, (data: IJoinGame<Game, Player>) => {
            this.setState({loading: false});
            this.updateGameAndPlayer(data.game, data.player)
        });
        socket.on(GetGameEvent.id, (data: IGetGame<Game>) => {
            const player = this.getPlayerFromGame(data.game);
            this.updateGameAndPlayer(data.game, player);
        });
        socket.on(KickPlayerEvent.id, this.goToMenu);
        socket.on(ConnectEvent.id, this.rejoinGame);
        socket.on(ReconnectEvent.id, this.rejoinGame);
        socket.on(ClientDisconnectEvent.id, this.resetGame);
        socket.on(ExceptionEvent.id, this.resetGame, false);
    }

    private removeSocketEventListeners(): void {
        const {socket} = this.state;
        socket.removeEventListener(JoinGameEvent.id);
        socket.removeEventListener(GetGameEvent.id);
        socket.removeEventListener(KickPlayerEvent.id, this.goToMenu);
        socket.removeEventListener(ConnectEvent.id, this.rejoinGame);
        socket.removeEventListener(ReconnectEvent.id, this.rejoinGame);
        socket.removeEventListener(ClientDisconnectEvent.id, this.resetGame);
        socket.removeEventListener(ExceptionEvent.id, this.resetGame);
    }

    private rejoinGame = (): void => {
        const {socket} = this.state;
        const {id} = this.props.match.params as UrlParams;
        // Use game id from url because game object in state might be null
        if (id) {
            socket.emit(JoinGameEvent.id, new JoinGameEvent(id));
        }
    }

    private resetGame = (): void => {
        window.onbeforeunload = null;
        this.setState({game: null, loading: false});
    }

    private onBackIconClick = (): void => {
        this.emitLeaveGame();
        this.goToMenu();
    }

    private emitLeaveGame = (): void => {
        const {socket, game} = this.state;
        game && socket.emit(LeaveGameEvent.id, new LeaveGameEvent());
    }

    private goToMenu = (): void => {
        window.onbeforeunload = null;
        UrlUtils.goTo(this.props.history, UrlUtils.getGameType());
    }
}
