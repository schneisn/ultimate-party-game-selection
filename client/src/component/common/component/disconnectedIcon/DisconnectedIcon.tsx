import React from 'react';
import {IdUtils} from '../../utils/id';
import {IconType} from '../icon/Icon';
import {Popover} from '../popover/Popover';
import {generatePopoverStyles} from '../popover/utils/styles';
import styles from './DisconnectedIcon.module.scss';

/**
 * An icon that indicates that the player is disconnected
 */
export class DisconnectedIcon extends React.PureComponent {
    render() {
        return <Popover id={'disconnected-' + IdUtils.createUuid()}
                        icon={IconType.exclamationTriangle}
                        customStyles={generatePopoverStyles(styles.DisconnectIcon)}>
            Player is not connected
        </Popover>;
    }
}
