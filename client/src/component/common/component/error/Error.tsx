import React from 'react';
import {Toast, ToastBody, ToastHeader} from 'reactstrap';
import {Logger} from '../../../../logger/logger';
import {Socket} from '../../../../server/socket';
import {ExceptionEvent} from '../../event/ExceptionEvent';
import {IException} from '../baseGame/event/in/IException';
import styles from './Error.module.scss';

interface IProps {
    socket: Socket;
}

interface IState {
    status: string;
    message: string;
    isOpen: boolean;
}

/**
 * Shows an error when an exception is thrown by the server
 */
export class Error extends React.Component<IProps, IState> {

    constructor(props: Readonly<IProps>) {
        super(props);
        this.state = {
            status: null,
            isOpen: false,
            message: null
        }
    }

    private static clearCacheAndReload() {
        // https://stackoverflow.com/questions/18967532/window-location-reload-not-working-for-firefox-and-chrome
        // https://stackoverflow.com/questions/2405117/difference-between-window-location-href-window-location-href-and-window-location
        // eslint-disable-next-line
        window.location.hash ? window.location.reload() : window.location.href = window.location.href;
    }

    componentDidMount() {
        this.props.socket.on(ExceptionEvent.id, this.updateError, false);
    }

    componentWillUnmount() {
        this.props.socket.removeEventListener(ExceptionEvent.id, this.updateError);
    }

    render(): JSX.Element {
        if (!this.state.isOpen) {
            return null;
        }
        return (
            <Toast isOpen={this.state.isOpen}
                   className={styles.Error}>
                <ToastHeader toggle={this.toggleIsOpen}
                             className={styles.Status}>
                    {this.state.status?.toUpperCase()}
                </ToastHeader>
                <ToastBody className={styles.ErrorText}>
                    <div>
                        {this.state.message}
                    </div>
                    <div className={styles.ReloadText}>
                        If the error persists, <span className={styles.ReloadLink}
                                                     role={'button'}
                                                     onClick={Error.clearCacheAndReload}>
                        clicking here to reload the page
                    </span> (or using Ctrl+F5) might help
                    </div>
                </ToastBody>
            </Toast>
        )
    }

    private toggleIsOpen = () => {
        this.setState({isOpen: !this.state.isOpen});
    }

    private updateError = (data: IException) => {
        Logger.error(data);
        this.setState({
            status: data.status,
            message: data.message,
            isOpen: true
        });
    }
}
