import {toDataURL} from 'qrcode';
import React from 'react';
import {RouteComponentProps, withRouter} from 'react-router-dom';
import {Collapse, Nav, Navbar as ReactNavbar, NavbarBrand, NavbarToggler, NavItem, NavLink} from 'reactstrap';
import {Routes} from '../../../../route/router';
import {Urls} from '../../../../server/serverUrls';
import {UrlUtils} from '../../utils/url';
import {Icon, IconType} from '../icon/Icon';
import {Image} from '../image/Image';
import {IModalStyles, Modal} from '../modal/Modal';
import styles from './Navbar.module.scss';
import {Config} from '../../../../config/config';
import {generateModalStyles} from '../modal/utils/styles';

interface IProps extends RouteComponentProps {
    modalStyles?: IModalStyles;
    /**
     * Called when clicking on back icon.
     * Additionally, it goes up one level
     * in the href hierarchy
     */
    onBackIconClick?: () => void;
    /**
     * Whether to show the go-back icon.
     * Default is false
     */
    showBackIcon?: boolean;
    /**
     * Whether to show a confirm modal when clicking
     * a link in the nav bar.
     * Default is false
     *
     * When this is true, the back icon click does not
     * go back in history.
     */
    showConfirmModal?: boolean;
    /**
     * Elements that do not collapse when the navbar width
     * is small.
     */
    noCollapseNavItems?: JSX.Element;
}

interface IModalState {
    modalIsOpen: boolean;
}

interface IConfirmModalState extends IModalState {
    confirmAction: () => void;
    modalBody: JSX.Element;
}

interface IQrCodeModalState extends IModalState {
    qrCodeSrc: string;
}

interface IState {
    confirmModalState: IConfirmModalState;
    qrCodeModalState: IQrCodeModalState;
    menuOpen: boolean;
}

interface UrlParams {
    id: string;
}

/**
 * The header of the page
 */
class Navbar extends React.PureComponent<IProps, IState> {

    constructor(props: Readonly<IProps>) {
        super(props);
        this.state = {
            confirmModalState: {
                modalIsOpen: false,
                confirmAction: null,
                modalBody: null
            },
            qrCodeModalState: {
                modalIsOpen: false,
                qrCodeSrc: null
            },
            menuOpen: false
        }
    }

    private static getConfirmModalBody(): JSX.Element {
        return <div>
            Are you sure you want to leave the game?
        </div>;
    }

    componentDidMount() {
        this.getQRCode()
    }

    render() {
        const {id} = this.props.match.params as UrlParams;
        return (
            <div className={styles.Wrapper}>
                <Modal modalTitle={'Confirm'}
                       styles={this.props.modalStyles}
                       isOpen={this.state.confirmModalState.modalIsOpen}
                       onConfirm={this.state.confirmModalState.confirmAction}
                       onToggle={this.toggleConfirmModal}>
                    {this.state.confirmModalState.modalBody}
                </Modal>
                <Modal modalTitle={id ? `Share this game` : 'Share the Ultimate Party Game Selection'}
                       isOpen={this.state.qrCodeModalState.modalIsOpen}
                       onToggle={this.toggleQrCodeModal}
                       showFooter={false}
                       styles={generateModalStyles(null, null, null, styles.QrModal)}>
                    <>
                        <Image additionalClassNames={styles.QrCode}
                               alt={'QR Code'}
                               src={this.state.qrCodeModalState.qrCodeSrc}>
                        </Image>
                        {navigator?.clipboard &&
                        <div>
                            {id ? <b>Game {id}</b> : <b>Copy url to clipboard</b>}
                          <Icon icon={IconType.copy}
                                title={'Copy url'}
                                additionalClasses={styles.CopyIcon}
                                onClick={() => navigator.clipboard?.writeText(window.location.href)}/>
                        </div>}
                    </>
                </Modal>
                <ReactNavbar className={styles.Navbar}
                             dark
                             expand={'md'}
                             onBlur={this.closeMenu}>
                    <div className={styles.Left}>
                        {this.props.showBackIcon &&
                        <Icon icon={IconType.arrowLeft}
                              additionalClasses={styles.Back}
                              title={'Go back'}
                              onClick={this.onBackIconClick}/>}
                        <NavbarBrand className={styles.Brand}
                                     href={'/'}
                                     title={'Ultimate Party Game Selection'}>
                            <Image src={`/images/icons/icon-192x192.png`}/><span className={styles.BrandName}>UPGS</span>
                        </NavbarBrand>
                    </div>
                    <div className={styles.NoCollapse}>
                        <Nav className={styles.Items} navbar>
                            {this.props.noCollapseNavItems}
                        </Nav>
                    </div>
                    <NavbarToggler aria-label={'Navbar Menu Toggle'}
                                   title={`${this.state.menuOpen ? 'Close' : 'Open'} menu`}
                                   className={styles.Toggle}
                                   onClick={this.toggleMenuOpen}/>
                    <Collapse navbar
                              className={styles.Collapse}
                              isOpen={this.state.menuOpen}>
                        <Nav className={styles.Items} navbar>
                            <NavItem>
                                <NavLink onClick={this.onChangeNameClick}
                                         title={'Edit your name'}
                                         disabled={window.location.pathname === Urls.ChangeName}
                                         href={window.location.pathname !== Urls.ChangeName ? '/changename' : undefined}>
                                    Change Name
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href={Routes.GITLAB}
                                         target={'_blank'}
                                         title={'Open the Gitlab page of the project'}
                                         rel={'noreferrer'}>
                                    Gitlab<Icon icon={IconType.externalLink}
                                                additionalClasses={styles.ExternalLinkIcon}/>
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink title={'Share'}
                                         href={''}
                                         onClick={this.toggleQrCodeModal}>
                                    Share
                                </NavLink>
                            </NavItem>
                        </Nav>
                    </Collapse>
                </ReactNavbar>
            </div>
        );
    }

    private toggleMenuOpen = (): void => {
        this.setState({menuOpen: !this.state.menuOpen});
    }

    private closeMenu = (): void => {
        this.setState({menuOpen: false});
    }

    /**
     * Executes the onItemClick.
     * Checks whether a confirm modal needs to be opened
     * @param onItemClick
     */
    private onNavItemClick = (onItemClick?: () => void): void => {
        if (this.props.showConfirmModal) {
            this.setState({
                confirmModalState: {
                    ...this.state.confirmModalState,
                    modalIsOpen: true,
                    confirmAction: onItemClick,
                    modalBody: Navbar.getConfirmModalBody()
                }
            });
            return;
        }
        onItemClick && onItemClick();
    }

    private onBackIconClick = (): void => {
        this.onNavItemClick(this.goBack);
    }

    private goBack = (): void => {
        if (!this.props.showConfirmModal || !this.props.onBackIconClick) {
            UrlUtils.goHierarchyUp(this.props.history);
        }
        this.props.onBackIconClick && this.props.onBackIconClick();
    }

    private onChangeNameClick = (event: React.MouseEvent<unknown>): void => {
        if (window.location.pathname === Urls.ChangeName) {
            return;
        }
        // If ctrl key is pressed to open a new tab, do not execute
        // the normal click functionality
        if (event.ctrlKey) {
            return;
        }
        // Prevent loading page due to href.
        // Href should not be removed because otherwise opening in new tab is not possible.
        event.preventDefault();
        this.onNavItemClick(() => this.props.history.push(Urls.ChangeName));
    }

    private toggleConfirmModal = (): void => {
        this.setState({
            confirmModalState: {
                ...this.state.confirmModalState,
                modalIsOpen: !this.state.confirmModalState.modalIsOpen
            }
        });
    }

    private toggleQrCodeModal = (event: React.MouseEvent<unknown>): void => {
        event.preventDefault();
        this.setState({
            qrCodeModalState: {
                ...this.state.qrCodeModalState,
                modalIsOpen: !this.state.qrCodeModalState.modalIsOpen
            }
        });
    }

    private getQRCode = (): void => {
        toDataURL(Config.getClientHref(), {
            width: 400,
        }).then((imgSrc: string) =>
            this.setState({
                qrCodeModalState: {
                    ...this.state.qrCodeModalState,
                    qrCodeSrc: imgSrc
                }
            })
        );
    }
}

export default withRouter(Navbar);
