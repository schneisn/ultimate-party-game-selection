import React from 'react';
import {Input} from 'reactstrap';

interface IProps {
    additionalClassNames?: string;
    /**
     * Whether the checkbox is checked.
     * Default is false.
     */
    checked?: boolean;
    /**
     * Whether the checkbox is disabled.
     * Default is false
     */
    disabled?: boolean;
    /**
     * Visibility of the checkbox.
     * Default is false
     */
    hidden?: boolean;
    /**
     * The name of the group. When another
     * item in this group is selected, this
     * item is not selected
     */
    name: string;
    onClick?: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

export class Radio extends React.Component<IProps> {

    render() {
        return (
            <Input type={'radio'}
                   role={'button'}
                   name={this.props.name}
                   checked={this.props.checked}
                   className={this.props.additionalClassNames}
                   onChange={this.onClick}
                   hidden={this.props.hidden}
                   disabled={this.props.disabled}/>
        );
    }

    private onClick = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.props.onClick && this.props.onClick(event);
    }
}
