import React from 'react';
import {RouteComponentProps} from 'react-router-dom';
import {LocalStorage, LocalStorageItem} from '../../../../store/localStorage';
import {StoredPlayer} from '../../../../store/model/StoredPlayer';
import {UrlUtils} from '../../utils/url';
import Navbar from '../navbar/Navbar';
import {InfoCard} from '../infoCard/InfoCard';
import {TextAreaWithButton} from '../textAreaWithButton/TextAreaWithButton';
import styles from './ChangeName.module.scss';

export const PLAYER_NAME_MAX_LENGTH = 20;

export class ChangeName extends React.Component<RouteComponentProps> {

    render(): JSX.Element {
        const player = LocalStorage.getJSONItem<StoredPlayer>(LocalStorageItem.PLAYER);
        return (
            <>
                <Navbar showBackIcon={!!player?.name}/>
                <div className={styles.ChangeName}>
                    <InfoCard titleText={'Party Game Selection'}
                              additionalClassNames={styles.ChangeNameCard}>
                        Enter a name and start playing!
                    </InfoCard>
                    <TextAreaWithButton additionalButtonClasses={styles.ChangeNameButton}
                                        textPlaceholder={'What\'s your name?'}
                                        textValue={player?.name || ''}
                                        onSubmit={this.setPlayerName}
                                        buttonText={'Start playing'}
                                        autoFocus={true}
                                        required={true}
                                        clearAfterSubmit={false}
                                        textMaxLength={PLAYER_NAME_MAX_LENGTH}
                    />
                </div>
            </>
        )
    }

    private setPlayerName = (name: string) => {
        const player = LocalStorage.getJSONItem<StoredPlayer>(LocalStorageItem.PLAYER)
        LocalStorage.setJSONItem<StoredPlayer>(LocalStorageItem.PLAYER, new StoredPlayer(player?.id, name));
        UrlUtils.goBackOnPage(this.props.history);
    };
}
