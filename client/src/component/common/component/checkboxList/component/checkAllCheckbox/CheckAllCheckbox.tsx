import React from 'react';
import styles from './CheckAllCheckbox.module.scss';
import {Icon, IconType} from '../../../icon/Icon';
import classNames from 'classnames';

export enum SelectionState {
    NONE,
    SOME,
    ALL
}

interface IProps {
    additionalClassNames?: string;
    /**
     * Whether the item is disabled.
     * Default is false.
     */
    disabled?: boolean;
    /**
     * Called when the box is clicked and then
     * all items are selected.
     * States are:
     * NONE Selected => ALL Selected
     * SOME Selected => ALL Selected
     */
    onSelectAll?: () => void;
    /**
     * Called when the box is clicked and then
     * no items are selected
     * ALL Selected => NONE Selected
     */
    onSelectNone?: () => void;
    /**
     * Used to set the selection state for the
     * checkbox
     */
    selectionState?: SelectionState
}

export class CheckAllCheckbox extends React.Component<IProps> {

    render() {
        const {selectionState, additionalClassNames} = this.props;
        return (
            <div className={classNames(styles.CheckAll, additionalClassNames)}>
                {selectionState === SelectionState.NONE &&
                <Icon icon={IconType.square}
                      onClick={() => this.setSelectionState(SelectionState.ALL)}
                      disabled={this.props.disabled}
                      additionalClasses={this.props.disabled === true && styles.Disabled}
                      title={'Select all'}/>}
                {selectionState === SelectionState.SOME &&
                <Icon icon={IconType.minusSquare}
                      onClick={() => this.setSelectionState(SelectionState.ALL)}
                      disabled={this.props.disabled}
                      additionalClasses={this.props.disabled === true && styles.Disabled}
                      title={'Select all'}/>}
                {selectionState === SelectionState.ALL &&
                <Icon icon={IconType.checkDouble}
                      onClick={() => this.setSelectionState(SelectionState.NONE)}
                      disabled={this.props.disabled}
                      additionalClasses={this.props.disabled === true && styles.Disabled}
                      title={'Select none'}/>}
            </div>
        );
    }

    private setSelectionState = (newState: SelectionState): void => {
        if (this.props.disabled) {
            return;
        }
        switch (newState) {
            case SelectionState.ALL:
            // fallthrough
            case SelectionState.SOME:
                this.props.onSelectAll();
                break;
            case SelectionState.NONE:
                this.props.onSelectNone();
                break;
            default:
                break;
        }
    }
}
