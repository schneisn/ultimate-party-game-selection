import React from 'react';
import {Dropdown as ReactStrapDropdown, DropdownMenu, DropdownToggle, FormGroup, Label} from 'reactstrap';
import {Icon, IconType} from '../../../icon/Icon';
import styles from './DropdownFilter.module.scss';
import {Checkbox} from '../../../checkbox/Checkbox';
import {Radio} from '../../../radio/Radio';
import {IconDefinition} from '@fortawesome/fontawesome-svg-core';
import classNames from 'classnames';

/**
 * A group of items. The group has a header
 * and can be either multi- or single-select
 */
export interface IFilterGroup {
    header: string;
    /**
     * The filter applies to this key in an object
     */
    key: string;
    /**
     * Whether only one value can be selected
     * for this filter.
     * Default is false (= multi-select)
     */
    isSingleSelect?: boolean;
    items?: IFilterItem[];
}

/**
 * The item in the filter menu
 */
export interface IFilterItem {
    id: string;
    content: JSX.Element | string;
    /**
     * Whether the item is disabled.
     * Default is false.
     */
    isDisabled?: boolean;
    /**
     * Whether the item is selected.
     * Default is false.
     */
    isSelected?: boolean;
    /**
     * All objects that match these values are shown
     */
    values?: string[];
}

interface IProps {
    additionalClassNames?: string;
    /**
     * The filters in the filter menu
     */
    filterGroups?: IFilterGroup[];
    /**
     * Whether the filter is disabled.
     * Default is false.
     */
    disabled?: boolean;
    /**
     * On change event when the selected items change.
     */
    onSelectedItemsChange?: (selectedFilters: IFilterItem[]) => void;
    /**
     * Optional custom icon for the filter.
     * Default is the normal filter icon.
     */
    filterIcon?: IconDefinition;
    /**
     * Title shown on hovering the filter icon
     */
    filterIconTitle?: string;
}

interface IState {
    isOpen: boolean;
}

/**
 * A filter that is accessed by a filter icon and appears as a dropdown
 */
export class DropdownFilter extends React.Component<IProps, IState> {

    constructor(props: Readonly<IProps>) {
        super(props);
        this.state = {
            isOpen: false,
        }
    }

    render() {
        const {additionalClassNames, filterGroups, filterIcon, filterIconTitle, disabled} = this.props;
        const {isOpen} = this.state;
        if (!filterGroups) {
            return null;
        }
        return (
            <div className={additionalClassNames}>
                <ReactStrapDropdown isOpen={isOpen}
                                    toggle={this.toggle}
                                    disabled={disabled}>
                    <DropdownToggle onClick={this.toggleIsOpen}
                                    disabled={disabled}
                                    className={styles.ToggleButton}>
                        <Icon icon={filterIcon ? filterIcon : IconType.filter}
                              disabled={disabled}
                              title={filterIconTitle}/>
                    </DropdownToggle>
                    <DropdownMenu positionFixed={true}
                                  className={styles.FilterMenu}>
                        {filterGroups?.map((group: IFilterGroup, groupIndex: number) => {
                            return (
                                <FormGroup key={group.key}>
                                    <Label>
                                        <div key={groupIndex}
                                             className={styles.Header}>
                                            {group.header}
                                        </div>
                                    </Label>
                                    {group.items?.map((item: IFilterItem, index: number) => {
                                        return (
                                            <FormGroup key={index}
                                                       className={styles.FilterItem}
                                                       onClick={() => this.onFilterItemClick(group, item)}
                                                       check>
                                                <Label check
                                                       disabled={item.isDisabled}
                                                       className={classNames(styles.Label, disabled && styles.Disabled)}
                                                       role={'button'}>
                                                    <div className={styles.FilterCheckboxContainer}>
                                                        {group.isSingleSelect ?
                                                            <Radio checked={item.isSelected}
                                                                   disabled={item.isDisabled}
                                                                   name={group.key}/>
                                                            :
                                                            <Checkbox checked={item.isSelected}
                                                                      disabled={item.isDisabled}/>}
                                                    </div>
                                                    <div className={styles.LabelContent}>
                                                        {item.content}
                                                    </div>
                                                </Label>
                                            </FormGroup>
                                        )
                                    })}
                                </FormGroup>)
                        })}
                    </DropdownMenu>
                </ReactStrapDropdown>
            </div>
        );
    }

    /**
     * Dummy method because the reactstrap dropdown needs a function -.-
     * @private
     */
    private toggle = (): void => {
    }

    private toggleIsOpen = (): void => {
        if (this.props.disabled === true) {
            return;
        }
        this.setState({
            isOpen: !this.state.isOpen
        })
    }

    private onFilterItemClick = (group: IFilterGroup, item: IFilterItem): void => {
        const {onSelectedItemsChange, disabled} = this.props;
        if (disabled === true) {
            return;
        }
        const selectedFilters = group.items;
        let itemSelectionChanged = item.isSelected;
        if (group.isSingleSelect) {
            group.items.forEach(groupItems => groupItems.isSelected = false);
            item.isSelected = true;
        } else {
            item.isSelected = !item.isSelected;
        }
        itemSelectionChanged = itemSelectionChanged !== item.isSelected;
        itemSelectionChanged && onSelectedItemsChange && onSelectedItemsChange(selectedFilters.filter(filter => filter.isSelected));
    }

}
