import {IFilterGroup, IFilterItem} from '../component/dropdownFilter/DropdownFilter';
import {ICheckboxListItem} from '../CheckboxList';

/**
 * Checks whether the given listItem matches to any of the filters criteria
 * @param listItem
 * @param group
 * @param filter
 */
export function listItemMatchesFilter(listItem: ICheckboxListItem, group: IFilterGroup, filter: IFilterItem): boolean {
    if (!filter.isSelected || !filter.values || !listItem.filters) {
        return false;
    }
    const listItemFilterValues = listItem.filters?.get(group.key);
    const filterValues = filter.values?.map(value => value.toLowerCase());
    const matchedFilterValues = listItemFilterValues?.filter(value => filterValues.includes(value.toLowerCase()));
    return matchedFilterValues?.length > 0;
}
