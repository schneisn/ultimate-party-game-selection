import {ICheckboxListStyles} from '../CheckboxList';

export function generateCheckboxListStyles(containerClasses?: string, itemClasses?: string, selectedItemClasses?: string): ICheckboxListStyles {
    return {
        containerClasses,
        itemClasses,
        selectedItemClasses
    }
}
