import React from 'react';
import {CheckboxWithLabel} from '../checkboxWithLabel/CheckboxWithLabel';
import {HeaderContainer} from '../headerContainer/HeaderContainer';
import styles from './CheckboxList.module.scss';
import {DropdownFilter, IFilterGroup, IFilterItem} from './component/dropdownFilter/DropdownFilter';
import {CheckAllCheckbox, SelectionState} from './component/checkAllCheckbox/CheckAllCheckbox';
import {IconDefinition} from '@fortawesome/fontawesome-svg-core';
import {listItemMatchesFilter} from './utils/filterUtils';

export interface ICheckboxListStyles {
    containerClasses?: string;
    itemClasses?: string;
    selectedItemClasses?: string;
}

/**
 * The items that are displayed in the list
 */
export interface ICheckboxListItem {
    /**
     * An identifier that can be used to identify the item
     */
    id?: string;
    /**
     * The visible content of the item
     */
    content: JSX.Element | string;
    /**
     * Function called when item is clicked.
     */
    onClick?: () => void;
    /**
     * Whether the item can be checked.
     * Default is true
     */
    checkable?: boolean;
    /**
     * Whether the item is checked.
     * Default is false
     */
    checked?: boolean;
    /**
     * Filters that apply to this list item.
     * E.g. <language, [german]>
     */
    filters?: Map<string, string[]>;
}

interface IProps {
    /**
     * Custom content at the top of the list
     * but under the header.
     */
    customTopContent?: JSX.Element;
    /**
     * The title of the list
     */
    listHeader?: string;
    /**
     * The content to display when there are not items.
     * Default is 'no data'
     */
    emptyItemsContent?: JSX.Element | string;
    /**
     * All items that can be shown in the list.
     * The filter is applied to these items.
     */
    items: ICheckboxListItem[];
    /**
     * The filters by which the items can be filtered.
     * If this is null, no filter icon is shown.
     */
    filterGroups?: IFilterGroup[];
    /**
     * Whether the filter is disabled.
     * Default is false.
     */
    filterIsDisabled?: boolean;
    /**
     * Custom styles.
     */
    listStyles?: ICheckboxListStyles;
    /**
     * Whether to show an icon to indicate whether a card
     * is selected or not. Default is true.
     */
    showCheckIcons?: boolean;
    /**
     * Whether to show the icon to select/deselect all items at once.
     * Default is false.
     */
    showCheckAllIcon?: boolean;
    /**
     * Called when the filter changes.
     * @param selectedFilters The selected filters
     * @param visibleItems The items that are shown with the new filters
     */
    onFilterChange?: (selectedFilters: IFilterItem[], visibleItems: ICheckboxListItem[]) => void;
    /**
     * Whether the filter is disabled.
     * Default is false.
     */
    checkAllIsDisabled?: boolean;
    /**
     * Called when the box is clicked and then
     * all items are selected.
     * States are:
     * NONE Selected => ALL Selected
     * SOME Selected => ALL Selected
     */
    onCheckboxSelectAll?: (filteredItems: ICheckboxListItem[]) => void;
    /**
     * Called when the box is clicked and then
     * no items are selected
     * ALL Selected => NONE Selected
     */
    onCheckboxSelectNone?: (filteredItems: ICheckboxListItem[]) => void;
    /**
     * Optional custom icon for the filter.
     * Default is the normal filter icon.
     */
    filterIcon?: IconDefinition;
    /**
     * Title shown on hovering the filter icon
     */
    filterIconTitle?: string;
}

interface IState {
    /**
     * The visible (filtered) items.
     * By default, all items are selected.
     */
    filteredItems: ICheckboxListItem[];
    checkboxSelectionState: SelectionState;
}

/**
 * A list with checkbox cards
 */
export class CheckboxList extends React.Component<IProps, IState> {

    constructor(props: Readonly<IProps>) {
        super(props);
        this.state = {
            filteredItems: this.props.items,
            checkboxSelectionState: SelectionState.ALL
        }
    }

    componentDidUpdate(prevProps: Readonly<IProps>, prevState: Readonly<IState>, snapshot?: any) {
        const {filterGroups, items} = this.props;
        if (this.props === prevProps) {
            return;
        }
        let filteredItems: ICheckboxListItem[] = items;
        if (filterGroups) {
            filterGroups.forEach(group => {
                const selectedFilters = group.items.filter(filter => filter.isSelected);
                filteredItems = items.filter(listItem =>
                    selectedFilters.some((filter: IFilterItem) => listItemMatchesFilter(listItem, group, filter))
                );
            })
        }

        // Update checkbox with the items that are visible (= filters are applied)
        const allItemsSelected = filteredItems.every(item => item.checked);
        const noItemsSelected = !filteredItems.some(item => item.checked);
        let newSelectionState: SelectionState = SelectionState.SOME;
        if (noItemsSelected) {
            newSelectionState = SelectionState.NONE;
        } else if (allItemsSelected) {
            newSelectionState = SelectionState.ALL;
        }
        this.setState({
            filteredItems: filteredItems,
            checkboxSelectionState: newSelectionState
        });
    }

    render() {
        const {customTopContent, listStyles} = this.props;
        return (
            <HeaderContainer additionalClassNames={listStyles?.containerClasses}
                             header={this.getHeader()}>
                {customTopContent}
                {this.getItemCards()}
            </HeaderContainer>
        );
    }

    private getHeader(): JSX.Element {
        const {checkAllIsDisabled, filterIsDisabled, listHeader, filterGroups, filterIcon, filterIconTitle, showCheckAllIcon} = this.props;
        const {checkboxSelectionState} = this.state;

        if (!listHeader && !filterGroups && !showCheckAllIcon) {
            return null;
        }

        // Default header with select all, title and filter icon
        return <div className={styles.HeaderContainer}>
            <div className={styles.CheckAllCheckboxPosition}>
                {showCheckAllIcon === true && <CheckAllCheckbox selectionState={checkboxSelectionState}
                                                                onSelectAll={this.selectAllVisibleItems}
                                                                onSelectNone={this.selectNoneVisibleItems}
                                                                disabled={checkAllIsDisabled}/>}
            </div>
            <div className={styles.Title}>
                {listHeader}
            </div>
            <div className={styles.FilterPosition}>
                <DropdownFilter filterGroups={filterGroups}
                                onSelectedItemsChange={this.onItemsChange}
                                disabled={filterIsDisabled}
                                filterIcon={filterIcon}
                                filterIconTitle={filterIconTitle || 'Filter'}/>
            </div>
        </div>;
    }

    private getItemCards(): JSX.Element[] {
        const {listStyles, emptyItemsContent, showCheckIcons} = this.props;
        const {filteredItems} = this.state;

        if (!filteredItems || filteredItems.length < 1) {
            return [<div key={'no-data'} className={styles.NoData}>{emptyItemsContent || 'No Data'}</div>];
        }

        return filteredItems
            .map((item: ICheckboxListItem, index: number) => {
                return <CheckboxWithLabel key={index}
                                          additionalClassNames={listStyles?.itemClasses}
                                          checkedClassNames={listStyles?.selectedItemClasses}
                                          checked={item.checked}
                                          disabled={item.checkable === false}
                                          onClick={() => this.onItemClick(item)}
                                          showIcon={showCheckIcons !== false}>
                    {item.content}
                </CheckboxWithLabel>
            });
    }

    private onItemClick = (clickedItem: ICheckboxListItem): void => {
        clickedItem.onClick && clickedItem.onClick();
    }

    private onItemsChange = (selectedFilters: IFilterItem[]): void => {
        if (!this.props.filterGroups) {
            return;
        }
        const newSelectedItems: ICheckboxListItem[] = this.props.items
            .filter(listItem =>
                selectedFilters.some((filter: IFilterItem) =>
                    listItemMatchesFilter(listItem, this.getFilterGroupOfFilter(filter), filter)));
        this.setState({
            filteredItems: newSelectedItems
        });
        this.props.onFilterChange && this.props.onFilterChange(selectedFilters, newSelectedItems);
    }

    private getFilterGroupOfFilter(filterItem: IFilterItem): IFilterGroup {
        return this.props.filterGroups?.find(group => group.items.some(item => item.id === filterItem.id));
    }

    private selectAllVisibleItems = (): void => {
        this.props.onCheckboxSelectAll && this.props.onCheckboxSelectAll(this.state.filteredItems);
    }

    private selectNoneVisibleItems = (): void => {
        this.props.onCheckboxSelectNone && this.props.onCheckboxSelectNone(this.state.filteredItems);
    }
}
