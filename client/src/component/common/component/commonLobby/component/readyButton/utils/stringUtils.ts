/**
 * Returns different texts that represent the players ready state,
 * depending on the parameter isReady
 * @param isReady Whether the player is ready
 */
export function getReadyButtonReadyStateText(isReady: boolean): string {
    return isReady ? getReadyButtonReadyText() : getReadyButtonNotReadyText();
}

/**
 * Returns the text that is shown when the player is ready
 */
export function getReadyButtonReadyText(): string {
    return 'Click if you are not ready';
}

/**
 * Returns the text that is shown when the player is not ready
 */
export function getReadyButtonNotReadyText(): string {
    return 'Click if you are ready';
}
