import {GameEvent} from '../../../../../event/GameEvent';

export class SetPlayerReadyStateEvent extends GameEvent {
    static readonly id = 'SetPlayerReadyState';
    isReady: boolean;

    constructor(gameId: string, isReady: boolean) {
        super(gameId);
        this.isReady = isReady;
    }
}
