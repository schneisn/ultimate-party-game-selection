import classNames from 'classnames';
import React from 'react';
import {Socket} from '../../../../../../server/socket';
import {ICommonPlayer} from '../../../../model/ICommonPlayer';
import {WideButton} from '../../../wideButton/WideButton';
import {ILobbyGame} from '../../CommonLobby';
import {SetPlayerReadyStateEvent} from './event/SetPlayerReadyStateEvent';
import styles from './ReadyButton.module.scss';
import {getReadyButtonReadyStateText} from './utils/stringUtils';

export interface IIsReadyPlayer extends ICommonPlayer {
    /**
     * The ready state of the player
     */
    isReady: boolean;
    /**
     * Whether the player is game master (has more permissions than other players).
     * Default is false.
     */
    isGameMaster?: boolean;
}

interface IProps {
    additionalClasses?: string;
    buttonIsDisabled?: boolean;
    buttonText?: string;
    game: ILobbyGame;
    player: IIsReadyPlayer;
    socket: Socket;
}

export class ReadyButton extends React.Component<IProps> {

    render(): JSX.Element {
        return (
            <WideButton additionalClassNames={classNames(
                styles.ReadyButton,
                styles.StickyButton,
                this.props.player.isReady ? styles.PlayerReady : styles.PlayerNotReady,
                this.props.additionalClasses)}
                        onClick={() => this.setPlayerIsReady()}
                        disabled={this.buttonIsDisabled()}>
                {this.getButtonText()}
            </WideButton>
        );
    }

    private setPlayerIsReady = () => {
        this.props.socket.emit(SetPlayerReadyStateEvent.id, new SetPlayerReadyStateEvent(this.props.game.id, !this.props.player.isReady));
    };

    private getButtonText(): string {
        if (this.props.buttonText) {
            return this.props.buttonText;
        }
        if (this.hasEnoughPlayers()) {
            return getReadyButtonReadyStateText(this.props.player.isReady);
        }
        return 'You need at least 2 players to start!';
    }

    private hasEnoughPlayers(): boolean {
        return this.props.game && this.props.game.players.length >= 2;
    }

    private buttonIsDisabled(): boolean {
        return this.props.buttonIsDisabled ? this.props.buttonIsDisabled : !this.hasEnoughPlayers();
    }
}
