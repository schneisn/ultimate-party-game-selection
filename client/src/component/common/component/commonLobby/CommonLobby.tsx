import React from 'react';
import {Socket} from '../../../../server/socket';
import {ICommonGame} from '../../model/ICommonGame';
import {IConnectionReadyPlayer} from '../../model/IConnectionReadyPlayer';
import {HeaderContainer} from '../headerContainer/HeaderContainer';
import {InfoCard} from '../infoCard/InfoCard';
import {PlayerCard} from '../playerCard/PlayerCard';
import {ReadyButton} from './component/readyButton/ReadyButton';

export interface ICommonLobbyStyles {
    infoCardClasses?: string;
    readyButtonClasses?: string;
    playerContainerClasses?: string;
    playerItemClasses?: string;
}

export interface ILobbyGame extends ICommonGame {
    players: IConnectionReadyPlayer[];
}

interface IProps {
    description?: string;
    buttonIsDisabled?: boolean;
    buttonText?: string;
    game: ILobbyGame;
    /**
     * The id of the player that is game master.
     * This is used to show a crown icon
     */
    gameMasterId?: string;
    /**
     * The current player
     */
    player: IConnectionReadyPlayer;
    socket: Socket;
    styles?: ICommonLobbyStyles;
}

/**
 * A common lobby component.
 *
 * If you want to insert e.g. settings or other elements, pass
 * them as children. This allows the ready button to be shown on
 * the bottom of the page.
 */
export class CommonLobby extends React.Component<IProps> {

    render() {
        const {player, buttonText, buttonIsDisabled, game, styles, socket, description, children} = this.props;
        return (
            <div>
                <InfoCard titleText={'Lobby'}
                          additionalClassNames={styles?.infoCardClasses}>
                    {description}
                </InfoCard>
                <HeaderContainer header={'Players'}
                                 additionalClassNames={styles?.playerContainerClasses}>
                    {this.listPlayers()}
                </HeaderContainer>
                {children}
                <ReadyButton additionalClasses={styles?.readyButtonClasses}
                             buttonIsDisabled={buttonIsDisabled}
                             buttonText={buttonText}
                             game={game}
                             player={player}
                             socket={socket}/>
            </div>
        )
    }

    private listPlayers = () => {
        const {styles, gameMasterId, game} = this.props;
        let players = game.players
            .map((player) => {
                if ((player.id === gameMasterId)) {
                    player.isGameMaster = true;
                }
                return player;
            })
            .map((player) =>
                <PlayerCard key={player.id}
                            additionalClassNames={styles?.playerItemClasses}
                            player={player}
                            showReadyState={true}/>);
        return <div>{players}</div>;
    };
}
