import {ICommonLobbyStyles} from '../CommonLobby';

export function generateCommonLobbyStyles(infoCardClasses: string,
                                          readyButtonClasses: string,
                                          playerContainerClasses: string,
                                          playerItemClasses: string): ICommonLobbyStyles {
    return {
        infoCardClasses,
        readyButtonClasses,
        playerContainerClasses,
        playerItemClasses
    }
}
