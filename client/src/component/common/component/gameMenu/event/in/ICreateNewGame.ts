export interface ICreateNewGame<Game, Player> {
    game: Game;
    player: Player;
}
