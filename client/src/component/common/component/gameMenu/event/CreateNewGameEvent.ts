import {AbstractEvent} from '../../../event/AbstractEvent';
import {INameModel} from '../../../event/model/INameModel';
import {LocalStorage, LocalStorageItem} from '../../../../../store/localStorage';

export class CreateNewGameEvent extends AbstractEvent {
    static readonly id = 'CreateNewGame';
    player: INameModel;

    constructor() {
        super();
        this.player = {name: LocalStorage.getJSONItem<INameModel>(LocalStorageItem.PLAYER)?.name};
    }
}
