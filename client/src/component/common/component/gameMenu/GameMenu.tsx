import React from 'react';
import {RouteComponentProps} from 'react-router-dom';
import {Path} from '../../../../server/serverUrls';
import {SocketProvider} from '../../../../server/socketProvider';
import {LocalStorage, LocalStorageItem} from '../../../../store/localStorage';
import {StoredPlayer} from '../../../../store/model/StoredPlayer';
import {ICommonGame} from '../../model/ICommonGame';
import {ICommonPlayer} from '../../model/ICommonPlayer';
import {StylesUtils} from '../../utils/styles';
import {Error} from '../error/Error';
import {WideButton} from '../wideButton/WideButton';
import {EventLoader} from '../eventLoader/EventLoader';
import {InfoCard} from '../infoCard/InfoCard';
import Navbar from '../navbar/Navbar';
import OpenGames, {IOpenGamesStyles} from './component/openGames/OpenGames';
import {generateOpenGamesStyles} from './component/openGames/utils/styles';
import {CreateNewGameEvent} from './event/CreateNewGameEvent';
import {ICreateNewGame} from './event/in/ICreateNewGame';
import styles from './GameMenu.module.scss';
import {generateGameMenuStyles} from './utils/styles';
import {ConnectionStatus} from '../connectionStatus/ConnectionStatus';
import {IPopoverStyles, Popover} from '../popover/Popover';
import {generatePopoverStyles} from '../popover/utils/styles';
import {GameDescription} from '../gameDescription/GameDescription';
import {GameUtils} from '../../utils/game';
import {UrlUtils} from '../../utils/url';
import {Emoji} from '../emoji/Emoji.';

export interface IGameMenuStyles {
    /**
     * Additional classes for the create new game button
     */
    createNewGameButtonClasses?: string;
    /**
     * Additional classes for the info card
     */
    infoCardClasses?: string;
    /**
     * Additional classes for the open games card
     */
    openGamesStyles?: IOpenGamesStyles;
    /**
     * Styling for the body when the game is mounted
     */
    bodyClasses?: string;
    /**
     * Styles for the game description icon
     */
    gameDescriptionStyles?: IPopoverStyles;
}

interface IState {
    customStyles?: IGameMenuStyles;
    /**
     * When creating a new game, isLoading is
     * true when waiting for a response from the server.
     */
    isLoading: boolean;
    /**
     * Whether the info that the server is idling is visible.
     * Default is false.
     */
    showIdleInfo: boolean;
    /**
     * The timer when the idle info is shown
     */
    showIdleInfoTimer: NodeJS.Timer;
}

interface UrlParams {
    /**
     * The type of game
     */
    game: Path;
}

/**
 * The menu where all open games are shown. Every game uses this component
 */
export class GameMenu extends React.Component<RouteComponentProps, IState> {

    constructor(props: Readonly<RouteComponentProps>) {
        super(props);
        const customStyles = this.getGameMenuStyles();
        this.state = {
            customStyles: customStyles,
            showIdleInfo: false,
            showIdleInfoTimer: null,
            isLoading: false,
        }
    }

    componentDidMount() {
        const {customStyles} = this.state;
        StylesUtils.addBodyClasses(customStyles?.bodyClasses);
        const {game} = this.props.match.params as UrlParams;
        const socket = SocketProvider.getSocket(game)
        // TODO Since this event is not removed when component unmounts,
        // every time the game menu is mounted, the function is registered
        // as listener again. This causes it to be called multiple times when
        // the server responds to the CreateNewGameEvent.
        // Fix e.g. by checking if this function is already added as listener
        socket.on(CreateNewGameEvent.id, this.updateLocalPlayer);
        socket.on(CreateNewGameEvent.id, this.goToGame, false);
    }

    componentWillUnmount() {
        const {customStyles, showIdleInfoTimer} = this.state;
        clearTimeout(showIdleInfoTimer);
        const {game} = this.props.match.params as UrlParams;
        const socket = SocketProvider.getSocket(game)
        socket.removeEventListener(CreateNewGameEvent.id, this.goToGame);
        StylesUtils.removeBodyClasses(customStyles?.bodyClasses);
    }

    render(): JSX.Element {
        const {customStyles, isLoading, showIdleInfo} = this.state;
        const {url} = this.props.match;
        const {game} = this.props.match.params as UrlParams;
        const socket = SocketProvider.getSocket(game)
        return (
            <>
                <Navbar showBackIcon={true}/>
                <main id={'main'} role={'main'}>
                    <div className={styles.GameMenu}>
                        <Error socket={socket}/>
                        <ConnectionStatus socket={socket}/>
                        {!Object.values(Path).includes(game) ?
                            <InfoCard titleText={'404'}>
                                There is not game named {game.toUpperCase()}
                            </InfoCard>
                            : <>
                                <GameDescription gameDescription={GameUtils.getGameDescription(UrlUtils.getPathFromString(url))}
                                                 gameDescriptionStyles={customStyles?.gameDescriptionStyles}/>
                                <InfoCard titleText={game}
                                          additionalClassNames={customStyles?.infoCardClasses}>
                                    Click on a game to join or create your own!
                                </InfoCard>
                                <OpenGames socket={socket}
                                           styles={customStyles?.openGamesStyles}/>
                                <Popover id={'show-idle-info'}
                                         isOpen={showIdleInfo}
                                         customElement={<div/>}>
                                    The server needs a few seconds to wake up <Emoji symbol={'😴'}/>
                                </Popover>
                                <WideButton additionalClassNames={customStyles?.createNewGameButtonClasses}
                                            onClick={this.createGame}
                                            title={'Create a new game'}
                                            disabled={isLoading}>
                                    <EventLoader isLoading={isLoading}>
                                        Create New Game
                                    </EventLoader>
                                </WideButton>
                            </>}
                    </div>
                </main>
            </>
        )
    }

    private createGame = (): void => {
        const {isLoading} = this.state;
        if (isLoading) {
            return;
        }
        const showIdleTimer = setTimeout(() => {
            this.setState({showIdleInfo: true});
        }, 3000);
        this.setState({isLoading: true, showIdleInfoTimer: showIdleTimer});
        const {game} = this.props.match.params as UrlParams;
        const socket = SocketProvider.getSocket(game)
        socket.emit(CreateNewGameEvent.id, new CreateNewGameEvent());
    };

    /**
     * Updates the id of the player in the local storage so that the id of the player is known.
     * This event listener should not be removed when the component unmounts, otherwise
     * when the player sends the create game request, leaves the game menu and before the
     * server responds leaves the game menu, the client does not know their new player id.
     *
     * This will indicate a memory leak in the console.
     */
    private updateLocalPlayer = (data: ICreateNewGame<ICommonGame, ICommonPlayer>): void => {
        const oldPlayer = LocalStorage.getJSONItem<StoredPlayer>(LocalStorageItem.PLAYER);
        LocalStorage.setJSONItem<StoredPlayer>(LocalStorageItem.PLAYER, new StoredPlayer(data.player.id, oldPlayer.name));
    }

    private goToGame = (data: ICreateNewGame<ICommonGame, ICommonPlayer>): void => {
        const {game} = this.props.match.params as UrlParams;
        UrlUtils.goTo(this.props.history, `/${game}/${data.game.id}`);
    }

    private getGameMenuStyles(): IGameMenuStyles {
        const {game} = this.props.match.params as UrlParams;
        switch (game) {
            case Path.CardsAgainst:
                return generateGameMenuStyles(
                    styles.CAPrimary,
                    generateOpenGamesStyles(styles.CASecondary, styles.CATertiary),
                    styles.CATertiary,
                    styles.CABackground,
                    generatePopoverStyles(styles.CATertiaryIcon, styles.CATertiary));
            case Path.Categories:
                return generateGameMenuStyles(styles.CPrimary,
                    generateOpenGamesStyles(styles.CSecondary, styles.CTertiary),
                    styles.CTertiary,
                    styles.CBackground,
                    generatePopoverStyles(styles.CTertiaryIcon, styles.CTertiary));
            case Path.Privacy:
                return generateGameMenuStyles(styles.PPrimary,
                    generateOpenGamesStyles(styles.PSecondary, styles.PTertiary),
                    styles.PTertiary,
                    styles.PBackground,
                    generatePopoverStyles(styles.PTertiaryIcon, styles.PTertiary));
            case Path.Spyfall:
                return generateGameMenuStyles(styles.SFPrimary,
                    generateOpenGamesStyles(styles.SFSecondary, styles.SFTertiary),
                    styles.SFTertiary,
                    styles.SFBackground,
                    generatePopoverStyles(styles.SFTertiaryIcon, styles.SFTertiary));
            case Path.Werewolf:
                return generateGameMenuStyles(styles.WWPrimary,
                    generateOpenGamesStyles(styles.WWSecondary, styles.WWTertiary),
                    styles.WWTertiary,
                    styles.WWBackground,
                    generatePopoverStyles(styles.WWTertiaryIcon, styles.WWTertiary));
            case Path.WhoAmI:
                return generateGameMenuStyles(styles.WAIPrimary,
                    generateOpenGamesStyles(styles.WAISecondary, styles.WAITertiary),
                    styles.WAITertiary,
                    styles.WAIBackground,
                    generatePopoverStyles(styles.WAITertiaryIcon, styles.WAITertiary));
            default:
                return null;
        }
    }

}

export default GameMenu;
