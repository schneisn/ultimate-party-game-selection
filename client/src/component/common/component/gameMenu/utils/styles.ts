import {IOpenGamesStyles} from '../component/openGames/OpenGames';
import {IGameMenuStyles} from '../GameMenu';
import {IPopoverStyles} from '../../popover/Popover';

export function generateGameMenuStyles(
    infoCardClasses: string,
    openGamesStyles: IOpenGamesStyles,
    createNewGameButtonClasses: string,
    bodyClasses?: string,
    gameDescriptionStyles?: IPopoverStyles): IGameMenuStyles {

    return {
        infoCardClasses,
        openGamesStyles,
        createNewGameButtonClasses,
        bodyClasses,
        gameDescriptionStyles
    }
}
