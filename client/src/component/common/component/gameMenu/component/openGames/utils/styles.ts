import {IOpenGamesStyles} from '../OpenGames';

export function generateOpenGamesStyles(gamesContainerClasses: string, gameCardClasses: string): IOpenGamesStyles {
    return {
        gamesContainerClasses,
        gameCardClasses
    }
}
