import {AbstractEvent} from '../../../../../event/AbstractEvent';

export class GetAllOpenGamesEvent extends AbstractEvent {
    static readonly id = 'GetAllOpenGames';
}
