import classNames from 'classnames';
import React from 'react';
import {RouteComponentProps, withRouter} from 'react-router-dom';
import {Path} from '../../../../../../server/serverUrls';
import {Socket} from '../../../../../../server/socket';
import {ConnectEvent} from '../../../../event/ConnectEvent';
import {ButtonList, IButtonListItem} from '../../../buttonList/ButtonList';
import {generateButtonListStyles} from '../../../buttonList/utils/styles';
import {GetAllOpenGamesEvent} from './event/GetAllOpenGamesEvent';
import {IGame} from './event/in/IGame';
import {IGetAllOpenGames} from './event/in/IGetAllOpenGames';
import styles from './OpenGames.module.scss';
import {LeaveRoomEvent} from '../../../../event/LeaveRoomEvent';
import {JoinRoomEvent} from '../../../../event/JoinRoomEvent';

export interface IOpenGamesStyles {
    gamesContainerClasses?: string;
    gameCardClasses?: string;
}

interface IProps extends RouteComponentProps {
    socket: Socket;
    styles?: IOpenGamesStyles;
}

interface IState {
    games: IGame[];
    /**
     * The timer when the component begins listening to the connect event.
     * This prevents the GetAllOpenGamesEvent to be sent twice.
     */
    connectEventTimer: NodeJS.Timeout;
}

interface UrlParams {
    game: Path;
}

/**
 * A list with open games
 */
class OpenGames extends React.Component<IProps, IState> {

    constructor(props: IProps) {
        super(props);
        this.state = {
            games: [],
            connectEventTimer: setTimeout(() => this.props.socket.on(ConnectEvent.id, this.getAllOpenGames), 1000)
        }
    }

    componentDidMount() {
        const {socket} = this.props;
        socket.emit(JoinRoomEvent.id, new JoinRoomEvent('OpenGames'));
        socket.on(GetAllOpenGamesEvent.id, (data: IGetAllOpenGames) => this.setState({games: data.games}));
        this.getAllOpenGames();
    }

    componentWillUnmount() {
        const {socket} = this.props;
        socket.emit(LeaveRoomEvent.id, new LeaveRoomEvent('OpenGames'));
        socket.removeEventListener(GetAllOpenGamesEvent.id);
        socket.removeEventListener(ConnectEvent.id, this.getAllOpenGames);
        clearTimeout(this.state.connectEventTimer);
    }

    render() {
        return <ButtonList listHeader={'Open Games'}
                           items={this.listOpenGames()}
                           emptyItemsContent={'No open games'}
                           styles={generateButtonListStyles(classNames(styles.Header, this.props.styles?.gamesContainerClasses), this.props.styles?.gameCardClasses)}/>
    }

    private getAllOpenGames = (): void => {
        this.props.socket.emit(GetAllOpenGamesEvent.id, new GetAllOpenGamesEvent());
    }

    private listOpenGames(): IButtonListItem[] {
        return this.state.games?.map((game) => {
            const content = <div className={styles.OpenGame} title={'Join game'}>
                <div className={styles.EllipseText}>
                    {game.gamemaster?.name || game.players[0]?.name}
                </div>
                <div className={styles.sGame}>
                    's Game
                </div>
            </div>;

            return {
                content: content,
                onClick: () => this.joinGame(game)
            }
        });
    };

    private joinGame = (openGame: IGame): void => {
        const {game} = this.props.match.params as UrlParams;
        this.props.history.push(`${game}/${openGame.id}`)
    };
}

export default withRouter(OpenGames);
