import {IGame} from './IGame';

export interface IGetAllOpenGames {
    games: IGame[];
}
