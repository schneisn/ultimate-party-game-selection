import {ICommonPlayer} from '../../../../../../model/ICommonPlayer';

export interface IGame {
    id: string;
    gamemaster: ICommonPlayer;
    players: ICommonPlayer[];
}
