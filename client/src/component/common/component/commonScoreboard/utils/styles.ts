import {ICommonScoreboardStyles} from '../CommonScoreboard';

export function generateCommonScoreboardStyles(titleCardClasses?: string,
                                               listClasses?: string,
                                               listItemClasses?: string,
                                               readyButtonClasses?: string): ICommonScoreboardStyles {
    return {
        titleCardClasses,
        listClasses,
        listItemClasses,
        readyButtonClasses
    }
}
