import React from 'react';
import {Socket} from '../../../../server/socket';
import {IConnectionReadyPlayer} from '../../model/IConnectionReadyPlayer';
import {ILobbyGame} from '../commonLobby/CommonLobby';
import {ReadyButton} from '../commonLobby/component/readyButton/ReadyButton';
import {HeaderContainer} from '../headerContainer/HeaderContainer';
import {InfoCard} from '../infoCard/InfoCard';
import {PlayerInfo} from '../playerInfo/PlayerInfo';
import {SmallCard} from '../smallCard/SmallCard';
import styles from './CommonScoreboard.module.scss';

type TotalScore = string | number;
type RoundScore = string | number | JSX.Element;

export interface ICommonScoreboardStyles {
    titleCardClasses?: string;
    listClasses?: string;
    listItemClasses?: string;
    readyButtonClasses?: string;
}

export interface IScore {
    player: IConnectionReadyPlayer;
    totalScore: TotalScore;
    roundScore: RoundScore;
}

interface IProps {
    /**
     * Additional content to display between top card
     * and list of scores
     */
    additionalContent?: JSX.Element;
    game: ILobbyGame;
    /**
     * The title of the scoreboard.
     * Default is 'Scoreboard'
     */
    title?: string;
    /**
     * The description that is shown
     * under the title
     */
    description?: string | JSX.Element;
    /**
     * The scores to display
     */
    scores: IScore[];
    /**
     * A custom header for the round column
     */
    customHeaderRound?: string | JSX.Element;
    /**
     * A custom header for the total column
     */
    customHeaderTotal?: string | JSX.Element;
    player: IConnectionReadyPlayer;
    styles?: ICommonScoreboardStyles;
    socket: Socket;
    /**
     * Whether to show if the player is ready.
     * Default is false
     */
    showIsReady?: boolean;
    /**
     * Whether to show a round score.
     * Default is true
     */
    showRoundScore?: boolean;
    readyButtonText?: string;
    /**
     * Whether the is ready button is shown.
     * Default is true
     */
    showButton?: boolean;
}

/**
 * A scoreboard with three columns (player, round score, total score) to display the score.
 */
export class CommonScoreboard extends React.Component<IProps> {

    private static getRowContent(left: JSX.Element, rightLeft: string | JSX.Element, rightRight: string | JSX.Element): JSX.Element {
        return <div className={styles.ScoresCardContent}>
            <div className={styles.ScoresCardContentLeft}>{left}</div>
            <div className={styles.ScoresCardContentRight}>
                <div>{rightLeft}</div>
                <div className={styles.ScoresCardContentRightRight}>{rightRight}</div>
            </div>
        </div>
    }

    render() {
        return (
            <>
                <InfoCard titleText={this.props.title || 'Scoreboard'}
                          additionalClassNames={this.props.styles?.titleCardClasses}>
                    {this.props.description || 'Click on ready to go to start the next round'}
                </InfoCard>
                {this.props.additionalContent}
                <HeaderContainer additionalClassNames={this.props.styles?.listClasses}>
                    {this.getScoresHeader()}
                    {this.getPlayers()}
                </HeaderContainer>
                {this.props.showButton !== false &&
                <ReadyButton additionalClasses={this.props.styles?.readyButtonClasses}
                             game={this.props.game}
                             player={this.props.player}
                             socket={this.props.socket}
                             buttonText={this.props.readyButtonText}/>}
            </>
        );
    }

    private getScoresHeader(): JSX.Element {
        const defaultPlayerHeader = <b>Player</b>;
        const defaultRoundsHeader = <b>Round</b>;
        const roundHeader = this.props.showRoundScore !== false && (this.props.customHeaderRound || defaultRoundsHeader);
        const defaultTotalHeader = <b>Total</b>;
        const totalHeader = this.props.customHeaderTotal || defaultTotalHeader;
        return CommonScoreboard.getRowContent(defaultPlayerHeader, roundHeader, totalHeader);
    }

    private getPlayers(): JSX.Element[] {
        return this.getPlayerSortedByTotalScore().map((player: IConnectionReadyPlayer) => {
            return <SmallCard key={player.id} additionalClassNames={this.props.styles?.listItemClasses}>
                {CommonScoreboard.getRowContent(
                    this.getPlayerNameAndReadyState(player),
                    <>{this.getPlayerRoundScore(player.id)}</>,
                    <>{this.getPlayerTotalScore(player.id)}</>)}
            </SmallCard>
        });
    }

    private getPlayerSortedByTotalScore(): IConnectionReadyPlayer[] {
        return this.props.scores.map(score => score.player).sort((p1: IConnectionReadyPlayer, p2: IConnectionReadyPlayer) => {
            return this.getPlayerTotalScore(p1.id) > this.getPlayerTotalScore(p2.id) ? -1 : 1;
        })
    }

    private getPlayerTotalScore(playerId: string): TotalScore {
        return this.props.scores.find(score => score.player.id === playerId)?.totalScore;
    }

    private getPlayerRoundScore(playerId: string): RoundScore {
        return this.props.showRoundScore !== false && this.props.scores.find(score => score.player.id === playerId)?.roundScore;
    }

    private getPlayerNameAndReadyState(player: IConnectionReadyPlayer): JSX.Element {
        return <PlayerInfo additionalClassnames={styles.PlayerInfo}
                           additionalNameClassnames={styles.PlayerName}
                           player={player}
                           showReadyState={this.props.showIsReady}/>
    }
}
