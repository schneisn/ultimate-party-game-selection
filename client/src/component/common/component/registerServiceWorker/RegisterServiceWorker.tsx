import React from 'react';
import {Alert, CardLink} from 'reactstrap';
import {Config} from '../../../../config/config';
import styles from './RegisterServiceWorker.module.scss';

interface IState {
    /**
     * Whether there is a new version of the app available
     */
    newVersionAvailable: boolean;
    /**
     * The waiting service worker
     */
    waitingWorker: ServiceWorker;
}

// In production, we register a service worker to serve assets from local cache.

// This lets the app load faster on subsequent visits in production, and gives
// it offline capabilities. However, it also means that developers (and users)
// will only see deployed updates on the "N+1" visit to a page, since previously
// cached resources are updated in the background.

// To learn more about the benefits of this model, read https://goo.gl/KwvDNy.
// This link also includes instructions on opting out of this behavior.
export class RegisterServiceWorker extends React.Component<{}, IState> {

    constructor(props: {}) {
        super(props);
        this.state = {
            newVersionAvailable: false,
            waitingWorker: null
        }
    }

    componentDidMount() {
        this.register();
    }

    render() {
        return (
            <div>
                {this.state.newVersionAvailable &&
                <Alert className={styles.RegisterServiceWorker}>
                  New version available. Click <CardLink className={styles.UpdateLink}
                                                         onClick={this.updateServiceWorker}>
                  here
                </CardLink> to update
                </Alert>}
            </div>
        );
    }

    private register(): void {
        if (Config.isDev() || !('serviceWorker' in navigator)) {
            console.log('Dev mode - Do not register service worker');
            return;
        }
        console.log('Registering service worker...');
        // The URL constructor is available in all browsers that support SW.
        const publicUrl = new URL(import.meta.env.PUBLIC_URL, window.location.toString());
        if (publicUrl.origin !== window.location.origin) {
            // Our service worker won't work if PUBLIC_URL is on a different origin
            // from what our page is served on. This might happen if a CDN is used to
            // serve assets; see https://github.com/facebookincubator/create-react-app/issues/2374
            return;
        }

        if (Config.isLocalHost()) {
            // This is running on localhost. Lets check if a service worker still exists or not.
            this.checkValidServiceWorker();

            // Add some additional logging to localhost, pointing developers to the
            // service worker/PWA documentation.
            navigator.serviceWorker.ready.then(() => {
                console.log(
                    'This web app is being served cache-first by a service ' +
                    'worker. To learn more, visit https://goo.gl/SC7cgQ'
                );
            });
        } else {
            // Is not local host. Just register service worker
            this.registerValidSW();
        }
    }


    private checkValidServiceWorker(): void {
        const swUrl = `${Config.getServerOrigin()}/service-worker.js`;
        console.log(`Checking if service worker at ${swUrl} is valid...`);
        // Check if the service worker can be found. If it can't reload the page.
        fetch(swUrl)
            .then(response => {
                // Ensure service worker exists, and that we really are getting a JS file.
                if (
                    response.status === 404 ||
                    response.headers.get('content-type').indexOf('javascript') === -1
                ) {
                    console.log(`No service worker found at ${swUrl}`);
                    // No service worker found. Probably a different app. Reload the page.
                    navigator.serviceWorker.ready.then(registration => {
                        registration.unregister().then(() => {
                            window.location.reload();
                        });
                    });
                } else {
                    // Service worker found. Proceed as normal.
                    this.registerValidSW();
                }
            })
            .catch(() => {
                console.log(
                    'No internet connection found. App is running in offline mode.'
                );
            });
    }

    private registerValidSW(): void {
        const swUrl = `${Config.getServerOrigin()}/service-worker.js`;
        navigator.serviceWorker
            .register(swUrl)
            .then(registration => {
                console.log('Service worker successfully registered');
                registration.onupdatefound = () => {
                    const installingWorker = registration.installing;
                    installingWorker.onstatechange = () => {
                        if (installingWorker.state === 'installed') {
                            if (navigator.serviceWorker.controller) {
                                // At this point, the old content will have been purged and
                                // the fresh content will have been added to the cache.
                                // It's the perfect time to display a "New content is
                                // available; please refresh." message in your web app.
                                console.log('New content is available; please refresh.');
                                this.onServiceWorkerUpdate(registration);
                            } else {
                                // At this point, everything has been precached.
                                // It's the perfect time to display a
                                // "Content is cached for offline use." message.
                                console.log('Content is cached for offline use.');
                            }
                        }
                    };
                };
            })
            .catch(error => {
                console.error('Error during service worker registration:', error);
            });
    }

    private unregister() {
        if ('serviceWorker' in navigator) {
            navigator.serviceWorker.ready.then(registration => {
                registration.unregister();
            });
        }
    }

    private onServiceWorkerUpdate = (registration: ServiceWorkerRegistration) => {
        this.setState({
            waitingWorker: registration && registration.waiting,
            newVersionAvailable: true
        })
    }

    private updateServiceWorker = () => {
        const {waitingWorker} = this.state;
        waitingWorker?.postMessage({type: 'SKIP_WAITING'});
        this.setState({newVersionAvailable: false});
        window.location.reload();
    }
}
