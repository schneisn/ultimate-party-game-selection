import classNames from 'classnames';
import React from 'react';
import {Card} from '../card/Card';
import styles from './SmallCard.module.scss';

interface IProps {
    additionalClassNames?: string;
}

/**
 * A card with small margin
 */
export class SmallCard extends React.Component<IProps> {
    render() {
        return (
            <Card additionalClassNames={classNames(styles.SmallCard, this.props.additionalClassNames)}>
                {this.props.children}
            </Card>
        );
    }
}
