import React, {ChangeEvent} from 'react';
import {Input, InputGroup} from 'reactstrap';
import {Button} from '../button/Button';
import styles from './NumberInput.module.scss';

interface IProps {
    prependDescription?: string;
    appendDescription?: string;
    /**
     * Whether the input is disabled.
     * Default is false.
     */
    disabled?: boolean;
    /**
     * The maximum value of the input.
     * Default is 100.
     */
    max?: number;
    /**
     * The minimum value of the input.
     * Default is 0.
     */
    min?: number;
    /**
     * This function is called when the input field loses focus
     * @param nr the value of the input field on focus loss
     */
    onBlur?: (nr: number) => void;
    /**
     * The value to show in the input field.
     * Default is 0
     */
    value?: number
}

interface IState {
    value: number;
}

/**
 * A number input
 */
export class NumberInput extends React.Component<IProps, IState> {

    constructor(props: Readonly<IProps>) {
        super(props);
        this.state = {
            value: this.props.value
        }
    }

    componentDidUpdate(prevProps: Readonly<IProps>, prevState: Readonly<IState>) {
        if (prevProps === this.props) {
            return;
        }
        this.setState({value: this.props.value});
    }

    render() {
        const {min, appendDescription, max, prependDescription, disabled} = this.props;
        const {value} = this.state;
        return (
            <div>
                <InputGroup>
                        <Button additionalClassNames={styles.IncDecButton}
                                disabled={disabled}
                                onClick={this.onDecrement}>
                            -
                        </Button>
                    {prependDescription &&
                        <div>{prependDescription}</div>
                    }
                    <Input placeholder={''}
                           value={value}
                           className={styles.Input}
                           disabled={disabled}
                           onBlur={() => this.onInputBlur(value)}
                           onChange={(event: ChangeEvent<HTMLInputElement>) => this.onChange(event)}
                           min={min ? min : 0}
                           max={max ? max : 100}
                           type={'number'}
                           step={1}/>
                    {appendDescription &&
                        <div>{appendDescription}</div>
                    }
                    <Button additionalClassNames={styles.IncDecButton}
                            disabled={disabled}
                            onClick={this.onIncrement}>
                        +
                    </Button>
                </InputGroup>
            </div>
        );
    }

    private onIncrement = (): void => {
        const {value, max} = this.props;
        const newMax = max ? Math.min(value + 1, max) : value + 1;
        this.onInputBlur(newMax);
    }

    private onDecrement = (): void => {
        const {value, min} = this.props;
        const newMin = min ? Math.max(value - 1, min) : value - 1;
        this.onInputBlur(newMin);
    }

    private onInputBlur = (newNr: number): void => {
        const {onBlur} = this.props;
        onBlur && onBlur(newNr);
    }

    private onChange = (event: ChangeEvent<HTMLInputElement>): void => {
        const newValue = Number(event.currentTarget.value);
        this.setState({value: newValue});
    }
}
