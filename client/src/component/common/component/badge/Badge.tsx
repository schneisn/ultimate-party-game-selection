import React from 'react';
import {Badge as ReactstrapBadge} from 'reactstrap';

interface IProps {
    additionalClassNames?: string
}

/**
 * A badge. Pass the content as children.
 * See https://reactstrap.github.io/components/badge/
 */
export class Badge extends React.Component<IProps> {

    render() {
        const {additionalClassNames, children} = this.props;

        return (
            <ReactstrapBadge className={additionalClassNames}>
                {children}
            </ReactstrapBadge>
        );
    }
}
