import React from 'react';

interface IProps {
    /**
     * The timeout to hide the children in ms.
     * Default is 3000
     */
    timeout?: number;
    /**
     * The components to hide after the timeout
     */
    children: JSX.Element;
    /**
     * Is called when the content is hidden
     */
    onExpire?: () => void;
}

interface IState {
    timer: NodeJS.Timer;
    visible: boolean;
}

/**
 * A wrapper to hide its children after a certain time
 */
export class Expire extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            timer: null,
            visible: true
        }
    }

    componentWillReceiveProps(nextProps: Readonly<IProps>) {
        // reset the timer if children are changed
        if (nextProps.children !== this.props.children) {
            this.setState({timer: this.setTimer(), visible: true});
        }
    }

    componentDidMount() {
        this.setState({
            timer: this.setTimer()
        });
    }

    componentWillUnmount() {
        clearTimeout(this.state.timer);
    }

    setTimer(): NodeJS.Timer {
        if (this.state.timer != null) {
            clearTimeout(this.state.timer)
        }
        return setTimeout(() => {
            // State has to be set before onExpire is called because
            // otherwise setState might be called on the unmounted expire component
            this.setState({timer: null, visible: false});
            this.props.onExpire && this.props.onExpire();
        }, this.props.timeout ? this.props.timeout : 3000);
    }

    render() {
        return this.state.visible && <div>{this.props.children}</div>;
    }
}
