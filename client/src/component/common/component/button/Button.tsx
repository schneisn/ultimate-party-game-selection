import classNames from 'classnames';
import React from 'react';
import {Button as ReactButton} from 'reactstrap';
import styles from './Button.module.scss';

interface IProps {
    additionalClassNames?: string;
    /**
     * Whether the button is disabled or not
     */
    disabled?: boolean;
    id?: string;
    /**
     * Whether the button is hidden.
     * Default is false.
     */
    hidden?: boolean;
    onClick?: (event: React.MouseEvent<any, MouseEvent>) => void;
    /**
     * Whether to prevent default event on click.
     * Default is false.
     */
    preventDefaultOnClick?: boolean;
    /**
     * Text shown on hover
     */
    title?: string;
}

/**
 * This button wraps the reactstrap button
 */
export class Button extends React.Component<IProps> {

    render(): JSX.Element {
        return (
            <ReactButton id={this.props.id}
                         className={classNames(
                             styles.Button,
                             this.props.additionalClassNames
                         )}
                         disabled={this.props.disabled}
                         onClick={this.onClick}
                         hidden={this.props.hidden}
                         title={this.props.title}
                         autoFocus={false}>
                {this.props.children}
            </ReactButton>
        )
    }

    private onClick = (event: React.MouseEvent<HTMLInputElement, MouseEvent>): void => {
        this.props.preventDefaultOnClick === true && event.preventDefault();
        if (this.props.disabled) {
            return;
        }
        this.props.onClick && this.props.onClick(event);
    }
}
