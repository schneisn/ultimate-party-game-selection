import React, {ChangeEvent} from 'react';
import {StringUtils} from '../../utils/string';
import {WideButton} from '../wideButton/WideButton';
import {Form} from '../form/Form';
import {TextArea} from '../textArea/TextArea';
import classNames from 'classnames';
import styles from './TextAreaWithButton.module.scss';

interface IProps {
    additionalButtonClasses?: string;
    additionalTextAreaClasses?: string;
    autoFocus?: boolean;
    buttonIsDisabled?: boolean;
    buttonText?: string;
    buttonTitle?: string;
    /**
     * Whether to clear the text field after submitting.
     * Default is true.
     */
    clearAfterSubmit?: boolean;
    hidden?: boolean;
    textPlaceholder?: string;
    /**
     * A list of invalid input strings.
     * When one of these strings is in the input field,
     * the button is disabled.
     */
    invalidInput?: string[];
    /**
     * Whether the text area is disabled.
     * Default is false.
     */
    textAreaIsDisabled?: boolean;
    textValue?: string;
    onSubmit?: (textValue: string) => void;
    onChange?: (event: ChangeEvent<HTMLInputElement>) => void;
    textMaxLength?: number;
    required?: boolean;
}

interface IState {
    textValue: string;
}

export class TextAreaWithButton extends React.Component<IProps, IState> {

    private textArea = React.createRef<HTMLInputElement>();

    constructor(props: Readonly<IProps>) {
        super(props);
        this.state = {
            textValue: this.props.textValue || ''
        }
    }

    componentDidUpdate(prevProps: Readonly<IProps>, prevState: Readonly<IState>) {
        if (prevProps.textValue !== this.props.textValue) {
            this.setState({
                textValue: this.props.textValue
            })
        }
    }

    render() {
        const {
            textPlaceholder,
            buttonTitle,
            additionalButtonClasses,
            buttonText,
            required,
            autoFocus,
            textMaxLength,
            hidden,
            textAreaIsDisabled,
            additionalTextAreaClasses
        } = this.props;
        return (
            <Form onSubmit={this.onSubmit} hidden={hidden}>
                <TextArea additionalClasses={additionalTextAreaClasses}
                          inputRef={this.textArea}
                          placeholder={textPlaceholder}
                          value={this.state.textValue}
                          onChange={this.onTextChange}
                          autoFocus={autoFocus}
                          disabled={textAreaIsDisabled}
                          required={required}
                          maxLength={textMaxLength}/>
                <WideButton additionalClassNames={classNames(styles.Button, additionalButtonClasses)}
                            disabled={this.isButtonDisabled()}
                            title={buttonTitle}
                            onClick={this.onSubmit}>
                    {buttonText}
                </WideButton>
            </Form>
        );
    }

    private onTextChange = (event: ChangeEvent<HTMLInputElement>): void => {
        this.setState({textValue: event.currentTarget.value});
        this.props.onChange && this.props.onChange(event);
    }

    private isButtonDisabled = (): boolean => {
        return this.props.buttonIsDisabled ||
            StringUtils.equals(this.state.textValue, '') ||
            this.props.invalidInput?.some(invalidInput => StringUtils.equals(this.state.textValue, invalidInput));
    }

    private onSubmit = (): void => {
        const resetText = (this.props.clearAfterSubmit !== false) && !this.isButtonDisabled();
        this.props.onSubmit && this.props.onSubmit(this.state.textValue);
        resetText && this.setState({textValue: ''});
        this.textArea.current.focus();
    }
}
