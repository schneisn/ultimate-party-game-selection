import React from 'react';
import classNames from 'classnames';
import styles from './Image.module.scss';

interface IProps {
    additionalClassNames?: string;
    alt?: string;
    src?: string;
}

export class Image extends React.Component<IProps> {

    render() {
        return (
            <img className={classNames(styles.Image, this.props.additionalClassNames)}
                 alt={this.props.alt}
                 src={this.props.src}>
            </img>
        );
    }
}
