export class StylesUtils {

    /**
     * Adds the given classes to the body of the document
     * @param bodyClasses
     */
    static addBodyClasses(bodyClasses: string): void {
        document.body.classList.add(bodyClasses);
    }

    /**
     * Removes the given classes from the body of the document
     * @param bodyClasses
     */
    static removeBodyClasses(bodyClasses: string): void {
        document.body.classList.remove(bodyClasses);
    }

}
