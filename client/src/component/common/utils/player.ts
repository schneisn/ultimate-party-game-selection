import {ICommonGame} from '../model/ICommonGame';
import {ICommonPlayer} from '../model/ICommonPlayer';
import {IConnectionPlayer} from '../model/IConnectionPlayer';

/**
 * Gets the player with the given id from the game
 * @param game
 * @param playerId
 */
export function getPlayer<P extends ICommonPlayer>(game: ICommonGame<P>, playerId: string): P {
    if (!game) {
        return null;
    }
    return game.players.find(p => p.id === playerId);
}

/**
 * Gets the game master of the game.
 * The game master is the first connected player in the game.
 * @param game
 */
export function getGameMaster<P extends IConnectionPlayer>(game: ICommonGame<P>): P {
    return game.players.find(p => p.isConnected);
}

/**
 * Checks whether the player with the given id is the game master.
 * The game master is the first connected player in the game
 * @param game
 * @param playerId
 */
export function playerIsGameMaster(game: ICommonGame<IConnectionPlayer>, playerId: string): boolean {
    return getGameMaster(game)?.id === playerId;
}
