export class StringUtils {

    /**
     * Makes the first letter of the given string upper case
     * @param string
     */
    static toFirstUpperCase(string: string): string {
        return string?.charAt(0).toUpperCase() + string?.slice(1);
    }

    /**
     * Removes all blanks from the string
     * @param string The edited string
     */
    static removeBlanks(string: string): string {
        return string?.replace(/\s/g, '');
    }

    /**
     * Checks whether the string does not exist or does not contain any content
     * @param string
     */
    static isEmpty(string: string): boolean {
        return !string || string?.length < 1 || this.removeBlanks(string) === '';
    }

    /**
     * Trims the string at the given index and appends '...'.
     * Only appends '...' when the string is longer than the index
     * @param string
     * @param trimIndex
     */
    static trimWithDots(string: string, trimIndex: number): string {
        return string?.length > trimIndex ? string?.slice(0, trimIndex) + '...' : string;
    }

    /**
     * Removes all non-alphanumeric characters from the given string
     * @param string
     */
    static removeSpecialCharacters(string: string): string {
        return string?.replace(/[^\w\s]/gi, '');
    }

    /**
     * Creates a valid id from the string.
     * Removes special characters and blanks.
     * Trims the string to 10 characters
     * @param string
     */
    static stringToId(string: string): string {
        return StringUtils.removeSpecialCharacters(StringUtils.removeBlanks(string)).slice(10);
    }

    /**
     * Compares string 1 and string 2 without blanks and lowercase
     * @param string1
     * @param string2
     */
    static equals(string1: string, string2: string): boolean {
        string1 = string1?.trim().toLowerCase();
        string2 = string2?.trim().toLowerCase();
        return string1 === string2;
    }
}
