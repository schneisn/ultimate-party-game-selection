import {Path} from '../../../server/serverUrls';

export class GameUtils {

    /**
     * Returns the respective description of the given game
     * @param game
     */
    static getGameDescription(game: Path): string {
        switch (game) {
            case Path.CardsAgainst:
                return 'Cards Against Everything is a variation of Cards Against Humanity. Cards Against Humanity is a party game for horrible people. Unlike most of the party games you\'ve played before, Cards Against Humanity is as despicable and awkward as you and your friends. The game is simple. Each round, one player asks a question from a black card, and everyone else answers with their funniest white card.';
            case Path.Categories:
                return 'Categories are what inspired Scattergories, the board game where players need to name objects within certain categories starting with a particular letter of the alphabet. The goal of the game is to score the most points by choosing legitimate answers that other players don\'t.';
            case Path.Privacy:
                return 'Privacy is the adult party game in which players finally get answers to the provocative questions they were afraid to ask. Players anonymously answer outrageous and depraved questions by securing “Yes” or “No” votes, then predict how many total “Yes” answers were submitted. Points are awarded to the players who guess correctly. Tell the truth or plead the fifth while using your deduction skills to learn what you’ve always wanted to know about your friends. Warning: Not intended for the shameless.';
            case Path.Spyfall:
                return 'At the start of the game, all players receive cards showing the same location, except one player - the "Agent" - that doesn\'t know the location. Players then start asking each other questions, trying to guess who among them is the agent. The agent has to find out where they are, so he has to listen carefully. When it\'s his time to answer, he\'d better create a good story! At any time during a round, one player may accuse another of being a agent. If all other players agree with the accusation, the round ends and the accused player has to reveal his identity. If the agent is uncovered, all other players score points. However, the agent can himself end a round by announcing that he understands what the secret location is.';
            case Path.Werewolf:
                return 'One minute you\'re a Villager, defending your theoretical home with every fiber of your being. The next, you\'re a Werewolf, framing your friends and accusing them of wanting to destroy your village, when really it\'s you who\'s been infiltrating it all along. The villagers win if they kill all werewolves. The werewolves win if they kill enough villagers so that the numbers are even.';
            case Path.WhoAmI:
                return 'Who am I is a game where you have to guess your identity. The other players decide who or what you are';
            default:
                return '';
        }
    }
}
