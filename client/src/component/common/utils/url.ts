import {History} from 'history';
import {Path} from '../../../server/serverUrls';
import {Logger} from '../../../logger/logger';

/**
 * UrlUtils handles everything concerning the history of the page
 */
export class UrlUtils {

    static getGameType(): string {
        return `/${window.location.pathname.split('/')[1]}`;
    }

    /**
     * Gets the type of game from the path and converts it to a Path enum
     * @param path
     */
    static getPathFromString(path: string): Path {
        const gamePath = path.split('/')[1];
        return gamePath as Path;
    }

    /**
     * Go to the given path.
     * If the current pathname is already the path, do nothing.
     * @param history
     * @param path
     */
    static goTo(history: History<unknown>, path: string): void {
        if (!path) {
            Logger.error('Path is undefined');
            return;
        }
        if (window.location.pathname === path) {
            return;
        }
        Logger.log(`Go to ${path}`);
        history.push(path);
    }

    /**
     * Go one step up in the url hierarchy.
     * E.g. go from '/privacy' to '/'.
     * @param history
     */
    static goHierarchyUp(history: History<unknown>): void {
        const newPathName = history.location.pathname.split('/').slice(0, -1);
        history.push(newPathName.join('/'));
    }

    /**
     * Go back in history. If the last location is empty or not on the page,
     * go to '/'
     * @param history
     */
    static goBackOnPage(history: History<unknown>): void {
        // Check for first page load by empty key. See https://github.com/ReactTraining/history/issues/582
        if (history.location.key === undefined || history.location.key === '') {
            history.push('/');
        } else {
            history.goBack();
        }
    }
}
