import moment from 'moment';

/**
 * DateUtils handles everything concerning time and date
 */
export class DateUtils {

    /**
     * Gets the time dif in milliseconds
     * @param date1 Time in ISO_8601
     * @param date2 Time in ISO_8601
     * @return Null if date1 or date2 is null
     */
    static getTimeDifInSec(date1: Date, date2: Date): number {
        if (!date1 || !date2) {
            return null;
        }
        return moment(date1, moment.ISO_8601).diff(moment(date2, moment.ISO_8601), 'seconds');
    }

    /**
     * Get dif between now and the time
     * @param date Time in ISO_8601
     * @return Null if date is null
     */
    static getTimeDifFromNowInSec(date: Date): number {
        if (!date) {
            return null;
        }
        return moment(date, moment.ISO_8601).diff(new Date(), 'seconds');
    }

    /**
     * Returns whether the time is in the future.
     * @param date Time in ISO_8601
     * @return False when the date is null
     */
    static isInFuture(date: Date): boolean {
        if (!date) {
            return false;
        }
        return moment(date, moment.ISO_8601).isAfter(new Date());
    }

}
