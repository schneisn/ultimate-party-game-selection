export class IdUtils {

    /**
     * Creates a unique number as string of length 10
     */
    static createUuid(): string {
        return Math.floor(Math.random() * Math.pow(10, 10)).toString();
    }
}
