import React from 'react';
import {RouteComponentProps} from 'react-router-dom';
import {Urls} from '../../server/serverUrls';
import {BaseGame} from '../common/component/baseGame/BaseGame';
import {generateConnectToastStyles} from '../common/component/baseGame/component/connectToast/utils/styles';
import {generateBaseGameStyles} from '../common/component/baseGame/utils/styles';
import {generateModalStyles} from '../common/component/modal/utils/styles';
import {generatePopoverStyles} from '../common/component/popover/utils/styles';
import {Lobby} from './component/lobby/Lobby';
import {Round} from './component/round/Round';
import {IGame} from './model/IGame';
import {IPlayer} from './model/IPlayer';
import styles from './styles/color.module.scss';
import {UrlUtils} from '../common/utils/url';

export class WhoAmI extends BaseGame<IGame, IPlayer> {

    constructor(props: RouteComponentProps) {
        super(props);
        const options = {
            gameName: 'Who am I',
            gamePath: UrlUtils.getPathFromString(this.props.match.path),
            socketNamespace: Urls.WhoAmI,
            styles: generateBaseGameStyles(
                generateModalStyles(styles.WAIPrimary, styles.WAISecondary, styles.WAITertiary),
                generateConnectToastStyles(styles.WAIConnect, styles.WAIDisconnect),
                null,
                styles.WAIBackground,
                generatePopoverStyles(styles.WAITertiaryIcon, styles.WAITertiary)
            )
        };
        this.state = {
            ...this.state,
            options: options
        };
    }

    renderGame(): JSX.Element {
        const {game, player, socket} = this.state;
        if (game.isInLobby) {
            return <Lobby game={game}
                          player={player}
                          socket={socket}/>;
        }
        return <Round game={game} player={player} socket={socket}/>;
    }

}
