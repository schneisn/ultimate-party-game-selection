import {IConnectionReadyPlayer} from '../../common/model/IConnectionReadyPlayer';
import {INote} from './INote';

export interface IPlayer extends IConnectionReadyPlayer {
    identity: string;
    identityAuthor: string;
    identityConfirmed: boolean;
    identityRevealed: boolean;
    notes: INote[];
}
