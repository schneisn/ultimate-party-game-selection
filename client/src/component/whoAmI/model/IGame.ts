import {ICommonGame} from '../../common/model/ICommonGame';
import {IPlayer} from './IPlayer';

export interface IGame extends ICommonGame<IPlayer> {
    isInLobby: boolean;
}
