import React from 'react';
import {Socket} from '../../../../server/socket';
import {CommonLobby} from '../../../common/component/commonLobby/CommonLobby';
import {generateCommonLobbyStyles} from '../../../common/component/commonLobby/utils/styles';
import {getGameMaster} from '../../../common/utils/player';
import {IGame} from '../../model/IGame';
import {IPlayer} from '../../model/IPlayer';
import styles from '../../styles/color.module.scss';

interface IProps {
    game: IGame;
    player: IPlayer;
    socket: Socket;
}

export class Lobby extends React.Component<IProps> {
    render(): JSX.Element {
        const {game, player, socket} = this.props;

        return (
            <CommonLobby game={game}
                         player={player}
                         socket={socket}
                         gameMasterId={getGameMaster(game).id}
                         styles={generateCommonLobbyStyles(styles.WAIPrimary, styles.WAITertiary, styles.WAISecondary, styles.WAITertiary)}/>
        );
    }
}
