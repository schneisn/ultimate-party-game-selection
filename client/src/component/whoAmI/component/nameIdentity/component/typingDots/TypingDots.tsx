import React from 'react';
import styles from './TypingDots.module.scss';

interface IProps {

}

/**
 * Animated dots that indicate typing.
 * See https://codepen.io/nzbin/pen/GGrXbp
 */
export class TypingDots extends React.Component<IProps> {

    render() {
        return (
            <div className={styles.TypingDotsContainer}>
                <div className={styles.TypingDots}/>
            </div>
        );
    }
}
