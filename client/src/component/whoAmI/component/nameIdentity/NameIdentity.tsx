import React from 'react';
import {Badge} from '../../../common/component/badge/Badge';
import {IconType} from '../../../common/component/icon/Icon';
import {Popover} from '../../../common/component/popover/Popover';
import {generatePopoverStyles} from '../../../common/component/popover/utils/styles';
import {getPlayer, playerIsGameMaster} from '../../../common/utils/player';
import {StringUtils} from '../../../common/utils/string';
import {IGame} from '../../model/IGame';
import {IPlayer} from '../../model/IPlayer';
import {TypingDots} from './component/typingDots/TypingDots';
import styles from './NameIdentity.module.scss';
import classNames from 'classnames';
import {PlayerInfo} from '../../../common/component/playerInfo/PlayerInfo';

interface IProps {
    game: IGame;
    player: IPlayer;
    teamMate: IPlayer;
}

interface IState {
    identityHidden: boolean;
}

/**
 * Displays the name, the identity and information about who chose the identity
 * of this player
 */
export class NameIdentity extends React.Component<IProps, IState> {

    private popoverId = 'identityAuthor';

    constructor(props: Readonly<IProps>) {
        super(props);
        this.state = {
            identityHidden: false
        }
    }

    render(): JSX.Element {
        const {game, player, teamMate} = this.props;
        if (playerIsGameMaster(game, teamMate.id)) {
            teamMate.isGameMaster = true;
        }
        return <div className={styles.NameIdentity}>
            <PlayerInfo player={teamMate}
                        showReadyState={false}/>
            <div className={styles.Identity}>
                {this.getIdentity()}
            </div>
            {((!StringUtils.isEmpty(teamMate.identityAuthor) && teamMate.identityAuthor !== player.id) ||
                (!StringUtils.isEmpty(teamMate.identity) && teamMate.identityAuthor === player.id)) &&
            <div className={styles.IdentityAuthor}>
              <Popover icon={IconType.userEdit}
                       id={this.popoverId + teamMate.id}
                       iconTitle={'Identity author'}
                       customStyles={generatePopoverStyles(styles.IdentityAuthorIcon, styles.WAITertiary)}
                       title={`${teamMate.identityAuthor === player.id ? 'You' : getPlayer(game, teamMate.identityAuthor)?.name} decided who or what this player is`}/>
                {teamMate.identityAuthor === player.id ? 'You' : getPlayer(game, teamMate.identityAuthor)?.name}
            </div>}
        </div>;
    }

    private getIdentity = (): JSX.Element => {
        const {teamMate} = this.props;
        if (this.teamMateCanSeeIdentity()) {
            return <div className={classNames(styles.IdentityContainer, this.state.identityHidden && styles.IdentityBlackened)}
                        onClick={() => this.setState({identityHidden: !this.state.identityHidden})}
                        role={'button'}
                        title={'Click to hide the identity'}>
                <b className={this.state.identityHidden ? styles.IdentityTextHidden : null}>
                    ({teamMate.identity})
                </b>
            </div>;
        }
        if (teamMate.identityRevealed) {
            return <Badge additionalClassNames={styles.IdentityRevealed}>{teamMate.identity}</Badge>;
        }
        if (this.identityIsSecret()) {
            return <b>(???)</b>;
        }
        if (this.teamMateGetsIdentity()) {
            return <TypingDots/>;
        }
        if (this.teamMateHasNoIdentity()) {
            return <Badge additionalClassNames={styles.NoIdentityBadge}>No identity</Badge>;
        }
    }

    private teamMateCanSeeIdentity(): boolean {
        const {player, teamMate} = this.props;
        return (teamMate.id !== player.id && teamMate.identityConfirmed) && !teamMate.identityRevealed;
    }

    private identityIsSecret() {
        const {player, teamMate} = this.props;
        return teamMate.id === player.id && !teamMate.identityRevealed && teamMate.identityConfirmed
    }

    private teamMateGetsIdentity(): boolean {
        const {player, teamMate} = this.props;
        return !teamMate.identityConfirmed && teamMate.identityAuthor && teamMate.identityAuthor !== player.id;
    }

    private teamMateHasNoIdentity(): boolean {
        const {teamMate} = this.props;
        return !teamMate.identity && !teamMate.identityConfirmed && !teamMate.identityAuthor;
    }
}
