import {GameEvent} from '../../../../../../common/event/GameEvent';

export class SelectPlayerEvent extends GameEvent {
    static readonly id = 'SelectPlayer';
    selectedPlayerId: string;

    constructor(gameId: string, selectedPlayerId: string) {
        super(gameId);
        this.selectedPlayerId = selectedPlayerId;
    }
}
