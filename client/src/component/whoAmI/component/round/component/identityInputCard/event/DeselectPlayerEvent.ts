import {GameEvent} from '../../../../../../common/event/GameEvent';

export class DeselectPlayerEvent extends GameEvent {
    static readonly id = 'DeselectPlayer';
    deselectedPlayerId: string;

    constructor(gameId: string, deselectedPlayerId: string) {
        super(gameId);
        this.deselectedPlayerId = deselectedPlayerId;
    }
}
