import {GameEvent} from '../../../../../../common/event/GameEvent';

export class ConfirmIdentityEvent extends GameEvent {
    static readonly id = 'ConfirmIdentity';
    selectedPlayerId: string;
    identity: string;

    constructor(gameId: string, selectedPlayerId: string, identity: string) {
        super(gameId);
        this.selectedPlayerId = selectedPlayerId;
        this.identity = identity;
    }
}
