import classNames from 'classnames';
import React from 'react';
import {Socket} from '../../../../../../server/socket';
import {WideButton} from '../../../../../common/component/wideButton/WideButton';
import {Card} from '../../../../../common/component/card/Card';
import {TextAreaWithButton} from '../../../../../common/component/textAreaWithButton/TextAreaWithButton';
import {IGame} from '../../../../model/IGame';
import {IPlayer} from '../../../../model/IPlayer';
import {NameIdentity} from '../../../nameIdentity/NameIdentity';
import {ConfirmIdentityEvent} from './event/ConfirmIdentityEvent';
import styles from './IdentityInputCard.module.scss';

const placeholders = [
    'Günter Jauch',
    'Thomas Gottschalk',
    'Johnny Depp',
    'Brad Pitt',
    'Meryl Streep',
    'Tom Cruise',
    'Til Schweiger',
    'Matthias Schweighöfer',
    'Marlon Brando',
    'Marilyn Monroe',
    'Justin Bieber',
    'Freddy Mercury',
    'Michael Wendler',
    'Elvis Presley',
    'Billie Eilish',
    'Madonna',
    'Britney Spears',
    'Taylor Swift',
    'Helene Fischer',
    'Cleopatra',
    'Frida Kahlo',
    'Napoleon',
    'Picasso',
    'Christoph Kolumbus',
    'Jeanne d\'Arc',
    'Leonardo da Vinci',
    'Wolfgang Amadeus Mozart',
    'Cristiano Ronaldo',
    'Boris Becker',
    'Thomas Müller (Fußball)',
    'Harry Potter',
    'Gandalf',
    'Der Grinch',
    'Gollum',
    'Luke Skywalker',
    'Darth Vader',
    'Voldemort',
    'The Hulk',
    'Angela Merkel',
    'Donald Trump',
    'Barack Obama',
    'Wladimir Putin',
    'Arnold Schwarzenegger',
    'Rapunzel',
    'Schneewittchen',
    'Aschenputtel',
    'Dornröschen',
    'Rotkäppchen',
    'Frau Holle',
    'Rumpelstilzchen',
    'Elsa (Frozen)',
    'Anna (Frozen)',
    'Arielle',
    'Nemo',
    'Jasmin (Aladdin)',
    'Belle (Die Schöne und das Biest)',
    'Mulan',
    'Simba',
    'SpongeBob Schwammkopf',
    'Bibi Blocksberg',
    'Pumuckl',
    'Barbie',
    'Ken (Barbie)',
    'Biene Maja',
    'Micky Maus',
    'Winnie Pooh',
    'J.R.R. Tolkien',
    'Queen Elizabeth II.',
    'Hercule Poirot',
    'Sean Bean',
    'Der Weihnachtsmann',
    'Christian Grey (50 Shades)',
    'Mr. Bean',
    'Die kleine Raupe Nimmersatt',
    'Pippi Langstrumpf',
    'Hermine Granger',
    'Heidi Klum',
    'Paris Hilton',
    'Asterix',
    'Obelix',
    'Krümelmonster',
    'Justus Jonas (Die drei ???)',
    'Mowgli',
    'Shrek',
    'Pocahontas (Disney)',
    'R\u00e9my (Die Ratte in Ratatouille)',
    'Sid (Ice Age)',
    'J (Men in Black)',
    'Inspektor Columbo',
    'Marry Poppins',
    'Marty McFly (Zurück in die Zukunft)',
    'Yoda',
    'Zeus',
    'Sauron',
    'der Elefant (Sendung mit der Maus)',
    'Ohnezahn (Drachen z\u00e4hmen leicht gemacht)',
    'Schroedingers Katze',
    'Sprechender Hut (Harry Potter)',
    'Elisabeth von Österreich-Ungarn (Sissi)',
    'R2D2 (Star Wars)',
    'Indiana Jones',
    'Walter White (Breaking Bad)',
    'Elizabeth Bennet (Stolz und Vorurteil)',
    'Das Sandmännchen',
    'Peter Pan',
    'Rogue (X-Men)',
    'Kvothe (Kingkiller-Chronicles)',
    'Shaun das Schaf',
    'Miley Cyrus',
    'Carry Bradshaw (Sex and the City)',
    'Münchhausen',
    'Lorelai Gilmore (Gilmore Girls)',
    'Martin Luther King',
    'Henry VIII',
    'Mia (Plötzlich Prinzessin)',
    'Albert Einstein',
    'Helena von Troja',
    '\u00d6tzi',
    'Die Zahnfee',
    'Anubis (Ägyptischer Gott)',
    'J.K. Rowling',
    'C.S. Lewis'
];

interface IProps {
    disabled: boolean;
    game: IGame;
    onDeselectPlayer: (target: IPlayer) => void;
    onPlayerNameClick: (target: IPlayer) => void;
    /**
     * The active player
     */
    player: IPlayer;
    /**
     * Whether the input for the identity is visible.
     * Default is false.
     */
    showIdentityInput: boolean;
    socket: Socket;
    /**
     * The player which identity can be chosen
     */
    target: IPlayer;
}

interface IState {
    identity: string;
}

export class IdentityInputCard extends React.Component<IProps, IState> {

    constructor(props: Readonly<IProps>) {
        super(props);
        this.state = {
            identity: props.target.identity || ''
        }
    }

    render(): JSX.Element {
        const {game, player, target, showIdentityInput, onDeselectPlayer, onPlayerNameClick, disabled} = this.props;
        const {identity} = this.state;
        return (
            <Card key={target.id}
                  additionalClassNames={classNames(styles.WAITertiary, styles.IdentityCard)}
                  disabled={disabled}>
                <WideButton onClick={() => onPlayerNameClick(target)}
                            additionalClassNames={styles.IdentityNameButton}
                            disabled={this.playerCanOpenInput()}>
                    <NameIdentity game={game} player={player} teamMate={target}/>
                </WideButton>
                {showIdentityInput && <div className={showIdentityInput ? styles.IdentityInput : undefined}>
                  <TextAreaWithButton buttonText={'Confirm'}
                                      textMaxLength={200}
                                      textPlaceholder={placeholders[Math.floor(Math.random() * placeholders.length)]}
                                      textValue={identity}
                                      autoFocus={true}
                                      additionalButtonClasses={styles.WAISecondary}
                                      invalidInput={[target.identity]}
                                      onChange={this.onIdentityChange}
                                      clearAfterSubmit={false}
                                      onSubmit={this.onIdentityConfirm}/>
                  <WideButton additionalClassNames={classNames(styles.ClearButton, styles.WAISecondary)}
                              onClick={() => onDeselectPlayer(target)}
                              hidden={!showIdentityInput}>
                    Clear
                  </WideButton>
                </div>}
            </Card>
        );
    }

    private playerCanOpenInput(): boolean {
        const {player, target} = this.props;
        return (target.identityAuthor && target.identityAuthor !== player.id) || target.id === player.id;
    }

    private onIdentityConfirm = (textValue: string): void => {
        const {game, socket, target} = this.props;
        socket.emit(ConfirmIdentityEvent.id, new ConfirmIdentityEvent(game.id, target.id, textValue));
    }

    private onIdentityChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
        this.setState({identity: event.currentTarget.value.trim()});
    }
}
