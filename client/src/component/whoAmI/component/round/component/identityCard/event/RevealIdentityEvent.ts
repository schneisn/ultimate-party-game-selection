import {GameEvent} from '../../../../../../common/event/GameEvent';

export class RevealIdentityEvent extends GameEvent {
    static readonly id = 'RevealIdentity';
    revealPlayerId: string;

    constructor(gameId: string, revealPlayerId: string) {
        super(gameId);
        this.revealPlayerId = revealPlayerId;
    }
}
