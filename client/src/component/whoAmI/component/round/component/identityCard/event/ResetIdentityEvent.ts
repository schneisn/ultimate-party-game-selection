import {GameEvent} from '../../../../../../common/event/GameEvent';

export class ResetIdentityEvent extends GameEvent {
    static readonly id = 'ResetIdentity';
    resetPlayerId: string;

    constructor(gameId: string, resetPlayerId: string) {
        super(gameId);
        this.resetPlayerId = resetPlayerId;
    }
}
