import classNames from 'classnames';
import React from 'react';
import {Socket} from '../../../../../../server/socket';
import {Card} from '../../../../../common/component/card/Card';
import {IconType} from '../../../../../common/component/icon/Icon';
import {IconContent} from '../../../../../common/component/iconContent/IconContent';
import {Modal} from '../../../../../common/component/modal/Modal';
import {generateModalStyles} from '../../../../../common/component/modal/utils/styles';
import {playerIsGameMaster} from '../../../../../common/utils/player';
import {IGame} from '../../../../model/IGame';
import {IPlayer} from '../../../../model/IPlayer';
import {NameIdentity} from '../../../nameIdentity/NameIdentity';
import {ResetIdentityEvent} from './event/ResetIdentityEvent';
import styles from './IdentityCard.module.scss';
import {RevealIdentityEvent} from './event/RevealIdentityEvent';

interface IProps {
    game: IGame;
    /**
     * The active player
     */
    player: IPlayer;
    teamMate: IPlayer;
    socket: Socket;
}

interface IState {
    showConfirmRevealModal: boolean;
    showConfirmResetModal: boolean;
}

export class IdentityCard extends React.Component<IProps, IState> {

    constructor(props: Readonly<IProps>) {
        super(props);
        this.state = {
            showConfirmRevealModal: false,
            showConfirmResetModal: false
        }
    }

    render(): JSX.Element {
        const {game, player, teamMate} = this.props;
        return (
            <>
                <Modal isOpen={this.state.showConfirmResetModal}
                       modalTitle={'Reset player?'}
                       onToggle={this.onToggleConfirmResetModal}
                       styles={generateModalStyles(styles.WAIPrimary, styles.WAISecondary, styles.WAITertiary)}
                       onConfirm={this.onConfirmResetIdentityClick}>
                    Are you sure you want to clear the identity of <b>{teamMate.name}</b>?
                </Modal>
                <Modal isOpen={this.state.showConfirmRevealModal}
                       modalTitle={'Reveal identity?'}
                       onToggle={this.onToggleConfirmRevealModal}
                       styles={generateModalStyles(styles.WAIPrimary, styles.WAISecondary, styles.WAITertiary)}
                       onConfirm={this.onToggleRevealIdentity}>
                    Are you sure you want <b>{teamMate.name}</b> to see their identity?
                </Modal>
                <Card additionalClassNames={classNames(styles.WAITertiary, styles.IdentityCard)}>
                    <IconContent
                        iconLeft={{
                            icon: playerIsGameMaster(game, player.id) && IconType.eraser,
                            iconTitle: 'Clear identity',
                            additionalIconClassnames: styles.Icon,
                            onIconClick: this.onToggleConfirmResetModal
                        }}
                        iconRight={{
                            icon: playerIsGameMaster(game, player.id) ? (teamMate.identityRevealed ? IconType.eyeSlash : IconType.eye) : null,
                            iconTitle: teamMate.identityRevealed ? 'Hide identity' : 'Reveal identity',
                            additionalIconClassnames: styles.Icon,
                            onIconClick: teamMate.identityRevealed ? this.onToggleRevealIdentity : this.onToggleConfirmRevealModal
                        }}
                        additionalClassNames={styles.IconContent}>
                        <NameIdentity game={game} player={player} teamMate={teamMate}/>
                    </IconContent>
                </Card>
            </>
        );
    }

    private onToggleConfirmResetModal = (): void => {
        this.setState({
            showConfirmResetModal: !this.state.showConfirmResetModal
        });
    }

    private onToggleConfirmRevealModal = (): void => {
        this.setState({
            showConfirmRevealModal: !this.state.showConfirmRevealModal
        });
    }

    private onConfirmResetIdentityClick = (): void => {
        const {game, teamMate, socket} = this.props;
        socket.emit(ResetIdentityEvent.id, new ResetIdentityEvent(game.id, teamMate.id));
    }

    private onToggleRevealIdentity = (): void => {
        const {game, teamMate, socket} = this.props;
        socket.emit(RevealIdentityEvent.id, new RevealIdentityEvent(game.id, teamMate.id));
    }
}
