import {GameEvent} from '../../../../../../common/event/GameEvent';

export class AddNoteEvent extends GameEvent {
    static readonly id = 'AddNote';
    note: string;
    isCorrect: boolean;

    constructor(gameId: string, note: string, isCorrect: boolean) {
        super(gameId);
        this.note = note;
        this.isCorrect = isCorrect;
    }
}
