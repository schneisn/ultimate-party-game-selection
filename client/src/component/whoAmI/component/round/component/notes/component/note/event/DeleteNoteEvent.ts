import {GameEvent} from '../../../../../../../../common/event/GameEvent';

export class DeleteNoteEvent extends GameEvent {
    static readonly id = 'DeleteNote';
    note: string;

    constructor(gameId: string, note: string) {
        super(gameId);
        this.note = note;
    }
}
