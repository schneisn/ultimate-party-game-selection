import React from 'react';
import {Socket} from '../../../../../../server/socket';
import {HeaderContainer} from '../../../../../common/component/headerContainer/HeaderContainer';
import {TextAreaWithButton} from '../../../../../common/component/textAreaWithButton/TextAreaWithButton';
import {StringUtils} from '../../../../../common/utils/string';
import {IGame} from '../../../../model/IGame';
import {IPlayer} from '../../../../model/IPlayer';
import {Note} from './component/note/Note';
import {AddNoteEvent} from './event/AddNoteEvent';
import styles from './Notes.module.scss';

interface IProps {
    game: IGame;
    player: IPlayer;
    socket: Socket;
}

export class Notes extends React.Component<IProps> {

    render(): JSX.Element {
        const {player} = this.props;
        return (
            <HeaderContainer additionalClassNames={styles.WAISecondary}
                             header={'Notes'}>
                {this.getNotes()}
                <TextAreaWithButton buttonText={'Add note'}
                                    buttonTitle={'Add note'}
                                    onSubmit={this.onAddNote}
                                    clearAfterSubmit={true}
                                    invalidInput={player.notes.map(note => note.note)}
                                    textPlaceholder={'New note...'}
                                    textMaxLength={200}
                                    additionalTextAreaClasses={styles.NoteInput}
                                    additionalButtonClasses={styles.WAITertiary}/>
            </HeaderContainer>
        );
    }

    private getNotes(): JSX.Element[] {
        const {game, player, socket} = this.props;
        if (player.notes.length < 1) {
            return [<span key={'no-notes'}>No notes</span>];
        }
        return player.notes.map(note => <Note key={note.note}
                                              note={note}
                                              socket={socket}
                                              game={game}
                                              player={player}/>);
    }

    private onAddNote = (note: string): void => {
        const {game, socket} = this.props;
        if (StringUtils.isEmpty(note)) {
            return;
        }
        socket.emit(AddNoteEvent.id, new AddNoteEvent(game.id, note, true));
    }
}
