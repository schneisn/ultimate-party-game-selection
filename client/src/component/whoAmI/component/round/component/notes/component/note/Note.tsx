import classNames from 'classnames';
import React from 'react';
import {Socket} from '../../../../../../../../server/socket';
import {Card} from '../../../../../../../common/component/card/Card';
import {IconType} from '../../../../../../../common/component/icon/Icon';
import {IGame} from '../../../../../../model/IGame';
import {INote} from '../../../../../../model/INote';
import {IPlayer} from '../../../../../../model/IPlayer';
import {DeleteNoteEvent} from './event/DeleteNoteEvent';
import styles from './Note.module.scss';
import {IconContent} from '../../../../../../../common/component/iconContent/IconContent';

interface IProps {
    game: IGame;
    player: IPlayer;
    note: INote;
    socket: Socket;
}

export class Note extends React.Component<IProps> {
    render(): JSX.Element {
        const {note} = this.props;
        return (
            <Card additionalClassNames={classNames(styles.WAITertiary, styles.Note)}>
                <IconContent iconRight={{
                    icon: IconType.trash,
                    iconTitle: 'Delete note',
                    onIconClick: this.onDeleteNoteClick,
                    additionalIconClassnames: styles.DeleteButton
                }}>
                    {note.note}
                </IconContent>
            </Card>
        );
    }

    private onDeleteNoteClick = (): void => {
        const {game, note, socket} = this.props;
        socket.emit(DeleteNoteEvent.id, new DeleteNoteEvent(game.id, note.note));
    }
}
