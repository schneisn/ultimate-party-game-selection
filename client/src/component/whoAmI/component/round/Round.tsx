import React from 'react';
import {Socket} from '../../../../server/socket';
import {HeaderContainer} from '../../../common/component/headerContainer/HeaderContainer';
import {Icon, IconType} from '../../../common/component/icon/Icon';
import {IconContent} from '../../../common/component/iconContent/IconContent';
import {InfoCard} from '../../../common/component/infoCard/InfoCard';
import {SmallCard} from '../../../common/component/smallCard/SmallCard';
import {playerIsGameMaster} from '../../../common/utils/player';
import {IGame} from '../../model/IGame';
import {IPlayer} from '../../model/IPlayer';
import {IdentityCard} from './component/identityCard/IdentityCard';
import {DeselectPlayerEvent} from './component/identityInputCard/event/DeselectPlayerEvent';
import {SelectPlayerEvent} from './component/identityInputCard/event/SelectPlayerEvent';
import {IdentityInputCard} from './component/identityInputCard/IdentityInputCard';
import {Notes} from './component/notes/Notes';
import styles from './Round.module.scss';

interface IProps {
    game: IGame;
    player: IPlayer;
    socket: Socket;
}

export class Round extends React.Component<IProps> {

    render(): JSX.Element {
        const {game, player, socket} = this.props;
        return (
            <>
                <InfoCard additionalClassNames={styles.WAIPrimary}
                          titleText={'Play'}>
                    Click on a player to define who or what this player is.
                    Then, ask the other players 'yes'/'no' questions about your identity.
                    You can also take notes about what you learned about yourself
                </InfoCard>
                {playerIsGameMaster(game, player.id) &&
                <SmallCard additionalClassNames={styles.WAISecondary}>
                  <IconContent iconLeft={{icon: IconType.infoCircle}}>
                    You can use <Icon icon={IconType.eye} additionalClasses={styles.InfoIcon}/> to reveal the player's identity
                  </IconContent>
                </SmallCard>}
                {playerIsGameMaster(game, player.id) &&
                <SmallCard additionalClassNames={styles.WAISecondary}>
                  <IconContent iconLeft={{icon: IconType.infoCircle}}>
                    You can use <Icon icon={IconType.eraser} additionalClasses={styles.InfoIcon}/> to clear the player's identity
                  </IconContent>
                </SmallCard>}
                <SmallCard additionalClassNames={styles.WAISecondary}>
                    <IconContent iconLeft={{icon: IconType.infoCircle}}>
                        Click on the identity of a player to show/hide their identity
                    </IconContent>
                </SmallCard>
                <HeaderContainer additionalClassNames={styles.WAISecondary}
                                 header={'Players'}>
                    {this.getPlayers()}
                </HeaderContainer>
                {player.identityConfirmed && <Notes game={game} player={player} socket={socket}/>}
            </>
        );
    }

    private getPlayers(): JSX.Element[] {
        const {game, player, socket} = this.props;
        return game.players.map(teamMate => {
            if (!teamMate.identityConfirmed) {
                return <IdentityInputCard key={teamMate.id}
                                          disabled={this.cardIsDisabled(teamMate)}
                                          game={game}
                                          player={player}
                                          target={teamMate}
                                          showIdentityInput={player.id === teamMate.identityAuthor}
                                          socket={socket}
                                          onPlayerNameClick={this.onCardClick}
                                          onDeselectPlayer={this.onPlayerDeselectClick}/>;
            }
            return <IdentityCard key={teamMate.id}
                                 game={game}
                                 player={player}
                                 socket={socket}
                                 teamMate={teamMate}/>
        })
    }


    private cardIsDisabled(target: IPlayer): boolean {
        const {player} = this.props;
        return player.id === target.id || (target.identityAuthor && target.identityAuthor !== player.id);
    }

    private onCardClick = (target: IPlayer): void => {
        const {game, socket} = this.props;
        if (this.cardIsDisabled(target)) {
            return;
        }
        socket.emit(SelectPlayerEvent.id, new SelectPlayerEvent(game.id, target.id));
    }

    private onPlayerDeselectClick = (target: IPlayer): void => {
        const {game, socket} = this.props;
        socket.emit(DeselectPlayerEvent.id, new DeselectPlayerEvent(game.id, target.id));
    }

}
