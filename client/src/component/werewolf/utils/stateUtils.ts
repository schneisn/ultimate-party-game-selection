import {IGame} from '../model/IGame';
import {GameEvent, IGameState} from '../model/IGameState';
import {IPlayer, Role} from '../model/IPlayer';
import {IStateEvent, StateEvent} from '../model/IStateEvent';
import {isLover} from './roleUtils';

export function getCurrentState(game: IGame): IGameState {
    const currentState = game.states[game.states.length - 1];
    if (!currentState) {
        return null;
    }
    return currentState;
}

export function getLatestEvent(game: IGame, eventId: StateEvent): IStateEvent {
    return game.states.slice().reverse().find(state => state.events.slice().reverse().find(ev => ev.id === eventId))?.events.slice().reverse().find(ev => ev.id === eventId);
}

export function getEventInCurrentNight(game: IGame, eventId: StateEvent): IStateEvent {
    const currentNight = getCurrentState(game).night;
    return game.states
        .filter(state => state.night === currentNight)
        .slice()
        .reverse()
        .find(state => state.events.slice().reverse().find(ev => ev.id === eventId))?.events.slice().reverse().find(ev => ev.id === eventId);
}

export function getLatestEventByTargetIdAndEventId(game: IGame, eventId: StateEvent, targetId: string): IStateEvent {
    const stateWithEvent = game.states.slice().reverse().find(state => findEventByTargetIdAndEventId(state, eventId, targetId));
    if (!stateWithEvent) {
        return null;
    }
    return findEventByTargetIdAndEventId(stateWithEvent, eventId, targetId);
}

function findEventByTargetIdAndEventId(state: IGameState, eventId: StateEvent, targetId: string): IStateEvent {
    return state.events.slice().reverse().find(ev => ev.id === eventId && ev.targetId === targetId);
}

export function getLatestEventByTargetId(game: IGame, targetId: string): IStateEvent {
    return game.states.slice().reverse().find(state => state.events.slice().reverse().find(ev => ev.targetId === targetId))?.events.slice().reverse().find(ev => ev.targetId === targetId);
}

export function getLatestStateWithEventForTarget(game: IGame, eventId: StateEvent, targetId: string): IGameState {
    return game.states.slice().reverse().find(state => state.events.find(ev => ev.id === eventId && ev.targetId === targetId));
}

export function eventExists(game: IGame, event: StateEvent): boolean {
    return game.states.some(state => state.events.some(ev => ev.id === event));
}

/**
 * Checks whether the player is a hunter and the hunter that was killed in the latest night
 * @param game
 * @param player
 */
export function playerIsActiveHunter(game: IGame, player: IPlayer): boolean {
    const currentState = getCurrentState(game);
    return playerIsDeadHunter(game, player) && currentState.id === GameEvent.HUNTER;
}

/**
 * Checks whether the player is a hunter and died in the latest night
 * @param game
 * @param player
 */
export function playerIsDeadHunter(game: IGame, player: IPlayer): boolean {
    return player.role === Role.HUNTER && playerDiedInCurrentNight(game, player.id);
}

/**
 * Checks whether this player died in the latest night
 * @param game
 * @param playerId
 */
export function playerDiedInCurrentNight(game: IGame, playerId: string): boolean {
    const playerDeathEvent = getLatestEventByTargetIdAndEventId(game, StateEvent.DEATH, playerId);
    return isKillEventOfCurrentNight(game, playerDeathEvent, playerId);
}

function isKillEventOfCurrentNight(game: IGame, event: IStateEvent, targetId: string): boolean {
    return event && getLatestStateWithEventForTarget(game, event.id, targetId).night === getCurrentState(game).night;
}

/**
 * The player has permission to see the private information for the role when:
 * * The player is dead, or
 * * The player is the gm, or
 * * The state is visible for all players, or
 * * The role of the player is currently active
 * @param game
 * @param player
 */
export function playerCanSeeCurrentState(game: IGame, player: IPlayer): boolean {
    return !player?.isAlive ||
        player.role === Role.GAME_MASTER ||
        gameStateIsVisibleForAll(game) ||
        isPlayersRoleTurn(game, player);
}

function gameStateIsVisibleForAll(game: IGame): boolean {
    return gameIsState(game, GameEvent.LOBBY) ||
        gameIsState(game, GameEvent.SELECT_ROLES) ||
        gameIsState(game, GameEvent.MAYOR) ||
        gameIsState(game, GameEvent.START_NIGHT) ||
        gameIsState(game, GameEvent.END_NIGHT) ||
        gameIsState(game, GameEvent.TRIAL) ||
        gameIsState(game, GameEvent.SHOW_HUNTER_KILL) ||
        gameIsState(game, GameEvent.SHOW_TRIAL_KILL) ||
        gameIsState(game, GameEvent.MAYOR_ELECTION_RESULT) ||
        gameIsState(game, GameEvent.GAME_OVER);
}

function gameIsState(game: IGame, state: GameEvent): boolean {
    const lastHistoryEntry = getCurrentState(game);
    return lastHistoryEntry && lastHistoryEntry.id === state;
}

function isPlayersRoleTurn(game: IGame, player: IPlayer): boolean {
    const currentState = getCurrentState(game);
    return currentState.id?.toString() === player.role?.toString() ||
        (gameIsState(game, GameEvent.LOVERS) && isLover(game, player.id)) ||
        (player.role === Role.WEREWOLF && currentState.id === GameEvent.SHOW_WEREWOLF_KILL) ||
        (player.role === Role.SEER && currentState.id === GameEvent.SHOW_SEER_SELECTION);
}
