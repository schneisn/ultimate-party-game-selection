import {IPlayer} from '../model/IPlayer';
import {IGame} from '../model/IGame';

export function targetHasVoteFromPlayer(game: IGame, player: IPlayer, target: IPlayer): boolean {
    return game.votes.some(vote => vote.targetId === target.id && vote.voterId === player.id);
}
