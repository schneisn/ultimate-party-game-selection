import {IPlayer, Role} from '../model/IPlayer';
import {IGame} from '../model/IGame';

/**
 * The player is the game master if they have the role game master
 * @param player
 */
export function playerIsGameMaster(player: IPlayer): boolean {
    if (!player) {
        return false;
    }
    return player.role === Role.GAME_MASTER;
}

/**
 * Gets the player from the game. If the id belongs to the gm, the gm is returned.
 * @param game
 * @param playerId
 */
export function getPlayer(game: IGame, playerId: string): IPlayer {
    if (game.gamemaster.id === playerId) {
        return game.gamemaster;
    }
    return game.players.find(player => player.id === playerId);
}
