import {getPlayer} from '../../common/utils/player';
import {IGame} from '../model/IGame';
import {GameEvent} from '../model/IGameState';
import {IPlayer, Role} from '../model/IPlayer';
import {StateEvent} from '../model/IStateEvent';
import {getLatestEvent} from './stateUtils';
import {playerIsGameMaster} from './playerUtils';

/**
 * Checks whether the given player is allowed to see the role of the given target
 * @param player
 * @param target
 */
export function playerCanSeeRole(player: IPlayer, target: IPlayer): boolean {
    return target.id === player.id ||
        ((target.role === player.role && target.role !== Role.CITIZEN) ||
            !target.isAlive ||
            playerIsGameMaster(player));
}

/**
 * Gets both lovers
 * @param game
 */
export function getLovers(game: IGame): IPlayer[] {
    const lovers = [];
    for (const player of game.players) {
        if (isLover(game, player.id)) {
            lovers.push(player);
        }
    }
    return lovers;
}

/**
 * Gets the love partner for the given player.
 * @param game
 * @param playerId
 * @return When there is no lover, null is returned.
 */
export function getLovePartner(game: IGame, playerId: string): IPlayer {
    const loverOneEvent = getLatestEvent(game, StateEvent.LOVER_ONE);
    if (loverOneEvent.targetId !== playerId) {
        return getPlayer(game, loverOneEvent.targetId);
    }
    const loverTwoEvent = getLatestEvent(game, StateEvent.LOVER_TWO);
    if (loverTwoEvent.targetId !== playerId) {
        return getPlayer(game, loverTwoEvent.targetId);
    }
    return null;
}

/**
 * Checks whether the player with the given id is one of the lovers
 * @param game
 * @param playerId
 */
export function isLover(game: IGame, playerId: string): boolean {
    return getLatestEvent(game, StateEvent.LOVER_ONE)?.targetId === playerId || getLatestEvent(game, StateEvent.LOVER_TWO)?.targetId === playerId;
}

export function getDoppelgangerTarget(game: IGame): IPlayer {
    const doppelgangerState = game.states.find(state => state.id === GameEvent.DOPPELGANGER);
    if (!doppelgangerState) {
        return null;
    }
    const doppelgangerTargetId = doppelgangerState.events.find(ev => ev.id === StateEvent.DOPPELGANGER_SELECT)?.targetId;
    return getPlayer(game, doppelgangerTargetId);
}

/**
 * Contains descriptions for all roles
 */
export const roleDescriptions = new Map<Role, string>([
    [Role.CITIZEN, 'The most commonplace role, a simple citizen, spends the game trying to root out who they believe the werewolves (and other citizens) are. While they do not need to lie, the role requires players to keenly sense and point out the flaws or mistakes of their fellow players. Someone is speaking too much? Could mean they\'re a werewolf. Someone isn\'t speaking enough? Could mean the same thing. It all depends on the people you\'re playing with, and how well you know them.'],
    [Role.WEREWOLF, 'Typically werewolves are outnumbered by citizens 2 to 1. So a game of 6 players would have 2 werewolves. The goal of the werewolves is to decide together on one citizen to secretly kill off during the night, while posing as citizens during the day so they\'re not killed off themselves. One by one they\'ll kill off citizens and win when there are either the same number of citizens and werewolves left, or all the citizens have died. This role is the hardest of all to maintain, because these players are lying for the duration of the game.'],
    [Role.DOPPELGANGER, 'The Doppelganger is a player that could potentially be on either side of the game. On the first night the when waking up, the Doppelganger chooses a player. When this player dies, the Doppelganger becomes the role of the chosen player with all abilities and victory conditions.'],
    [Role.HUNTER, 'The Hunter is a citizen that chooses a player to kill whenever the Hunter is killed. The Hunter is basically neutral. They could kill a Werewolf and help the citizens, but it is more likely that they will kill a citizen.'],
    [Role.HEALER, 'Each night, the Healer chooses one players to heal. If this player is targeted by the werewolves, they will not die at the beginning of the next day.'],
    [Role.SEER, 'The Seer, while first and foremost a citizen, has the added ability to "see" who the werewolves are once night falls. When called awake, the Seer can choose any of their fellow players to see whether or not they are indeed a Werewolf. The Seer can then choose to keep this information a secret during the day, or reveal themselves as the Seer and use the knowledge they gained during the night in their defense or to their advantage during the day. The strategy here is up to you.'],
    [Role.PROSTITUTE, 'Every night the prostitute chooses a player to spend the night with. If the prostitute is targeted by the werewolves, she will not die. However, if the player she spends the night with is targeted, both of them die.'],
    [Role.CHILD, 'The child can look up at any time during the night to find out more about the other players. If it is not careful enough, someone might get suspicious and come to its house... (This role only makes sense if you can physically see the other players)'],
    [Role.ARMOR, 'At the beginning of the game, the armor chooses two players, which fall in love. If one of the lovers dies, the other dies from a broken heart, too.'],
    [Role.WITCH, 'The witch wakes up after the werewolves. She has the power to heal the targeted player and kill one player with her poison. Beware, she can only use each of her skills once!'],
])
