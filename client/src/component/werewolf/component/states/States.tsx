import React from 'react';
import {IPlayer} from '../../model/IPlayer';
import {GameEvent} from '../../model/IGameState';
import {getCurrentState, getLatestEvent, playerCanSeeCurrentState} from '../../utils/stateUtils';
import {getLovePartner, isLover} from '../../utils/roleUtils';
import {StateEvent} from '../../model/IStateEvent';
import {getPlayer} from '../../../common/utils/player';
import {Lobby} from './component/lobby/Lobby';
import {RoleSelection} from '../roleSelection/RoleSelection';
import {Mayor} from './component/mayor/Mayor';
import {InfoEvent} from '../infoEvent/InfoEvent';
import {Armor} from './component/role/Armor';
import {Lover} from './component/role/Lover';
import {Doppelganger} from './component/role/Doppelganger';
import {Prostitute} from './component/role/Prostitute';
import {Werewolves} from './component/role/Werewolves';
import {Seer} from './component/role/Seer';
import {Healer} from './component/role/Healer';
import {Witch} from './component/role/Witch';
import {EndNight} from './component/endNight/EndNight';
import {Hunter} from './component/role/Hunter';
import {Trial} from './component/trial/Trial';
import styles from './States.module.scss';
import {IGame} from '../../model/IGame';
import {Socket} from '../../../../server/socket';
import {HighlightedRole, RoleInfo} from '../roleInfo/RoleInfo';
import {PlayerNameAndRole} from '../playerNameAndRole/PlayerNameAndRole';
import {SeerSelection} from './component/role/SeerSelection';
import {InfoCard} from '../../../common/component/infoCard/InfoCard';
import GameOver from './component/gameOver/GameOver';

interface IProps {
    game: IGame;
    player: IPlayer;
    socket: Socket;
}

interface IState {
    highlightedRoleInfo: HighlightedRole;
}

/**
 * The game states
 */
export class States extends React.Component<IProps, IState> {

    constructor(props: Readonly<IProps>) {
        super(props);
        this.state = {
            highlightedRoleInfo: null,
        }
    }

    componentDidUpdate(prevProps: Readonly<IProps>, prevState: Readonly<IState>) {
        if (this.props === prevProps) {
            return;
        }
        const {game} = this.props;
        const currentState = getCurrentState(game);
        switch (currentState.id) {
            case GameEvent.MAYOR_ELECTION_RESULT:
                this.setState({highlightedRoleInfo: HighlightedRole.Mayor});
                break;
            case GameEvent.LOVERS:
                this.setState({highlightedRoleInfo: HighlightedRole.Lover});
                break;
            default:
                this.setState({highlightedRoleInfo: null});
        }
    }

    render() {
        const {player, game} = this.props;
        return (
            <div className={styles.States}>
                <RoleInfo player={player}
                          game={game}
                          highlightRole={this.state.highlightedRoleInfo}/>
                {this.getRoleOfCurrentTurn()}
            </div>
        );
    }

    private getMayorElectionName = (): string => {
        const mayorId = getLatestEvent(this.props.game, StateEvent.MAYOR_ELECTED).targetId;
        return getPlayer(this.props.game, mayorId).name + ' has been elected as mayor';
    }

    private getTrialKillTarget = (): JSX.Element => {
        const {game} = this.props;
        const targetId = getLatestEvent(game, StateEvent.TRIAL_KILL).targetId;
        const target = getPlayer(game, targetId);
        if (isLover(game, targetId)) {
            const lovePartner = getLovePartner(game, targetId);
            return <span><PlayerNameAndRole game={game} player={target}/> and <PlayerNameAndRole game={game} player={lovePartner}/> have been sentenced to death</span>;
        }
        return <span><PlayerNameAndRole game={game} player={target}/> has been sentenced to death</span>;
    }

    private getHunterKillTarget = (): JSX.Element => {
        const {game} = this.props;
        const targetId = getLatestEvent(game, StateEvent.HUNTER_KILL).targetId;
        const target = getPlayer(game, targetId);
        if (isLover(game, targetId)) {
            const lovePartner = getLovePartner(game, targetId);
            return <span><PlayerNameAndRole game={game} player={target}/> and <PlayerNameAndRole game={game} player={lovePartner}/> were killed by the hunter</span>;
        }
        return <span><PlayerNameAndRole game={game} player={target}/> was killed by the hunter</span>;
    }

    private getRoleOfCurrentTurn = (): JSX.Element => {
        let state: JSX.Element;
        const {player, socket, game} = this.props;
        const entry = getCurrentState(game);
        if (!entry) {
            return null;
        }
        switch (entry.id) {
            case GameEvent.LOBBY:
                state = <Lobby socket={socket} game={game} player={player}/>;
                break;
            case GameEvent.SELECT_ROLES:
                state = <RoleSelection socket={socket} game={game} player={player}/>;
                break;
            case GameEvent.MAYOR:
                state = <Mayor socket={socket} game={game} player={player}/>;
                break;
            case GameEvent.MAYOR_ELECTION_RESULT:
                state = <InfoEvent buttonText={'Continue'}
                                   eventTitle={'Mayor election'}
                                   text={this.getMayorElectionName()}
                                   socket={socket}
                                   game={game}
                                   player={player}/>;
                break;
            case GameEvent.START_NIGHT:
                state = <InfoEvent buttonText={'Everybody is asleep'}
                                   eventTitle={'Start of night'}
                                   text={'The night begins and everybody in the village goes to sleep.'}
                                   socket={socket}
                                   game={game}
                                   player={player}/>;
                break;
            case GameEvent.ARMOR:
                state = <Armor socket={socket} game={game} player={player}/>;
                break;
            case GameEvent.LOVERS:
                state = <Lover socket={socket} game={game} player={player}/>;
                break;
            case GameEvent.DOPPELGANGER:
                state = <Doppelganger socket={socket} game={game} player={player}/>;
                break;
            case GameEvent.PROSTITUTE:
                state = <Prostitute socket={socket} game={game} player={player}/>;
                break;
            case GameEvent.WEREWOLF:
                state = <Werewolves socket={socket} game={game} player={player}/>;
                break;
            case GameEvent.SHOW_WEREWOLF_KILL:
                const targetId = getLatestEvent(game, StateEvent.WEREWOLF_KILL).targetId;
                const target = getPlayer(game, targetId);
                state = <InfoEvent buttonText={'The werewolves are asleep'}
                                   eventTitle={'Werewolves'}
                                   text={`The werewolves chose their target and go to sleep`}
                                   game={game}
                                   player={player}
                                   socket={socket}>
                    {playerCanSeeCurrentState(game, player) &&
                    <InfoCard additionalClassNames={styles.WWSecondary}>
                      The werewolves chose {target.name}
                    </InfoCard>}
                </InfoEvent>;
                break;
            case GameEvent.SEER:
                state = <Seer socket={socket} game={game} player={player}/>;
                break;
            case GameEvent.SHOW_SEER_SELECTION:
                state = <SeerSelection game={game} player={player} socket={socket}/>;
                break;
            case GameEvent.HEALER:
                state = <Healer socket={socket} game={game} player={player}/>;
                break;
            case GameEvent.WITCH:
                state = <Witch socket={socket} game={game} player={player}/>;
                break;
            case GameEvent.END_NIGHT:
                state = <EndNight socket={socket} game={game} player={player}/>;
                break;
            case GameEvent.HUNTER:
                state = <Hunter socket={socket} game={game} player={player}/>;
                break;
            case GameEvent.SHOW_HUNTER_KILL:
                state = <InfoEvent buttonText={'Continue'}
                                   eventTitle={'Hunter kill'}
                                   text={this.getHunterKillTarget()}
                                   socket={socket}
                                   game={game}
                                   player={player}/>;
                break;
            case GameEvent.TRIAL:
                state = <Trial socket={socket} game={game} player={player}/>;
                break;
            case GameEvent.SHOW_TRIAL_KILL:
                state = <InfoEvent buttonText={'Continue'}
                                   eventTitle={'Death sentence'}
                                   text={this.getTrialKillTarget()}
                                   socket={socket}
                                   game={game}
                                   player={player}/>;
                break;
            case GameEvent.GAME_OVER:
                state = <GameOver game={game} player={player} socket={socket}/>;
                break;
            default:
                state = <div>Invalid state {entry.id} detected</div>
        }
        return state;
    }
}
