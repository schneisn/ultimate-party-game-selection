import classNames from 'classnames';
import React from 'react';
import {Socket} from '../../../../../../server/socket';
import {WideButton} from '../../../../../common/component/wideButton/WideButton';
import {HeaderContainer} from '../../../../../common/component/headerContainer/HeaderContainer';
import {InfoCard} from '../../../../../common/component/infoCard/InfoCard';
import {NextStateEvent} from '../../../../event/NextStateEvent';
import {IGame} from '../../../../model/IGame';
import {IPlayer} from '../../../../model/IPlayer';
import {playerDiedInCurrentNight} from '../../../../utils/stateUtils';
import styles from './EndNight.module.scss';
import {PlayerNameAndRole} from '../../../playerNameAndRole/PlayerNameAndRole';
import {playerIsGameMaster} from '../../../../utils/playerUtils';
import {ContinueInfo} from '../continueInfo/ContinueInfo';

interface IProps {
    game: IGame;
    player: IPlayer;
    socket: Socket;
}

export class EndNight extends React.Component<IProps> {

    render() {
        return (
            <>
                <InfoCard titleText={'End of night'}
                          additionalClassNames={styles.WWPrimary}>
                    The village is waking up
                </InfoCard>
                <HeaderContainer header={this.getEndNightTitle()}
                                 additionalClassNames={classNames(styles.WWSecondary, styles.EndNight)}>
                    {this.getKilledPlayerElements()}
                </HeaderContainer>
                {playerIsGameMaster(this.props.player) ?
                    <WideButton onClick={this.onEndNightButtonClick}
                                additionalClassNames={styles.WWTertiary}>
                        End night
                    </WideButton> :
                    <ContinueInfo/>}
            </>
        )
    }

    private getKilledPlayerElements(): JSX.Element[] {
        const killedPlayers = this.getKilledPlayers();
        if (killedPlayers.length < 1) {
            return null;
        }
        return killedPlayers.map(target =>
            <div key={target.id}>
                <PlayerNameAndRole game={this.props.game} player={target}/>
            </div>
        );
    };

    private getKilledPlayers(): IPlayer[] {
        const {game} = this.props;
        return game.players.filter(player => playerDiedInCurrentNight(game, player.id));
    }

    private getEndNightTitle(): string {
        const killedPlayers = this.getKilledPlayers();
        return killedPlayers.length < 1 ? 'Everybody wakes up again' : 'The following people died:';
    }

    private onEndNightButtonClick = (): void => {
        const {socket, game} = this.props;
        socket.emit(NextStateEvent.id, new NextStateEvent(game.id));
    }
}
