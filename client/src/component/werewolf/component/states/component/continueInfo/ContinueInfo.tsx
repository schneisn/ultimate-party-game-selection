import React from 'react';
import {SmallCard} from '../../../../../common/component/smallCard/SmallCard';
import styles from '../../../../styles/color.module.scss';
import {IconContent} from '../../../../../common/component/iconContent/IconContent';
import {IconType} from '../../../../../common/component/icon/Icon';

/**
 * An info card to display an information that the gm will continue
 */
export class ContinueInfo extends React.PureComponent {

    render() {
        return (
            <SmallCard additionalClassNames={styles.WWSecondary}>
                <IconContent iconLeft={{
                    icon: IconType.infoCircle,
                }}>
                    The game master will continue the game
                </IconContent>
            </SmallCard>
        );
    }
}
