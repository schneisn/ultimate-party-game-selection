import React from 'react';
import {Socket} from '../../../../../../server/socket';
import {InfoCard} from '../../../../../common/component/infoCard/InfoCard';
import {IGame} from '../../../../model/IGame';
import {IPlayer, Role} from '../../../../model/IPlayer';
import styles from '../../../../styles/color.module.scss';
import {VoteList} from '../../../voteList/VoteList';
import {StateEvent} from '../../../../model/IStateEvent';

interface IProps {
    game: IGame;
    player: IPlayer;
    socket: Socket;
}

export class Trial extends React.PureComponent<IProps> {

    render() {
        return (
            <div>
                <InfoCard titleText={'Trial'}
                          additionalClassNames={styles.WWPrimary}>
                    The villagers come together to discuss what happened during the night.
                    They vote for a player they want to kill.
                </InfoCard>
                <VoteList game={this.props.game}
                          player={this.props.player}
                          playerCanSelectOwnRole={true}
                          voteType={StateEvent.TRIAL_KILL}
                          buttonIsHidden={this.props.player.role !== Role.GAME_MASTER}
                          socket={this.props.socket}/>
            </div>
        )
    }
}
