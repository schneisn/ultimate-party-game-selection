import React from 'react';
import {Socket} from '../../../../../../server/socket';
import {InfoCard} from '../../../../../common/component/infoCard/InfoCard';
import {IGame} from '../../../../model/IGame';
import {IPlayer, Role} from '../../../../model/IPlayer';
import styles from '../../../../styles/color.module.scss';
import {VoteList} from '../../../voteList/VoteList';
import {StateEvent} from '../../../../model/IStateEvent';

interface IProps {
    game: IGame;
    player: IPlayer;
    socket: Socket;
}

export class Mayor extends React.PureComponent<IProps> {

    render() {
        return (
            <>
                <InfoCard titleText={'Elect a Mayor'}
                          additionalClassNames={styles.WWPrimary}>
                    <div>The village needs a mayor. Vote for the player you think is most trustworthy.</div>
                    <div>The mayor has a voting power of 1.5 compared to other players</div>
                </InfoCard>
                <VoteList socket={this.props.socket}
                          game={this.props.game}
                          player={this.props.player}
                          voteType={StateEvent.MAYOR_ELECTED}
                          activeButtonText={'Elect mayor'}
                          buttonIsHidden={this.props.player.role !== Role.GAME_MASTER}
                          playerCanSelectOwnRole={true}/>
            </>
        )
    }
}
