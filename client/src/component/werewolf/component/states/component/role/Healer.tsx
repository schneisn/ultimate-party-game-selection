import React from 'react';
import {Socket} from '../../../../../../server/socket';
import {InfoCard} from '../../../../../common/component/infoCard/InfoCard';
import {IGame} from '../../../../model/IGame';
import {IPlayer} from '../../../../model/IPlayer';
import styles from '../../../../styles/color.module.scss';
import {VoteList} from '../../../voteList/VoteList';
import {playerIsGameMaster} from '../../../../utils/playerUtils';
import {StateEvent} from '../../../../model/IStateEvent';
import {playerCanSeeCurrentState} from '../../../../utils/stateUtils';

interface IProps {
    game: IGame;
    player: IPlayer;
    socket: Socket;
}

export class Healer extends React.PureComponent<IProps> {

    render() {
        const {player, socket, game} = this.props;
        return (
            <>
                <InfoCard titleText={'Healer'}
                          additionalClassNames={styles.WWPrimary}>
                    The healer is waking up. They can choose a player to heal.
                    If that player were targeted by the werewolves or the witch, they would survive
                </InfoCard>
                {playerCanSeeCurrentState(game, player) &&
                <VoteList socket={socket}
                          game={game}
                          player={player}
                          voteType={StateEvent.HEALER_HEAL}
                          playerCanSelectOwnRole={true}
                          buttonIsHidden={!playerIsGameMaster(player)}
                          buttonIsDisabled={!player.isAlive}/>}
            </>
        )
    }

}
