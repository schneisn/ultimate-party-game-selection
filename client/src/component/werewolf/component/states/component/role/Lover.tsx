import React from 'react';
import {Socket} from '../../../../../../server/socket';
import {IGame} from '../../../../model/IGame';
import {IPlayer} from '../../../../model/IPlayer';
import {getLovers} from '../../../../utils/roleUtils';
import {InfoEvent} from '../../../infoEvent/InfoEvent';
import {playerCanSeeCurrentState} from '../../../../utils/stateUtils';
import styles from '../../../../styles/color.module.scss';
import {InfoCard} from '../../../../../common/component/infoCard/InfoCard';

interface IProps {
    game: IGame;
    player: IPlayer;
    socket: Socket;
}

export class Lover extends React.PureComponent<IProps> {

    render() {
        const {player, socket, game} = this.props;
        const lovers = getLovers(game);
        if (!lovers) {
            return null;
        }
        return (
            <>
                <InfoEvent buttonText={'Go to sleep'}
                           eventTitle={'Lovers'}
                           text={'The lovers look into each others eyes and fall in love eternally'}
                           game={game}
                           player={player}
                           socket={socket}>
                    {playerCanSeeCurrentState(game, player) &&
                    <InfoCard additionalClassNames={styles.WWSecondary}>
                      <div>{lovers[0].name + ' and ' + lovers[1].name + ' are lovers.'}</div>
                      <div>If one of them dies, the other one dies, too</div>
                    </InfoCard>}
                </InfoEvent>
            </>
        )
    }
}
