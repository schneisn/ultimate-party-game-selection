import React from 'react';
import {Socket} from '../../../../../../server/socket';
import {InfoCard} from '../../../../../common/component/infoCard/InfoCard';
import {IGame} from '../../../../model/IGame';
import {IPlayer} from '../../../../model/IPlayer';
import styles from '../../../../styles/color.module.scss';
import {VoteList} from '../../../voteList/VoteList';
import {playerCanSeeCurrentState, playerIsActiveHunter} from '../../../../utils/stateUtils';
import {playerIsGameMaster} from '../../../../utils/playerUtils';
import {StateEvent} from '../../../../model/IStateEvent';

interface IProps {
    game: IGame;
    player: IPlayer;
    socket: Socket;
}

export class Hunter extends React.PureComponent<IProps> {

    render() {
        const {player, socket, game} = this.props;
        return (
            <>
                <InfoCard titleText={'Hunter'}
                          additionalClassNames={styles.WWPrimary}>
                    With his last breath the hunter shoots another player
                </InfoCard>
                {playerCanSeeCurrentState(game, player) &&
                <VoteList socket={socket}
                          game={game}
                          player={player}
                          voteType={StateEvent.HUNTER_KILL}
                          playerCanSelectOwnRole={true}
                          buttonIsHidden={!playerIsGameMaster(player)}
                          buttonIsDisabled={!player.isAlive && !playerIsActiveHunter(game, player)}/>}
            </>
        )
    }
}
