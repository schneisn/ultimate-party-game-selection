import React from 'react';
import {Socket} from '../../../../../../server/socket';
import {InfoCard} from '../../../../../common/component/infoCard/InfoCard';
import {IGame} from '../../../../model/IGame';
import {IPlayer, Role} from '../../../../model/IPlayer';
import styles from '../../../../styles/color.module.scss';
import {VoteList} from '../../../voteList/VoteList';
import {playerIsGameMaster} from '../../../../utils/playerUtils';
import {StateEvent} from '../../../../model/IStateEvent';
import {playerCanSeeCurrentState} from '../../../../utils/stateUtils';

interface IProps {
    game: IGame;
    player: IPlayer;
    socket: Socket;
}

export class Seer extends React.Component<IProps> {

    render() {
        const {player, socket, game} = this.props;
        return (
            <>
                <InfoCard titleText={'Seer'}
                          additionalClassNames={styles.WWPrimary}>
                    The seer is waking up. They can choose a player to see if the player is a werewolf
                </InfoCard>
                {playerCanSeeCurrentState(game, player) &&
                <VoteList socket={socket}
                          game={game}
                          player={player}
                          activeButtonText={'Show if the player is a werewolf'}
                          buttonIsDisabled={!player.isAlive}
                          voteType={StateEvent.SEER_SELECT}
                          buttonIsHidden={!playerIsGameMaster(player) && player.role !== Role.SEER}
                          disabledButtonText={'Select a player to see if they are a werewolf'}/>}
            </>
        )
    }

}
