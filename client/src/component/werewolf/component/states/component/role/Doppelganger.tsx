import React from 'react';
import {Socket} from '../../../../../../server/socket';
import {InfoCard} from '../../../../../common/component/infoCard/InfoCard';
import {IGame} from '../../../../model/IGame';
import {IPlayer} from '../../../../model/IPlayer';
import styles from '../../../../styles/color.module.scss';
import {VoteList} from '../../../voteList/VoteList';
import {playerIsGameMaster} from '../../../../utils/playerUtils';
import {StateEvent} from '../../../../model/IStateEvent';
import {playerCanSeeCurrentState} from '../../../../utils/stateUtils';

interface IProps {
    game: IGame;
    player: IPlayer;
    socket: Socket;
}

export class Doppelganger extends React.PureComponent<IProps> {

    render() {
        const {player, socket, game} = this.props;
        return (
            <>
                <InfoCard titleText={'Doppelganger'}
                          additionalClassNames={styles.WWPrimary}>
                    The doppelganger is waking up. It chooses a player. If the player dies, the doppelganger takes the role of this player
                </InfoCard>
                {playerCanSeeCurrentState(game, player) &&
                <VoteList socket={socket}
                          game={game}
                          player={player}
                          voteType={StateEvent.DOPPELGANGER_SELECT}
                          playerCanSelectOwnRole={false}
                          buttonIsHidden={!playerIsGameMaster(player)}
                          buttonIsDisabled={!player.isAlive}/>}
            </>
        )
    }

}
