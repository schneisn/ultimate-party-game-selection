import React from 'react';
import {NextStateEvent} from '../../../../event/NextStateEvent';
import {IGame} from '../../../../model/IGame';
import {IPlayer, Role} from '../../../../model/IPlayer';
import {Socket} from '../../../../../../server/socket';
import {InfoCard} from '../../../../../common/component/infoCard/InfoCard';
import styles from './SeerSelection.module.scss';
import {SmallCard} from '../../../../../common/component/smallCard/SmallCard';
import {getLatestEvent, playerCanSeeCurrentState} from '../../../../utils/stateUtils';
import {StateEvent} from '../../../../model/IStateEvent';
import {playerIsGameMaster} from '../../../../utils/playerUtils';
import {WideButton} from '../../../../../common/component/wideButton/WideButton';
import classNames from 'classnames';
import {getPlayer} from '../../../../../common/utils/player';

interface IProps {
    game: IGame;
    player: IPlayer;
    socket: Socket;
}

/**
 * Shows if the selected player by the seer is a werewolf
 */
export class SeerSelection extends React.PureComponent<IProps> {

    render() {
        const {game, player} = this.props;
        const seerTarget = this.getSeerSelection();
        return (
            <>
                <InfoCard titleText={'Seer'}
                          additionalClassNames={styles.WWPrimary}>
                    The seer chose their target and sees whether the player is a werewolf.
                </InfoCard>
                {playerCanSeeCurrentState(game, player) &&
                <SmallCard additionalClassNames={classNames(styles.IsWerewolf, styles.WWSecondary)}>
                    {seerTarget?.name} is {seerTarget.role === Role.WEREWOLF ? 'a werewolf' : 'not a werewolf'}
                </SmallCard>}
                {playerIsGameMaster(player) &&
                <WideButton additionalClassNames={styles.WWTertiary}
                            onClick={this.handleContinueClick}>
                  Continue
                </WideButton>}
            </>
        );
    }

    private getSeerSelection(): IPlayer {
        const seerTargetId = getLatestEvent(this.props.game, StateEvent.SEER_SELECT)?.targetId;
        return getPlayer(this.props.game, seerTargetId);
    };

    private handleContinueClick = () => {
        const {socket, game} = this.props;
        socket.emit(NextStateEvent.id, new NextStateEvent(game.id));
    };

}
