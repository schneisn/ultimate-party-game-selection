import React from 'react';
import {Socket} from '../../../../../../server/socket';
import {WideButton} from '../../../../../common/component/wideButton/WideButton';
import {InfoCard} from '../../../../../common/component/infoCard/InfoCard';
import {getPlayer} from '../../../../../common/utils/player';
import {IGame} from '../../../../model/IGame';
import {IPlayer} from '../../../../model/IPlayer';
import {StateEvent} from '../../../../model/IStateEvent';
import styles from '../../../../styles/color.module.scss';
import {getEventInCurrentNight, getLatestEvent, playerCanSeeCurrentState} from '../../../../utils/stateUtils';
import {VoteList} from '../../../voteList/VoteList';
import {cloneDeep} from 'lodash';
import {playerIsGameMaster} from '../../../../utils/playerUtils';
import {VoteItem} from '../../../voteList/component/voteItem/VoteItem';
import {targetHasVoteFromPlayer} from '../../../../utils/voteUtils';
import {SmallCard} from '../../../../../common/component/smallCard/SmallCard';
import {HeaderContainer} from '../../../../../common/component/headerContainer/HeaderContainer';
import {CloseVoteEvent} from '../../../voteList/event/CloseVoteEvent';
import {playerCanSeeRole} from '../../../../utils/roleUtils';

interface IProps {
    game: IGame;
    player: IPlayer;
    socket: Socket;
}

export class Witch extends React.Component<IProps> {

    render() {
        const {player, game} = this.props;
        return (
            <>
                <InfoCard titleText={'Witch'}
                          additionalClassNames={styles.WWPrimary}>
                    The witch is waking up. She can heal a player once and kill a player once
                </InfoCard>
                {playerCanSeeCurrentState(game, player) && this.showHeal() && this.getHealCard()}
                {playerCanSeeCurrentState(game, player) && this.showKill() && this.getKillCard()}
            </>
        )
    }

    private showHeal(): boolean {
        const {game} = this.props;
        const latestHealEvent = getLatestEvent(game, StateEvent.WITCH_HEAL);
        const healIsAlreadyUsed = !!latestHealEvent?.targetId;
        const healEventAlreadyOccurredInCurrentNight = !!getEventInCurrentNight(game, StateEvent.WITCH_HEAL);
        return !healIsAlreadyUsed && !healEventAlreadyOccurredInCurrentNight;
    };

    private showKill(): boolean {
        const {game} = this.props;
        const healedPlayer = getLatestEvent(game, StateEvent.WITCH_HEAL)?.targetId;
        const playerAlreadyHealed = !!getPlayer(game, healedPlayer);
        const healEventAlreadyOccurredInCurrentNight = !!getEventInCurrentNight(game, StateEvent.WITCH_HEAL);
        const killedPlayer = getLatestEvent(game, StateEvent.WITCH_KILL)?.targetId;
        const playerAlreadyKilled = !!getPlayer(game, killedPlayer);
        return (playerAlreadyHealed || healEventAlreadyOccurredInCurrentNight) && !playerAlreadyKilled;
    };

    private getWerewolfTarget(): IPlayer {
        const werewolfKill = getLatestEvent(this.props.game, StateEvent.WEREWOLF_KILL);
        return getPlayer(this.props.game, werewolfKill.targetId);
    };

    private handleClickHealPlayer = () => {
        const {socket, game} = this.props;
        socket.emit(CloseVoteEvent.id, new CloseVoteEvent(game.id, StateEvent.WITCH_HEAL));
    };

    private getHealCard(): JSX.Element {
        const werewolfTarget = this.getWerewolfTarget();
        const targetId = werewolfTarget?.id;
        if (!targetId) {
            return null;
        }
        const {game, player, socket} = this.props;
        // Edit copy of target to prevent overriding original target
        const targetCopy = cloneDeep(werewolfTarget);
        targetCopy.name = `Heal ${werewolfTarget.name}`;
        // A dummy player to represent not healing anyone
        const noHealTarget: IPlayer = {
            id: 'no-heal',
            isAlive: true,
            role: null,
            isComputer: true,
            name: `Do not heal ${werewolfTarget.name}`,
            isConnected: true
        }
        return <>
            <HeaderContainer additionalClassNames={styles.WWSecondary}>
                <VoteItem key={targetId}
                          areVotesVisible={playerIsGameMaster(player)}
                          disabled={!player.isAlive}
                          game={game}
                          isRoleVisible={playerCanSeeRole(player, targetCopy)}
                          player={player}
                          isSelected={targetHasVoteFromPlayer(game, player, targetCopy)}
                          socket={socket}
                          target={targetCopy}/>
                <VoteItem key={noHealTarget.id}
                          areVotesVisible={playerIsGameMaster(player)}
                          disabled={!player.isAlive}
                          game={game}
                          isRoleVisible={false}
                          player={player}
                          isSelected={targetHasVoteFromPlayer(game, player, noHealTarget)}
                          socket={socket}
                          target={noHealTarget}/>
            </HeaderContainer>
            {playerIsGameMaster(player) ?
                <WideButton onClick={this.handleClickHealPlayer}
                            disabled={!this.props.player.isAlive}
                            additionalClassNames={styles.WWTertiary}>
                    Confirm
                </WideButton> :
                <SmallCard additionalClassNames={styles.WWSecondary}>The game master will continue the game</SmallCard>}
        </>;
    }

    private getKillCard(): JSX.Element {
        const {player, socket, game} = this.props;
        // A dummy player to represent not killing anyone
        const noKillTarget: IPlayer = {
            id: 'no-kill',
            isAlive: true,
            role: null,
            isComputer: true,
            name: `I do not want to kill anyone`,
            isConnected: true
        }
        return <>
            <HeaderContainer additionalClassNames={styles.WWSecondary}>
                <VoteItem key={noKillTarget.id}
                          areVotesVisible={playerIsGameMaster(player)}
                          disabled={!player.isAlive}
                          game={game}
                          isRoleVisible={false}
                          player={player}
                          isSelected={targetHasVoteFromPlayer(game, player, noKillTarget)}
                          socket={socket}
                          target={noKillTarget}/>
            </HeaderContainer>
            <VoteList socket={socket}
                      game={game}
                      player={player}
                      listHeader={'Select a player to kill'}
                      voteType={StateEvent.WITCH_KILL}
                      activeButtonText={'Confirm'}
                      buttonIsDisabled={!player.isAlive}
                      buttonIsHidden={!playerIsGameMaster(player)}
                      playerCanSelectOwnRole={false}/>
        </>;
    }
}
