import React from 'react';
import {Socket} from '../../../../../../server/socket';
import {InfoCard} from '../../../../../common/component/infoCard/InfoCard';
import {IGame} from '../../../../model/IGame';
import {IPlayer} from '../../../../model/IPlayer';
import styles from '../../../../styles/color.module.scss';
import {VoteList} from '../../../voteList/VoteList';
import {playerIsGameMaster} from '../../../../utils/playerUtils';
import {playerCanSeeCurrentState} from '../../../../utils/stateUtils';

interface IProps {
    game: IGame;
    player: IPlayer;
    socket: Socket;
}

export class Armor extends React.PureComponent<IProps> {

    render() {
        const {player, socket, game} = this.props;
        return (
            <>
                <InfoCard titleText={'Armor'}
                          additionalClassNames={styles.WWPrimary}>
                    The armor is waking up. He can choose two players. They fall in love eternally.
                    If one player dies, the other dies of a broken heart, too
                </InfoCard>
                {playerCanSeeCurrentState(game, player) &&
                <VoteList socket={socket}
                          game={game}
                          player={player}
                          buttonIsDisabled={game.votes.length < 2}
                          buttonIsHidden={!playerIsGameMaster(player)}
                          playerCanSelectOwnRole={true}/>}
            </>
        )
    }

}
