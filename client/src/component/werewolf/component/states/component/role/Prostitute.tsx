import React from 'react';
import {Socket} from '../../../../../../server/socket';
import {InfoCard} from '../../../../../common/component/infoCard/InfoCard';
import {IGame} from '../../../../model/IGame';
import {IPlayer} from '../../../../model/IPlayer';
import styles from '../../../../styles/color.module.scss';
import {VoteList} from '../../../voteList/VoteList';
import {playerIsGameMaster} from '../../../../utils/playerUtils';
import {StateEvent} from '../../../../model/IStateEvent';
import {playerCanSeeCurrentState} from '../../../../utils/stateUtils';

interface IProps {
    game: IGame;
    player: IPlayer;
    socket: Socket;
}

export class Prostitute extends React.Component<IProps> {
    render() {
        const {player, socket, game} = this.props;
        return (
            <>
                <InfoCard titleText={'Prostitute'}
                          additionalClassNames={styles.WWPrimary}>
                    The prostitute is waking up. She chooses a player she wants to be with during the night.
                    If the werewolves kill the player she is with, she dies, too.
                    If the werewolves target her, she survives.
                </InfoCard>
                {playerCanSeeCurrentState(game, player) &&
                <VoteList socket={socket}
                          game={game}
                          player={player}
                          voteType={StateEvent.PROSTITUTE_SELECT}
                          buttonIsHidden={!playerIsGameMaster(player)}
                          buttonIsDisabled={!player.isAlive}/>}
            </>
        )
    }

}
