import React from 'react';
import {Socket} from '../../../../../../server/socket';
import {InfoCard} from '../../../../../common/component/infoCard/InfoCard';
import {IGame} from '../../../../model/IGame';
import {IPlayer} from '../../../../model/IPlayer';
import styles from '../../../../styles/color.module.scss';
import {playerIsGameMaster} from '../../../../utils/playerUtils';
import {VoteList} from '../../../voteList/VoteList';
import {StateEvent} from '../../../../model/IStateEvent';
import {playerCanSeeCurrentState} from '../../../../utils/stateUtils';

interface IProps {
    game: IGame;
    player: IPlayer;
    socket: Socket;
}

export class Werewolves extends React.PureComponent<IProps> {

    render() {
        const {player, socket, game} = this.props;
        return (
            <>
                <InfoCard titleText={'Werewolves'}
                          additionalClassNames={styles.WWPrimary}>
                    The werewolves are waking up. They choose their target silently
                </InfoCard>
                {playerCanSeeCurrentState(game, player) &&
                <VoteList socket={socket}
                          game={game}
                          player={player}
                          voteType={StateEvent.WEREWOLF_KILL}
                          playerCanSelectOwnRole={true}
                          buttonIsHidden={!playerIsGameMaster(player)}/>}
            </>
        )
    }

}
