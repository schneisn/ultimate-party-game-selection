import {GameEvent} from '../../../../../../common/event/GameEvent';

export class ResetGameEvent extends GameEvent {
    static readonly id = 'ResetGame';
}
