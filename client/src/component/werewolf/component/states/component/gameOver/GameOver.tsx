import React from 'react';
import {Socket} from '../../../../../../server/socket';
import {WideButton} from '../../../../../common/component/wideButton/WideButton';
import {Emoji} from '../../../../../common/component/emoji/Emoji.';
import {HeaderContainer} from '../../../../../common/component/headerContainer/HeaderContainer';
import {InfoCard} from '../../../../../common/component/infoCard/InfoCard';
import {IGame} from '../../../../model/IGame';
import {IPlayer} from '../../../../model/IPlayer';
import styles from './GameOver.module.scss';
import {getPlayer, playerIsGameMaster} from '../../../../utils/playerUtils';
import {ResetGameEvent} from './event/ResetGameEvent';
import {PlayerNameAndRole} from '../../../playerNameAndRole/PlayerNameAndRole';
import classNames from 'classnames';
import {getCurrentState} from '../../../../utils/stateUtils';
import {StateEvent} from '../../../../model/IStateEvent';
import {UrlUtils} from '../../../../../common/utils/url';
import {RouteComponentProps, withRouter} from 'react-router-dom';

interface IProps extends RouteComponentProps {
    game: IGame;
    player: IPlayer;
    socket: Socket;
}

class GameOver extends React.PureComponent<IProps> {

    render() {
        return (
            <>
                <InfoCard titleText={'Game Over'}
                          additionalClassNames={styles.WWPrimary}>
                    {this.getWinText()} <Emoji symbol={'🎉'}/>
                </InfoCard>
                {this.showWinningPlayers()}
                {playerIsGameMaster(this.props.player) &&
                <WideButton additionalClassNames={styles.WWTertiary}
                            onClick={() => this.props.socket.emit(ResetGameEvent.id, new ResetGameEvent(this.props.game.id))}>
                  Go to lobby
                </WideButton>}
                <WideButton additionalClassNames={classNames(styles.LeaveButton, styles.WWTertiary)}
                            onClick={() => UrlUtils.goHierarchyUp(this.props.history)}>
                    Leave game
                </WideButton>
            </>
        )
    }

    private getWinText(): string {
        const currentState = getCurrentState(this.props.game);
        if (currentState.events.some(e => e.id === StateEvent.LOVERS_WON)) {
            return 'The lovers won the game!';
        }
        if (currentState.events.some(e => e.id === StateEvent.WEREWOLVES_WON)) {
            return 'The werewolves won the game!';
        }
        return 'The citizens won the game!';
    }

    private showWinningPlayers = () => {
        const currentState = getCurrentState(this.props.game);
        const winnerDivs = currentState.events
            .filter(ev => ev.id === StateEvent.WINNER)
            .map(ev => ev.targetId)
            .map(winnerId => getPlayer(this.props.game, winnerId))
            .map(winner => this.getWinningPlayerDiv(winner));
        return <HeaderContainer header={'The winners are...'}
                                additionalClassNames={styles.WWSecondary}>
            {winnerDivs}
        </HeaderContainer>;
    };

    private getWinningPlayerDiv(player: IPlayer): JSX.Element {
        return <div key={player.id}>
            <PlayerNameAndRole game={this.props.game} player={player}/>
        </div>;
    }
}

export default withRouter(GameOver);
