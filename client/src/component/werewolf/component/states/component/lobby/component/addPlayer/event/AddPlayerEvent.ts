import {GameEvent} from '../../../../../../../../common/event/GameEvent';

export class AddPlayerEvent extends GameEvent {
    static readonly id = 'AddPlayer';
    playerName: string;

    constructor(gameId: string, playerName: string) {
        super(gameId);
        this.playerName = playerName;
    }
}
