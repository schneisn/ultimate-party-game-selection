import {GameEvent} from '../../../../../../common/event/GameEvent';

export class StartGameEvent extends GameEvent {
    static readonly id = 'StartGame';
}
