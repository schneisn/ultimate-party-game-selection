import classNames from 'classnames';
import React from 'react';
import {Socket} from '../../../../../../server/socket';
import {WideButton} from '../../../../../common/component/wideButton/WideButton';
import {Card} from '../../../../../common/component/card/Card';
import {HeaderContainer} from '../../../../../common/component/headerContainer/HeaderContainer';
import {IconType} from '../../../../../common/component/icon/Icon';
import {IconContent} from '../../../../../common/component/iconContent/IconContent';
import {InfoCard} from '../../../../../common/component/infoCard/InfoCard';
import {IGame} from '../../../../model/IGame';
import {IPlayer} from '../../../../model/IPlayer';
import {playerIsGameMaster} from '../../../../utils/playerUtils';
import {AddPlayer} from './component/addPlayer/AddPlayer';
import {RemovePlayerEvent} from './event/RemovePlayerEvent';
import {StartGameEvent} from './event/StartGameEvent';
import styles from './Lobby.module.scss';

export interface ILobbyClassNames {
    infoCardClassNames: string;
    playerContainerClassNames: string;
    playerCardsClassNames: string;
    startGameButtonClassNames: string;
}

interface IProps {
    additionalClassNames?: ILobbyClassNames
    game: IGame;
    player: IPlayer;
    socket: Socket;
}

export class Lobby extends React.Component<IProps> {

    render() {
        return (
            <div>
                <InfoCard titleText={'Lobby'} additionalClassNames={styles.WWPrimary}>
                    Add players or wait for other players to join
                </InfoCard>
                <HeaderContainer header={'Players'} additionalClassNames={styles.WWSecondary}>
                    {this.listPlayers()}
                </HeaderContainer>
                {playerIsGameMaster(this.props.player) && <AddPlayer socket={this.props.socket} player={this.props.player} game={this.props.game}/>}
                <WideButton additionalClassNames={classNames(styles.StartGameButton, styles.WWTertiary)}
                            onClick={this.onStartGame}
                            disabled={!playerIsGameMaster(this.props.player) || !this.gameHasEnoughPlayers()}>
                    {this.getButtonText()}
                </WideButton>
            </div>
        )
    }

    private listPlayers(): JSX.Element {
        if (this.props.game.players.length < 1) {
            return <div>No players in game</div>
        }
        let players = this.props.game.players.map((player) =>
            <Card key={player.id}
                  additionalClassNames={classNames(styles.PlayerCard, styles.WWTertiary)}>
                <IconContent iconRight={{
                    icon: playerIsGameMaster(this.props.player) && player.isComputer ? IconType.trash : null,
                    additionalIconClassnames: styles.TrashIcon,
                    onIconClick: () => this.handleRemovePlayerClick(player)
                }}>
                    {player.name}
                </IconContent>
            </Card>);
        return <>{players}</>;
    };

    private handleRemovePlayerClick = (player: IPlayer): void => {
        this.props.socket.emit(RemovePlayerEvent.id, new RemovePlayerEvent(this.props.game.id, player));
    }

    private getButtonText(): string {
        if (!playerIsGameMaster(this.props.player)) {
            return 'The Game-Master will start the game';
        } else if (!this.gameHasEnoughPlayers()) {
            return 'You need at least 4 players to start the game';
        }
        return 'Start Game. Nobody can join afterwards!';
    };

    private gameHasEnoughPlayers(): boolean {
        return this.props.game.players.length > 3;
    }

    private onStartGame = (): void => {
        this.props.socket.emit(StartGameEvent.id, new StartGameEvent(this.props.game.id));
    }
}
