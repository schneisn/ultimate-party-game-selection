import {GameEvent} from '../../../../../../common/event/GameEvent';
import {IIdNameModel} from '../../../../../../common/event/model/IIdNameModel';
import {ICommonPlayer} from '../../../../../../common/model/ICommonPlayer';

export class RemovePlayerEvent extends GameEvent {
    static readonly id = 'RemovePlayer';
    removePlayer: IIdNameModel;

    constructor(gameId: string, player: ICommonPlayer) {
        super(gameId);
        this.removePlayer = new IIdNameModel(player.id, player.name);
    }
}
