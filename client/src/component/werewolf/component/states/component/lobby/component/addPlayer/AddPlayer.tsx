import React from 'react';
import {Socket} from '../../../../../../../../server/socket';
import {PLAYER_NAME_MAX_LENGTH} from '../../../../../../../common/component/changeName/ChangeName';
import {TextAreaWithButton} from '../../../../../../../common/component/textAreaWithButton/TextAreaWithButton';
import {StringUtils} from '../../../../../../../common/utils/string';
import {IGame} from '../../../../../../model/IGame';
import {IPlayer} from '../../../../../../model/IPlayer';
import styles from '../../../../../../styles/color.module.scss';
import {AddPlayerEvent} from './event/AddPlayerEvent';

interface IProps {
    game: IGame;
    player: IPlayer;
    socket: Socket;
}

interface IState {
    playerName: string;
}

export class AddPlayer extends React.Component<IProps, IState> {

    constructor(props: Readonly<IProps>) {
        super(props);
        this.state = {
            playerName: ''
        }
    }

    render() {
        return (
            <TextAreaWithButton textPlaceholder={'Player\'s name'}
                                textValue={this.state.playerName}
                                onChange={this.onNameChange}
                                onSubmit={this.onButtonClick}
                                buttonIsDisabled={this.cannotAddPlayer()}
                                additionalButtonClasses={styles.WWTertiary}
                                buttonText={this.gameContainsPlayerName() ? 'Game already contains name' : 'Add player'}
                                textMaxLength={PLAYER_NAME_MAX_LENGTH}/>
        )
    }

    private onNameChange = (event: React.FormEvent<HTMLInputElement>) => {
        this.setState({
            playerName: event.currentTarget.value,
        })
    };

    private onButtonClick = (input: string) => {
        if (StringUtils.isEmpty(input) || this.gameContainsPlayerName()) {
            return;
        }
        this.props.socket.emit(AddPlayerEvent.id, new AddPlayerEvent(this.props.game.id, this.state.playerName));

        this.setState({
            playerName: '',
        })
    };

    private gameContainsPlayerName = () => {
        for (const player of this.props.game.players) {
            if (player.name === this.state.playerName.trim()) {
                return true;
            }
        }
        return false;
    };

    private cannotAddPlayer(): boolean {
        return !this.state.playerName || !StringUtils.removeBlanks(this.state.playerName).length || this.gameContainsPlayerName();
    }
}
