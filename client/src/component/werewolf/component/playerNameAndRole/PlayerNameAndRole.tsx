import React from 'react';
import {isLover} from '../../utils/roleUtils';
import {Popover} from '../../../common/component/popover/Popover';
import {IconType} from '../../../common/component/icon/Icon';
import {generatePopoverStyles} from '../../../common/component/popover/utils/styles';
import styles from './PlayerNameAndRole.module.scss';
import {RolePopover} from '../rolePopover/RolePopover';
import {IGame} from '../../model/IGame';
import {IPlayer} from '../../model/IPlayer';
import {getPlayer} from '../../../common/utils/player';

interface IProps {
    game: IGame;
    player: IPlayer;
}

/**
 * Displays the players name,
 * the role,
 * and if they are a lover.
 *
 * If the prop player is not a real player (id is not correct),
 * do not show a role
 */
export class PlayerNameAndRole extends React.Component<IProps> {

    render() {
        const {game, player} = this.props;
        if (!player) {
            return <span/>;
        }
        const isValidPlayer = !!getPlayer(game, player.id);
        return (
            <span>
                {player.name}
                {isValidPlayer && <RolePopover role={player.role}
                                               additionalIconClassNames={styles.Role}
                                               iconId={'player-name-role' + player.id}/>}
                {isLover(game, player.id) &&
                <Popover id={'player-name-lover'}
                         icon={IconType.heart}
                         iconTitle={'Lover'}
                         customStyles={generatePopoverStyles(styles.PlayerNameLover)}>
                  Lover
                </Popover>}
            </span>
        );
    }
}
