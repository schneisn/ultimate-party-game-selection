import React from 'react';
import {IconType} from '../../../common/component/icon/Icon';
import {Popover} from '../../../common/component/popover/Popover';
import {generatePopoverStyles} from '../../../common/component/popover/utils/styles';
import {StringUtils} from '../../../common/utils/string';
import {IGame} from '../../model/IGame';
import {IPlayer} from '../../model/IPlayer';
import {playerIsGameMaster} from '../../utils/playerUtils';
import {getLovePartner, isLover, roleDescriptions} from '../../utils/roleUtils';
import styles from './RoleInfo.module.scss';

export enum HighlightedRole {
    Lover,
    Mayor,
    Role
}

interface IProps {
    game: IGame;
    player: IPlayer;
    /**
     * Open the popover so that the player
     * sees a change in their role/status
     */
    highlightRole?: HighlightedRole;
}

export class RoleInfo extends React.Component<IProps> {

    render() {
        const {game, player, highlightRole} = this.props;
        const popoverPosition = 'right-start';

        return (
            <div className={styles.RoleCard}>
                {!player.isAlive &&
                <Popover id={'aliveStatus'}
                         icon={IconType.skullCrossbones}
                         placement={popoverPosition}
                         customStyles={generatePopoverStyles(styles.RoleInfoIcon)}>
                  <div>
                    Your are dead
                  </div>
                </Popover>}
                {isLover(game, player.id) &&
                <Popover id={'lover'}
                         icon={IconType.heart}
                         isOpen={highlightRole === HighlightedRole.Lover}
                         placement={popoverPosition}
                         customStyles={generatePopoverStyles(styles.RoleInfoIcon)}>
                  <div>
                    You are in love with {getLovePartner(game, player.id).name}.
                  </div>
                  <div>
                    If one of you dies, the other one dies of a broken heart, too
                  </div>
                </Popover>}
                {player.id === game.mayor &&
                <Popover id={'mayor'}
                         icon={IconType.award}
                         isOpen={highlightRole === HighlightedRole.Mayor}
                         placement={popoverPosition}
                         customStyles={generatePopoverStyles(styles.RoleInfoIcon)}>
                  <div>
                    You are the mayor
                  </div>
                </Popover>}
                {player.role &&
                <Popover id={'role'}
                         icon={IconType.theaterMask}
                         isOpen={highlightRole === HighlightedRole.Role}
                         placement={popoverPosition}
                         customStyles={generatePopoverStyles(styles.RoleInfoIcon)}>
                  <div>
                    Your Role: {StringUtils.toFirstUpperCase(player.role)}
                  </div>
                  <div>
                      {roleDescriptions.get(player.role)}
                  </div>
                </Popover>}
                {!playerIsGameMaster(player) &&
                <Popover id={'gamemaster'}
                         icon={IconType.userTie}
                         placement={popoverPosition}
                         customStyles={generatePopoverStyles(styles.RoleInfoIcon)}>
                  <div>
                    Gamemaster: {game.gamemaster.name}
                  </div>
                </Popover>}
            </div>
        )
    }
}
