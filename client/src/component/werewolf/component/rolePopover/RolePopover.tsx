import React from 'react';
import {Role} from '../../model/IPlayer';
import {Popover} from '../../../common/component/popover/Popover';
import {StringUtils} from '../../../common/utils/string';
import {Button} from '../../../common/component/button/Button';
import styles from './RolePopover.module.scss';
import {Emoji} from '../../../common/component/emoji/Emoji.';
import classNames from 'classnames';

interface IProps {
    role: Role;
    iconId: string;
    additionalIconClassNames?: string;
}

/**
 * An popover with the icon of the given role
 */
export class RolePopover extends React.PureComponent<IProps> {

    render() {
        const {iconId, role} = this.props;
        let emoji = null;
        switch (role) {
            case Role.ARMOR:
                emoji = '💘';
                break;
            case Role.CHILD:
                emoji = '👶'
                break;
            case Role.CITIZEN:
                emoji = '👫'
                break;
            case Role.DOPPELGANGER:
                emoji = '🕴'
                break;
            case Role.HEALER:
                emoji = '👨‍⚕️'
                break;
            case Role.HUNTER:
                emoji = '🏹'
                break;
            case Role.PROSTITUTE:
                emoji = '👠'
                break;
            case Role.SEER:
                emoji = '👓'
                break;
            case Role.WEREWOLF:
                emoji = '🐺'
                break;
            case Role.WITCH:
                emoji = '🧙‍♀️'
                break;
        }
        return <Popover id={'roleIcon-' + iconId}
                        customElement={this.getCustomRoleIcon(emoji)}>
            {StringUtils.toFirstUpperCase(role)}
        </Popover>
    }

    private getCustomRoleIcon(emoji: string): JSX.Element {
        const {additionalIconClassNames} = this.props;
        return <Button additionalClassNames={classNames(additionalIconClassNames, styles.RolePopoverButton)}>
            <Emoji symbol={emoji} onClick={(event => event.preventDefault())}/>
        </Button>;
    }
}
