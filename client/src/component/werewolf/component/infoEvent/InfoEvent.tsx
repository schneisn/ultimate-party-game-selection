import React from 'react';
import {Socket} from '../../../../server/socket';
import {WideButton} from '../../../common/component/wideButton/WideButton';
import {InfoCard} from '../../../common/component/infoCard/InfoCard';
import {NextStateEvent} from '../../event/NextStateEvent';
import {IGame} from '../../model/IGame';
import {IPlayer, Role} from '../../model/IPlayer';
import styles from '../../styles/color.module.scss';

interface IProps {
    buttonText: string;
    eventTitle: string;
    text: string | JSX.Element;
    game: IGame;
    player: IPlayer;
    socket: Socket;
}

/**
 * An informative game event. Only the game master can continue.
 * Children are displayed between info card and button.
 */
export class InfoEvent extends React.Component<IProps> {
    render() {
        const {player, eventTitle, children, text, buttonText} = this.props;
        return (
            <>
                <InfoCard titleText={eventTitle}
                          additionalClassNames={styles.WWPrimary}>
                    {text}
                </InfoCard>
                {children}
                {player.role === Role.GAME_MASTER &&
                <WideButton onClick={this.onButtonClick}
                            additionalClassNames={styles.WWTertiary}>
                    {buttonText}
                </WideButton>}
            </>
        )
    }

    private onButtonClick = (): void => {
        const {socket, game} = this.props;
        socket.emit(NextStateEvent.id, new NextStateEvent(game.id));
    }
}
