import React from 'react';
import {Socket} from '../../../../server/socket';
import {WideButton} from '../../../common/component/wideButton/WideButton';
import {InfoCard} from '../../../common/component/infoCard/InfoCard';
import {IGame} from '../../model/IGame';
import {IPlayer, Role} from '../../model/IPlayer';
import styles from '../../styles/color.module.scss';
import {roleDescriptions} from '../../utils/roleUtils';
import {RoleSelector} from './components/roleSelector/RoleSelector';
import {AssignRolesEvent} from './event/AssignRolesEvent';
import {GetDefaultRoleAssignmentEvent} from './event/GetDefaultRoleAssignmentEvent';
import {GetRolesEvent} from './event/GetRolesEvent';
import {IRole} from './event/in/IRole';
import {IRoleData} from './event/in/IRoleData';
import {SetRolesAssignmentEvent} from './event/SetRolesAssignmentEvent';

interface IProps {
    game: IGame;
    player: IPlayer;
    socket: Socket;
}

interface IState {
    roles: IRole[];
}

export class RoleSelection extends React.Component<IProps, IState> {
    constructor(props: Readonly<IProps>) {
        super(props);
        this.state = {
            roles: []
        }
    }

    componentDidMount() {
        this.props.socket.emit(GetDefaultRoleAssignmentEvent.id, new GetDefaultRoleAssignmentEvent(this.props.game.id));
        this.props.socket.on(GetDefaultRoleAssignmentEvent.id, (data: IRoleData) => {
            this.setState({
                roles: data.roles,
            });
        });

        this.props.socket.on(GetRolesEvent.id, (data: IRoleData) => {
            this.setState({
                roles: data.roles,
            });
        })
    }

    componentWillUnmount() {
        this.props.socket.removeEventListener(GetDefaultRoleAssignmentEvent.id);
        this.props.socket.removeEventListener(GetRolesEvent.id);
    }

    render() {
        return (
            <div>
                <InfoCard titleText={'Role selection'}
                          additionalClassNames={styles.WWPrimary}>
                    Choose the roles you want to play with
                </InfoCard>
                {this.getButtonAssignRoles()}
                {this.getRoles()}
                {this.getButtonAssignRoles()}
            </div>
        )
    }

    private setRoleAmount = (roleName: string, amount: number) => {
        this.props.socket.emit(SetRolesAssignmentEvent.id,
            new SetRolesAssignmentEvent(this.props.game.id, this.computeRoleAmount(roleName, amount)));
    };

    private computeRoleAmount = (roleName: string, amount: number) => {
        const roles = this.state.roles;
        for (const role of roles) {
            if (role.name === roleName) {
                role.amount = amount;
                break;
            }
        }
        return roles;
    };

    private getRoleAmount = (roleName: string) => {
        if (!this.state.roles) {
            return -1;
        }

        for (const role of this.state.roles) {
            if (role.name === roleName) {
                return role.amount;
            }
        }
        return -1;
    };

    private getRoleAmountSum = () => {
        if (!this.state.roles) {
            return 0;
        }

        let sum = 0;
        for (const role of this.state.roles) {
            sum += role.amount;
        }
        return sum;
    };

    private defineRoles = () => {
        this.props.socket.emit(AssignRolesEvent.id, new AssignRolesEvent(this.props.game.id, this.state.roles));
    };

    private getButtonAssignRoles(): JSX.Element {
        const correctRoleNumber = this.getRoleAmountSum() === this.props.game.players.length;
        return this.props.player.role === Role.GAME_MASTER ?
            <WideButton additionalClassNames={styles.WWTertiary}
                        disabled={!correctRoleNumber}
                        onClick={this.defineRoles}>
                {this.getButtonAssignRolesText()}
            </WideButton> : null;
    }

    private getButtonAssignRolesText(): JSX.Element {
        let buttonText = <div>Assign Roles to Players</div>;
        const roleAmountDif = this.getRoleAmountSum() - this.props.game.players.length;
        if (roleAmountDif > 0) {
            buttonText = <>
                <div>You have selected too many roles.</div>
                <div>You need to remove {roleAmountDif} roles</div>
            </>;
        } else if (roleAmountDif < 0) {
            buttonText = <>
                <div>You need to select {roleAmountDif * -1} more roles</div>
            </>;
        }
        return buttonText;
    }

    private getRoles(): JSX.Element {
        return <>
            {this.state.roles.map((role: IRole) =>
                <RoleSelector key={role.name}
                              player={this.props.player}
                              game={this.props.game}
                              roleId={role.name}
                              roleAmount={this.getRoleAmount(role.name)}
                              roleAmountSum={this.getRoleAmountSum()}
                              roleDescription={roleDescriptions.get(role.name)}
                              setRoleAmount={this.setRoleAmount}/>
            )}
        </>;
    }
}
