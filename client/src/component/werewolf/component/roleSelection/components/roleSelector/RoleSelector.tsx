import classNames from 'classnames';
import React from 'react';
import {WideButton} from '../../../../../common/component/wideButton/WideButton';
import {Card} from '../../../../../common/component/card/Card';
import {IconType} from '../../../../../common/component/icon/Icon';
import {Popover} from '../../../../../common/component/popover/Popover';
import {generatePopoverStyles} from '../../../../../common/component/popover/utils/styles';
import {IGame} from '../../../../model/IGame';
import {IPlayer, Role} from '../../../../model/IPlayer';
import styles from './RoleSelector.module.scss';

interface IProps {
    game: IGame;
    player: IPlayer;
    roleId: Role;
    roleAmount: number;
    roleAmountSum: number;
    roleDescription: string;
    setRoleAmount: (roleId: string, amount: number) => void;
}

export class RoleSelector extends React.Component<IProps, null> {

    render() {
        return (
            <Card additionalClassNames={classNames(styles.RoleSelector, styles.WWSecondary)}>
                <div className={styles.RoleInformation}>
                    <div className={styles.InfoButton}>
                        <Popover id={this.props.roleId}
                                 icon={IconType.infoCircle}
                                 customStyles={generatePopoverStyles(styles.WWTertiaryIcon, styles.WWTertiary)}>
                            {this.props.roleDescription}
                        </Popover>
                    </div>
                    <div className={styles.RoleName}>
                        <b>{this.props.roleId.toLocaleUpperCase()}</b>
                    </div>
                    <div className={styles.RoleAmount}>
                        {this.props.roleAmount} Players
                    </div>
                </div>
                {this.props.player.role === Role.GAME_MASTER &&
                <div className={styles.RoleAmountButtons}>
                  <WideButton additionalClassNames={classNames(styles.RoleAmountButton, styles.WWTertiary)}
                              onClick={this.decRoleAmount}
                              disabled={this.buttonDecIsDisabled()}>
                    -
                  </WideButton>
                  <WideButton additionalClassNames={classNames(styles.RoleAmountButton, styles.WWTertiary)}
                              onClick={this.incRoleAmount}
                              disabled={this.buttonIncIsDisabled()}>
                    +
                  </WideButton>
                </div>}
            </Card>
        )
    }

    private incRoleAmount = (): void => {
        let amount: number;
        if (this.props.roleId === Role.CITIZEN || this.props.roleId === Role.WEREWOLF) {
            amount = this.props.roleAmount + 1;
        } else {
            amount = 1;
        }

        this.props.setRoleAmount(this.props.roleId, amount);
    };

    private buttonIncIsDisabled(): boolean {
        switch (this.props.roleId) {
            case Role.CITIZEN:
                return this.props.roleAmountSum >= this.props.game.players.length
            case Role.WEREWOLF:
                return this.props.roleAmountSum >= this.props.game.players.length ||
                    this.props.roleAmount >= Math.ceil(this.props.game.players.length / 2 - 1)
            default:
                return this.props.roleAmount >= 1 || this.props.roleAmountSum >= this.props.game.players.length
        }
    };

    private buttonDecIsDisabled(): boolean {
        return this.props.roleId === Role.WEREWOLF ? this.props.roleAmount <= 1 : this.props.roleAmount <= 0;
    }

    private decRoleAmount = (): void => {
        this.props.setRoleAmount(this.props.roleId, (this.props.roleAmount) > 0 ? this.props.roleAmount - 1 : 0);
    }
}
