import {Role} from '../../../../model/IPlayer';

export interface IRole {
    name: Role;
    amount: number;
}
