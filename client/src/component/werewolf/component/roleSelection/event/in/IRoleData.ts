import {IRole} from './IRole';

export interface IRoleData {
    roles: IRole[];
}
