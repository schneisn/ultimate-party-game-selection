import {GameEvent} from '../../../../common/event/GameEvent';

export class GetDefaultRoleAssignmentEvent extends GameEvent {
    static readonly id = 'GetDefaultRoleAssignment';
}
