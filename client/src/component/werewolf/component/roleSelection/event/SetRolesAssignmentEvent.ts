import {GameEvent} from '../../../../common/event/GameEvent';
import {IRole} from './in/IRole';

export class SetRolesAssignmentEvent extends GameEvent {
    static readonly id = 'SetRolesAssignment';
    roles: IRole[];

    constructor(gameId: string, roles: IRole[]) {
        super(gameId);
        this.roles = roles;
    }
}
