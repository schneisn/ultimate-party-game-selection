import {GameEvent} from '../../../../common/event/GameEvent';
import {IRole} from './in/IRole';

export class AssignRolesEvent extends GameEvent {
    static readonly id = 'AssignRoles';
    roles: IRole[];

    constructor(gameId: string, roles: IRole[]) {
        super(gameId);
        this.roles = roles;
    }
}
