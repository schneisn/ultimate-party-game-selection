import {GameEvent} from '../../../../common/event/GameEvent';

export class GetRolesEvent extends GameEvent {
    static readonly id = 'GetRoles';
}
