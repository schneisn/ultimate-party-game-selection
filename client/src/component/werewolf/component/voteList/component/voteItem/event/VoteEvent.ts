import {GameEvent} from '../../../../../../common/event/GameEvent';

export class VoteEvent extends GameEvent {
    static readonly id = 'Vote';
    targetId: string;

    constructor(gameId: string, targetId: string) {
        super(gameId);
        this.targetId = targetId;
    }
}
