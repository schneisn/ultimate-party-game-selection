import classNames from 'classnames';
import React from 'react';
import {Socket} from '../../../../../../server/socket';
import {CheckboxWithLabel} from '../../../../../common/component/checkboxWithLabel/CheckboxWithLabel';
import {DisconnectedIcon} from '../../../../../common/component/disconnectedIcon/DisconnectedIcon';
import {IconType} from '../../../../../common/component/icon/Icon';
import {Popover} from '../../../../../common/component/popover/Popover';
import {IGame} from '../../../../model/IGame';
import {IPlayer, Role} from '../../../../model/IPlayer';
import {getPlayer, playerIsGameMaster} from '../../../../utils/playerUtils';
import {getDoppelgangerTarget, isLover} from '../../../../utils/roleUtils';
import {VoteEvent} from './event/VoteEvent';
import styles from './VoteItem.module.scss';
import {generatePopoverStyles} from '../../../../../common/component/popover/utils/styles';
import {RolePopover} from '../../../rolePopover/RolePopover';

interface IProps {
    /**
     * Whether the player can see who voted for the item.
     * Default is false
     */
    areVotesVisible?: boolean;
    game: IGame;
    /**
     * The player that is viewing this item
     */
    player: IPlayer;
    /**
     * Whether the item is disabled.
     * Default is false.
     */
    disabled: boolean;
    /**
     * Whether the role of the player is visible.
     * Default is false
     */
    isRoleVisible?: boolean;
    /**
     * Whether the item is selected.
     * Default is false
     */
    isSelected: boolean;
    socket: Socket;
    /**
     * The player that this item represents
     */
    target: IPlayer;
}

/**
 * A checkbox to vote for the given target
 */
export class VoteItem extends React.Component<IProps> {

    /**
     * Prevents clicking the underlying checkbox card when clicking a popover icon
     * @param event
     * @private
     */
    private static preventDefaultOnClick(event: React.MouseEvent) {
        event.preventDefault();
    }

    render() {
        return (
            <CheckboxWithLabel additionalClassNames={classNames(styles.VoteItem, styles.WWTertiary)}
                               disabled={this.props.disabled}
                               checked={this.props.isSelected}
                               checkedClassNames={styles.Selected}
                               showIcon={true}
                               onClick={this.voteForItem}>
                {this.getPlayerInfo()}
            </CheckboxWithLabel>
        )
    }

    private getAliveIcon(): JSX.Element {
        return <Popover id={'alive' + this.props.target.id}
                        customStyles={generatePopoverStyles(styles.RoleIcon)}
                        icon={IconType.skullCrossbones}>
            Player is dead
        </Popover>;
    };

    private getVotesPopover(): JSX.Element {
        const {game, target} = this.props;
        const count = game.votes.filter((vote) => vote.targetId === target.id).length;
        return <>
            <Popover id={target.id}
                     icon={IconType.userCheck}
                     customStyles={generatePopoverStyles(styles.RoleIcon)}
                     onClick={(event: React.MouseEvent) => VoteItem.preventDefaultOnClick(event)}
                     title={'Votes for the player'}>
                {this.getVotersForTarget()}
            </Popover>
            {count}
        </>;
    };

    private getVotersForTarget(): JSX.Element[] {
        const {game, target} = this.props;
        return game.votes
            .filter(vote => vote.targetId === target.id)
            .filter(vote => getPlayer(game, vote.voterId))
            .map(vote => getPlayer(game, vote.voterId))
            .map(target => <div key={target.id}>
                {target.name}
            </div>);
    };

    private getPlayerInfo(): JSX.Element {
        const {game, target, areVotesVisible, isRoleVisible} = this.props;
        return <div className={styles.PlayerInfo}>
            {areVotesVisible && <div className={styles.Votes}>{this.getVotesPopover()}</div>}
            <div className={styles.Player}>
                <div className={styles.Name}><b>{target.name}</b></div>
                {!target.isConnected && !target.isComputer && <DisconnectedIcon/>}
                {!target.isAlive && <div className={styles.SpecialRole}>{this.getAliveIcon()}</div>}
                {target.id === game.mayor && <div className={styles.SpecialRole}>{this.showIsMayor()}</div>}
                {this.canSeeLovers() && <div className={styles.SpecialRole}>{this.getLoverIcon()}</div>}
                {this.canSeeDoppelgangerTarget() && <div className={styles.SpecialRole}>{this.getDoppelgangerTargetIcon()}</div>}
                {isRoleVisible && <div className={styles.SpecialRole}><RolePopover role={target.role} iconId={'targetRole' + target.id}/></div>}
            </div>
        </div>;
    }

    private canSeeLovers(): boolean {
        return isLover(this.props.game, this.props.target.id) &&
            (playerIsGameMaster(this.props.player) || isLover(this.props.game, this.props.player.id) || this.props.player.role === Role.ARMOR);
    }

    private canSeeDoppelgangerTarget(): boolean {
        return (this.props.player.role === Role.DOPPELGANGER || playerIsGameMaster(this.props.player)) &&
            this.props.target.id === getDoppelgangerTarget(this.props.game)?.id;
    }

    private getDoppelgangerTargetIcon(): JSX.Element {
        return <Popover id={'doppelganger-' + this.props.target.id}
                        icon={IconType.crosshairs}
                        customStyles={generatePopoverStyles(styles.RoleIcon)}
                        onClick={(event: React.MouseEvent) => VoteItem.preventDefaultOnClick(event)}>
            {playerIsGameMaster(this.props.player) ? 'The doppelganger is' : 'You are'} going to take the role of this player when the player dies
        </Popover>;
    }

    private showIsMayor(): JSX.Element {
        return <Popover id={'mayor-' + this.props.target.id}
                        icon={IconType.award}
                        customStyles={generatePopoverStyles(styles.RoleIcon)}
                        onClick={(event: React.MouseEvent) => VoteItem.preventDefaultOnClick(event)}>
            Mayor
        </Popover>;
    };

    private getLoverIcon(): JSX.Element {
        return <Popover id={'lover-' + this.props.target.id}
                        icon={IconType.heart}
                        customStyles={generatePopoverStyles(styles.RoleIcon)}
                        onClick={(event: React.MouseEvent) => VoteItem.preventDefaultOnClick(event)}>
            Lover
        </Popover>
    }

    private voteForItem = (): void => {
        const {socket, target, game, disabled} = this.props;
        if (disabled) {
            return;
        }
        socket.emit(VoteEvent.id, new VoteEvent(game.id, target.id));
    };
}
