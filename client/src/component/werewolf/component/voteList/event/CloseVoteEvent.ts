import {GameEvent} from '../../../../common/event/GameEvent';
import {StateEvent} from '../../../model/IStateEvent';

export class CloseVoteEvent extends GameEvent {
    static readonly id = 'CloseVote';
    voteType: StateEvent;

    constructor(gameId: string, voteType: StateEvent) {
        super(gameId);
        this.voteType = voteType;
    }
}
