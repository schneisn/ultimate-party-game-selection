import React from 'react';
import {Socket} from '../../../../server/socket';
import {WideButton} from '../../../common/component/wideButton/WideButton';
import {IGame} from '../../model/IGame';
import {GameEvent} from '../../model/IGameState';
import {IPlayer, Role} from '../../model/IPlayer';
import {playerIsGameMaster} from '../../utils/playerUtils';
import {getCurrentState, playerIsActiveHunter} from '../../utils/stateUtils';
import {VoteItem} from './component/voteItem/VoteItem';
import {CloseVoteEvent} from './event/CloseVoteEvent';
import styles from './VoteList.module.scss';
import {playerCanSeeRole} from '../../utils/roleUtils';
import {targetHasVoteFromPlayer} from '../../utils/voteUtils';
import {StateEvent} from '../../model/IStateEvent';
import {ContinueInfo} from '../states/component/continueInfo/ContinueInfo';
import {SmallCard} from '../../../common/component/smallCard/SmallCard';
import {IconContent} from '../../../common/component/iconContent/IconContent';
import {IconType} from '../../../common/component/icon/Icon';
import {HeaderContainer} from '../../../common/component/headerContainer/HeaderContainer';

interface IProps {
    /**
     * The text that is shown when the button is active
     */
    activeButtonText?: string;
    buttonIsHidden?: boolean;
    /**
     * Whether the button to confirm the selection is disabled.
     * Default is disabled when there are no selected players.
     */
    buttonIsDisabled?: boolean;
    /**
     * Overwrites the default on click event.
     * The default event sends a close vote
     * message to the server given in the socket prop
     */
    customOnButtonClick?: () => void;
    /**
     * The text that is shown when the button is inactive
     */
    disabledButtonText?: string;
    /**
     * Optional title to display as header
     */
    listHeader?: string;
    game: IGame;
    player: IPlayer;
    /**
     * Whether the player can vote for players with
     * the same role as themself.
     * Default is false
     */
    playerCanSelectOwnRole?: boolean;
    socket: Socket;
    /**
     * The type of vote. This is needed by the server
     * to interpret which event is meant with this vote
     */
    voteType?: StateEvent;
}

/**
 * A list to vote for players in the game
 */
export class VoteList extends React.Component<IProps> {

    componentDidMount() {
        // Scroll back to top to see the top card when there are many players in the game
        window.scrollTo({top: 0, left: 0, behavior: 'smooth'});
    }

    render() {
        return (
            <>
                <HeaderContainer additionalClassNames={styles.WWSecondary}
                                 header={this.props.listHeader}>
                    {this.listTargetPlayers()}
                </HeaderContainer>
                {playerIsGameMaster(this.props.player) &&
                <SmallCard additionalClassNames={styles.WWSecondary}>
                  <IconContent iconLeft={{
                      icon: IconType.infoCircle
                  }}>
                    <div>
                      Wait for the other players to vote.
                    </div>
                    <div>
                      You can also vote. Your vote is a veto and overrides the votes of the other players
                    </div>
                  </IconContent>
                </SmallCard>}
                {this.buttonIsVisible() &&
                <WideButton additionalClassNames={styles.WWTertiary}
                            onClick={this.handleButtonClick}
                            disabled={this.buttonIsDisabled()}>
                    {this.buttonIsDisabled() ? this.getDisabledButtonText() : this.getActiveButtonText()}
                </WideButton>}
                {!this.buttonIsVisible() && <ContinueInfo/>}
            </>
        )
    }

    private listTargetPlayers(): JSX.Element[] {
        const {socket, game, player} = this.props;
        return game.players
            .filter(target => this.showTarget(target.id))
            .map((target) =>
                <VoteItem key={target.id}
                          areVotesVisible={this.playerCanSeeVote()}
                          disabled={this.targetItemIsDisabled(target)}
                          game={game}
                          isRoleVisible={playerCanSeeRole(player, target)}
                          isSelected={targetHasVoteFromPlayer(game, player, target)}
                          player={player}
                          socket={socket}
                          target={target}/>
            );
    };

    private targetItemIsDisabled(target: IPlayer): boolean {
        const {playerCanSelectOwnRole, player, game} = this.props;
        const currentState = getCurrentState(game);
        if (!(player.isAlive || playerIsActiveHunter(game, player))) {
            return true;
        }
        if (playerCanSelectOwnRole && (player.role === target.role || playerIsGameMaster(player)) && target.isAlive) {
            return false;
        }
        if (playerCanSelectOwnRole === false && player.role === target.role) {
            return true;
        }
        return !target.isAlive || (target.role?.toString() === currentState.id.toString() && currentState.id !== GameEvent.ARMOR);
    }

    private playerCanSeeVote(): boolean {
        const {player, game} = this.props;
        return (player.role === Role.WEREWOLF && getCurrentState(game).id === GameEvent.WEREWOLF) ||
            player.role === Role.GAME_MASTER ||
            getCurrentState(game).id === GameEvent.TRIAL ||
            getCurrentState(game).id === GameEvent.MAYOR ||
            !player.isAlive;
    }

    private showTarget(playerId: string): boolean {
        const {playerCanSelectOwnRole, player} = this.props;
        return playerId !== player.id || playerCanSelectOwnRole;
    }

    private handleButtonClick = () => {
        const {voteType, socket, customOnButtonClick, game} = this.props;
        if (customOnButtonClick) {
            customOnButtonClick();
            return;
        }
        socket.emit(CloseVoteEvent.id, new CloseVoteEvent(game.id, voteType));
    };

    private buttonIsVisible = () => {
        const {buttonIsHidden, player} = this.props;
        return !buttonIsHidden || player.role === Role.GAME_MASTER;
    };

    private buttonIsDisabled = () => {
        const {buttonIsDisabled, game} = this.props;
        return buttonIsDisabled ? buttonIsDisabled : game.votes.length <= 0;
    };

    private getActiveButtonText(): string {
        const {activeButtonText} = this.props;
        return activeButtonText ? activeButtonText : 'Next';
    }

    private getDisabledButtonText(): string {
        const {disabledButtonText} = this.props;
        return disabledButtonText ? disabledButtonText : 'Select someone before continuing';
    }
}
