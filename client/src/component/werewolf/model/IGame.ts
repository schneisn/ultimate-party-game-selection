import {ICommonGame} from '../../common/model/ICommonGame';
import {IGameState} from './IGameState';
import {IPlayer} from './IPlayer';
import {IVote} from './IVote';

export interface IGame extends ICommonGame<IPlayer> {
    gamemaster: IPlayer;
    states: IGameState[];
    mayor: string;
    votes: IVote[];
}
