import {IStateEvent} from './IStateEvent';

export enum GameEvent {
    ARMOR = 'armor',
    DOPPELGANGER = 'doppelganger',
    END_NIGHT = 'endnight',
    GAME_OVER = 'gameover',
    HEALER = 'healer',
    HUNTER = 'hunter',
    SHOW_HUNTER_KILL = 'show_hunter_kill',
    LOBBY = 'lobby',
    LOVERS = 'lovers',
    MAYOR = 'votemayor',
    MAYOR_ELECTION_RESULT = 'mayor_election_result',
    PROSTITUTE = 'prostitute',
    WEREWOLF = 'werewolf',
    SHOW_WEREWOLF_KILL = 'show_werewolf_kill',
    SEER = 'seer',
    SHOW_SEER_SELECTION = 'show_seer_selection',
    SELECT_ROLES = 'selectroles',
    START_NIGHT = 'startnight',
    TRIAL = 'trial',
    SHOW_TRIAL_KILL = 'show_trial_kill',
    WITCH = 'witch',
}

export interface IGameState {
    id: GameEvent;
    night: number;
    playerId: string;
    events: IStateEvent[];
}
