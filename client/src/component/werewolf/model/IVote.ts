export interface IVote {
    voterId: string;
    targetId: string;
}
