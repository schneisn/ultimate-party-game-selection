export enum StateEvent {
    /**
     * The doppelganger selected a target that he becomes
     * when the target dies
     */
    DOPPELGANGER_SELECT = 'doppelganger_select',
    /**
     * The doppelganger becomes the target he selected
     */
    DOPPELGANGER_CHANGE = 'doppelganger_change',
    /**
     * The healer heals a target
     */
    HEALER_HEAL = 'healer_heal',
    /**
     * The hunter killed a target
     */
    HUNTER_KILL = 'hunter_kill',
    /**
     * The first lover is selected
     */
    LOVER_ONE = 'lover_one',
    /**
     * The second lover is selected
     */
    LOVER_TWO = 'lover_two',
    /**
     * A player was elected as mayor
     */
    MAYOR_ELECTED = 'mayor',
    /**
     * The prostitute selected the player where she wants to stay
     */
    PROSTITUTE_SELECT = 'prostitute_select',
    WEREWOLF_KILL = 'werewolf_kill',
    SEER_SELECT = 'seer_select',
    /**
     * A player was selected to be killed during trial
     */
    TRIAL_KILL = 'trial_kill',
    /**
     * The witch healed someone
     */
    WITCH_HEAL = 'witch_heal',
    /**
     * The witch killed someone
     */
    WITCH_KILL = 'witch_kill',
    /**
     * The player is permanently dead.
     */
    DEATH = 'death',
    /**
     * The player is one of the winners.
     */
    WINNER = 'winner',
    /**
     * The werewolves won the game.
     */
    WEREWOLVES_WON = 'werewolves_won',
    /**
     * The lovers won the game.
     */
    LOVERS_WON = 'lovers_won',
    /**
     * The citizens won the game.
     */
    CITIZENS_WON = 'citizens_won',
}

export interface IStateEvent {
    id: StateEvent;
    targetId: string;
}
