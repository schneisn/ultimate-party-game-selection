import React from 'react';
import {RouteComponentProps} from 'react-router-dom';
import {Urls} from '../../server/serverUrls';
import {BaseGame} from '../common/component/baseGame/BaseGame';
import {generateConnectToastStyles} from '../common/component/baseGame/component/connectToast/utils/styles';
import {GetGameEvent} from '../common/component/baseGame/event/GetGameEvent';
import {IGetGame} from '../common/component/baseGame/event/in/IGetGame';
import {generateBaseGameStyles} from '../common/component/baseGame/utils/styles';
import {InfoCard} from '../common/component/infoCard/InfoCard';
import {generateModalStyles} from '../common/component/modal/utils/styles';
import {generatePopoverStyles} from '../common/component/popover/utils/styles';
import {IGame} from './model/IGame';
import {GameEvent} from './model/IGameState';
import {IPlayer} from './model/IPlayer';
import {getCurrentState, playerIsActiveHunter} from './utils/stateUtils';
import styles from './styles/color.module.scss';
import {UrlUtils} from '../common/utils/url';
import {States} from './component/states/States';
import {Card} from '../common/component/card/Card';
import {PlayerInfo} from '../common/component/playerInfo/PlayerInfo';
import {Modal} from '../common/component/modal/Modal';
import {IConnectionReadyPlayer} from '../common/model/IConnectionReadyPlayer';
import {ICommonPlayer} from '../common/model/ICommonPlayer';

export class Werewolf extends BaseGame<IGame, IPlayer> {

    constructor(props: RouteComponentProps) {
        super(props);
        const options = {
            gameName: 'Werewolf',
            gamePath: UrlUtils.getPathFromString(this.props.match.path),
            socketNamespace: Urls.Werewolf,
            styles: generateBaseGameStyles(
                generateModalStyles(styles.WWPrimary, styles.WWSecondary, styles.WWTertiary),
                generateConnectToastStyles(styles.WWConnect, styles.WWDisconnect),
                null,
                styles.WWBackground,
                generatePopoverStyles(styles.WWTertiaryIcon, styles.WWTertiary)
            )
        };
        this.state = {
            ...this.state,
            options: options
        };
    }

    componentDidMount() {
        super.componentDidMount();
        this.state.socket.on(GetGameEvent.id, (data: IGetGame<IGame>) => {
            let player = this.getPlayerFromGame(data.game);
            if (!player) {
                player = data.game?.gamemaster;
            }
            this.updateGameAndPlayer(data.game, player);
        });
    }

    renderGame = () => {
        const {player, game, socket} = this.state;
        return (
            <>
                {this.getPlayerDeadInfo()}
                <States game={game} player={player} socket={socket}/>
            </>
        )
    }

    protected getPlayerModal(): JSX.Element {
        const {isPlayerModalOpen, game} = this.state;
        const playerElements = game?.players
            .map(teamMate => {
                return <Card key={teamMate.id}>
                    <PlayerInfo player={teamMate} showConnectionState={!teamMate.isComputer}/>
                </Card>
            });
        const gameMaster = game.gamemaster;
        const transformedGm: ICommonPlayer & Partial<IConnectionReadyPlayer> = {
            ...gameMaster,
            isGameMaster: true,
        }
        playerElements.unshift(<Card key={transformedGm.id}>
            <PlayerInfo player={transformedGm}/>
        </Card>)

        return <Modal modalTitle={'Players'}
                      isOpen={isPlayerModalOpen}
                      showFooter={false}
                      onToggle={this.togglePlayerModal}>
            {playerElements}
        </Modal>
    }

    private getPlayerDeadInfo(): JSX.Element {
        const {game, player} = this.state;
        const currentStateId = getCurrentState(game).id;
        const gameIsOver = currentStateId === GameEvent.GAME_OVER;
        const endNight = currentStateId === GameEvent.END_NIGHT
        if (player.isAlive || gameIsOver || endNight || playerIsActiveHunter(game, player)) {
            return null;
        }
        return <InfoCard titleText={'You are dead'}
                         additionalClassNames={styles.WWPrimary}>
            You can see everything that happens but you cannot do anything
        </InfoCard>;
    }
}
