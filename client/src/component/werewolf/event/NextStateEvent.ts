import {GameEvent} from '../../common/event/GameEvent';

export class NextStateEvent extends GameEvent {
    static readonly id = 'NextState';
}
