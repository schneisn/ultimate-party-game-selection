import {IRound} from '../model/IRound';

/**
 * Replaces all blanks in the statement with the answers inserted as bold html
 * @param statement
 * @param answers
 * @return null if statement or answers is null or empty
 */
export function replaceBlanks(statement: string, answers: string[]): string {
    if (!statement) {
        return null;
    }
    if (!answers || answers.length < 1) {
        return statement;
    }
    let lastStartIndex = 0;
    let blankIndex = 0;
    if (statement.indexOf('___', lastStartIndex) < 0) {
        statement += ' ___';
    }
    while (statement.indexOf('___', lastStartIndex) >= 0) {
        const startIndex = statement.indexOf('___');
        const playerAnswer = answers[blankIndex];
        if (!playerAnswer) {
            break;
        }
        const boldPlayerAnswer = `<b>${playerAnswer}</b>`;
        statement = statement.replace('___', boldPlayerAnswer);
        lastStartIndex = startIndex;
        blankIndex++;
    }
    return statement;
}

/**
 * Gets the statement filled with the winning answers in bold text
 * @param round
 */
export function getWinnerStatement(round: IRound): string {
    if (!round || !round.answers) {
        return null;
    }
    const winningAnswer = round.answers.find(answer => answer.hasWon);
    return replaceBlanks(round.statement, winningAnswer?.answers);
}
