import {getPlayer} from '../../common/utils/player';
import {IGame} from '../model/IGame';
import {IPlayer} from '../model/IPlayer';
import {IRound} from '../model/IRound';

export function getCurrentRound(game: IGame): IRound {
    return game.rounds[game.rounds.length - 1];
}

/**
 * Checks the amount of '___' in the statement.
 * If there are none, it assumes 1 answer is required
 * @param statement
 * @return The nr of '___' in the statement. Otherwise 1
 */
export function getNrBlanks(statement: string): number {
    return statement.match(/___/g)?.length || 1;
}

export function getWinner(game: IGame): IPlayer {
    const winnerId = getCurrentRound(game)?.answers?.find(answer => answer.hasWon)?.playerId;
    return getPlayer(game, winnerId);
}
