import React from 'react';
import {Socket} from '../../../../../../server/socket';
import {WideButton} from '../../../../../common/component/wideButton/WideButton';
import {InfoCard} from '../../../../../common/component/infoCard/InfoCard';
import {SmallCard} from '../../../../../common/component/smallCard/SmallCard';
import {getPlayer} from '../../../../../common/utils/player';
import {IGame} from '../../../../model/IGame';
import {IPlayer} from '../../../../model/IPlayer';
import styles from '../../../../styles/color.module.scss';
import {getCurrentRound} from '../../../../utils/roundUtils';
import {replaceBlanks} from '../../../../utils/statementUtils';
import {Statement} from '../../../statement/Statement';
import {NextAnswerEvent} from '../../event/NextAnswerEvent';

interface IProps {
    answerIndex: number;
    game: IGame;
    player: IPlayer;
    socket: Socket;
}

/**
 * See all the answers that the other players have given one by one.
 * The critic can go to the next answer by clicking the next button.
 * The other players can just see the whole sentence with the solution given.
 */
export class Preview extends React.Component<IProps> {

    render() {
        if (this.props.answerIndex < 0) {
            return <>
                <InfoCard titleText={'All answers arrived!'}
                          additionalClassNames={styles.CAPrimary}>
                    Let's check them out!
                </InfoCard>
                {this.getButton()}
            </>
        }
        return (
            <>
                <Statement title={'Answer ' + (this.props.answerIndex + 1)}
                           statement={this.getStatement()}
                           round={getCurrentRound(this.props.game)}/>
                {this.getButton()}
            </>
        )
    }

    private getStatement = () => {
        const currentRound = getCurrentRound(this.props.game);
        const answers = currentRound.answers[this.props.answerIndex].answers;
        return replaceBlanks(currentRound.statement, answers);
    };

    private onNextAnswer = () => {
        this.props.socket.emit(NextAnswerEvent.id, new NextAnswerEvent(this.props.game.id, this.props.answerIndex + 1));
    };

    private getButtonText(): string {
        if (this.props.answerIndex < 0) {
            return 'Check out the answers';
        }
        return getCurrentRound(this.props.game).answers.length === this.props.answerIndex + 1 ? 'Go to best answer selection' : 'Next answer';
    }

    private playerIsCritic(): boolean {
        return this.props.player.id === getCurrentRound(this.props.game).criticId;
    }

    private getButton(): JSX.Element {
        const {game} = this.props;
        const currentRound = getCurrentRound(game);
        return this.playerIsCritic() ?
            <WideButton onClick={this.onNextAnswer} additionalClassNames={styles.CATertiary}>
                {this.getButtonText()}
            </WideButton> :
            <SmallCard additionalClassNames={styles.CASecondary}>
                {getPlayer(game, currentRound?.criticId)?.name} is going to continue
            </SmallCard>;
    }
}
