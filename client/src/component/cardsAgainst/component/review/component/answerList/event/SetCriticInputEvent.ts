import {GameEvent} from '../../../../../../common/event/GameEvent';

export class SetCriticInputEvent extends GameEvent {
    static readonly id = 'SetCriticInput';
    winnerId: string;

    constructor(gameId: string, winnerId: string) {
        super(gameId);
        this.winnerId = winnerId;
    }
}
