import classNames from 'classnames';
import React from 'react';
import {Socket} from '../../../../../../server/socket';
import {WideButton} from '../../../../../common/component/wideButton/WideButton';
import {CheckboxWithLabel} from '../../../../../common/component/checkboxWithLabel/CheckboxWithLabel';
import {Emoji} from '../../../../../common/component/emoji/Emoji.';
import {InfoCard} from '../../../../../common/component/infoCard/InfoCard';
import {getPlayer} from '../../../../../common/utils/player';
import {IGame} from '../../../../model/IGame';
import {IPlayer} from '../../../../model/IPlayer';
import {getCurrentRound} from '../../../../utils/roundUtils';
import {replaceBlanks} from '../../../../utils/statementUtils';
import {Statement} from '../../../statement/Statement';
import styles from './AnswerList.module.scss';
import {SetCriticInputEvent} from './event/SetCriticInputEvent';

interface IProps {
    game: IGame;
    isCritic: boolean;
    player: IPlayer;
    socket: Socket;
}

interface IState {
    selectedWinner: string;
    statement: string;
}

/**
 * The voting component for the critic to select the winner of the round
 */
export class AnswerList extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);

        this.state = {
            statement: getCurrentRound(this.props.game).statement,
            selectedWinner: null,
        };
    }

    render() {
        const {isCritic, game} = this.props;
        const currentRound = getCurrentRound(this.props.game);
        const {selectedWinner, statement} = this.state;
        return (
            <div>
                {!isCritic && <InfoCard additionalClassNames={styles.CAPrimary}
                                        titleText={<Emoji symbol={'🥁🥁🥁'}/>}>
                  Wait for {getPlayer(game, currentRound?.criticId)?.name} to select the best answer. You can see all answers below
                </InfoCard>}
                <Statement title={this.getStatementCardTitle()}
                           statement={statement}
                           round={currentRound}/>
                {this.listHandCards()}
                {isCritic && <WideButton additionalClassNames={classNames(styles.ConfirmButton, styles.CATertiary)}
                                         disabled={!selectedWinner}
                                         onClick={this.onConfirm}>
                  Confirm
                </WideButton>}
            </div>
        )
    }

    private getStatementCardTitle(): string {
        return this.props.isCritic ? 'Select the best answer' : null;
    }

    private listHandCards(): JSX.Element {
        const handCards: JSX.Element[] = [];
        this.getAnswers()?.forEach((answers: string[], playerId: string) => {
            handCards.push(<CheckboxWithLabel key={playerId}
                                              additionalClassNames={styles.CATertiary}
                                              checked={playerId === this.state.selectedWinner}
                                              checkedClassNames={styles.Selected}
                                              onClick={() => this.onCardClick(playerId)}>
                {this.formatValue(answers)}
            </CheckboxWithLabel>);
        });
        return <div>{handCards}</div>
    };

    private formatValue = (valueArray: string[]) => {
        let valueString = '';
        for (const value of valueArray) {
            valueString += value + ' - '
        }
        valueString = valueString.slice(0, -3);
        return valueString;
    };

    private getAnswers(): Map<string, string[]> {
        const playerCards = new Map();
        for (const caAnswer of getCurrentRound(this.props.game).answers) {
            playerCards.set(caAnswer.playerId, caAnswer.answers)
        }
        return playerCards;
    };

    private onCardClick = (winnerId: string) => {
        this.setState({
            selectedWinner: winnerId,
        });
        this.replaceBlanks(winnerId);
    };

    private replaceBlanks = (winnerId: string) => {
        let newStatement = getCurrentRound(this.props.game).statement;
        newStatement = replaceBlanks(newStatement, this.getAnswers().get(winnerId));
        this.setState({
            statement: newStatement,
        })
    };

    private onConfirm = () => {
        this.props.socket.emit(SetCriticInputEvent.id, new SetCriticInputEvent(this.props.game.id, this.state.selectedWinner));
    };
}
