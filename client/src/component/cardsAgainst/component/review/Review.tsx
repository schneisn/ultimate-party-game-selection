import React from 'react';
import {Socket} from '../../../../server/socket';
import {IGame} from '../../model/IGame';
import {IPlayer} from '../../model/IPlayer';
import {getCurrentRound} from '../../utils/roundUtils';
import {AnswerList} from './component/answerList/AnswerList';
import {Preview} from './component/preview/Preview';
import {INextAnswer} from './event/in/INextAnswer';
import {NextAnswerEvent} from './event/NextAnswerEvent';

interface IProps {
    game: IGame;
    player: IPlayer;
    socket: Socket;
}

interface IState {
    currentAnswerIndex: number;
}

export class Review extends React.Component<IProps, IState> {

    constructor(props: IProps) {
        super(props);
        this.state = {
            currentAnswerIndex: -1
        }
    }

    componentDidMount() {
        this.props.socket.on(NextAnswerEvent.id, (data: INextAnswer) => this.setState({currentAnswerIndex: data.nextAnswer}));
    }

    componentWillUnmount() {
        this.props.socket.removeEventListener(NextAnswerEvent.id);
    }

    render() {
        const currentRound = getCurrentRound(this.props.game);
        const hasSeenAllCards = this.state.currentAnswerIndex > currentRound.answers.length - 1;
        const playerIsCritic = currentRound.criticId === this.props.player.id;
        if (hasSeenAllCards) {
            return <AnswerList player={this.props.player} socket={this.props.socket} game={this.props.game} isCritic={playerIsCritic}/>;
        }

        return <Preview socket={this.props.socket}
                        game={this.props.game}
                        player={this.props.player}
                        answerIndex={this.state.currentAnswerIndex}/>;
    }
}
