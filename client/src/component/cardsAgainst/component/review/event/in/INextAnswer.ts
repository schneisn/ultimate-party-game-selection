export interface INextAnswer {
    /**
     * The index of the answer that is shown
     */
    nextAnswer: number;
}
