import {GameEvent} from '../../../../common/event/GameEvent';

export class NextAnswerEvent extends GameEvent {
    static readonly id = 'NextAnswer';
    nextAnswer: number;

    constructor(gameId: string, nextAnswer: number) {
        super(gameId);
        this.nextAnswer = nextAnswer;
    }
}
