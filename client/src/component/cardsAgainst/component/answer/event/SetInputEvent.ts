import {GameEvent} from '../../../../common/event/GameEvent';

export class SetInputEvent extends GameEvent {
    static readonly id = 'SetInput';
    answers: string[];

    constructor(gameId: string, answers: string[]) {
        super(gameId);
        this.answers = answers;
    }
}
