import {GameEvent} from '../../../../common/event/GameEvent';

export class RefillHandCardsEvent extends GameEvent {
    static readonly id = 'RefillHandCards';
}
