import classNames from 'classnames';
import React from 'react';
import {Socket} from '../../../../server/socket';
import {WideButton} from '../../../common/component/wideButton/WideButton';
import {CheckboxList, ICheckboxListItem} from '../../../common/component/checkboxList/CheckboxList';
import {generateCheckboxListStyles} from '../../../common/component/checkboxList/utils/stylesUtils';
import {getPlayer} from '../../../common/utils/player';
import {IGame} from '../../model/IGame';
import {IPlayer} from '../../model/IPlayer';
import {getCurrentRound, getNrBlanks} from '../../utils/roundUtils';
import {replaceBlanks} from '../../utils/statementUtils';
import {Statement} from '../statement/Statement';
import styles from './Answer.module.scss';
import {SetInputEvent} from './event/SetInputEvent';
import {Icon, IconType} from '../../../common/component/icon/Icon';
import {RefillHandCardsEvent} from './event/RefillHandCardsEvent';
import {Modal} from '../../../common/component/modal/Modal';
import {generateModalStyles} from '../../../common/component/modal/utils/styles';
import {IRound} from '../../model/IRound';

interface IProps {
    game: IGame;
    player: IPlayer;
    socket: Socket;
}

interface IState {
    selectedCards: string[];
    statement: string;
    refillCardsModalIsOpen: boolean;
}

export class Answer extends React.Component<IProps, IState> {

    constructor(props: IProps) {
        super(props);
        this.state = {
            statement: getCurrentRound(this.props.game).statement,
            selectedCards: [],
            refillCardsModalIsOpen: false
        }
    }

    render() {
        const {statement, refillCardsModalIsOpen} = this.state;
        const {game} = this.props;
        return (
            <>
                <Modal styles={generateModalStyles(styles.CAPrimary, styles.CASecondary, styles.CATertiary)}
                       modalTitle={'Get new hand-cards'}
                       isOpen={refillCardsModalIsOpen}
                       onToggle={() => this.setState({refillCardsModalIsOpen: !refillCardsModalIsOpen})}
                       onConfirm={this.onRefillCardsClick}>
                    <div>
                        Are you sure you want to get new hand-cards?
                    </div>
                    <div>
                        You can only do this once this round!
                    </div>
                </Modal>
                <Statement title={this.getStatementCardTitle()}
                           statement={statement}
                           round={getCurrentRound(game)}/>
                <CheckboxList
                    listStyles={generateCheckboxListStyles(styles.CASecondary, styles.CATertiary, styles.Selected)}
                    items={this.listHandCards()}
                    customTopContent={this.getRefillIcon()}
                    showCheckIcons={true}/>
                <WideButton additionalClassNames={classNames(styles.ConfirmButton, styles.CATertiary)}
                            disabled={!this.hasEnoughAnswers()}
                            onClick={this.onConfirm}>
                    Confirm
                </WideButton>
            </>
        )
    }

    private listHandCards(): ICheckboxListItem[] {
        return this.props.player.handCards?.map((cardText: string) => {
            return {
                content: cardText,
                onClick: () => this.onCardClick(cardText),
                checked: this.state.selectedCards.includes(cardText)
            }
        });
    };

    private onCardClick = (cardText: string) => {
        const selectedCardIndex = this.state.selectedCards.findIndex(card => card === cardText);
        const alreadySelected = selectedCardIndex >= 0;
        const currentRound = getCurrentRound(this.props.game);
        const nrBlanks = getNrBlanks(currentRound.statement);
        const allBlanksFilled = !alreadySelected && this.state.selectedCards.length >= nrBlanks;
        if (allBlanksFilled && nrBlanks > 1) {
            return;
        }
        if (nrBlanks <= 1 && !alreadySelected) {                              // Change the selected card
            this.state.selectedCards.splice(0, 1);
            this.state.selectedCards.push(cardText);
        } else if (alreadySelected) {                                         // Remove selected card
            this.state.selectedCards.splice(selectedCardIndex, 1);
        } else {                                                              // Add card to selected cards
            this.state.selectedCards.push(cardText);
        }
        const newStatement = replaceBlanks(currentRound.statement, this.state.selectedCards);
        this.setState({
            statement: newStatement,
        });
    };

    private onConfirm = () => {
        if (!this.hasEnoughAnswers) {
            return;
        }
        this.props.socket.emit(SetInputEvent.id, new SetInputEvent(this.props.game.id, this.state.selectedCards));
    };

    private hasEnoughAnswers(): boolean {
        return getNrBlanks(getCurrentRound(this.props.game).statement) <= this.state.selectedCards.length;
    };

    private getStatementCardTitle(): JSX.Element {
        const currentRound = getCurrentRound(this.props.game);
        return <>
            <div>{'Round ' + (currentRound?.number + 1)}</div>
            <div className={styles.CriticName}>
                {getPlayer(this.props.game, currentRound?.criticId)?.name} will judge you
            </div>
        </>;
    }

    private roundAllowsRefilling(): boolean {
        const {game, player} = this.props;
        const nrOfPlayerRounds = game.rounds.reduce((nrOfAnswers: number, round: IRound) =>
            nrOfAnswers + (round.answers.some(a => a.playerId === player.id) ? 1 : 0), 0);
        const isRefillRound = game.replaceCardsRound > 1 && (nrOfPlayerRounds % (game.replaceCardsRound)) === 0;
        return game.replaceCardsRound === 1 || isRefillRound;
    }

    private getRefillIcon(): JSX.Element {
        const {game, player} = this.props;
        if (game.replaceCardsRound === 0) {
            return null;
        }
        const currentRound = getCurrentRound(game);
        let roundAllowsRefillingCards = this.roundAllowsRefilling();
        const playerHasAlreadyRefilled = currentRound.number === player.refilledCardsInRound;
        if (!roundAllowsRefillingCards || playerHasAlreadyRefilled) {
            return null;
        }
        return (
            <div className={styles.RefillIconContainer}>
                Get new cards
                <Icon additionalClasses={styles.RefillIcon}
                      icon={IconType.recycle}
                      title={'Change all your hand cards and get new ones'}
                      onClick={() => this.setState({refillCardsModalIsOpen: true})}/>
            </div>
        );
    }

    private onRefillCardsClick = () => {
        const {socket, game} = this.props;
        socket.emit(RefillHandCardsEvent.id, new RefillHandCardsEvent(game.id));
    }
}
