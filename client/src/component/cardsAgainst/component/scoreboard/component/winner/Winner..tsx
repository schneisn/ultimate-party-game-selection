import React from 'react';
import {Socket} from '../../../../../../server/socket';
import {Emoji} from '../../../../../common/component/emoji/Emoji.';
import {Expire} from '../../../../../common/component/expire/Expire';
import {SmallCard} from '../../../../../common/component/smallCard/SmallCard';
import {Toast} from '../../../../../common/component/toast/Toast';
import {IGame} from '../../../../model/IGame';
import styles from '../../../../styles/color.module.scss';
import {getWinner} from '../../../../utils/roundUtils';

interface IProps {
    game: IGame;
    socket: Socket;
}

interface IState {
    winnerPopUpVisible: boolean
}

export class Winner extends React.Component<IProps, IState> {

    constructor(props: Readonly<IProps>) {
        super(props);
        this.state = {
            winnerPopUpVisible: true
        }
    }

    render() {
        if (!this.state.winnerPopUpVisible) {
            return null;
        }
        const winner = getWinner(this.props.game);
        if (!winner) {
            return null;
        }
        return <Toast>
            <Expire onExpire={this.onWinnerPopUpExpire}>
                <SmallCard additionalClassNames={styles.CATertiary}>
                    <span>{winner.name} won <Emoji symbol={'🎉'}/></span>
                </SmallCard>
            </Expire>
        </Toast>;
    }

    private onWinnerPopUpExpire = (): void => {
        this.setState({
            winnerPopUpVisible: false
        })
    }
}
