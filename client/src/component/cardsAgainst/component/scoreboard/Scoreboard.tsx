import React from 'react';
import {Socket} from '../../../../server/socket';
import {WideButton} from '../../../common/component/wideButton/WideButton';
import {CommonScoreboard, IScore} from '../../../common/component/commonScoreboard/CommonScoreboard';
import {generateCommonScoreboardStyles} from '../../../common/component/commonScoreboard/utils/styles';
import {Emoji} from '../../../common/component/emoji/Emoji.';
import {IGame} from '../../model/IGame';
import {IPlayer} from '../../model/IPlayer';
import styles from '../../styles/color.module.scss';
import {getCurrentRound, getWinner} from '../../utils/roundUtils';
import {getWinnerStatement} from '../../utils/statementUtils';
import {Winner} from './component/winner/Winner.';
import {UrlUtils} from '../../../common/utils/url';
import {RouteComponentProps, withRouter} from 'react-router-dom';

interface IProps extends RouteComponentProps {
    game: IGame;
    player: IPlayer;
    socket: Socket;
    /**
     * Whether the scoreboard is displayed at the end of the game
     * when no more cards are available.
     * Default is false
     */
    isGameOver?: boolean;
}

class Scoreboard extends React.Component<IProps> {
    render() {
        return (
            <>
                <Winner game={this.props.game} socket={this.props.socket}/>
                <CommonScoreboard game={this.props.game}
                                  scores={this.getScores()}
                                  player={this.props.player}
                                  socket={this.props.socket}
                                  description={this.getStatement()}
                                  showIsReady={true}
                                  title={this.props.isGameOver && 'Game Over'}
                                  showButton={!this.props.isGameOver}
                                  showRoundScore={!this.props.isGameOver}
                                  styles={generateCommonScoreboardStyles(styles.CAPrimary, styles.CASecondary, styles.CATertiary)}/>
                {this.props.isGameOver &&
                <WideButton additionalClassNames={styles.CATertiary}
                            onClick={() => UrlUtils.goHierarchyUp(this.props.history)}>
                  Leave game
                </WideButton>}
            </>
        )
    }

    private getStatement(): JSX.Element {
        if (this.props.isGameOver) {
            return <span>There are no more cards to play with</span>;
        }
        return <span dangerouslySetInnerHTML={{__html: getWinnerStatement(getCurrentRound(this.props.game))}}/>;
    }

    private getScores(): IScore[] {
        return this.props.game.players.map(player => {
            return {
                player: player,
                totalScore: this.getScoreForPlayer(player.id),
                roundScore: this.getIsWinnerString(player.id),
            }
        })
    }

    private getScoreForPlayer(playerId: string): number {
        let score = 0;
        for (const round of this.props.game.rounds) {
            for (const answer of round.answers) {
                if (answer.playerId === playerId && answer.hasWon) {
                    score++;
                }
            }
        }
        return score;
    };

    private getIsWinnerString(playerId: string): JSX.Element {
        const winner = getWinner(this.props.game);
        if (!winner || winner.id !== playerId) {
            return null;
        }
        return <Emoji symbol={'🎉'}/>;
    }
}

export default withRouter(Scoreboard);
