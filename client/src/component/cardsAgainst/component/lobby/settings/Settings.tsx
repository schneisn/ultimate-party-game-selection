import React from 'react';
import styles from './Settings.module.scss';
import {NumberInput} from '../../../../common/component/numberInput/NumberInput';
import {CommonSettings} from '../../../../common/component/commonSettings/CommonSettings';
import {IGame} from '../../../model/IGame';
import {IPlayer} from '../../../model/IPlayer';
import {Socket} from '../../../../../server/socket';
import {UpdateSettingsEvent} from '../../../../common/component/commonSettings/event/UpdateSettingsEvent';
import {playerIsGameMaster} from '../../../../common/utils/player';

interface ICAGameSettings {
    replaceCardsRound: number;
}

interface IProps {
    game: IGame;
    player: IPlayer;
    socket: Socket;
}

export class Settings extends React.Component<IProps> {

    render() {
        const {player, game} = this.props;
        return (
            <CommonSettings additionalClassNames={styles.CASecondary}>
                <div className={styles.ChangeCardsTitle}>
                    How often can players change all their cards?
                </div>
                <NumberInput min={0}
                             max={50}
                             disabled={!playerIsGameMaster(game, player.id)}
                             prependDescription={game.replaceCardsRound > 0 ? 'Every' : null}
                             appendDescription={game.replaceCardsRound === 0 ? 'Never' : `Round${game.replaceCardsRound !== 1 ? 's' : ''}`}
                             value={game.replaceCardsRound}
                             onBlur={this.updateSettings}/>
            </CommonSettings>
        );
    }

    private updateSettings = (nr: number) => {
        const {socket} = this.props;
        const updatedSettings: ICAGameSettings = {
            replaceCardsRound: nr
        }
        socket.emit(UpdateSettingsEvent.id, new UpdateSettingsEvent(this.props.game.id, updatedSettings));
    }
}
