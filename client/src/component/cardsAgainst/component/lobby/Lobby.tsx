import React from 'react';
import {Socket} from '../../../../server/socket';
import {CheckboxList, ICheckboxListItem} from '../../../common/component/checkboxList/CheckboxList';
import {generateCheckboxListStyles} from '../../../common/component/checkboxList/utils/stylesUtils';
import {CommonLobby} from '../../../common/component/commonLobby/CommonLobby';
import {generateCommonLobbyStyles} from '../../../common/component/commonLobby/utils/styles';
import {IconType} from '../../../common/component/icon/Icon';
import {Popover} from '../../../common/component/popover/Popover';
import {getGameMaster, playerIsGameMaster} from '../../../common/utils/player';
import {IGame} from '../../model/IGame';
import {IPlayer} from '../../model/IPlayer';
import styles from './Lobby.module.scss';
import {GetStatementCategoriesEvent} from './event/GetStatementCategoriesEvent';
import {IGetStatementCategories} from './event/in/IGetStatementCategories';
import {SetStatementCategoriesEvent} from './event/SetStatementCategoriesEvent';
import {IStatementCategory} from './model/IStatementCategory';
import {IFilterGroup, IFilterItem} from '../../../common/component/checkboxList/component/dropdownFilter/DropdownFilter';
import {StringUtils} from '../../../common/utils/string';
import {generatePopoverStyles} from '../../../common/component/popover/utils/styles';
import {Settings} from './settings/Settings';

interface IProps {
    game: IGame;
    player: IPlayer;
    socket: Socket;
}

interface IState {
    /**
     * All statement categories available
     */
    statementCategories: IStatementCategory[];
    /**
     * Set of selected filters
     */
    selectedFilters: Set<string>;
}

export class Lobby extends React.Component<IProps, IState> {

    constructor(props: IProps) {
        super(props);
        this.state = {
            statementCategories: [],
            selectedFilters: new Set<string>(),
        }
    }

    private static getCategoryInfo(category: IStatementCategory): JSX.Element {
        return <div className={styles.CategoryInfo}>
            <div className={styles.CategoryName}>
                {category.name}
            </div>
            <div className={styles.CategoryInfoPopoverPosition}>
                <Popover id={'category' + category.name}
                         icon={IconType.infoCircle}
                         iconTitle={'Pack description'}
                         customStyles={generatePopoverStyles(styles.CategoryInfoPopoverIcon)}>
                    {category.description || 'No description'}
                </Popover>
            </div>
        </div>;
    }

    componentDidMount() {
        this.props.socket.on(GetStatementCategoriesEvent.id, (data: IGetStatementCategories) => {
            this.setState({
                statementCategories: data.statementCategories,
            });
            // Select english language by default
            this.setState({
                selectedFilters: new Set<string>(['english']),
            });
        });
        this.props.socket.emit(GetStatementCategoriesEvent.id);
    };

    componentWillUnmount() {
        this.props.socket.removeEventListener(GetStatementCategoriesEvent.id);
    }

    render() {
        const {player, socket, game} = this.props;
        return (
            <div>
                <CommonLobby styles={generateCommonLobbyStyles(styles.CAPrimary, null, styles.CASecondary, styles.CATertiary)}
                             description={'Choose the card packs to play with. The best ones are already selected'}
                             game={game}
                             gameMasterId={getGameMaster(game).id}
                             player={player}
                             socket={socket}
                             buttonText={this.getButtonText()}
                             buttonIsDisabled={!this.hasEnoughPlayers() || !this.gameHasCategory()}>
                    <Settings game={game} player={player} socket={socket}/>
                    <CheckboxList listHeader={'Card packs'}
                                  listStyles={generateCheckboxListStyles(styles.CASecondary, styles.CATertiary)}
                                  filterGroups={this.getFilterGroups()}
                                  onFilterChange={this.onFilterChange}
                                  filterIconTitle={'Language filter'}
                                  items={this.getCategoryListItems()}
                                  showCheckIcons={true}
                                  onCheckboxSelectAll={this.onSelectAll}
                                  onCheckboxSelectNone={this.onSelectNone}
                                  showCheckAllIcon={true}
                                  filterIcon={IconType.language}
                                  filterIsDisabled={!playerIsGameMaster(game, player.id)}
                                  checkAllIsDisabled={!playerIsGameMaster(game, player.id)}/>
                </CommonLobby>
            </div>
        )
    }

    private getFilterGroups(): IFilterGroup[] {
        const filterGroup: IFilterGroup = {
            header: 'Languages',
            key: 'language',
            items: this.getLanguageFilters(),
            isSingleSelect: true
        }
        return [filterGroup];
    }

    private getLanguageFilters(): IFilterItem[] {
        return Array.from(this.getDistinctLanguagesFromAllCategories())
            .map(language => {
                return {
                    id: language,
                    content: StringUtils.toFirstUpperCase(language),
                    isSelected: this.state.selectedFilters.has(language),
                    values: [language]
                };
            });
    }

    private getDistinctLanguagesFromAllCategories = (): Set<string> => {
        const languagesWithDuplicates = this.state.statementCategories?.map(category => category.language);
        return new Set(languagesWithDuplicates);
    }

    private onFilterChange = (selectedFilters: IFilterItem[], visibleItems: ICheckboxListItem[]): void => {
        const selectedLanguages = selectedFilters.reduce((value: string[], prev: IFilterItem) => value.concat(prev.values), [])
        this.setState({selectedFilters: new Set<string>(selectedLanguages)});
        // When changing language, select the categories that are preselected and have the same language that is
        // currently selected.
        const categoriesWithSelectedLanguage = visibleItems
            .map(item => this.state.statementCategories.find(category => category.name === item.id))
            .filter(category => selectedLanguages.some(language => language === category.language));

        const selectedCategories = categoriesWithSelectedLanguage.filter(item => item.preselected).map(category => category.name);
        this.emitSelection(selectedCategories);
    }

    private getCategoryListItems(): ICheckboxListItem[] {
        const {player, game} = this.props;
        return this.state.statementCategories?.map((category: IStatementCategory) => {
            return {
                id: category.name,
                content: Lobby.getCategoryInfo(category),
                onClick: () => (this.setCategoriesSelection(category.name)),
                checked: this.categoryIsSelected(category.name),
                checkable: playerIsGameMaster(game, player.id),
                filters: new Map<string, string[]>([
                    ['language', [category.language]],
                ])
            }
        });
    };

    private categoryIsSelected(categoryName: string): boolean {
        return this.props.game?.statementCategories.some(gameCat => gameCat === categoryName);
    }

    private setCategoriesSelection(categoryName: string): void {
        const {game} = this.props;
        const selectedCategories = game.statementCategories;
        const selectedCategoryIndex = selectedCategories.findIndex(selCat => selCat === categoryName);
        if (selectedCategoryIndex >= 0) {
            selectedCategories.splice(selectedCategoryIndex, 1);
        } else {
            selectedCategories.push(categoryName);
        }
        this.emitSelection(selectedCategories);
    };

    private gameHasCategory(): boolean {
        return this.props.game.statementCategories.length > 0;
    }

    private getButtonText(): string {
        if (this.hasEnoughPlayers() && !this.gameHasCategory()) {
            return 'You need to select a category!';
        }
        return null;
    }

    private hasEnoughPlayers(): boolean {
        return this.props.game.players.length >= 2;
    }

    private emitSelection = (selectedCategories: string[]): void => {
        const {socket, game} = this.props;
        socket.emit(SetStatementCategoriesEvent.id, new SetStatementCategoriesEvent(game.id, selectedCategories));
    }

    /**
     * Selects all visible (= filtered categories)
     * @private
     */
    private onSelectAll = (selectedItems: ICheckboxListItem[]): void => {
        const selectedCategories = selectedItems.map(category => category.id);
        this.emitSelection(selectedCategories);
    }

    private onSelectNone = (): void => {
        this.emitSelection([]);
    }
}
