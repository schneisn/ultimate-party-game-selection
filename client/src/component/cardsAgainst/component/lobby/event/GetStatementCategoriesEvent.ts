import {AbstractEvent} from '../../../../common/event/AbstractEvent';

export class GetStatementCategoriesEvent extends AbstractEvent {
    static readonly id = 'GetStatementCategories';
}
