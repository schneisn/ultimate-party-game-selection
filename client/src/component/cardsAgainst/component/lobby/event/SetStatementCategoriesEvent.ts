import {GameEvent} from '../../../../common/event/GameEvent';

export class SetStatementCategoriesEvent extends GameEvent {
    static readonly id = 'SetStatementCategories';
    statementCategories: string[];

    constructor(gameId: string, statementCategories: string[]) {
        super(gameId);
        this.statementCategories = statementCategories;
    }
}
