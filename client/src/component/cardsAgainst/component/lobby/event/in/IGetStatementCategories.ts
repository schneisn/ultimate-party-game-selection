import {IStatementCategory} from '../../model/IStatementCategory';

export interface IGetStatementCategories {
    statementCategories: IStatementCategory[];
}
