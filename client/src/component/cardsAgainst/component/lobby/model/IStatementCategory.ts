export interface IStatementCategory {
    name: string;
    description: string;
    language: string;
    preselected: string;
}
