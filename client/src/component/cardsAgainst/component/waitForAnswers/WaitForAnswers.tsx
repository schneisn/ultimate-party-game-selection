import React from 'react';
import {InfoCard} from '../../../common/component/infoCard/InfoCard';
import {IGame} from '../../model/IGame';
import {IPlayer} from '../../model/IPlayer';
import {getCurrentRound} from '../../utils/roundUtils';
import {replaceBlanks} from '../../utils/statementUtils';
import {Statement} from '../statement/Statement';
import styles from './WaitForAnswers.module.scss';

interface IProps {
    game: IGame;
    title: string;
    playerId: string;
}

export class WaitForAnswers extends React.Component<IProps> {

    render() {
        const {game, title} = this.props;
        const currentRound = getCurrentRound(game);
        return (
            <>
                <Statement title={'Round ' + (currentRound.number + 1)}
                           round={currentRound}
                           statement={this.getStatement()}/>
                <InfoCard titleText={title}
                          additionalClassNames={styles.CASecondary}>
                    <div className={styles.Player}>
                        <b>The following players still have to answer</b>
                    </div>
                    {this.getPlayersWithoutAnswers()}
                </InfoCard>
            </>
        );
    }

    private getStatement(): string {
        const currentRound = getCurrentRound(this.props.game);
        const playerAnswers = currentRound.answers.find(a => a.playerId === this.props.playerId)?.answers;
        return replaceBlanks(currentRound.statement, playerAnswers);
    }

    private getPlayersWithoutAnswers(): JSX.Element[] {
        const currentRound = getCurrentRound(this.props.game);
        return this.props.game.players
            .filter(player => currentRound.criticId !== player.id)
            .filter((player: IPlayer) => currentRound.answers.some(answer => answer.playerId === player.id && answer.answers.length < 1))
            .map(player => <div key={player.id}>{player.name}</div>);
    }
}
