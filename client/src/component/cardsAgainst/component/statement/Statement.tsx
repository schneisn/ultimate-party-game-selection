import React from 'react';
import {InfoCard} from '../../../common/component/infoCard/InfoCard';
import {IRound} from '../../model/IRound';
import styles from './Statement.module.scss';

interface IProps {
    /**
     * Title to display. If it is null, no title is displayed
     */
    title?: string | JSX.Element;
    /**
     * The statement to display. May contain html for styling.
     * If this is not provided. The default statement of the round
     * will be used.
     */
    statement?: string;
    /**
     * Used to display the default round statement when no
     * statement prop is provided.
     */
    round?: IRound;
}

/**
 * Allows rendering html in its card body.
 *
 * Do not use with unsafe code!
 */
export class Statement extends React.Component<IProps> {
    render() {
        return <InfoCard titleText={this.props.title}
                         additionalClassNames={styles.CAPrimary}>
            <span dangerouslySetInnerHTML={{__html: this.props.statement || this.props.round?.statement}}/>
        </InfoCard>
    }
}
