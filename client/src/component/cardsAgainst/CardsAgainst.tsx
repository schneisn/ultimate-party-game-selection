import React from 'react';
import {RouteComponentProps} from 'react-router-dom';
import {Urls} from '../../server/serverUrls';
import {BaseGame} from '../common/component/baseGame/BaseGame';
import {generateConnectToastStyles} from '../common/component/baseGame/component/connectToast/utils/styles';
import {generateBaseGameStyles} from '../common/component/baseGame/utils/styles';
import {InfoCard} from '../common/component/infoCard/InfoCard';
import {generateModalStyles} from '../common/component/modal/utils/styles';
import {generatePopoverStyles} from '../common/component/popover/utils/styles';
import {StringUtils} from '../common/utils/string';
import {Answer} from './component/answer/Answer';
import {Lobby} from './component/lobby/Lobby';
import {Review} from './component/review/Review';
import {WaitForAnswers} from './component/waitForAnswers/WaitForAnswers';
import {IGame} from './model/IGame';
import {IPlayer} from './model/IPlayer';
import styles from './styles/color.module.scss';
import {getCurrentRound, getNrBlanks} from './utils/roundUtils';
import {WideButton} from '../common/component/wideButton/WideButton';
import {UrlUtils} from '../common/utils/url';
import Scoreboard from './component/scoreboard/Scoreboard';
import {ICommonPlayer} from '../common/model/ICommonPlayer';
import {IConnectionReadyPlayer} from '../common/model/IConnectionReadyPlayer';
import {playerIsGameMaster} from '../common/utils/player';
import {Card} from '../common/component/card/Card';
import {PlayerInfo} from '../common/component/playerInfo/PlayerInfo';
import {Modal} from '../common/component/modal/Modal';

export class CardsAgainst extends BaseGame<IGame, IPlayer> {

    constructor(props: Readonly<RouteComponentProps>) {
        super(props);
        const options = {
            gameName: 'Cards Against Everything',
            gamePath: UrlUtils.getPathFromString(this.props.match.path),
            socketNamespace: Urls.CardsAgainst,
            styles: generateBaseGameStyles(
                generateModalStyles(styles.CAPrimary, styles.CASecondary, styles.CATertiary),
                generateConnectToastStyles(styles.CAConnect, styles.CADisconnect),
                null,
                styles.CABackground,
                generatePopoverStyles(styles.CATertiaryIcon, styles.CATertiary)
            )
        };
        this.state = {
            ...this.state,
            options: options
        }
    }

    renderGame() {
        const {player, socket, game} = this.state;
        const {history} = this.props;

        if (game.rounds.length === 0) {
            return <Lobby socket={socket} game={game} player={player}/>;
        }

        const atMostOnePlayerConnected = game.players.filter(p => p.isConnected).length <= 1;
        if (atMostOnePlayerConnected) {
            return <>
                <InfoCard titleText={'Wait for more players'}
                          additionalClassNames={styles.CAPrimary}>
                    You are the only player. Wait for more players to play or leave the game
                </InfoCard>
                <WideButton additionalClassNames={styles.CATertiary}
                            onClick={() => UrlUtils.goHierarchyUp(history)}>
                    Leave game
                </WideButton>
            </>;
        }

        if (StringUtils.isEmpty(getCurrentRound(game).statement)) {
            return <Scoreboard game={game} player={player} socket={socket} isGameOver={true}/>
        }

        if (!this.playerHasEnoughCards()) {
            return <InfoCard additionalClassNames={styles.CAPrimary}
                             titleText={'Whoops'}>
                You do not have enough cards to play
            </InfoCard>;
        }

        if (!this.playerIsCritic() && !this.playerParticipatesInRound() && !this.criticHasReviewed() && !this.allPlayersAnswered()) {
            return <WaitForAnswers game={game} title={'Wait for all players to answer to join'} playerId={player.id}/>;
        }

        if (!this.playersHaveEnoughCards() || (this.allPlayersAnswered() && this.criticHasReviewed())) {
            return <Scoreboard game={game} player={player} socket={socket}/>;
        }

        if (this.playerHasAnswered() && !this.allPlayersAnswered()) {
            return <WaitForAnswers game={game} title={'Wait for the other players to answer'} playerId={player.id}/>;
        }

        if (this.allPlayersAnswered()) {
            return <Review socket={socket} game={game} player={player}/>;
        }

        if (!this.allPlayersAnswered() && this.playerIsCritic()) {
            return <WaitForAnswers game={game} title={'You are the judge'} playerId={player.id}/>
        }

        return <Answer socket={socket} game={game} player={player}/>;
    }

    protected getPlayerModal(): JSX.Element {
        const {isPlayerModalOpen, game} = this.state;
        const playerElements = game?.players
            .map((teamMate: ICommonPlayer & Partial<IConnectionReadyPlayer>) => {
                if ((playerIsGameMaster(game, teamMate.id))) {
                    teamMate.isGameMaster = true;
                }
                return teamMate;
            })
            .map(teamMate => {
                return <Card key={teamMate.id}>
                    <PlayerInfo player={teamMate}/>
                </Card>
            });

        return <Modal modalTitle={'Players'}
                      isOpen={isPlayerModalOpen}
                      showFooter={false}
                      onToggle={this.togglePlayerModal}>
            {playerElements}
        </Modal>
    }

    private allPlayersAnswered = (): boolean => {
        return getCurrentRound(this.state.game).answers.every(answer => answer.answers.length > 0);
    };

    private playerIsCritic = (): boolean => {
        return getCurrentRound(this.state.game).criticId === this.state.player.id;
    };

    private playerHasAnswered = (): boolean => {
        return getCurrentRound(this.state.game).answers.find(answer => answer.playerId === this.state.player.id)?.answers.length > 0;
    };

    private playersHaveEnoughCards = (): boolean => {
        return this.state.game.players.every(player => player.handCards.length >= getNrBlanks(getCurrentRound(this.state.game).statement));
    };

    private playerHasEnoughCards = (): boolean => {
        return this.state.player.handCards.length >= getNrBlanks(getCurrentRound(this.state.game).statement);
    };

    private criticHasReviewed = (): boolean => {
        return getCurrentRound(this.state.game).answers.some(answer => answer.hasWon);
    };

    private playerParticipatesInRound = (): boolean => {
        return getCurrentRound(this.state.game).answers.some(answer => answer.playerId === this.state.player.id);
    }
}
