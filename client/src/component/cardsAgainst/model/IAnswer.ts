export interface IAnswer {
    playerId: string;
    answers: string[];
    hasWon: boolean;
}
