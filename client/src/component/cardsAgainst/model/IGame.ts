import {ICommonGame} from '../../common/model/ICommonGame';
import {IPlayer} from './IPlayer';
import {IRound} from './IRound';

export interface IGame extends ICommonGame<IPlayer> {
    rounds: IRound[];
    statementCategories: string[];
    replaceCardsRound: number;
}
