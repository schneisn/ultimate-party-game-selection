import {IAnswer} from './IAnswer';

export interface IRound {
    number: number;
    statement: string;
    criticId: string;
    answers: IAnswer[];
}
