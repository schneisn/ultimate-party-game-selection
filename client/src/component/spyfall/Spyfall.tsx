import React from 'react';
import {RouteComponentProps} from 'react-router-dom';
import {Urls} from '../../server/serverUrls';
import {BaseGame} from '../common/component/baseGame/BaseGame';
import {generateConnectToastStyles} from '../common/component/baseGame/component/connectToast/utils/styles';
import {generateBaseGameStyles} from '../common/component/baseGame/utils/styles';
import {DisconnectedIcon} from '../common/component/disconnectedIcon/DisconnectedIcon';
import {HeaderContainer} from '../common/component/headerContainer/HeaderContainer';
import {InfoCard} from '../common/component/infoCard/InfoCard';
import {generateModalStyles} from '../common/component/modal/utils/styles';
import {generatePopoverStyles} from '../common/component/popover/utils/styles';
import {AgentVote} from './component/agentVote/AgentVote';
import {Lobby} from './component/lobby/Lobby';
import {SecretRole} from './component/secretRole/SecretRole';
import {VoteAlert} from './component/voteAlert/VoteAlert';
import {IVoteItem} from './component/voteList/model/IVoteItem';
import {generateVoteListStyles} from './component/voteList/utils/styles';
import {VoteList} from './component/voteList/VoteList';
import {GuessLocationEvent} from './event/GuessLocationEvent';
import {StartAgentVoteEvent} from './event/StartAgentVoteEvent';
import {IGame} from './model/IGame';
import {AGENT, IPlayer} from './model/IPlayer';
import styles from './styles/color.module.scss';
import {UrlUtils} from '../common/utils/url';

export class Spyfall extends BaseGame<IGame, IPlayer> {

    constructor(props: RouteComponentProps) {
        super(props);
        const options = {
            gameName: 'Spyfall',
            gamePath: UrlUtils.getPathFromString(this.props.match.path),
            socketNamespace: Urls.Spyfall,
            styles: generateBaseGameStyles(
                generateModalStyles(styles.SFPrimary, styles.SFSecondary, styles.SFTertiary),
                generateConnectToastStyles(styles.SFConnect, styles.SFDisconnect),
                null,
                styles.SFBackground,
                generatePopoverStyles(styles.SFTertiaryIcon, styles.SFTertiary)
            )
        };
        this.state = {
            ...this.state,
            options: options
        };
    }

    private static getPlayerNameAndConnectionStatus(player: IPlayer): JSX.Element {
        return <div className={styles.EllipseText}>{player.name}{!player.isConnected && <DisconnectedIcon/>}</div>;
    }

    renderGame(): JSX.Element {
        if (!this.state.game.statePlaying) {
            return <Lobby socket={this.state.socket} game={this.state.game} player={this.state.player}/>;
        }

        return <>
            <VoteAlert game={this.state.game} socket={this.state.socket}/>
            {this.state.game.suspectedAgent ?
                <AgentVote game={this.state.game} player={this.state.player} socket={this.state.socket}/> : this.getPlayersAndLocation()}
        </>;
    }

    private getPlayersAndLocation(): JSX.Element {
        return <>
            <InfoCard titleText={'Round'} additionalClassNames={styles.SFPrimary}>
                Ask questions to find out who the agent is. If you are the agent, try to find out where you are. All players see the players and
                locations, but you can only guess the location if you are the agent and vice versa.
            </InfoCard>
            <SecretRole game={this.state.game} player={this.state.player}/>
            <HeaderContainer header={'Players'}
                             additionalClassNames={styles.SFSecondary}>
                <VoteList<IPlayer> items={this.getAgentVoteItems()}
                                   onVote={this.startAgentVote}
                                   numberOfVotes={1}
                                   styles={generateVoteListStyles(styles.SFTertiary, styles.SFTertiary)}
                                   voteButtonText={'Start a vote for the suspected player'}/>
            </HeaderContainer>
            <HeaderContainer header={'Possible locations'}
                             additionalClassNames={styles.SFSecondary}>
                <VoteList<string> items={this.getLocationVoteItems()}
                                  onVote={this.voteLocation}
                                  numberOfVotes={1}
                                  styles={generateVoteListStyles(styles.SFTertiary, styles.SFTertiary)}
                                  voteButtonText={'Guess your location'}
                                  isListDisabled={this.state.player.role !== AGENT}/>
            </HeaderContainer>
        </>;
    }

    private getAgentVoteItems = (): IVoteItem<IPlayer>[] => {
        return this.state.game.players.map((player: IPlayer): IVoteItem<IPlayer> => (
            {
                id: player.id,
                label: Spyfall.getPlayerNameAndConnectionStatus(player),
                data: player,
            }
        ));
    }

    private getLocationVoteItems = (): IVoteItem<string>[] => {
        return this.state.game.possibleLocations.map((location: string, id: number): IVoteItem<string> => (
            {
                id: id.toString(),
                label: location,
                data: location,
            }
        ));
    }

    private voteLocation = (data: IVoteItem<string>[]) => {
        this.state.socket.emit(GuessLocationEvent.id, new GuessLocationEvent(this.state.game.id, data[0].data));
    };

    private startAgentVote = (data: IVoteItem<IPlayer>[]) => {
        this.state.socket.emit(StartAgentVoteEvent.id, new StartAgentVoteEvent(this.state.game.id, data[0].data));
    };
}
