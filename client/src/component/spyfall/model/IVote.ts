export interface IVote {
    voterId: string;
    isAgent: boolean;
}
