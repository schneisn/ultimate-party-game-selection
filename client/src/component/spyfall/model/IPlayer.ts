import {IConnectionReadyPlayer} from '../../common/model/IConnectionReadyPlayer';

export const AGENT = 'agent';

export interface IPlayer extends IConnectionReadyPlayer {
    role: string;
}
