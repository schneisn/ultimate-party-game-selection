import {ICommonGame} from '../../common/model/ICommonGame';
import {IPlayer} from './IPlayer';
import {IVote} from './IVote';

export enum WinState {
    agentGuessedLocationCorrect = 'agentGuessedLocationCorrect',
    agentGuessedLocationWrong = 'agentGuessedLocationWrong',
    playersGuessedAgentCorrect = 'playersGuessedAgentCorrect',
    playersGuessedAgentWrong = 'playersGuessedAgentWrong'
}

export interface IGame extends ICommonGame<IPlayer> {
    winState: WinState;
    location: string;
    possibleLocations: string[];
    maxNrPossibleLocations: number;
    statePlaying: boolean;
    guessedLocation: string;
    voteInitiator: string;
    suspectedAgent: string;
    yesVotes: IVote[];
}
