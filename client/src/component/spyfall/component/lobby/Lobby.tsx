import React from 'react';
import {Socket} from '../../../../server/socket';
import {CommonLobby} from '../../../common/component/commonLobby/CommonLobby';
import {getReadyButtonReadyStateText} from '../../../common/component/commonLobby/component/readyButton/utils/stringUtils';
import {generateCommonLobbyStyles} from '../../../common/component/commonLobby/utils/styles';
import {CommonSettings} from '../../../common/component/commonSettings/CommonSettings';
import {UpdateSettingsEvent} from '../../../common/component/commonSettings/event/UpdateSettingsEvent';
import {NumberInput} from '../../../common/component/numberInput/NumberInput';
import {SmallCard} from '../../../common/component/smallCard/SmallCard';
import {getGameMaster, getPlayer, playerIsGameMaster} from '../../../common/utils/player';
import {StringUtils} from '../../../common/utils/string';
import {IGame, WinState} from '../../model/IGame';
import {AGENT, IPlayer} from '../../model/IPlayer';
import styles from '../../styles/color.module.scss';

interface ISettings {
    maxNrPossibleLocations: number;
}

interface IProps {
    game: IGame;
    player: IPlayer;
    socket: Socket;
}

export class Lobby extends React.Component<IProps> {

    render() {

        const {player, socket, game} = this.props;
        return (
            <>
                {this.printScoreboard()}
                <CommonLobby game={game}
                             player={player}
                             socket={socket}
                             gameMasterId={getGameMaster(game).id}
                             styles={generateCommonLobbyStyles(styles.SFPrimary, null, styles.SFSecondary, styles.SFTertiary)}
                             buttonIsDisabled={game.players.length < 3}
                             buttonText={this.getButtonText()}>
                    <CommonSettings additionalClassNames={styles.SFSecondary}>
                        <div>
                            Number of locations to guess from
                        </div>
                        <NumberInput disabled={!playerIsGameMaster(game, player.id)}
                                     onBlur={this.onUpdateMaxNrPossibleLocations}
                                     value={game.maxNrPossibleLocations}
                                     min={2}/>
                    </CommonSettings>
                </CommonLobby>
            </>
        )
    }

    private getButtonText(): string {
        const {player, game} = this.props;
        if (game.players.length < 3) {
            return 'You need at least 3 players to start';
        }
        return getReadyButtonReadyStateText(player.isReady);
    }

    private getAgentName(): string {
        const agent = this.props.game.players.find(player => player.role === AGENT);
        return agent ? StringUtils.trimWithDots(agent.name, 20) : '';
    }

    private printScoreboard(): JSX.Element {
        const {game, player} = this.props;
        if (!player.role) {
            return null;
        }
        let winnerText: string;
        switch (game.winState) {
            case WinState.agentGuessedLocationCorrect:
                winnerText = `Agent ${this.getAgentName()} guessed their location (${game.location}) and won the game!`;
                break;
            case WinState.agentGuessedLocationWrong:
                winnerText = `Agent ${this.getAgentName()} guessed the wrong location (${game.guessedLocation}) and lost the game! ${game.location} was the correct location`;
                break;
            case WinState.playersGuessedAgentCorrect:
                winnerText = `Agent ${this.getAgentName()} was revealed! The civilians won the game`;
                break;
            case WinState.playersGuessedAgentWrong:
                winnerText = `The players wrongly suspected ${getPlayer(game, game.suspectedAgent)?.name} to be the agent. The agent ${this.getAgentName()} won the game`;
                break;
            default:
                winnerText = `Error: Invalid win state ${game.winState}`;
        }
        return <SmallCard additionalClassNames={styles.SFSecondary}>
            {winnerText}
        </SmallCard>
    }

    private onUpdateMaxNrPossibleLocations = (maxNr: number): void => {
        const {socket, game} = this.props;
        if (maxNr === game.maxNrPossibleLocations) {
            return;
        }
        const updatedSettings: ISettings = {maxNrPossibleLocations: maxNr};
        socket.emit(UpdateSettingsEvent.id, new UpdateSettingsEvent(game.id, updatedSettings));
    }
}
