export interface IVoteItem<D = null> {
    id: string;
    label: string | JSX.Element;
    data: D;
    isDisabled?: boolean;
}
