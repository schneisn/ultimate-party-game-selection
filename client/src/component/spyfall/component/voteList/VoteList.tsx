import classNames from 'classnames';
import React from 'react';
import {WideButton} from '../../../common/component/wideButton/WideButton';
import {CheckboxWithLabel} from '../../../common/component/checkboxWithLabel/CheckboxWithLabel';
import {IVoteItem} from './model/IVoteItem';
import styles from './VoteList.module.scss';

export interface IVoteListStyles {
    listItemClasses?: string;
    voteButtonClasses?: string;
}

interface IProps<D> {
    items: IVoteItem<D>[];
    /**
     * The maximum number of votes the player can place
     */
    numberOfVotes: number;
    /**
     * A function that is called when the client voted
     */
    onVote: (selectedItems: IVoteItem<D>[]) => void;
    styles?: IVoteListStyles;
    voteButtonText: string;
    /**
     * When the list is disabled, no player can be selected
     * and the button cannot be pressed.
     * Default is false
     */
    isListDisabled?: boolean;
}

interface IState {
    selectedItems: Set<string>;
}

/**
 * A general (not synchronized) voting component
 */
export class VoteList<D> extends React.Component<IProps<D>, IState> {

    constructor(props: IProps<D>) {
        super(props);
        this.state = {
            selectedItems: new Set<string>()
        }
    }

    render() {
        if (!this.props.items) {
            return <div>No data</div>;
        }
        return (
            <div>
                {this.getVoteItems()}
                <WideButton onClick={() => this.props.onVote(this.getSelectedItems())}
                            additionalClassNames={classNames(styles.VoteButton, this.props.styles?.voteButtonClasses)}
                            disabled={this.voteButtonIsDisabled()}>
                    {this.props.voteButtonText}
                </WideButton>
            </div>
        )
    }

    private toggleSelect = (data: string) => {
        const newSelectedItems = this.state.selectedItems;
        if (this.props.numberOfVotes < 2) {
            const exists = newSelectedItems.has(data);
            newSelectedItems.clear();
            !exists && newSelectedItems.add(data);
        } else {
            newSelectedItems.has(data) ? newSelectedItems.delete(data) :
                this.state.selectedItems.size <= this.props.numberOfVotes && newSelectedItems.add(data);
        }
        this.setState({selectedItems: newSelectedItems});
    };

    private getVoteItems(): JSX.Element {
        const voteItems = this.props.items.filter(item => item.id && item.label && item.data).map((item: IVoteItem<D>) => {
                return <CheckboxWithLabel key={item.id}
                                          onClick={() => this.toggleSelect(item.id)}
                                          additionalClassNames={this.props.styles?.listItemClasses}
                                          checked={this.itemIsSelected(item.id)}
                                          showIcon={false}
                                          checkedClassNames={!this.props.isListDisabled && styles.Selected}>
                    {item.label}
                </CheckboxWithLabel>;
            }
        );

        return <div>{voteItems}</div>;
    };

    private itemIsSelected(itemId: string): boolean {
        return this.state.selectedItems.has(itemId);
    }

    private voteButtonIsDisabled(): boolean {
        return this.props.isListDisabled ||
            (!this.props.numberOfVotes && this.state.selectedItems.size === 1) ||
            (this.props.numberOfVotes && this.state.selectedItems.size !== this.props.numberOfVotes);
    }

    private getSelectedItems(): IVoteItem<D>[] {
        return Array.from(this.props.items.filter(item => this.state.selectedItems.has(item.id)));
    }
}
