import {IVoteListStyles} from '../VoteList';

export function generateVoteListStyles(listItemClasses: string, voteButtonClasses: string): IVoteListStyles {
    return {
        listItemClasses,
        voteButtonClasses
    }
}
