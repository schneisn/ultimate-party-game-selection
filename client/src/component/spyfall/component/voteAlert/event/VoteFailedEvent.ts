import {GameEvent} from '../../../../common/event/GameEvent';

export class VoteFailedEvent extends GameEvent {
    static readonly id = 'VoteFailed';
}
