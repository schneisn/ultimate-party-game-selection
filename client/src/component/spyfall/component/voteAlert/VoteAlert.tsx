import React from 'react';
import {Socket} from '../../../../server/socket';
import {Expire} from '../../../common/component/expire/Expire';
import {SmallCard} from '../../../common/component/smallCard/SmallCard';
import {Toast} from '../../../common/component/toast/Toast';
import {IGame} from '../../model/IGame';
import {VoteFailedEvent} from './event/VoteFailedEvent';
import styles from './VoteAlert.module.scss';

interface IProps {
    game: IGame;
    socket: Socket;
}

interface IState {
    showVoteFailed: boolean;
}

export class VoteAlert extends React.Component<IProps, IState> {

    constructor(props: Readonly<IProps>) {
        super(props);
        this.state = {
            showVoteFailed: false
        }
    }


    componentDidMount() {
        this.props.socket.on(VoteFailedEvent.id, () => {
            this.setState({showVoteFailed: true});
        })
    }

    componentWillUnmount() {
        this.props.socket.removeEventListener(VoteFailedEvent.id);
    }

    render() {
        if (!this.state.showVoteFailed) {
            return null;
        }
        return (
            <Toast>
                <Expire onExpire={() => this.setState({showVoteFailed: false})}>
                    <SmallCard additionalClassNames={styles.Red}>
                        The vote failed
                    </SmallCard>
                </Expire>
            </Toast>
        );
    }
}
