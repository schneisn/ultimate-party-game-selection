import React from 'react';
import {Collapse} from '../../../common/component/collapse/Collapse';
import {HeaderContainer} from '../../../common/component/headerContainer/HeaderContainer';
import {StringUtils} from '../../../common/utils/string';
import {IGame} from '../../model/IGame';
import {AGENT, IPlayer} from '../../model/IPlayer';
import styles from '../../styles/color.module.scss';

interface IProps {
    game: IGame;
    player: IPlayer;
}

export class SecretRole extends React.Component<IProps> {

    render() {
        if (!this.props.player?.role) {
            return null;
        }

        return (
            <Collapse buttonClasses={styles.SFTertiary}
                      openButtonText={'Hide role and location'}
                      hiddenButtonText={'Show role and location'}>
                <HeaderContainer additionalClassNames={styles.SFSecondary}>
                    <div>
                        Secret location: <b>{StringUtils.toFirstUpperCase(this.getMaskedLocation())}</b>
                    </div>
                    <div>
                        Secret role: <b>{StringUtils.toFirstUpperCase(this.props.player.role)}</b>
                    </div>
                    {this.props.children}
                </HeaderContainer>
            </Collapse>
        )
    }

    private getMaskedLocation() {
        return (this.props.player.role === AGENT) ? '???' : this.props.game.location;
    }
}
