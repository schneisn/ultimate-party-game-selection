import React from 'react';
import {Socket} from '../../../../server/socket';
import {WideButton} from '../../../common/component/wideButton/WideButton';
import {HeaderContainer} from '../../../common/component/headerContainer/HeaderContainer';
import {InfoCard} from '../../../common/component/infoCard/InfoCard';
import {SmallCard} from '../../../common/component/smallCard/SmallCard';
import {getPlayer} from '../../../common/utils/player';
import {AgentVoteEvent} from '../../event/AgentVoteEvent';
import {IGame} from '../../model/IGame';
import {IPlayer} from '../../model/IPlayer';
import styles from './AgentVote.module.scss';

interface IProps {
    game: IGame;
    player: IPlayer;
    socket: Socket;
}


export class AgentVote extends React.Component<IProps> {
    render(): JSX.Element {
        if (this.props.game.suspectedAgent === this.props.player.id) {
            return this.getSuspectedPlayerScreen();
        }

        return <>
            <InfoCard titleText={getPlayer(this.props.game, this.props.game.voteInitiator).name + ' initiated a vote'}
                      additionalClassNames={styles.SFPrimary}>
                Do you think {getPlayer(this.props.game, this.props.game.suspectedAgent).name} is the agent?
            </InfoCard>
            {this.playerHasVoted() && <SmallCard additionalClassNames={styles.HasVoted}>Wait for the other players or change your vote</SmallCard>}
            {<HeaderContainer additionalClassNames={styles.SFSecondary}>
                <WideButton additionalClassNames={this.getPlayerVote() !== false && styles.Yes}
                            onClick={() => this.onVote(true)}>
                    YES
                </WideButton>
                <WideButton additionalClassNames={this.getPlayerVote() !== true && styles.No}
                            onClick={() => this.onVote(false)}
                            disabled={this.props.game.suspectedAgent === this.props.player.id}>
                    NO. This will cancel the vote
                </WideButton>
            </HeaderContainer>}
        </>;
    }

    private getSuspectedPlayerScreen(): JSX.Element {
        return <>
            <InfoCard titleText={getPlayer(this.props.game, this.props.game.voteInitiator).name + ' suspects you to be the agent'}
                      additionalClassNames={styles.SFPrimary}>
                Wait for the other players to vote
            </InfoCard>
        </>;
    }

    private playerHasVoted(): boolean {
        return this.props.game.yesVotes?.some(vote => vote.voterId === this.props.player.id);
    }

    private getPlayerVote(): boolean {
        return this.props.game.yesVotes?.find(vote => vote.voterId === this.props.player.id)?.isAgent;
    }

    private onVote = (isAgent: boolean): void => {
        const isNotAllowedToVote = this.props.game.suspectedAgent === this.props.player.id && !isAgent;
        if (isNotAllowedToVote) {
            return;
        }
        if (this.props.game.yesVotes.some(vote => vote.voterId === this.props.player.id && vote.isAgent === isAgent)) {
            return;
        }
        this.props.socket.emit(AgentVoteEvent.id, new AgentVoteEvent(this.props.game.id, isAgent));
    }
}
