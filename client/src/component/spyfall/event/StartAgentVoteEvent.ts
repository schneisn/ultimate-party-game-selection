import {GameEvent} from '../../common/event/GameEvent';
import {IPlayer} from '../model/IPlayer';

export class StartAgentVoteEvent extends GameEvent {
    static readonly id = 'StartAgentVote';
    suspectedAgent: IPlayer;

    constructor(gameId: string, suspectedAgent: IPlayer) {
        super(gameId);
        this.suspectedAgent = suspectedAgent;
    }
}
