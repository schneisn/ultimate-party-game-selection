import {GameEvent} from '../../common/event/GameEvent';

export class GuessLocationEvent extends GameEvent {
    static readonly id = 'GuessLocation';
    location: string;

    constructor(gameId: string, location: string) {
        super(gameId);
        this.location = location;
    }
}
