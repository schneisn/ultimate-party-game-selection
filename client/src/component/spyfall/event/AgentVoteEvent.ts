import {GameEvent} from '../../common/event/GameEvent';

export class AgentVoteEvent extends GameEvent {
    static readonly id = 'AgentVote';
    isAgent: boolean;

    constructor(gameId: string, isAgent: boolean) {
        super(gameId);
        this.isAgent = isAgent;
    }
}
