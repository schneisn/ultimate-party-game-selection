export class Config {

    static isDev(): boolean {
        return import.meta.env.NODE_ENV === 'development';
    }

    static isProd(): boolean {
        return import.meta.env.NODE_ENV === 'production';
    }

    /**
     * Returns the protocol in the form of http{s}://
     */
    static getProtocol(): string {
        return window.location.protocol + '//';
    }

    /**
     * Gets the href ot the client.
     * If the app runs on localhost, the server origin is used instead of the window.location
     */
    static getClientHref(): string {
        if (this.isLocalHost()) {
            return `${this.getServerOrigin()}${window.location.pathname}`;
        }
        return window.location.href;
    }

    /**
     * Gets the server host name from the .env file. If the .env variable does not exist,
     * the location.hostname is used.
     */
    static getServerHostName(): string {
        return import.meta.env.VITE_REACT_APP_SERVER_HOST || window.location.hostname;
    }

    /**
     * Gets the server port from the .env file.
     */
    static getServerPort(): string {
        return import.meta.env.VITE_REACT_APP_SERVER_PORT;
    }

    /**
     * Gets the origin of the server in the format http{s}://hostname:port.
     * If the app runs on localhost in dev mode, the window hostname is used.
     * Otherwise, the hostname and port from the env files are used.
     */
    static getServerOrigin(): string {
        // If no port is provided, assume the backend is available
        // under the given host name without port.
        if (this.getServerHostName() && !this.getServerPort()) {
            return `${this.getServerHostName()}`;
        }
        // In dev or when both host and port are provided, use host and port
        if ((this.isLocalHost() || this.isDev()) && this.getServerHostName() && this.getServerPort()) {
            return `${this.getProtocol()}${this.getServerHostName()}:${this.getServerPort()}`;
        }
        // If env variables do not define another server url, assume it is same as client
        return window.location.origin;
    }

    /**
     * Checks if the client is running on localhost
     */
    static isLocalHost(): boolean {
        return Boolean(
            window.location.hostname === 'localhost' ||
            // [::1] is the IPv6 localhost address.
            window.location.hostname === '[::1]' ||
            // 127.0.0.1/8 is considered localhost for IPv4.
            window.location.hostname.match(
                /^127(?:\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}$/
            )
        );
    }
}
