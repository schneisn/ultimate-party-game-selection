import React from 'react';
import ReactDOM from 'react-dom';
import {Loader} from './component/common/component/loader/Loader';
import './index.scss';
import styles from './styles/CommonClasses.module.scss';

const App = React.lazy(() => import('./App'));

ReactDOM.render(
    <React.Suspense fallback={<Loader additionalWrapperClasses={styles.Loader}/>}>
        <App/>
    </React.Suspense>,
    document.getElementById('root')
);
