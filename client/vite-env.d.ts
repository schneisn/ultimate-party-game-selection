/// <reference types="vite/client" />

interface ImportMetaEnv {
    readonly VITE_REACT_APP_SERVER_HOST: string
    readonly VITE_REACT_APP_SERVER_PORT: string
}

interface ImportMeta {
    readonly env: ImportMetaEnv
}